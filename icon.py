"""
Draw the repo icon.
"""
import typing
import warnings

import matplotlib
import matplotlib.pyplot as plt

import more_mpl_toolkits.categorized_legends as cl
from more_mpl_toolkits import rays

Color = typing.TypeVar('Color')
Marker = typing.TypeVar('Marker')
FillStyle = typing.TypeVar('FillStyle', bound=str)

FIGURE_KWARGS = dict(dpi=600, figsize=(1.1, 1.1))
PAD = .22
BACKGROUND_DIM = .6
COLORS = 'red', 'gold', 'limegreen'
LEGEND_LINE_VALUES = dict(
    more=COLORS,
    mpl=['D', 's'],
    toolkits=['top', 'left'],
)
LEGEND_LINE_PROPERTIES = dict(
    more=dict(style='italic', weight='bold'),
    mpl=dict(name='monospace'),
    toolkits={},
)
GET_HANDLES_KWARGS = dict(
    category_name_sorter=('more', 'mpl', 'toolkits').index,
    category_value_sorters=dict(more=COLORS.index),
    values_per_line=3,
)
LEGEND_KWARGS = dict(
    frameon=False,
    mode='expand',
    loc='center',
    labelcolor='white',
    labelspacing=.9,
)
RC_PARAMS = {
    'figure.facecolor': matplotlib.colors.to_rgb('mediumblue'),
    'font.monospace': 'PT Mono',
    'grid.color': matplotlib.colors.to_rgb('lightsteelblue'),
    'grid.alpha': 1,
}


def get_matplotlib_version() -> typing.Tuple[int]:
    """
    Get the version number of `matplotlib` as a tuple;
    alphabetical suffixes are stripped."""
    version = matplotlib.__version__
    result = []
    if isinstance(version, str):
        version = version.split('.')
    for chunk in version:
        if isinstance(chunk, str):
            alphabet = ''.join(
                chr(c)
                for c in (*range(0x41, 0x41 + 26), *range(0x61, 0x61 + 26))
            )
            chunk = chunk.rstrip(alphabet)
        result.append(int(chunk))
    return tuple(result)


MPL_VERSION = get_matplotlib_version()


def formatter(
    *,
    more: typing.Optional[Color] = None,
    mpl: typing.Optional[Marker] = None,
    toolkits: typing.Optional[FillStyle] = None,
) -> typing.Dict[str, typing.Any]:
    """Get the appropriate kwargs for `Axes.plot()`."""
    color = more or 'none'
    if MPL_VERSION[:2] >= (3, 6):
        marker_kwargs = dict(joinstyle='round')
    else:
        msg = (
            'marker = dict(joinstyle=...) only available from '
            '`matplotlib 3.6` onwards'
        )
        warnings.warn(msg, RuntimeWarning)
        marker_kwargs = {}
    return dict(
        markeredgecolor=('none' if more else 'white'),
        markerfacecolor=color,
        marker=matplotlib.markers.MarkerStyle(
            (mpl or 'o'), fillstyle=toolkits, **marker_kwargs,
        ),
        markerfacecoloralt=color,
        linestyle='none',
    )


def main(filename: str = 'icon.png') -> None:
    """Create the icon and save to `icon.png`."""
    fig = cl.Figure(instantiator=plt.figure, **FIGURE_KWARGS)
    ax = fig.subplots(
        1, 1, gridspec_kw=dict(left=0, right=1, bottom=0, top=1), squeeze=True,
    )
    # Cleanup
    for artist in (ax.patch, *ax.spines.values()):
        artist.set_visible(False)
    ax.tick_params(color='none', labelcolor='none')
    # Draw a grid, a diagonal, and a circle
    grid_params = {
        key: matplotlib.rcParams['grid.' + key]
        for key in ('color', 'linestyle', 'linewidth', 'alpha')
    }
    ax.grid(True)
    ax.locator_params(nbins=5)
    ax.set_axisbelow(True)
    rays.axline(ax, (0, 0), slope=1, zorder=-1, **grid_params)
    circle = matplotlib.patches.Circle(
        (.5, .5),
        radius=.5*.8,
        transform=ax.transAxes,
        facecolor='none',
        edgecolor=grid_params['color'],
        **{k: v for k, v in grid_params.items() if k != 'color'},
    )
    ax.add_artist(circle)
    # Add overlay
    if BACKGROUND_DIM:
        patch_overlay = matplotlib.patches.Rectangle(
            (0, 0), 1, 1,
            transform=ax.transAxes,
            facecolor=[0, 0, 0, BACKGROUND_DIM],
            edgecolor='none',
        )
        ax.add_artist(patch_overlay)
    # Prepare the handles
    fig.register_cl_method('cl_plot', formatter)
    for key, values in LEGEND_LINE_VALUES.items():
        for value in values:
            ax.cl_plot(0, 0, **{key: value}, visible=False)
    # Group handles together in legend
    handles, labels = [], []
    for (
        category, handle_label_pairs,
    ) in fig.get_cl_handles_labels(**GET_HANDLES_KWARGS).items():
        (handle, _), = handle_label_pairs
        handles.append(handle)
        labels.append(category.capitalize() if category != 'mpl' else category)
    legend = fig.legend(handles, labels, **LEGEND_KWARGS)
    # Tweak the legend texts
    for text in legend.texts:
        props = LEGEND_LINE_PROPERTIES.get(text.get_text().lower())
        if props:
            text.set(**props)
    # Redraw figure patch
    patch_color = fig.patch.get_facecolor()
    try:
        fig.patch.remove()
    except Exception:  # Cannot remove the old patch
        fig.patch.set_visible(False)
    fancy_patch = matplotlib.patches.FancyBboxPatch(
        (PAD, PAD), (1 - 2 * PAD), (1 - 2 * PAD),
        transform=fig.transFigure,
        boxstyle=matplotlib.patches.BoxStyle('Round', pad=PAD),
        edgecolor='none',
        facecolor=patch_color,
        zorder=-1,
    )
    fig.patch = fig.add_artist(fancy_patch)
    # Clip everything to within the patch
    for artist in (
        *ax.patches,
        *ax.lines,
        *ax.get_xgridlines(),
        *ax.get_ygridlines()
    ):
        artist.set_clip_path(fig.patch)
    fig.savefig(filename)


if __name__ == '__main__':
    with matplotlib.rc_context(RC_PARAMS):
        main()
