"""
Actually working with figures and other matplotlib artists
"""
import functools
import numbers
import typing

from matplotlib.artist import Artist
from matplotlib.gridspec import GridSpecBase
from matplotlib.transforms import BboxTransformTo

import tjekmate

from . import _utils
from ._types import (
    AXIS_SPECIFICATIONS,
    BboxFitWarning,
    Literal,
    Interval,
    ExtentsOrBbox, Bbox,
    Figure, FigureBase,
    Keyword, Keywords,
    NonnegNum, NonnegQuantity,
    AxisSpecifications, ResolvedAxisSpecs,
    LegendLocation,
    SortingKey,
)
from .find_boxes import find_locally_maximal_bboxes

__all__ = (
    'avoid_figure_elements',
    'resolve_kwargs_from_bbox',
    'resolve_kwargs_from_bbox_and_dims',
)

FIT_STRAT_DEFAULT_TYPE_CHECKERS: typing.Mapping[
    Keyword, typing.Callable[[typing.Any], bool]
] = dict(
    best_bbox=Bbox.__instancecheck__,
    **dict.fromkeys(
        ('legend_width', 'legend_height'), tjekmate.is_nonnegative_real,
    ),
    **dict.fromkeys(
        ('snap', 'clip'), tjekmate.is_in(AXIS_SPECIFICATIONS),
    ),
    **dict.fromkeys(
        ('snap_bbox', 'clip_bbox'),
        (tjekmate.identical_to(None) | _utils.is_extents_or_bbox),
    ),
    tolerance=tjekmate.is_sequence(
        length=2, item_type=numbers.Real, allow_item=True,
    ),
    warn=(
        tjekmate.identical_to(None)
        | tjekmate.is_strict_boolean
        | tjekmate.is_keyword_mapping
    ),
)

FIT_STRAT_DEFAULT_ANNOTATIONS = dict(
    best_bbox=Bbox,
    **dict.fromkeys(('legend_width', 'legend_height'), NonnegNum),
    **dict.fromkeys(('snap', 'clip'), AxisSpecifications),
    **dict.fromkeys(('snap_bbox', 'clip_bbox'), typing.Optional[ExtentsOrBbox]),
    tolerance=typing.Union[numbers.Real, Interval],
    warn=typing.Optional[typing.Union[bool, Keywords]],
)

LEGEND_LOCATION_STRINGS: typing.Mapping[
    typing.Tuple[LegendLocation, LegendLocation], str
] = {
    (0, 0): 'lower left',
    (0, 1): 'lower center',
    (0, 2): 'lower right',
    (1, 0): 'center left',
    (1, 1): 'center',
    (1, 2): 'center right',
    (2, 0): 'upper left',
    (2, 1): 'upper center',
    (2, 2): 'upper right',
}


def _resolve_flmb_kwargs(
    fig: FigureBase,
    bboxes: typing.Sequence[Bbox],
    min_height: NonnegQuantity,
    min_width: NonnegQuantity,
    min_area: NonnegQuantity,
) -> typing.Mapping[Keyword, NonnegNum]:
    """
    Resolve the minimal dimensions which are to be passed to
    `find_locally_maximal_bboxes()`.

    Parameters
    ----------
    fig
        (Sub)Figure
    bboxes
        Bounding boxes in the units of (sub)figure fractions
    min_height, min_width, min_area
        Nonnegative dimensions or string specifications therefor in any
        of the permitted units

    Return
    ------
    Mapping of the keyword arguments `min_height`, `min_width`, and
    `min_area`

    See also
    --------
    `<MODULE>.legend_placement.avoid_figure_elements()`

    Example
    -------
    >>> from contextlib import nullcontext
    >>>
    >>> import matplotlib.pyplot as plt
    >>> from matplotlib.transforms import Bbox
    >>> from numpy import allclose
    >>>
    >>> fig_width, fig_height = 10, 6  # Inches
    >>> box_width = .5  # Figure fraction
    >>> required_height = 3  # Inches
    >>> width_frac = .25
    >>> required_area = 1  # Square cm
    >>> cm = 1 / 2.54  # Inches
    >>> with (plt.ioff() or nullcontext()):
    ...     fig = plt.figure(figsize=(fig_width, fig_height))
    ...     bbox = Bbox.from_extents(0, 0, box_width, .5)
    ...     results = _resolve_flmb_kwargs(
    ...         fig, [bbox],
    ...         min_height=f'{required_height} in',
    ...         min_width=f'{width_frac:%}',
    ...         min_area=f'{required_area} cm',
    ...     )
    >>> plt.close(fig)
    >>> dimensions = [
    ...     results['min_' + key] for key in ['height', 'width', 'area']
    ... ]
    >>> expected = [
    ...     required_height / fig_height,
    ...     box_width * width_frac,
    ...     (required_area * (cm ** 2) / (fig_width * fig_height)),
    ... ]
    >>> assert allclose(dimensions, expected), (dimensions, expected)
    """
    resolved: typing.Dict[Keyword, NonnegNum] = {}
    base_fig = fig
    seen_figs: typing.Set[int] = set()
    while getattr(base_fig, 'figure', None):
        # Get [Sub]Figure.figure
        # (Note: may be None in older matplotlib versions)
        if base_fig is base_fig.figure:
            break
        fig_id = id(base_fig)
        if fig_id in seen_figs:
            raise RuntimeError(
                f'fig = {fig!r}: '
                'loop detected when resolving base figure thereof'
            )
        seen_figs.add(fig_id)
        base_fig = base_fig.figure
    assert isinstance(base_fig, Figure)
    fig_box = _get_bbox_in_fig_fracs(fig.patch, base_fig)
    bboxes = [  # Make sure that the bboxes are in base-fig fractions
        bbox.transformed(BboxTransformTo(fig_box)) for bbox in bboxes
    ]
    var_prefix = 'min_'

    for varname, (getter_from_bbox, pow, bf2f) in dict(
        height=(lambda b: abs(b.height), 1, fig_box.height),
        width=(lambda b: abs(b.width), 1, fig_box.width),
        area=(
            lambda b: abs(b.height * b.width),
            2,
            (fig_box.height * fig_box.width),
        ),
    ).items():
        raw_value = locals()[var_prefix + varname]
        if raw_value is None:
            raw_value = 0
        try:
            numeric_value, unit = _utils.get_nonneg_quantity(raw_value)
        except TypeError:  # Trying to unpack None
            raise TypeError(
                f'{var_prefix}{varname} = {raw_value!r}: '
                'expected `None`, or string of nonnegative number with any of '
                f'these optional suffixes: {list(_utils.MIN_DIM_RESOLVERS)!r}'
            ) from None
        if unit in _utils.MIN_DIM_RESOLVERS:
            # Physical units -> base-figure fraction
            numeric_value *= _utils.MIN_DIM_RESOLVERS[unit](
                base_fig, bboxes, getter_from_bbox, pow
            )
            # -> figure fraction
            numeric_value /= bf2f
        resolved[var_prefix + varname] = numeric_value
    return resolved


@tjekmate.type_checked(
    checkers=FIT_STRAT_DEFAULT_TYPE_CHECKERS,
    annotations=FIT_STRAT_DEFAULT_ANNOTATIONS,
)
def resolve_kwargs_from_bbox_and_dims(
    best_bbox, legend_width, legend_height,
    *,
    clip=True,
    clip_bbox=None,
    snap=False,
    snap_bbox=None,
    tolerance=0.,
    warn=None,
    **kwargs
) -> typing.Tuple[bool, typing.Dict[Keyword, typing.Any]]:
    """
    From the suggested best bounding box, refine it to get a suitable
    bbox and the appropriate alignment, such that the legend of known
    dimensions can best be accommodated.

    Parameters
    ----------
    best_bbox
        Proposed bbox candidate (in (sub)figure-fraction coordinates)
    legend_width, legend_height
        Dimensions of the legend in (sub)figure fractions
    clip, snap
        True, 'xy'
            Clip/Snap to edges
        False
            Don't clip/snap to edges
        'x', 'y'
            Only clip/snap to the specified edge
    clip_bbox, snap_bbox
        Clip/Snap to this bbox (in (sub)figure-fraction coordinates);
        if only either thereof is provided, both defaults to that value;
        if neither is, both defaults to the unit bbox, i.e. (sub)figure
        bbox
    tolerance
        Consider a bbox to be touching (resp. hanging over) another if
        the distance between the former's edge and the latter's is equal
        to (resp. greater than) this number;
        if two numbers, take the them to be tolerance values for the x-
        and y-directions respectively
    warn
        True, None
            Issue warnings for bad fits
        False
            Don't issue warnings for bad fits
        Keyword mapping
            Issue warnings for bad fits with these keyword arguments for
            `BboxFitWarning.warn()`
    **kwargs
        (Ignored)

    Return
    ------
    Tuple `(legend_fits_successfully, keywords)`;
    `keywords` may contain `bbox_to_anchor`, `loc`, etc.

    Side effects
    ------------
    `BboxFitWarning` is issued when:

    - `best_bbox` is disjoint with the clipping range, if clipping is
      enabled.

    - `snap_bbox` is disjoint with the clipping range, if snapping and
      clipping are both enabled.

    - The supplied dimensions for the legend is too big.

    - The resultant bbox is not inside `best_bbox`.

    Strategies
    ----------
    Along each axis:

    1.1. If `clip = True`:

         - `best_bbox` and `snap_bbox` are first clipped by `clip_bbox`;
           if `best_bbox` doesn't intersect `clip_bbox`, the fit is
           considered to have already failed, and we abort.
           if `snap_bbox` doesn't intersect `clip_bbox`, its value falls
           back to the "canvas box".

         - The "canvas box" is defined to be `clip_bbox`.

    1.2. Else:
         The "canvas box" is defined to be the union of the unit box
         (i.e. the full extent of the (sub)figure), `best_bbox`, and
         `snap_bbox` (if `snap = True`).
         Note that in both cases the "canvas box" always non-strictly
         encloses `best_bbox` and `snap_bbox` i.e. they may share edges.

    2.   If the legend wouldn't fit inside the "canvas box" no matter
         the position (i.e. it's too tall/wide):
         Center it to `best_bbox` as that's the best we can do, and
         consider the fit to have failed.

    3.   Enumerate these boxes of the provided size of the legend:
         - Aligned to the lower and upper limits of `snap_bbox` (if
           `snap = True`),
         - Aligned to the center of `best_bbox`,
         - Aligned to the lower and upper limits of the "canvas box",
           and
         - Aligned to the center of the "canvas box";
         those not contained in the "canvas box" are excluded.

    4.   Of the remainder, a box is chosen by:
         - Whether it is contained in `best_bbox`, then
         - Whether it is aligned against an edge of `snap_bbox` (if
           `snap = True`), then
         - How close its center is to the center of `best_bbox`.
         If the chosen box is not contained in `best_bbox`, a warning is
         issued.

    See also
    --------
    `<MODULE>.legend_placement.resolve_kwargs_from_bbox()`

    Examples
    --------
    >>> from pprint import pprint
    >>> from warnings import catch_warnings
    >>>
    >>> def is_interval(obj: typing.Any) -> bool:
    ...     if not isinstance(obj, typing.Sequence):
    ...         return False
    ...     if len(obj) != 2:
    ...         return False
    ...     return all(isinstance(limit, numbers.Real) for limit in obj)
    ...
    >>> def print_kwargs(
    ...     bbox: Bbox,
    ...     legend: typing.Tuple[NonnegNum, NonnegNum],
    ...     *,
    ...     clip: typing.Optional[
    ...         typing.Mapping[Literal['x', 'y'], Interval]
    ...     ] = None,
    ...     snap: typing.Optional[
    ...         typing.Mapping[Literal['x', 'y'], Interval]
    ...     ] = None,
    ...     tolerance: typing.Union[
    ...         float, typing.Tuple[float, float]
    ...     ] = 0.,
    ...     warn: bool = True,
    ...     width: int = 68,
    ...     **kwargs
    ... ) -> None:
    ...     resolver_kwargs = {}
    ...     for arg_name, value in dict(clip=clip, snap=snap).items():
    ...         if not value:
    ...             resolver_kwargs[arg_name] = False
    ...             continue
    ...         assert isinstance(value, typing.Mapping)
    ...         assert all(key in ('x', 'y') for key in value)
    ...         assert all(
    ...             is_interval(interval) for interval in value.values()
    ...         )
    ...         directions = ''.join(value)
    ...         align_bbox = Bbox.unit()
    ...         for axis, interval in value.items():
    ...             setattr(align_bbox, 'interval' + axis, interval)
    ...         resolver_kwargs.update({
    ...             arg_name: directions,
    ...             arg_name + '_bbox': align_bbox,
    ...         })
    ...     resolver = functools.partial(
    ...         resolve_kwargs_from_bbox_and_dims,
    ...         bbox, *legend,
    ...         tolerance=tolerance,
    ...         warn=warn,
    ...         **resolver_kwargs,
    ...     )
    ...     with catch_warnings(record=True) as warn_ctx:
    ...         success, legend_kwargs = resolver()
    ...         print('Good' if success else 'Bad', 'fit:')
    ...         pprint(legend_kwargs, width=width, **kwargs)
    ...         for warning_summary in warn_ctx:
    ...             print(
    ...                 '{0.category.__name__}: {0.message}'
    ...                 .format(warning_summary)
    ...             )

    Fitting an oversized legend:

    >>> print_kwargs(
    ...     Bbox.unit().shrunk(.5, .5),  # Lower left quadrant
    ...     (1.25, .25),  # Legend is too wide
    ...     snap={'y': (0, 1)},  # Only snap vertically
    ... )
    Bad fit:
    {'bbox_to_anchor': Bbox([[0.0, 0.0], [0.5, 0.25]]),
     'loc': 'lower center'}
    BboxFitWarning: ... x-direction: legend is too big for the canvas

    Clipped canvas:

    >>> print_kwargs(
    ...     (  # Upper left quadrant
    ...         Bbox.unit().shrunk(.5, .5).translated(0, .5)
    ...     ),
    ...     (.125, .125),  # Legend fits
    ...     clip=dict.fromkeys('xy', (0, .75)),
    ...     # Result "canvas": Bbox([[0, .5], [.5, .75]])
    ... )
    Good fit:
    {'bbox_to_anchor': Bbox([[0.1875, 0.5625], [0.3125, 0.6875]]),
     'loc': 'center'}
    >>> print_kwargs(
    ...     Bbox.unit().shrunk(.25, .25).translated(.5, .5),
    ...     (.125, .125),
    ...     clip=dict(x=(0, .25)),  # Disjoint with the bbox
    ... )
    Bad fit:
    {}
    BboxFitWarning: ... candidate does not intersect the clipping ...
    >>> print_kwargs(  # doctest: +NORMALIZE_WHITESPACE
    ...     Bbox.unit(),
    ...     (.25, .5),
    ...     clip=dict(x=(0, .25)),
    ...     snap=dict(x=(.5, .75)),  # Disjoint with the clipping box
    ... )
    Good fit:
    {'bbox_to_anchor': Bbox([[0.0, 0.25], [0.25, 0.75]]),
     'loc': 'center'}
    BboxFitWarning: ... snapping box does not intersect the clipping
      ...
    defaulting to the "canvas" (Bbox([[0.0, 0.0], [0.25, 1.0]]))

    Bbox nudging:

    >>> print_kwargs(  # doctest: +NORMALIZE_WHITESPACE
    ...     Bbox.unit().shrunk(.5, .5).translated(0, .5),
    ...     # Legend snaps to the snapping box but not contained in
    ...     # best_bbox (too big)
    ...     (.75, .75),
    ...     snap=dict.fromkeys('xy', (0, 1)),
    ... )
    Bad fit:
    {'bbox_to_anchor': Bbox([[0.0, 0.25], [0.75, 1.0]]),
     'loc': 'upper left'}
    BboxFitWarning:
      ...
    xy-direction: legend is too big for the candidate bbox
    >>> print_kwargs(  # doctest: +NORMALIZE_WHITESPACE
    ...     Bbox.unit().shrunk(.5, .5).translated(0, .5),
    ...     (.5, .5),
    ...     # Legend fits in best_bbox, but not contained in it after
    ...     # snapping -> prioritize fitting over snapping
    ...     snap=dict(y=(0.125, .25)),
    ... )
    Good fit:
    {'bbox_to_anchor': Bbox([[0.0, 0.5], [0.5, 1.0]]),
     'loc': 'center'}
    >>> print_kwargs(
    ...     Bbox.unit().shrunk(.5, 1),
    ...     (.5, .5),
    ...     # Legend fits in best_bbox after snapping -> snap
    ...     snap=dict(y=(0.125, .25)),
    ... )
    Good fit:
    {'bbox_to_anchor': Bbox([[0.0, 0.125], [0.5, 0.625]]),
     'loc': 'lower center'}
    """
    Candidate = typing.Tuple[Interval, LegendLocation]

    def bbox_contains(host: Bbox, other: Bbox) -> bool:
        """
        Check if `other` is inside `host` according to the tolerance.
        """
        return all(
            _interval_contains(to_int(host), to_int(other), tolerance=tol)
            for to_int, tol in zip(
                (get_interval['x'], get_interval['y']),
                (tolerances['x'], tolerances['y']),
            )
        )

    def format_arg_list(arg: Keyword, *args: Keyword) -> str:
        """
        Autoformatting of parameter values for error/warning messages.
        """
        return _utils.format_arg_list(
            [arg, *args],
            values=arg_values,
            original_values=arg_origins,
            sources=arg_sources,
            aliases=arg_aliases,
        )

    def add_arg_sources(
        arg: Keyword, source: Keyword, *sources: Keyword,
    ) -> None:
        """
        Mark `arg` as being resolved/derived from `sources`.
        """
        source_arg_names = arg_sources.setdefault(arg, [])
        source_arg_names.extend([source, *sources])

    def prioritize_containment(
        candidate: Candidate, is_contained: typing.Callable[[Interval], bool],
    ) -> int:
        """
        Sort key on whether the candidate interval is inside a host
        interval according to the return value of `is_contained()`.
        """
        interval, _ = candidate
        if is_contained(interval):
            return 0
        return 1

    def prioritize_alignment(
        candidate: Candidate,
        is_aligned: typing.Callable[[Interval], typing.Tuple[bool, bool]],
    ) -> int:
        """
        Sort key on whether the candidate interval is aligned against a
        host interval according to the return value of `is_aligned()`.
        """
        interval, _ = candidate
        return -sum(is_aligned(interval))

    def prioritize_central_distance(
        candidate: Candidate, other: Interval,
    ) -> numbers.Real:
        """
        Sort key on how far the candidate interval is from another
        interval.
        """
        interval, _ = candidate
        return abs(sum(interval) - sum(other))

    axes = 'x', 'y'
    bboxes: typing.Dict[Keyword, typing.Union[Bbox, None]] = dict(
        canvas=Bbox.unit(),
        best=best_bbox,
        clip=clip_bbox,
        snap=snap_bbox,
    )
    get_interval, set_interval = (
        {axis: func_maker(axis) for axis in axes}
        for func_maker in (_make_interval_getter, _make_interval_setter)
    )
    tolerances: typing.Dict[Literal[axes], numbers.Real] = {}
    tolerance_names = dict(x='tol_x', y='tol_y')
    legend_dimensions = dict(x=legend_width, y=legend_height)
    legend_dimension_names = dict(x='legend_width', y='legend_height')
    # Resolve defaults and massage values
    if bboxes['clip'] is bboxes['snap'] is None:
        bboxes['snap'] = bboxes['clip'] = Bbox.unit()
    elif bboxes['snap'] is None:
        bboxes['snap'] = bboxes['clip']
    elif bboxes['clip'] is None:
        bboxes['clip'] = bboxes['snap']
    bboxes = {  # Ensure all bboxes are decoupled
        key: _utils.to_bbox(bbox) for key, bbox in bboxes.items()
    }
    assert all(bboxes.values())
    clip = _resolve_axis_specs(clip)
    snap = _resolve_axis_specs(snap)
    try:
        tolerances['x'], tolerances['y'] = tolerance
    except TypeError:  # Singular real number
        tolerances['x'] = tolerances['y'] = tolerance
    if warn in (True, None):
        warn = {}
    # (For formatting error and warning messages)
    arg_aliases: typing.Dict[Keyword, str] = dict(
        best_bbox='<candidate bbox>',
        canvas_bbox='<canvas>',
        tol_x='<tolerance (x)>',
        tol_y='<tolerance (y)>',
    )
    arg_values: typing.Dict[Keyword, typing.List[Keyword]] = dict(
        {key + '_bbox': bbox for key, bbox in bboxes.items()},
        **{
            legend_dimension_names[axis]: dim
            for axis, dim in legend_dimensions.items()
        },
        **{tolerance_names[axis]: dim for axis, dim in tolerances.items()},
        clip=clip,
        snap=snap,
        tolerance=tolerance,
        warn=warn,
    )
    arg_origins: typing.Dict[Keyword, typing.Any] = dict(
        best_bbox=best_bbox,
        clip_bbox=clip_bbox,
        snap_bbox=snap_bbox,
    )
    arg_sources: typing.Dict[Keyword, typing.List[Keyword]] = {}
    for axis in axes:
        add_arg_sources('tol_' + axis, 'tolerance')
    for arg, arg_names in [
        (clip, ['clip', 'clip_bbox']),
        (snap, ['snap', 'snap_bbox']),
    ]:
        if not arg:
            continue
        for key in bboxes:
            add_arg_sources(key + '_bbox', *arg_names)
    add_arg_sources('canvas_bbox', 'best_bbox')
    # Clip boxes to within the prescribed bounds
    if clip:
        clip_kwargs = {}
        for axis, lo, hi in ('x', 'left', 'right'), ('y', 'lower', 'upper'):
            if axis not in clip:
                continue
            clip_interval = get_interval[axis](bboxes['clip'])
            set_interval[axis](bboxes['canvas'], clip_interval)
            clip_kwargs.update(zip((lo, hi), clip_interval))
        for key in 'best', 'snap':
            bboxes[key] = _clip_bbox_to_lims(
                bboxes[key], limits=None, **clip_kwargs, tolerance=tolerance,
            )
            arg_values[key + '_bbox'] = bboxes[key]
    # Refine canvas size
    for axis in axes:
        canvas_references = set(bboxes)
        # Drop clipping/snapping boxes if not used
        for arg_name, arg in dict(clip=clip, snap=snap).items():
            if axis not in arg:
                canvas_references.remove(arg_name)
        lower_lims, upper_lims = zip(*(
            get_interval[axis](bboxes[ref]) for ref in canvas_references
            # We just clipped, best_bbox and snap_bbox might have become
            # None
            if bboxes[ref]
        ))
        # Note: we don't use `Bbox.union()` because it doen't know about
        # whether each direction of the snapping/clipping box is
        # actually relavant
        set_interval[axis](
            bboxes['canvas'], (min(lower_lims), max(upper_lims)),
        )
    if bboxes['best'] is None:  # Doesn't even intersect the clipping box
        assert clip, '`best_bbox` should exist if not clipped'
        if isinstance(warn, typing.Mapping):
            arg_list = format_arg_list('best_bbox', 'clip_bbox', 'clip')
            msg = (
                f'{arg_list}: box candidate does not intersect the clipping box'
            )
            BboxFitWarning.warn(msg, **warn)
        return False, {}
    if bboxes['snap'] is None and snap:  # Nowhere to snap to
        assert clip, '`snap_bbox` should exist if not clipped'
        if isinstance(warn, typing.Mapping):
            arg_list = format_arg_list('snap_bbox', 'clip_bbox', 'clip')
            msg = (
                f'{arg_list}: '
                'provided snapping box does not intersect the clipping box, '
                f'defaulting to the "canvas" ({bboxes["canvas"]!r})'
            )
            BboxFitWarning.warn(msg, **warn)
        bboxes['snap'] = _utils.to_bbox(bboxes['canvas'])
    # Do the fit
    fits_ints_locs_msgs_causes: typing.List[
        typing.Tuple[
            bool,  # Whether a correct interval is found
            Interval,  # The interval
            LegendLocation,  # The alignment
            str,  # Desc of why a fit is not found
            typing.Sequence[Keyword],  # Param names preventing fit
        ]
    ] = []
    for key in 'best', 'snap':
        assert (
            bbox_contains(bboxes['canvas'], bboxes[key])
        ), f'`{key}_bbox` not in canvas'
    for axis in axes:
        tol, tol_name = tolerances[axis], tolerance_names[axis]
        legend_dim = legend_dimensions[axis]
        legend_dim_name = legend_dimension_names[axis]
        interval_contains, interval_is_aligned = (
            functools.partial(func, tolerance=tol)
            for func in [_interval_contains, _interval_is_aligned]
        )
        anchor_legend_interval = functools.partial(_anchor_interval, legend_dim)
        best_interval, snap_interval, canvas_interval = (
            get_interval[axis](bboxes[key])
            for key in ['best', 'snap', 'canvas']
        )
        if not interval_contains(canvas_interval, legend_dim):
            # Too big to fit, center and call it a day
            fits_ints_locs_msgs_causes.append((
                False,
                best_interval,
                1,
                'legend is too big for the canvas',
                ['canvas_bbox', legend_dim_name, tol_name],
            ))
            continue
        # Choose from proposals
        contained_in_best = functools.partial(
            prioritize_containment,
            is_contained=functools.partial(interval_contains, best_interval),
        )
        close_to_best = functools.partial(
            prioritize_central_distance, other=best_interval,
        )
        sort_criteria: typing.List[
            typing.Callable[[Candidate], SortingKey]
        ]
        candidates: typing.List[Candidate] = [  # Center rel. to best
            (anchor_legend_interval(best_interval, 1), 1),
        ]
        candidates.extend(  # Align relative to canvas
            (anchor_legend_interval(canvas_interval, loc), loc)
            for loc in range(3)
        )
        causes = ['best_bbox', 'canvas_bbox', legend_dim_name, tol_name]
        if axis in snap:
            aligned_to_snap = functools.partial(
                prioritize_alignment,
                is_aligned=functools.partial(
                    interval_is_aligned, snap_interval,
                ),
            )
            sort_criteria = [contained_in_best, aligned_to_snap, close_to_best]
            candidates.extend(  # Align relative to snap_bbox
                (anchor_legend_interval(snap_interval, loc), loc)
                for loc in (0, 2)
            )
            causes.extend(['snap', 'snap_bbox'])
        else:
            sort_criteria = [contained_in_best, close_to_best]
        candidates = [
            (interval, loc) for interval, loc in candidates
            if interval_contains(canvas_interval, interval)
        ]
        assert candidates, 'no candidates inside canvas'
        interval, loc = min(
            candidates,
            key=_utils.CompositeSorter(*sort_criteria),
        )
        if interval_contains(best_interval, legend_dim):
            msg = 'adjusted candidate bbox not contained in original bbox'
            fit = interval_contains(best_interval, interval)
        else:
            msg = 'legend is too big for the candidate bbox'
            fit = False
        fits_ints_locs_msgs_causes.append((fit, interval, loc, msg, causes))
    # Gather results
    assert (  # Sanity check
        bbox_contains(bboxes['canvas'], bboxes['best'])
    ), 'candidate not inside canvas'
    assert (
        len(fits_ints_locs_msgs_causes) == 2
    ), f'got {len(fits_ints_locs_msgs_causes)} intervals (expected 2)'
    fits, intervals, locs, *_ = zip(*fits_ints_locs_msgs_causes)
    is_fit = all(fits)
    bbox_to_anchor = Bbox.unit()
    for axis, interval in zip(axes, intervals):
        set_interval[axis](bbox_to_anchor, interval)
    arg_values['best_bbox'] = bbox_to_anchor  # For error messages
    if axes[0] != 'y':  # x, y -> y, x
        locs = locs[::-1]
    # Issue warnings
    if isinstance(warn, typing.Mapping):
        collated_error_messages: typing.Dict[
            str,  # Message
            typing.Tuple[
                typing.List[Literal['x', 'y']],  # Axis
                typing.List[Keyword],  # Param culprits
            ],
        ] = {}
        for axis, (fit, *_, msg, causes) in zip(
            axes, fits_ints_locs_msgs_causes,
        ):
            if fit:
                continue
            directions, _causes = collated_error_messages.setdefault(
                msg, ([], []),
            )
            directions.append(axis)
            _causes.extend(causes)
        for msg, (directions, causes) in collated_error_messages.items():
            direction = ''.join(directions)
            if direction == 'yx':
                direction = 'xy'
            msg = '{}: {}-direction: {}'.format(
                format_arg_list(*causes), direction, msg,
            )
            BboxFitWarning.warn(msg, **warn)
    return is_fit, dict(
        bbox_to_anchor=bbox_to_anchor,
        loc=LEGEND_LOCATION_STRINGS[tuple(locs)],
    )


@tjekmate.type_checked(
    checkers=dict(
        FIT_STRAT_DEFAULT_TYPE_CHECKERS,
        snap_bbox=_utils.is_extents_or_bbox,
    ),
    annotations=dict(
        FIT_STRAT_DEFAULT_ANNOTATIONS,
        snap_bbox=ExtentsOrBbox,
    ),
)
def resolve_kwargs_from_bbox(
    best_bbox,
    *,
    snap=True,
    snap_bbox=(0, 0, 1, 1),
    tolerance=0.,
    warn=None,
    **kwargs
) -> typing.Tuple[bool, typing.Dict[Keyword, typing.Any]]:
    """
    From the suggested best bounding box, figure out where to put the
    legend therein.

    Parameters
    ----------
    best_bbox
        Proposed bbox candidate (in (sub)figure-fraction coordinates)
    snap
        True, 'xy'
            Snap to edges
        False
            Don't snap to edges
        'x', 'y'
            Only snap to the specified edge
    snap_bbox
        Snap to this bbox (in (sub)figure-fraction coordinates, default
        is the (sub)figure box)
    tolerance
        Consider `best_bbox` to be touching (resp. hanging over)
        `snap_bbox` if the distance between the former's edge and the
        latter's is equal to (resp. greater than) this number;
        if two numbers, take the them to be tolerance values for the x-
        and y-directions respectively
    warn
        True, None
            Issue warnings for bad fits
        False
            Don't issue warnings for bad fits
        Keyword mapping
            Issue warnings for bad fits with these keyword arguments for
            `BboxFitWarning.warn()`
    **kwargs
        (Ignored)

    Return
    ------
    Tuple `(best_bbox_overlaps_snap_bbox, keywords)`;
    `best_bbox_overlaps_snap_bbox` is always true if `snap = False`;
    `keywords` may contain the keys 'bbox_to_anchor', 'loc', etc.

    Side effects
    ------------
    `BboxFitWarning` is issued if:

    - `best_bbox` is disjoint with `snap_bbox` when snapping is enabled.

    Strategies
    ----------
    Along each axis:

    1. If `snap = False`:
       The legend is centered to `best_bbox`.

    2. Else, if `best_bbox` is disjoint with `snap_bbox`:
       The legend is centered to `best_bbox` with a warning.

    3. Else, if exactly one edge of `best_bbox` coincides (to within
       tolerance) with the corresponding edge of `snap_bbox`:
       The legend is aligned against said edge in `best_bbox`.

    4. Else:
       The legend is centered to `best_bbox`.

    See also
    --------
    `<MODULE>.legend_placement.resolve_kwargs_from_bbox_and_dims()`

    Examples
    --------
    >>> from pprint import pprint
    >>> from warnings import catch_warnings
    >>>
    >>> def print_kwargs(
    ...     bbox: Bbox,
    ...     *,
    ...     snap: AxisSpecifications = True,
    ...     tolerance: typing.Union[
    ...         float, typing.Tuple[float, float]
    ...     ] = 0.,
    ...     warn: bool = False,
    ...     width: int = 68,
    ...     **kwargs
    ... ) -> None:
    ...     resolver = functools.partial(
    ...         resolve_kwargs_from_bbox,
    ...         bbox,
    ...         snap=snap, tolerance=tolerance, warn=warn,
    ...     )
    ...     with catch_warnings(record=True) as warn_ctx:
    ...         success, legend_kwargs = resolver()
    ...         print('Good' if success else 'Bad', 'fit:')
    ...         pprint(legend_kwargs, width=width, **kwargs)
    ...         for warning_summary in warn_ctx:
    ...             print(
    ...                 '{0.category.__name__}: {0.message}'
    ...                 .format(warning_summary)
    ...             )

    Centering to a bbox:

    >>> bbox_in_figure = Bbox([[.25, .25], [.75, .75]])
    >>> print_kwargs(bbox_in_figure)
    Good fit:
    {'bbox_to_anchor': Bbox([[0.25, 0.25], [0.75, 0.75]]),
     'loc': 'center'}

    Using a bbox touching the snapping-box edge:

    >>> bbox_touching = Bbox([[.5, .25], [1, 1]])
    >>> print_kwargs(bbox_touching)  # snap = True = 'xy'
    Good fit:
    {'bbox_to_anchor': Bbox([[0.5, 0.25], [1.0, 1.0]]),
     'loc': 'upper right'}
    >>> print_kwargs(bbox_touching, snap='x')
    Good fit:
    {'bbox_to_anchor': Bbox([[0.5, 0.25], [1.0, 1.0]]),
     'loc': 'center right'}
    >>> print_kwargs(bbox_touching, snap='y')
    Good fit:
    {'bbox_to_anchor': Bbox([[0.5, 0.25], [1.0, 1.0]]),
     'loc': 'upper center'}
    >>> print_kwargs(bbox_touching, snap=False)
    Good fit:
    {'bbox_to_anchor': Bbox([[0.5, 0.25], [1.0, 1.0]]), 'loc': 'center'}

    Using a bbox near the snapping-box edge:

    >>> bbox_overhang = Bbox([[.5, .25], [1.125, 1.125]])
    >>> print_kwargs(bbox_overhang)
    Good fit:
    {'bbox_to_anchor': Bbox([[0.5, 0.25], [1.125, 1.125]]),
     'loc': 'center'}
    >>> print_kwargs(bbox_overhang, tolerance=.2)
    Good fit:
    {'bbox_to_anchor': Bbox([[0.5, 0.25], [1.125, 1.125]]),
     'loc': 'upper right'}

    Using a bbox disjoint with the snapping box:

    >>> bbox_disjoint = Bbox([[-1, .25], [-.5, 1]])
    >>> print_kwargs(  # doctest: +NORMALIZE_WHITESPACE
    ...     bbox_disjoint,
    ...     warn=True,
    ... )
    Bad fit:
    {'bbox_to_anchor': Bbox([[-1.0, 0.25], [-0.5, 1.0]]),
     'loc': 'upper center'}
    BboxFitWarning:
      ...
    x-direction: candidate does not intersect the snapping box
    """
    bbox_to_anchor = _utils.to_bbox(best_bbox)
    # Resolve arguments
    snap_bbox = _utils.to_bbox(snap_bbox)
    snap = _resolve_axis_specs(snap)
    try:
        tol_x, tol_y = tolerance
    except TypeError:  # Singular real number
        tol_x = tol_y = tolerance
    if warn in (True, None):
        warn = {}
    # Do the snap fit
    locs: typing.List[LegendLocation] = []
    statuses: typing.List[bool] = []
    axes: typing.List[Literal['x', 'y']] = []
    for axis, tol in ('y', tol_y), ('x', tol_x):
        best_interval, snap_interval = (
            sorted(getattr(bbox, 'interval' + axis))
            for bbox in (best_bbox, snap_bbox)
        )
        resolved_interval, loc, interval_is_ok = _resolve_snapping(
            best_interval,
            snap_interval,
            snap=(axis in snap),
            tolerance=tol,
        )
        setattr(bbox_to_anchor, 'interval' + axis, resolved_interval)
        locs.append(loc)
        statuses.append(interval_is_ok)
        axes.append(axis)
    # Gather results
    is_ok = all(statuses)
    if axes[0] != 'y':  # x, y -> y, x
        locs = locs[::-1]
    # Issue warnings
    if not is_ok and isinstance(warn, typing.Mapping):
        offending_direction = ''.join(
            axis for axis, is_ok in zip(axes, statuses) if not is_ok
        )
        if offending_direction == 'yx':
            offending_direction = 'xy'
        offending_params = dict(
            best_bbox=best_bbox,
            snap=snap,
            snap_bbox=snap_bbox,
        )
        if any(statuses):
            offending_tol = next(
                tol for tol, is_ok in zip((tol_y, tol_x), statuses) if not is_ok
            )
            offending_params['tol_' + offending_direction] = offending_tol
        else:
            offending_params['tolerance'] = tolerance
        msg = (
            '{}: {}-direction: candidate does not intersect the snapping box'
            .format(
                _utils.format_arg_list(
                    offending_params,
                    aliases=dict(
                        best_bbox='<candidate bbox>',
                        tol_x='<tolerance (x)>',
                        tol_y='<tolerance (y)>',
                    ),
                    sources=dict.fromkeys(('tol_x', 'tol_y'), ['tolerance']),
                ),
                offending_direction,
            )
        )
        BboxFitWarning.warn(msg, **warn)
    return is_ok, dict(
        bbox_to_anchor=bbox_to_anchor,
        loc=LEGEND_LOCATION_STRINGS[tuple(locs)],
    )


def _clip_bbox_to_lims(
    bbox: ExtentsOrBbox,
    limits: typing.Union[ExtentsOrBbox, Interval, None] = (0., 1.),
    *,
    lower: typing.Optional[numbers.Real] = None,
    upper: typing.Optional[numbers.Real] = None,
    left: typing.Optional[numbers.Real] = None,
    right: typing.Optional[numbers.Real] = None,
    tolerance: typing.Union[
        numbers.Real, typing.Tuple[numbers.Real, numbers.Real],
    ] = 0.,
) -> Bbox:
    """
    Clip a bounding box.

    Parameters
    ----------
    bbox
        Bounding box
    limits
        Default limits of the box anchor-point coordinates to clip to;
        can be supplied as:
        - `None` (no default limits),
        - A pair of real numbers `min, max` (applies to both axes),
        - Extents `xmin, ymin, xmax, ymax`, or
        - `Bbox`
        Default is to clip to the unit bbox
    lower, left
        Directional lower limits of the box anchor-point coordinates
        (default to the lower bound specified by `limits`)
    upper, right
        Directional upper limits of the box anchor-point coordinates
        (default to the upper bound specified by `limits`)
    tolerance
        Only trim `bbox` if its overhang over the limits exceeds this
        number;
        if two numbers, take the them to be tolerance values for the x-
        and y-directions respectively

    Return
    ------
    `bbox` is within the clipping limits or intersects them
        Clipped bbox
    Else
        None

    Examples
    --------
    Clipping to the unit box:

    >>> bbox = Bbox([[-2, -2], [.5, .5]])
    >>> _clip_bbox_to_lims(bbox)
    Bbox([[0.0, 0.0], [0.5, 0.5]])
    >>> _clip_bbox_to_lims(  # More tolerance on the y-axis
    ...     Bbox([[-.0625, -.0625], [.5, .5]]), tolerance=(0, .1),
    ... )
    Bbox([[0.0, -0.0625], [0.5, 0.5]])

    Clipping to some other boxes:

    >>> _clip_bbox_to_lims(bbox, (-1, 1))  # Bbox([[-1, -1], [1, 1]])
    Bbox([[-1.0, -1.0], [0.5, 0.5]])
    >>> _clip_bbox_to_lims(bbox, left=-1)  # Bbox([[-1, 0], [1, 1]])
    Bbox([[-1.0, 0.0], [0.5, 0.5]])
    >>> _clip_bbox_to_lims(bbox, Bbox([[-1, 0], [1, 1]]))
    Bbox([[-1.0, 0.0], [0.5, 0.5]])

    Clipping with indefinite bounds:

    >>> _clip_bbox_to_lims(  # Bbox([[-1, -inf], [inf, inf]])
    ...     bbox, None, left=-1,
    ... )
    Bbox([[-1.0, -2.0], [0.5, 0.5]])

    Boxes outside the limits:

    >>> print(_clip_bbox_to_lims(Bbox([[1, 2], [3, 4]])))
    None
    >>> _clip_bbox_to_lims(Bbox([[1.125, 1.125], [2, 2]]), tolerance=.2)
    Bbox([[1.0, 1.0], [1.125, 1.125]])

    Notes
    -----
    If `limits` are supplied as `None` or `min, max`, the keyword
    arguments have priority over them;
    else, they have priority and the keyword arguments are ignored.
    """
    # Copy of bbox created here so it's safe to mutate it later
    bbox = _utils.to_bbox(bbox)
    try:
        tol_x, tol_y = tolerance
    except TypeError:  # Singular real number
        tol_x = tol_y = tolerance
    # Resolve clipping limits
    lmin = lmax = None
    if limits is None:
        pass
    elif isinstance(limits, Bbox):
        left, lower, right, upper = limits.extents
    elif len(limits) == 2:  # Limits
        lmin, lmax = limits
    elif len(limits) == 4:  # Extents
        left, lower, right, upper = limits
    else:
        raise TypeError(
            f'limits = {limits!r}: '
            'expected `Bbox`, extents (4 numbers), limits (2 numbers), '
            'or none'
        )
    clip_min, clip_max = [left, lower], [right, upper]
    for clip, lim, default_lim in [
        (clip_min, lmin, float('-inf')),
        (clip_max, lmax, float('inf')),
    ]:
        if lim is None:
            lim = default_lim
        for i, coord in enumerate(clip):
            if coord is None:
                clip[i] = lim

    clip_x, clip_y = zip(clip_min, clip_max)
    bbox_x, bbox_y = sorted(bbox.intervalx), sorted(bbox.intervaly)
    for axis, clip_interval, bbox_interval, tol in [
        ('x', clip_x, bbox_x, tol_x), ('y', clip_y, bbox_y, tol_y),
    ]:
        clipped_interval = _resolve_clipping(
            bbox_interval, clip_interval, clip=True, tolerance=tol,
        )
        if not clipped_interval:  # Disjoint
            return None
        setattr(bbox, 'interval' + axis, clipped_interval)
    return bbox


def _clip_bbox_to_gridspec(
    bbox: ExtentsOrBbox,
    gridspec: GridSpecBase,
    tolerance: typing.Union[
        numbers.Real, typing.Tuple[numbers.Real, numbers.Real],
    ] = 0.,
) -> typing.Union[Bbox, None]:
    """
    Clip a `bbox` so that it lies flush with a `SubplotSpec` formed from
    item access on the `gridspec`.

    Parameters
    ----------
    bbox
        Bounding box in figure fractions
    gridspec
        GridSpecBase object
    tolerance
        Allow overhang of `bbox` over the subplot-spec limits up to this
        number;
        if two numbers, take the them to be tolerance values for the x-
        and y-directions respectively

    Return
    ------
    If `bbox` doesn't occupy an entire grid space
        None
    Else
        Bbox representing the span of the biggest subplot spec on this
        grid which fits in `bbox`

    Example
    -------
    Consider the following gridspec:

    y=1
     +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
     |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
     +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
     |   |   |                       |   |                   |   |   |
     +---+---+                       +---+                   +---+---+
     |   |   |        (0, 0)         |   |      (0, 1)       |   |   |
     +---+---+                       +---+                   +---+---+
     |   |   |                       |   |                   |   |   |
    0.5--+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
     |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
     +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
     |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
     +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
     |   |   |        (1, 0)         |   |      (1, 1)       |   |   |
     +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
     |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
     +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
    0                               x=0.5                           x=1

    Each grid division along the x- (resp. y-) direction thus
    corresponds to 0.0625 (resp. 0.125).

    >>> import contextlib
    >>>
    >>> import matplotlib.pyplot as plt
    >>> import numpy as np
    >>> from matplotlib.transforms import Bbox
    >>>
    >>> with (plt.ioff() or contextlib.nullcontext()):
    ...     fig = plt.figure()
    ...     gridspec = fig.add_gridspec(
    ...         2, 2,
    ...         left=.125, right=.875, bottom=.125, top=.875,
    ...         wspace=2/11,
    ...         hspace=1,
    ...         width_ratios=[6, 5],
    ...         height_ratios=[3, 1],
    ...     )
    ...     top = gridspec[0, :].get_position(fig)
    ...     bottom_right = gridspec[3].get_position(fig)
    ...     assert np.allclose([.125, .5, .875, .875], top.extents)
    ...     assert np.allclose(
    ...         [.5625, .125, .875, .25], bottom_right.extents,
    ...     )
    ...     # This box encloses gridspec[0, :], so it will be clipped
    ...     # thereto
    ...     bbox = Bbox([[.1, .4], [.9, .9]])
    ...     clipped = _clip_bbox_to_gridspec(bbox, gridspec)
    ...     assert np.allclose(top.extents, clipped.extents)
    ...     # This box is a bit too small for gridspec[1, 1]
    ...     bbox_small = Bbox([[.625, .125], [.9, .3]])
    ...     assert _clip_bbox_to_gridspec(bbox_small, gridspec) is None
    ...     # Increase tolerance in the x-direction to make it fit
    ...     clipped_small = _clip_bbox_to_gridspec(
    ...         bbox_small, gridspec, (.1, 0.),
    ...     )
    ...     assert np.allclose(
    ...         bottom_right.extents, clipped_small.extents,
    ...     )
    ...     plt.close(fig)
    """
    # Gather all the parameters
    bottoms, tops, lefts, rights = gridspec.get_grid_positions(gridspec.figure)
    box_left, box_bottom, box_right, box_top = _utils.to_extents(bbox)
    try:
        tol_x, tol_y = tolerance
    except TypeError:
        tol_x = tol_y = tolerance
    # Calculate the matching bbox, if there is one
    bottoms = bottoms[box_bottom - bottoms <= tol_y]
    tops = tops[tops - box_top <= tol_y]
    lefts = lefts[box_left - lefts <= tol_x]
    rights = rights[rights - box_right <= tol_x]
    if bottoms.size and tops.size and lefts.size and rights.size:
        return Bbox([[lefts.min(), bottoms.min()], [rights.max(), tops.max()]])
    return None


def _resolve_snapping(
    interval: Interval,
    snap_to: Interval,
    *,
    snap: bool = True,
    tolerance: numbers.Real = 0.,
) -> typing.Tuple[Interval, LegendLocation, bool]:
    """
    Implements the snapping strategy in `resolve_kwargs_from_bbox()`.

    Parameters
    ----------
    interval, snap_to
        SORTED real intervals
    snap
        Whether to snap to the edge in this direction
    tolerance
        Real tolerance for considering contacts/overhangs

    Return
    ------
    Tuple `*resolved_interval, loc, is_fit`, where
    - `resolved_interval` is two real numbers for a `Bbox`'s
      `.interval{x,y}`;
    - `loc` is 0 (align to lower limit), 1 (centered), or 2 (align to
      upper limit);
    - `is_fit` is `True` if `interval` overlaps `snap_to` within
      `tolerance`, and `False` if they are disjoint.

    Example
    -------
    >>> _resolve_snapping((.25, .75), (0, 1))
    ((0.25, 0.75), 1, True)
    >>> _resolve_snapping(  # Touches LHS -> snapped to LHS (0)
    ...     (0, .5), (0, 1),
    ... )
    ((0, 0.5), 0, True)
    >>> _resolve_snapping(  # No snapping -> centered (1)
    ...     (0, .5), (0, 1), snap=False,
    ... )
    ((0, 0.5), 1, True)
    >>> _resolve_snapping(  # Touches both sides -> centered
    ...     (0, 1), (0, 1),
    ... )
    ((0, 1), 1, True)
    >>> _resolve_snapping(  # Misalignment -> no snapping, centered
    ...     (.25, 1.0625), (0, 1),
    ... )
    ((0.25, 1.0625), 1, True)
    >>> _resolve_snapping(
    ...     # Alignment to RHS within tolerance -> snapped to RHS (2)
    ...     (.25, 1.0625), (0, 1), tolerance=.1,
    ... )
    ((0.25, 1.0625), 2, True)
    >>> _resolve_snapping(  # Disjoint -> centered
    ...     (1.25, 2), (0, 1),
    ... )
    ((1.25, 2), 1, False)
    """
    if not snap:
        return interval, 1, True,  # No snapping, align to center
    if not _interval_intersects(interval, snap_to, tolerance):
        return interval, 1, False,  # Disjoint, report failure
    # Get alignment
    alignments = _interval_is_aligned(interval, snap_to, tolerance)
    loc = {(True, False): 0, (False, True): 2}.get(alignments, 1)
    return interval, loc, True,


def _resolve_clipping(
    interval: Interval,
    clip_to: Interval,
    clip: bool,
    tolerance: numbers.Real,
) -> typing.Union[Interval, None]:
    """
    Carry out directional snapping.

    Parameters
    ----------
    interval, clip_to
        SORTED real intervals
    clip
        Whether to clip in this direction
    tolerance
        Real tolerance before overhangs are clipped

    Return
    ------
    If `interval` overlaps `clip_to`
        Clipped interval
    Else
        None

    Example
    -------
    >>> _resolve_clipping((.25, .75), (0, 1), clip=True, tolerance=0)
    (0.25, 0.75)
    >>> _resolve_clipping(
    ...     # No clip -> no overhang detection
    ...     (.25, 1.0625), (0, 1), clip=False, tolerance=0,
    ... )
    (0.25, 1.0625)
    >>> _resolve_clipping(  # Hangs over RHS -> clipped to RHS
    ...     (.25, 1.0625), (0, 1), clip=True, tolerance=0,
    ... )
    (0.25, 1)
    >>> _resolve_clipping(
    ...     # Hangs over RHS but within tolerance -> not clipped
    ...     (.25, 1.0625), (0, 1), clip=True, tolerance=.1,
    ... )
    (0.25, 1.0625)
    >>> print(_resolve_clipping(  # Disjoint -> None
    ...     (-2, -.0625), (0, 1), clip=True, tolerance=0,
    ... ))
    None
    >>> _resolve_clipping(
    ...     # Disjoint but within tol. -> hairwidth interval < tol.
    ...     (-2, -.0625), (0, 1), clip=True, tolerance=.1,
    ... )
    (-0.0625, 0)
    """
    if not clip:
        return interval  # No clipping, align to center
    distances_from_edge = (
        clip_to[0] - interval[0],
        interval[1] - clip_to[1],
    )
    overhangs = tuple(tolerance < dist for dist in distances_from_edge)
    disjoints = (
        clip_to[0] - interval[1] > tolerance,
        interval[0] - clip_to[1] > tolerance,
    )
    if any(disjoints):
        return None
    resolved_lo, resolved_hi = (
        clipped if is_overhanging else orig
        for is_overhanging, orig, clipped in zip(overhangs, interval, clip_to)
    )
    if resolved_lo > resolved_hi:
        # Possible when we have a positive tolerance
        assert resolved_lo - resolved_hi < tolerance
        resolved_lo, resolved_hi = resolved_hi, resolved_lo
    return resolved_lo, resolved_hi


def _interval_contains(
    host: Interval,
    other: typing.Union[Interval, NonnegNum],
    tolerance: numbers.Real = 0.,
) -> bool:
    """
    Returns whether

    >>> min(host) <= other <= max(host)  # noqa # doctest: +SKIP

    or

    >>> (max(host) - min(host)) >= other  # noqa # doctest: +SKIP

    up to `tolerance`.

    Example
    -------
    >>> host = -1, 1
    >>> [
    ...     _interval_contains(host, interval)
    ...     for interval in [
    ...         (0, 1), (-.5, .5), (0, 1.1), (-2, .5), (1, 3),
    ...     ]
    ... ]
    [True, True, False, False, False]
    >>> interval = -1, 1 + 1E-6
    >>> _interval_contains(host, interval)
    False
    >>> _interval_contains(host, interval, tolerance=1E-5)
    True
    >>> [_interval_contains(host, length) for length in (1, 3, 5)]
    [True, False, False]
    """
    hmin, hmax = sorted(host)
    try:
        omin, omax = sorted(other)
    except TypeError:  # Length
        return (hmax - hmin) - other >= 2*tolerance
    return omax - hmax <= tolerance and hmin - omin <= tolerance


def _interval_intersects(
    interval: Interval, other: Interval, tolerance: numbers.Real = 0.,
) -> bool:
    """
    Returns whether

    >>> (  # noqa # doctest: +SKIP
    ...     min(interval) <= max(other) and max(interval) >= min(other)
    ... )

    up to `tolerance`.

    Examples
    --------
    >>> unit = 0, 1
    >>> [
    ...     _interval_intersects(unit, interval)
    ...     for interval in [
    ...         (-.5, .5), (.1, .2), (.5, 2), (1, 2), (1.5, 2),
    ...     ]
    ... ]
    [True, True, True, True, False]
    >>> _interval_intersects(unit, (1.1, 2))
    False
    >>> _interval_intersects(unit, (1.1, 2), tolerance=.2)
    True
    """
    imin, imax = sorted(interval)
    omin, omax = sorted(other)
    return imin - omax <= tolerance and omin - imax <= tolerance


def _interval_is_aligned(
    interval: Interval, other: Interval, tolerance: numbers.Real = 0.,
) -> typing.Tuple[bool, bool]:
    """
    Returns whether

    >>> (  # noqa # doctest: +SKIP
    ...     min(interval) == min(other),
    ...     max(interval) == max(other),
    ... )

    up to `tolerance`.

    Examples
    --------
    >>> unit = 0, 1
    >>> [ # doctest: +NORMALIZE_WHITESPACE
    ...     _interval_is_aligned(unit, interval)
    ...     for interval in [
    ...         (-2, -1), (0, .2), (-.5, .5),
    ...         (.5, 1.5), (.5, 1), (-3, 1), (1, 2),
    ...     ]
    ... ]
    [(False, False), (True, False), (False, False),
     (False, False), (False, True), (False, True), (False, False)]
    >>> _interval_is_aligned(unit, (.0625, 2))
    (False, False)
    >>> _interval_is_aligned(unit, (.0625, 2), tolerance=.1)
    (True, False)
    """
    return tuple(
        abs(ilim - olim) <= tolerance
        for ilim, olim in zip(sorted(interval), sorted(other))
    )


def _make_interval_getter(
    axis: Literal['x', 'y'],
) -> typing.Callable[[Bbox], Interval]:
    def get_interval(bbox: Bbox) -> Interval:
        interval = getattr(bbox, 'interval' + axis)
        interval.sort()
        return tuple(interval)

    return get_interval


def _make_interval_setter(
    axis: Literal['x', 'y'],
) -> typing.Callable[[Bbox, Interval], None]:
    def set_interval(bbox: Bbox, interval: Interval) -> None:
        setattr(bbox, 'interval' + axis, interval)

    return set_interval


def _anchor_interval(
    interval_or_length: typing.Union[Interval, NonnegNum],
    host: Interval,
    loc: LegendLocation,
) -> Interval:
    """
    Example
    -------
    >>> host = -1, 1
    >>> interval_1, interval_2 = (-.75, .25), (4, 5)
    >>> length = 1.
    >>> left, center, right = (-1, 0), (-.5, .5), (0, 1)
    >>> for int_or_len in interval_1, interval_2, length:
    ...     assert [left, center, right] == [
    ...         _anchor_interval((-.75, .25), host, loc)
    ...         for loc in range(3)  # 0 -> left, 1 -> center, ...
    ...     ]
    """
    try:
        lo, hi = interval_or_length
    except TypeError:  # Length
        length = interval_or_length
    else:
        length = abs(hi - lo)
    left, right = host
    if loc == 0:  # 0 -> left
        return left, left + length
    elif loc == 1:  # 1 -> center
        lr = left + right
        return .5 * (lr - length), .5 * (lr + length)
    elif loc == 2:  # 2 -> right
        return right - length, right
    assert False


def _resolve_axis_specs(spec: AxisSpecifications) -> ResolvedAxisSpecs:
    return {True: 'xy', False: ''}.get(spec, spec)


def _get_bbox_in_fig_fracs(
    artist: Artist, fig: FigureBase, tight: bool = False,
) -> Bbox:
    """
    Get the bounding box of `artist` in (sub)figure fractions;
    `fig.canvas.get_renderer()` is used to force renderer
    allocation.

    Parameters
    ----------
    artist
        Artist to be drawn
    fig
        (Sub-)Figure whose renderer is used to draw `artist`
    tight
        Whether to use `artist.get_tightbbox()` (if true) or
        `artist.get_window_extent()` (if false)

    Example
    -------
    >>> from contextlib import nullcontext
    >>>
    >>> import matplotlib.pyplot as plt
    >>> from numpy import allclose
    >>>
    >>> left, bottom, right, top = .1, .1, .7, .8
    >>> with (plt.ioff() or nullcontext()):
    ...     fig = plt.figure(figsize=(10, 6))
    ...     ax = fig.subplots(
    ...         1, 1,
    ...         gridspec_kw=dict(
    ...             left=left, right=right, bottom=bottom, top=top,
    ...         ),
    ...         squeeze=True,
    ...     )
    ...     ax.tick_params(
    ...         direction='out',
    ...         labelbottom=True,
    ...         labelleft=True,
    ...         bottom=True,
    ...         left=True,
    ...     )
    ...     bbox = _get_bbox_in_fig_fracs(ax, fig)
    ...     tight_bbox = _get_bbox_in_fig_fracs(  # Includes ticks etc.
    ...         ax, fig, tight=True,
    ...     )
    ...     plt.close(fig)
    ...     assert allclose(bbox.extents, (left, bottom, right, top))
    ...     assert tight_bbox.xmin < bbox.xmin
    ...     assert tight_bbox.ymin < bbox.ymin
    ...     assert tight_bbox.xmax >= bbox.xmax
    ...     assert tight_bbox.ymax >= bbox.ymax
    """
    if not hasattr(fig.canvas, 'renderer'):
        fig.canvas.get_renderer()
    fig_transform = getattr(fig, 'transSubfigure', fig.transFigure)
    box_getter = artist.get_tightbbox if tight else artist.get_window_extent
    return (
        box_getter(fig.canvas.renderer).transformed(fig_transform.inverted())
    )


@tjekmate.type_checked(
    checkers=dict(
        FIT_STRAT_DEFAULT_TYPE_CHECKERS,
        fig=tjekmate.is_instance(FigureBase),
        legend=(
            tjekmate.identical_to(None)
            | tjekmate.is_sequence(
                length=2, item_checker=_utils.get_nonneg_quantity,
            )
            | tjekmate.is_instance(Artist)
        ),
        bounds=(
            tjekmate.identical_to(None)
            | _utils.is_extents_or_bbox
            | tjekmate.is_instance(GridSpecBase)
        ),
        tight=tjekmate.is_strict_boolean,
    ),
    annotations=FIT_STRAT_DEFAULT_ANNOTATIONS,
)
def avoid_figure_elements(
    fig: FigureBase,
    others: typing.Optional[typing.Iterable[Artist]] = None,
    *,
    legend: typing.Optional[
        typing.Union[Artist, typing.Tuple[NonnegQuantity, NonnegQuantity]]
    ] = None,
    bounds: typing.Optional[
        typing.Union[ExtentsOrBbox, GridSpecBase]
    ] = None,
    tight: bool = False,
    min_height: NonnegQuantity = '20%',
    min_width: NonnegQuantity = '20%',
    min_area: NonnegQuantity = '10%',
    tolerance=0.,
    warn=None,
    # Arguments for the strategies
    clip_bbox=None,
    snap_bbox=None,
    **kwargs
) -> typing.Tuple[bool, Keywords]:
    """
    Get the "best" placement of a legend by avoiding other figure
    elements.

    Parameters
    ----------
    fig
        (Sub)Figure where the legend is to be drawn on
    others
        Optional artists (e.g. `Axes` objects) which are to be avoided
        in placing the legend;
        if not provided, all visible direct children of `fig` except for
        its `.patch` are used
    legend
        Optional dimensions `width, height` as a stand-in for the
        legend, to help with figuring out the legend placement (see
        Strategies);
        each dimension can be given as:
            Simple nonnegative numbers
                Taken to be in fractions of the (sub)figure dimensions
            String percentages (e.g. '20%')
                Taken to be relative to the smallest of `others`
            None
                = 0
            String dimensions (e.g. '200 px', '0.2 in', '1 cm', '6 pt')
                Taken to be absolute
        alternatively, an `Artist` object can also be provided, and its
        dimensions will be used
    bounds
        Optional `matplotlib.transform.Bbox` or extents
        `left, bottom, right, top` in (sub)figure-fraction coordinates
        within which the legend is to be placed;
        default: `Bbox([[0., 0.], [1., 1.]])` (i.e. the entire extent of
        `fig`);
        alternatively, supply a `matplotlib.gridspec.GridSpec[Base]` to
        limit the legend box to coincide with the dimensions of
        some subplot spec on the grid
    tight
        Whether to convert artists in `others` and `legend` to bboxes
        with `.get_tightbbox()` (true) or `.get_window_extent()` (false)
    min_height, min_width, min_area
        Minimal measurements for the calculation of locally-maximal
        bounding boxes (see
        `<MODULE>.legend_placement.find_locally_maximal_bboxes()`);
        only candidates big enough are further considered, and only
        artists in `other` whose boxes are big enough are avoided;
        can be given as:
            Simple nonnegative numbers
                Taken to be in fractions of the (sub)figure dimensions
            String percentages (e.g. '20%')
                Taken to be relative to the smallest of `others`
            None
                = 0
            String dimensions (e.g. '200 px', '0.2 in', '1 cm', '6 pt')
                Taken to be absolute
    clip_bbox, snap_bbox
        Optional bboxes for clipping/snapping, passed to the
        `resolve_kwargs_from_{...}()` functions (see Strategies);
        if neither is provided, both default to `bounds`;
        if either is provided, the missing one defaults to the provided
    warn
        True, None
            Issue warnings for bad fits
        False
            Don't issue warnings for bad fits
        Keyword mapping
            Issue warnings for bad fits with these keyword arguments for
            `BboxFitWarning.warn()`
        Also passed to the `resolve_kwargs_from_{...}()` functions
    **kwargs
        Passed to the `resolve_kwargs_from_{...}()` functions;
        see their documentations for details

    Return
    ------
    Tuple `(fitting_bbox_is_found, resolved_legend_kwargs)`, where
    - fitting_bbox_is_found:
      Whether a fitting bbox is found
    - resolved_legend_kwargs:
      Keyword arguments to be passed to `fig.legend()`

    Side effects
    ------------
    `BboxFitWarning` issued if:

    - The chosen strategy (see Strategies) fails to generate any
      candidates.

    - The fit fails on the candidate chosen by the strategy.

    - The strategy otherwise issues a warning.

    See also
    --------
    `<MODULE>.legend_placement.find_locally_maximal_bboxes()`
    `<MODULE>.legend_placement.resolve_kwargs_from_bbox()`
    `<MODULE>.legend_placement.resolve_kwargs_from_bbox_and_dims()`

    Notes
    -----
    For `min_area`, it is understood that '50 in' means 50 square
    inches, etc.

    Strategies
    ----------
    1.  Use `find_locally_maximal_bboxes()` to propose candidate bboxes
        where the legend should preliminarily be placed;
        if no bboxes are found, issue a `BboxFitWarning` and declate
        failure.

    2.1. If `legend` is provided:
         The candidate bbox which has the greatest areal overlap with
         `legend` is chosen, with the bbox area being the tiebreaker.
         Keyword arguments are then generated by

         >>> resolve_kwargs_from_bbox_and_dims(  # noqa # doctest: +SKIP
         ...    <candidate bbox>, <width of legend>, <height of legend>,
         ...    ...,
         ... )

    2.2. Else:
         The candidate bbox which has the greatest area is chosen.
         Keyword arguments are then generated by

         >>> resolve_kwargs_from_bbox(  # noqa # doctest: +SKIP
         ...     <candidate bbox>, ...,
         ... )

    Example
    -------
    >>> import warnings
    >>> from contextlib import nullcontext
    >>>
    >>> import matplotlib.pyplot as plt
    >>> from numpy import allclose
    >>>
    >>> with (plt.ioff() or nullcontext()):
    ...     fig = plt.figure(figsize=(10, 6))
    ...     gridspec = fig.add_gridspec(
    ...         2, 2,
    ...         left=.05, top=11/12, bottom=1/12,  # .5 in
    ...         right=.8,  # 2 in
    ...         hspace=0, wspace=0,  # No space between subplots
    ...     )
    ...     for i in range(3):  # Leave bottom-right corner vacant
    ...         ax = fig.add_subplot(gridspec[i])
    ...         ax.set_axis_off()
    ...
    ...     # Automatic avoidance of figure children
    ...     bbox_found, kwargs = avoid_figure_elements(fig)
    ...     assert bbox_found
    ...     assert kwargs['loc'] == 'lower right'
    ...     assert allclose(
    ...         kwargs['bbox_to_anchor'].extents,
    ...         [.425, 0, 1, .5],  # <- lower right corner of figure
    ...     )
    ...
    ...     # Restrict search area of optimal bbox
    ...     bbox_found, kwargs = avoid_figure_elements(
    ...         fig,
    ...         bounds=(.05, 1/12, .8, 11/12),  # = gridspec[:]
    ...         # Only align if bbox is tangent to LHS/RHS of `bounds`
    ...         snap='x',
    ...     )
    ...     assert bbox_found
    ...     assert kwargs['loc'] == 'center right'
    ...     assert allclose(
    ...         kwargs['bbox_to_anchor'].extents,
    ...         [.425, 1/12, .8, .5],  # <- LR corner of gridspec
    ...     )
    ...
    ...     # Search for boxes which line up with the gridspec
    ...     bbox_found, kwargs = avoid_figure_elements(
    ...         fig, bounds=gridspec,
    ...     )
    ...     assert bbox_found
    ...     assert allclose(
    ...         kwargs['bbox_to_anchor'].extents,
    ...         gridspec[-1].get_position(fig).extents,
    ...     )
    ...
    ...     # Search for a specific geometry
    ...     bbox_found, kwargs = avoid_figure_elements(
    ...         fig,
    ...         legend=('1.5 in', '4 in'),
    ...         snap=False,
    ...         tolerance=1E-6,
    ...     )
    ...     assert bbox_found
    ...     assert allclose(
    ...         kwargs['bbox_to_anchor'].extents,
    ...         [.825, 1/6, .975, 5/6],  # <- Empty space to the right
    ...     )
    ...
    ...     try:
    ...         with warnings.catch_warnings():
    ...             warnings.simplefilter('error', BboxFitWarning)
    ...             avoid_figure_elements(
    ...                 # doctest: +IGNORE_EXCEPTION_DETAIL
    ...                 fig,
    ...                 legend=('3 in', '4 in'),  # Too big
    ...             )
    ...     finally:
    ...         plt.close(fig)
    Traceback (most recent call last):
      ...
    BboxFitWarning: ... legend is too big for the candidate bbox
    """
    def get_box_area(bbox: Bbox) -> NonnegNum:
        """
        Get the unsigned area of `bbox`.
        """
        return abs(bbox.width * bbox.height)

    def get_contained_legend_area(
        bbox: Bbox, legend_width: NonnegNum, legend_height: NonnegNum,
    ) -> NonnegNum:
        """
        Get the area of the overlap between a movable legend of
        dimensions `legend_width, legend_height` and `bbox`.
        """
        width, height = abs(bbox.width), abs(bbox.height)
        return min(legend_width, width) * min(legend_height, height)

    def format_arg_list(arg: Keyword, *args: Keyword) -> str:
        """
        Autoformatting of parameter values for error/warning messages.
        """
        return _utils.format_arg_list(
            [arg, *args],
            values=arg_values,
            original_values=arg_origins,
            sources=arg_sources,
            aliases=arg_aliases,
        )

    def add_arg_sources(
        arg: Keyword, source: Keyword, *sources: Keyword,
    ) -> None:
        """
        Mark `arg` as being resolved/derived from `sources`.
        """
        source_arg_names = arg_sources.setdefault(arg, [])
        source_arg_names.extend([source, *sources])

    get_bbox_in_fig_fracs = functools.partial(
        _get_bbox_in_fig_fracs, fig=fig, tight=tight,
    )
    candidate_sorter: typing.Union[
        typing.Callable[[Bbox], numbers.Real],
        typing.Callable[[Bbox], typing.Tuple[numbers.Real, ...]],
    ]
    apply_strategy: typing.Callable[[Bbox], typing.Tuple[bool, Keywords]]

    # Type checks and other preparations
    if others is None:
        fig_patch = getattr(fig, 'patch', None)
        others = [
            child for child in fig.get_children()
            if child.get_visible() and child is not fig_patch
        ]
    else:
        others = list(others)
    if not all(isinstance(artist, Artist) for artist in others):
        raise TypeError(
            f'others = {others!r}: '
            'expected `matplotlib.artist.Artist` objects or `None`'
        )
    if warn in (None, True):
        warn = {}

    # Helper for formatting error/warning messages
    arg_aliases: typing.Dict[Keyword, str] = dict(
        bbox_to_anchor='<candidate bbox>',
        candidate_sorter='<bbox selector>',
        apply_strategy='<kwargs resolution strategy>',
    )
    arg_values: typing.Dict[Keyword, typing.Any] = dict(
        fig=fig,
        bounds=bounds,
        tolerance=tolerance,
    )
    arg_origins: typing.Dict[Keyword, typing.Any] = dict(
        others=others,
        legend=legend,
        min_width=min_width,
        min_height=min_height,
        min_area=min_area,
        tolerance=tolerance,
    )
    arg_sources: typing.Dict[Keyword, typing.List[Keyword]] = {}

    # Prepare the box-finding and box-fitting strategies
    bounds_are_gridspec = isinstance(bounds, GridSpecBase)
    others_bboxes = [get_bbox_in_fig_fracs(artist) for artist in others]
    arg_values.update(others=others_bboxes)
    resolve_dimensions = functools.partial(
        _resolve_flmb_kwargs, fig, others_bboxes,
    )
    if legend is None:
        # Since we don't know any better, just use the biggest empty
        # space
        candidate_sorter = get_box_area
        strategy = resolve_kwargs_from_bbox
        use_bounds = snap_bbox is None and not bounds_are_gridspec
        strategy_kwargs = dict(
            kwargs,
            snap_bbox=(bounds if use_bounds else snap_bbox),
            tolerance=tolerance,
            warn=warn,
        )
        # Bookkeeping
        arg_values.update(legend=legend)
        if use_bounds:
            add_arg_sources('apply_strategy', 'bounds')
    else:  # legend is a dummy artist
        # Check whether the dimensions are actually right for the
        # provided legend
        if isinstance(legend, Artist):
            legend_tight_bbox = get_bbox_in_fig_fracs(legend)
            lw = abs(legend_tight_bbox.width)
            lh = abs(legend_tight_bbox.height)
            add_arg_sources('legend', 'fig')
        else:
            lw, lh = legend
            if not all(
                tjekmate.is_nonnegative_real(ldim) for ldim in legend
            ):
                # One or more legend dimensions need conversion
                legend_dims = resolve_dimensions(
                    min_height=lh, min_width=lw, min_area=None,
                )
                lw, lh = legend_dims['min_width'], legend_dims['min_height']
                add_arg_sources('legend', 'fig', 'others')

        get_contained_area = functools.partial(
            get_contained_legend_area,
            legend_width=lw, legend_height=lh,
        )
        strategy = resolve_kwargs_from_bbox_and_dims
        strategy_kwargs = dict(
            kwargs,
            legend_width=lw,
            legend_height=lh,
            clip_bbox=clip_bbox,
            snap_bbox=snap_bbox,
            tolerance=tolerance,
            warn=warn,
        )
        use_bounds = clip_bbox is snap_bbox is None and not bounds_are_gridspec
        if use_bounds:
            # Note: only clobber default bboxes if neither is given;
            # if either is given, resolve_kwargs_from_bbox_and_dims()
            # takes the value of the one and uses it for the other
            strategy_kwargs.update(
                clip_bbox=bounds,
                snap_bbox=bounds,
            )
        candidate_sorter: _utils.CompositeSorter[
            Bbox, numbers.Real
        ] = _utils.CompositeSorter(get_contained_area, get_box_area)
        # Bookkeeping
        arg_values.update(legend=(lw, lh))
        add_arg_sources('candidate_sorter', 'legend')
        if use_bounds:
            add_arg_sources('apply_strategy', 'bounds')
    for key in 'clip_bbox', 'snap_bbox':
        if strategy_kwargs.get(key) is None:
            strategy_kwargs.pop(key, None)
    apply_strategy = functools.partial(strategy, **strategy_kwargs)
    # Bookkeeping
    for key, value in kwargs.items():
        arg_origins.setdefault(key, value)
    for key, value in strategy_kwargs.items():
        arg_values.setdefault(key, value)
    arg_values.update(
        candidate_sorter=candidate_sorter,
        apply_strategy=apply_strategy,
    )
    if legend is None:
        add_arg_sources('apply_strategy', *strategy_kwargs)
    else:
        add_arg_sources(
            'apply_strategy',
            'legend',
            *(
                arg_name for arg_name in strategy_kwargs
                if arg_name not in ('legend_width', 'legend_height')
            ),
        )

    # Set the stage
    dimensions_requirements = resolve_dimensions(
        min_height=min_height, min_width=min_width, min_area=min_area,
    )
    arg_values.update(dimensions_requirements)
    add_arg_sources('others', 'fig')
    for arg_name in dimensions_requirements:
        add_arg_sources(arg_name, 'fig', 'others')

    # Get the best box to anchor to
    candidates = find_locally_maximal_bboxes(
        (Bbox.unit() if bounds is None or bounds_are_gridspec else bounds),
        *others_bboxes,
        **dimensions_requirements,
    )
    if bounds_are_gridspec:
        candidates = [
            _clip_bbox_to_gridspec(bbox, bounds, tolerance=tolerance)
            for bbox in candidates
        ]
        candidates = [bbox for bbox in candidates if bbox is not None]
    if not candidates:  # No space
        if isinstance(warn, typing.Mapping):
            culprits = [
                'bounds', 'others', *dimensions_requirements,
            ]
            if bounds_are_gridspec:
                culprits.append('tolerance')
            msg = '{}: no candidates'.format(format_arg_list(*culprits))
            BboxFitWarning.warn(msg, **warn)
        return False, {}
    bbox_to_anchor = max(candidates, key=candidate_sorter)
    add_arg_sources(
        'bbox_to_anchor', 'bounds', 'others', *dimensions_requirements,
    )

    # Align with the box
    arg_values.update(bbox_to_anchor=bbox_to_anchor)
    fit_successful, legend_kwargs = apply_strategy(bbox_to_anchor)
    if not fit_successful and isinstance(warn, typing.Mapping):
        msg = 'fit failed following these strategies: {}'.format(
            format_arg_list(
                'bbox_to_anchor', 'candidate_sorter', 'apply_strategy',
            ),
        )
        BboxFitWarning.warn(msg, **warn)
    return fit_successful, legend_kwargs


for func in __all__:
    try:
        func = locals()[func]
        func.__doc__ = func.__doc__.replace(
            '<MODULE>',
            '.'.join(func.__module__.split('.')[:-2]),
        )
    except Exception:
        pass
