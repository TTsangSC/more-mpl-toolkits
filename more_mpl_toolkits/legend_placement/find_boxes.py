"""
Enumeration of boxes
"""
import functools
import itertools
import numbers
import typing

import numpy as np

import tjekmate

from . import _utils
from ._types import (
    Literal,
    Interval,
    Extents, ExtentsOrBbox, Bbox,
    NonnegNum,
)

__all__ = (
    'find_locally_maximal_bboxes',
)


def _get_bbox_overlap(
    bbox1: Extents,
    bbox2: Extents,
) -> typing.Union[Extents, None]:
    """
    Get the overlap (if any) between the two boxes.
    """
    l1, b1, r1, t1 = bbox1
    l2, b2, r2, t2 = bbox2
    bounds: typing.List[
        typing.Tuple[numbers.Real, numbers.Real]  # x, y
    ] = []
    for index_offset in 0, 1:
        min1, max1 = bbox1[index_offset::2]
        min2, max2 = bbox2[index_offset::2]
        overlap_min = max(min1, min2)
        overlap_max = min(max1, max2)
        if overlap_min >= overlap_max:  # Disjoint
            return None
        bounds.append((overlap_min, overlap_max))
    return bounds[0][0], bounds[1][0], bounds[0][1], bounds[1][1]


def _bbox_is_covered(bbox: Extents, other: Extents) -> bool:
    """
    Check whether `bbox` is entirely eclipsed by `other`.
    """
    bleft, bbottom, bright, btop = bbox
    oleft, obottom, oright, otop = other
    return (
        bleft >= oleft and bright <= oright and
        bbottom >= obottom and btop <= otop
    )


def _clip_ranges(
    clipped: Interval,
    clipping: Interval,
    check_order: bool = False,
) -> typing.List[Interval]:
    """
    Example
    -------
    >>> r1, r2, r3 = (1, 4), (2, 5), (3, 4)
    >>> _clip_ranges(r1, r2)
    [(1, 2)]
    >>> _clip_ranges(r2, r1)
    [(4, 5)]
    >>> _clip_ranges(r1, r3)
    [(1, 3)]
    >>> _clip_ranges(r2, r3)
    [(2, 3), (4, 5)]
    >>> _clip_ranges(r3, r1)
    []
    """
    results: typing.List[Interval] = []
    parent_lo, parent_hi = clipped
    sub_lo, sub_hi = clipping
    # Make sure that the ranges are well-ordered
    if check_order:
        if parent_lo > parent_hi:
            parent_lo, parent_hi = parent_hi, parent_lo
        if sub_lo > sub_hi:
            sub_lo, sub_hi = sub_hi, sub_lo
    if sub_lo > parent_lo:
        results.append((parent_lo, sub_lo))
    if sub_hi < parent_hi:
        results.append((sub_hi, parent_hi))
    return results


def _clip_two_bboxes(
    bbox: Extents,
    clip: Extents,
    *,
    overlap_checker: typing.Callable[[Extents], bool] = lambda e: True,
    check_overlap_size: bool = True,
) -> typing.Tuple[
    bool, typing.Tuple[typing.List[Extents], typing.List[Extents]],
]:
    r"""
    Clip into `bbox` with `clip` and get the resultant locally-maximal
    bboxes.

    Parameters
    ----------
    bbox, clip
        Extents
    overlap_checker()
        Function taking the overlap between `bbox` and `clip` and
        returning whether it should be considered
    check_overlap_size
        True
            Use `overlap_checker(_get_bbox_overlap(...))` to verify if
            two boxes have substantial overlap (so that `bbox` should be
            clipped)
        False
            Quick-compare the two sets of extents without explicitly
            calculating their overlaps and consider any non-zero-area
            intersection to be an overlap

    Return
    ------
    Tuple

    >>> tuple[  # noqa # doctest: +SKIP
    ...     bbox_was_clipped,
    ...     tuple[list[bbox_to_left_right], list[bbox_above_below], ]
    ... ]

    Notes
    -----
    Each set of extents in the first list overlaps all sets of extents
    in the second list but none other in the first, and vice-versa.

    Example
    -------
    >>> box = (0, 0, 3, 3)
    >>> for other_box in [
    ...     (3, 0, 4, 1),  # Doesn't intersect box
    ...     (0, 0, 4, 4),  # Eclipses box
    ...     (-1, -1, 1, 4),  # Clips left-hand side of box entirely
    ...     (1, 0, 4, 2),  # Clips lower right corner of box
    ...     (1, 0, 2, 3),  # Bifurcates box
    ...     (1, 0, 2, 2),  # Clips bottom of box
    ...     (1, 1, 2, 2),  # Clips middle of box
    ... ]:
    ...     is_clipped, clipped_groups = _clip_two_bboxes(
    ...         box, other_box,
    ...     )
    ...     if is_clipped:
    ...         result = repr(sorted(
    ...             box for boxes in clipped_groups for box in boxes
    ...         ))
    ...     else:
    ...         result = '(not clipped)'
    ...     print(
    ...         '{!r}:\n-> {}'.format(other_box, result)
    ...     )
    (3, 0, 4, 1):
    -> (not clipped)
    (0, 0, 4, 4):
    -> []
    (-1, -1, 1, 4):
    -> [(1, 0, 3, 3)]
    (1, 0, 4, 2):
    -> [(0, 0, 1, 3), (0, 2, 3, 3)]
    (1, 0, 2, 3):
    -> [(0, 0, 1, 3), (2, 0, 3, 3)]
    (1, 0, 2, 2):
    -> [(0, 0, 1, 3), (0, 2, 3, 3), (2, 0, 3, 3)]
    (1, 1, 2, 2):
    -> [(0, 0, 1, 3), (0, 0, 3, 1), (0, 2, 3, 3), (2, 0, 3, 3)]
    """
    x_ranges: typing.List[Interval]
    y_ranges: typing.List[Interval]
    extents_left_right: typing.List[Extents]
    extents_down_up: typing.List[Extents]
    # Don't clip if the overlap is small/nnoexistent
    if check_overlap_size:
        clip = overlap = _get_bbox_overlap(bbox, clip)
        has_enough_overlap = overlap and overlap_checker(overlap)
    else:
        (lb, bb, rb, tb), (lc, bc, rc, tc) = bbox, clip
        has_enough_overlap = lb < rc and rb > lc and bb < tc and tb > bc
    if not has_enough_overlap:
        return False, ([bbox], [])
    # Enumerate viable new extents along the axes
    x_ranges, y_ranges = (
        _clip_ranges(bbox[offset::2], clip[offset::2]) for offset in (0, 1)
    )
    extents_left_right = [
        (xmin, bbox[1], xmax, bbox[3]) for xmin, xmax in x_ranges
    ]
    extents_down_up = [
        (bbox[0], ymin, bbox[2], ymax) for ymin, ymax in y_ranges
    ]
    return True, (extents_left_right, extents_down_up)


def _get_consecutive_ranges_from_lengths(
    lengths: typing.Iterable[numbers.Integral]
) -> typing.List[range]:
    """
    Example
    -------
    >>> _get_consecutive_ranges_from_lengths([3, 4, 5])
    [range(0, 3), range(3, 7), range(7, 12)]
    """
    ranges: typing.List[range] = []
    offset: numbers.Integral = 0
    for length in lengths:
        cumulative_length = offset + length
        ranges.append(range(offset, cumulative_length))
        offset = cumulative_length
    return ranges


def _get_adjacency_matrix(
    overlaps: typing.Mapping[
        numbers.Integral,
        typing.Collection[numbers.Integral],
    ],
    n: typing.Optional[numbers.Integral] = None,
    *,
    process: typing.Optional[Literal['upper', 'lower', 'symmetric']] = None,
    symmetrize: bool = False,
    check_double: bool = False,
    dtype: typing.Any = bool,
) -> np.ndarray:
    r"""
    Convert an `overlaps` mapping used by `_update_bbox_list()` and
    `_clip_bboxes()` into an adjacency matrix.

    Parameters
    ----------
    overlaps
        Mapping from non-negative indices to a collection of
        non-negative indices which it overlaps (without double-counting
        and self-intersection)
    n
        Optional size of the returned matrix;
        default is one plus the biggest index encountered
    process
        Post-processing of the resultant matrix:
        'symmetric'
            Make it symmetric:
            If A is B's neighbor (`mat[A, B] == True`) then B is also
            A's (`mat[B, A] == True`)
        'upper', 'lower'
            Make it upper- (resp. lower-) triangular:
            if A is B's neighbor then A <= (resp. >=) B
    check_double
        Whether to check `overlaps` for double-counting:

        >>> def oneway_check(i, j):
        ...     i_nbrs, j_nbrs = (
        ...         overlaps.get(index, []) for index in (i, j)
        ...     )
        ...     assert not (j in i_nbrs and i in j_nbrs)
        >>> def unique_check(i):
        ...     neighbors = overlaps.get(i, [])
        ...     assert len(neighbors) == set(len(neighbors))

    dtype
        Data type of the returned matrix (`bool`, `int`, etc.)

    Return
    ------
    Adjacency matrix of shape `n, n`

    Example
    -------
    >>> print_bool_adj_mat = lambda mat: print(  # noqa: E731
    ...     *(' '.join('T' if b else 'F' for b in row) for row in mat),
    ...     sep='\n',
    ... )
    >>> overlaps = {0: {1}, 2: {1}}
    >>> print_bool_adj_mat(_get_adjacency_matrix(overlaps))
    F T F
    F F F
    F T F
    >>> print_bool_adj_mat(_get_adjacency_matrix(overlaps, n=4))
    F T F F
    F F F F
    F T F F
    F F F F
    >>> print_bool_adj_mat(
    ...     _get_adjacency_matrix(overlaps, process='symmetric')
    ... )
    F T F
    T F T
    F T F
    >>> print_bool_adj_mat(
    ...     _get_adjacency_matrix(overlaps, process='upper')
    ... )
    F T F
    F F T
    F F F
    >>> print(_get_adjacency_matrix(overlaps, dtype=int))
    [[0 1 0]
     [0 0 0]
     [0 1 0]]
    >>> _get_adjacency_matrix({**overlaps, 1: {0}}, check_double=True)
    Traceback (most recent call last):
      ...
    ValueError: overlaps = {0: {1}, 2: {1}, 1: {0}}: double-counting
    >>> _get_adjacency_matrix({**overlaps, 0: [1]*2}, check_double=True)
    Traceback (most recent call last):
      ...
    ValueError: overlaps = {0: [1, 1], 2: {1}}: double-counting
    """
    if not overlaps:
        return np.zeros(shape=((0, 0) if not n else (n, n)), dtype=dtype)
    if n is None:
        max_index = max(overlaps)
        for neighbors in overlaps.values():
            if neighbors:
                max_index = max(max_index, *neighbors)
        n = max_index + 1
    assert n > 0
    adj_matrix = np.zeros((n, n), dtype=int)
    for i, neighbors in overlaps.items():
        for j in neighbors:
            adj_matrix[i, j] += 1
    symm_adjacencies = adj_matrix + adj_matrix.T
    if check_double and any(symm_adjacencies.flatten() > 1):
        raise ValueError(f'overlaps = {overlaps!r}: double-counting')
    if process is None:
        result = adj_matrix
    elif process == 'symmetric':
        result = symm_adjacencies
    elif process in ('upper', 'lower'):
        result = symm_adjacencies
        for i in range(n):
            result[i:, i] = 0
        if process == 'lower':
            result = result.T
    else:
        raise TypeError(
            'process = {!r}: allowed values: {!r}'.format(
                process, [None, 'symmetric', 'upper', 'lower']
            )
        )
    return result.astype(dtype)


def _update_bbox_list(
    bboxes: typing.Sequence[Extents],
    overlaps: typing.Mapping[int, typing.List[int]],
    clipper: typing.Callable[
        [Extents],
        typing.Tuple[bool, typing.Collection[typing.Collection[Extents]]]
    ],
    *,
    size_checker: typing.Callable[[Extents], bool] = lambda e: True,
    eclipse_checker: typing.Callable[
        [Extents, Extents], bool
    ] = _bbox_is_covered,
) -> typing.Tuple[typing.List[Extents], typing.Dict[int, typing.List[int]]]:
    """
    Clip into all the `bboxes` with `clipper()` and get the resultant
    locally-maximal bboxes.

    Parameters
    ----------
    bboxes
        Sequence of extent sets
    overlaps
        Mapping from indices of an extent set in `bboxes` to the indices
        of other items which it overlaps (without double-counting)
    clipper()
        Function taking a set of extents to be clipped and returning in
        a 2-tuple:
        (1) whether any clipping occurred, and
        (2) the groups of clipped extents, where each extent set is
            within the bounds of the provided extents;
        any two extents sets within the same group should not overlap
        one another, but extents sets in a group may overlap those in
        another
    size_checker()
        Function taking the bounding boxes, overlaps, etc. and returning
        whether it should be considered, assumed to be based on some
        size criteria
    eclipse_checker()
        Function taking two bounding boxes and returning whether the
        first is to be considered eclipsed by the second

    Return
    ------
    Tuple of updated `(bboxes, overlaps)`

    Example
    -------
    >>> unit_box = (0, 0, 1, 1)
    >>> lower_right_corner_box = (.5, -.5, 1.5, .5)
    >>> clip_with_lrcb = functools.partial(
    ...     _clip_two_bboxes, clip=lower_right_corner_box,
    ... )
    >>> clipped_boxes, overlaps = _update_bbox_list(
    ...     [unit_box], {}, clip_with_lrcb,
    ... )
    >>> sorted(clipped_boxes)
    [(0, 0, 0.5, 1), (0, 0.5, 1, 1)]
    >>> _get_adjacency_matrix(
    ...     overlaps, process='upper', check_double=True,
    ... )
    array([[False,  True],
           [False, False]])
    """
    def has_enough_overlap(b1: Extents, b2: Extents) -> bool:
        overlap = _get_bbox_overlap(b1, b2)
        if overlap is None:
            return False
        return bool(size_checker(overlap))

    def remember_overlap(
        ii: typing.Tuple[int, int], jj: typing.Tuple[int, int],
    ) -> None:
        ii_overlaps.setdefault(ii, set()).add(jj)

    # Create splits for each bbox
    ii_bboxes: typing.List[typing.List[Extents]] = []
    ii_overlaps: typing.Dict[
        typing.Tuple[int, int], typing.Set[typing.Tuple[int, int]]
    ] = {}
    i_clipped: typing.Set[int] = set()
    bboxes = [bbox for bbox in bboxes if size_checker(bbox)]
    for i_old, bbox in enumerate(bboxes):
        # Get groups of bboxes, where it is guaranteed that boxes in a
        # single group does not intersect one onother
        clipped, raw_groups = clipper(bbox)
        # Has any clipping actually occurred?
        if not clipped:
            ii_bboxes.append([bbox])
            continue
        i_clipped.add(i_old)
        big_enough_clipped_groups = (
            [member for member in group if size_checker(member)]
            for group in raw_groups
        )
        bbox_groups = [group for group in big_enough_clipped_groups if group]
        # Store all new bboxes
        new_bboxes = sum(bbox_groups, [])
        index_ranges = _get_consecutive_ranges_from_lengths(
            len(g) for g in bbox_groups
        )
        ii_bboxes.append(new_bboxes)
        # Check for inter-group overlaps
        for i1, i2 in (
            ij for group_indices in itertools.product(*index_ranges)
            for ij in itertools.combinations(group_indices, 2)
        ):
            if not has_enough_overlap(new_bboxes[i1], new_bboxes[i2]):
                continue
            remember_overlap((i_old, i1), (i_old, i2))
    # Check for overlaps among split bboxes originating from
    # originally-overlapping bboxes, and flag bboxes covered by another
    eclipsed_double_indices: typing.Set[typing.Tuple[int, int]] = set()
    for i_old, j_old, splits_i, splits_j in (
        (i, j, si, ii_bboxes[j])
        for i, si in enumerate(ii_bboxes) for j in overlaps.get(i, [])
    ):
        # No need to re-check overlaps if the boxes aren't updated
        if not (i_old in i_clipped or j_old in i_clipped):
            remember_overlap((i_old, 0), (j_old, 0))
            continue
        for (i_new, split_i), (j_new, split_j) in itertools.product(
            enumerate(splits_i), enumerate(splits_j),
        ):
            ii, jj = (i_old, i_new), (j_old, j_new)
            if not has_enough_overlap(split_i, split_j):
                continue
            remember_overlap(ii, jj)
            if eclipse_checker(split_i, split_j):
                eclipsed_double_indices.add(ii)
            elif eclipse_checker(split_j, split_i):
                eclipsed_double_indices.add(jj)
    # Gather all finalized splits and get their indices
    i2ii: typing.List[typing.Tuple[int, int]] = [
        (i_old, i_new)
        for i_old, splits in enumerate(ii_bboxes)
        for i_new in range(len(splits))
        if (i_old, i_new) not in eclipsed_double_indices
    ]
    ii2i: typing.Dict[typing.Tuple[int, int], int] = {
        ii: i for i, ii in enumerate(i2ii)
    }
    # Build returned list of bboxes and overlaps
    new_bboxes: typing.List[Extents] = [
        ii_bboxes[i_old][i_new] for i_old, i_new in i2ii
    ]
    new_overlaps: typing.Dict[int, typing.List[int]] = {}
    for ii, set_jj in ii_overlaps.items():
        if ii in eclipsed_double_indices:
            continue
        finalized_overlaps = sorted(
            ii2i[jj] for jj in set_jj if jj not in eclipsed_double_indices
        )
        if finalized_overlaps:
            new_overlaps[ii2i[ii]] = finalized_overlaps
    return new_bboxes, new_overlaps


def _clip_bboxes(
    bboxes: typing.Sequence[Extents],
    overlaps: typing.Mapping[int, typing.List[int]],
    clipping_bboxes: typing.Sequence[Extents],
    *,
    min_width: NonnegNum = 0,
    min_height: NonnegNum = 0,
    min_area: NonnegNum = 0,
) -> typing.Tuple[
    typing.List[Extents],
    typing.Dict[int, typing.List[int]],
]:
    """
    Clip a list of bounding boxes by another list, so that the new list
    of bboxes consists only of boxes disjoint with the clipping list.

    Parameters
    ----------
    bboxes
        Sequence of bbox extents sets with non-zero areas to be clipped
    overlaps
        Mapping for which of the `bboxes` overlap another, in the form
        of `{index: list[index]}`;
        should have no double-counting, e.g. if `2 in overlaps[4]` then
        `4 not in overlaps.get(2, [])`
    clipping_bboxes
        Sequence of bbox extents sets with non-zero areas, with which to
        clip `bboxes`
    min_width, min_height, min_area
        Minimal dimensions (>= 0) of the (clipping and clipped) boxes to
        be considered;
        increase to filter out boxes that are too small/thin early,
        which helps speeding up the calculation

    Return
    ------
    Updated `(bboxes, overlaps)`
    """
    if min_width or min_height or min_area:
        def get_width(e: Extents) -> NonnegNum:
            return e[2] - e[0]

        def get_height(e: Extents) -> NonnegNum:
            return e[3] - e[1]

        def is_big_enough(bbox: Extents) -> bool:
            """
            Determine whether the extents are big enough
            """
            width, height = get_width(bbox), get_height(bbox)
            area = width * height
            return (
                width >= min_width and height >= min_height and area >= min_area
            )

        def is_covered_by(bbox: Extents, covering: Extents) -> bool:
            """
            Determine whether the portion of `bbox` not overlapping
            `covering` is NOT big enough
            """
            remainders: typing.List[Extents] = sum(
                _clip_two_bboxes(
                    bbox, covering,
                    overlap_checker=is_big_enough,
                    check_overlap_size=False,
                )[1],
                []
            )
            widths = [get_width(bbox) for bbox in remainders]
            heights = [get_height(bbox) for bbox in remainders]
            total_area = sum(w * h for w, h in zip(widths, heights))
            return not (
                any(
                    w >= min_width and h >= min_height
                    for w, h in zip(widths, heights)
                ) and
                total_area >= min_area
            )

    else:
        is_big_enough: typing.Callable[[Extents], bool] = lambda e: True
        is_covered_by = _bbox_is_covered
    for clip in clipping_bboxes:
        if not is_big_enough(clip):
            continue
        results = _bboxes, _overlaps = _update_bbox_list(
            bboxes, overlaps,
            clipper=functools.partial(
                _clip_two_bboxes,
                clip=clip,
                overlap_checker=is_big_enough,
            ),
            size_checker=is_big_enough,
            eclipse_checker=is_covered_by,
        )
        bboxes, overlaps = results
    return bboxes, overlaps


# TODO: This seems to be the problem described in
# doi:10.1007/3-540-53487-3_50;
# perhaps try implementing their algorithm?
@tjekmate.check(
    **dict.fromkeys(
        ('min_width', 'min_height', 'min_area'), tjekmate.is_nonnegative_real,
    ),
    bounds=_utils.is_extents_or_bbox,
    obstacles=tjekmate.is_sequence(item_checker=_utils.is_extents_or_bbox),
)
def find_locally_maximal_bboxes(
    bounds: ExtentsOrBbox,
    *obstacles: typing.Sequence[ExtentsOrBbox],
    min_width: NonnegNum = 0,
    min_height: NonnegNum = 0,
    min_area: NonnegNum = 0,
) -> typing.List[Bbox]:
    r"""
    Find the locally-maximal bounding boxes within the bounds given
    obstacles to avoid.

    Parameters
    ----------
    bounds
        `matplotlib.transforms.Bbox` or extents
        `left, bottom, right, top` representing the search bounds (e.g.
        the entire canvas)
    `obstacles
        `Bbox` objects or sets of extents
    min_width, min_height, min_area
        Minimal dimensions (>= 0) for a box (locally-maximal box,
        obstacle, or the overlap between any thereof) to be considered;
        increase to filter out boxes that are too small/thin early,
        which helps speeding up the calculation

    Return
    ------
    List of `matplotlib.transforms.Bbox` objects of the found
    locally-maximal bounding boxes which don't (significantly) overlap
    with the obstacles

    Example
    -------
    Consider this grid:

    0
    -+---+---+---+---+---+---+---+---+---+---+---+---+ x
     |<<<<<<<|   |   |   |   |   |   |```````````````|
    -+<<<+---+---+---+---+---+---+---+```````````````|
     |<<<|xxx|>>>>>>>|   |   |   |   |```````````````|
    -+<<<|xxx|>>>>>>>+---+---+---+---+```````````````|
     |<<<|xxx|>>>>>>>|   |   |   |   |```````````````|
    -+<<<|xxx|>>>>>>>+---+---+---+---+```````````````|
     |<<<|xxx|>>>>>>>|   |   |   |   |```````````````|
    -+<<<+---+---+---+---+---+---+---+```````````````|
     |<<<<<<<|   |   |   |   |   |   |```````````````|
    -+<<<<<<<+---+---+---+---+---+---+```````````````|
     |<<<<<<<|   |   |   |   |   |   |```````````````|
    -+<<<<<<<+---+---+---+---+---+---+---+```````````|
     |<<<<<<<|   |   |   |...........|;;;|```````````|
    -+<<<<<<<+---+---+---+...........|;;;|```````````|
     |<<<<<<<|   |   |   |...........|;;;|```````````|
    -+<<<<<<<+---+---+---+...........+---+---+---+---+
     |<<<<<<<|   |   |   |...............|   |   |   |
    -+<<<<<<<+---+---+---+...............+---+---+---+
     |<<<<<<<|   |   |   |...............|   |   |   |
    -+<<<<<<<+---+---+---+...............+---+---+---+
     |<<<<<<<|   |   |   |...............|   |   |   |
    -+<<<<<<<+---+---+---+...............+---+---+---+
     |<<<<<<<|   |   |   |...............|   |   |   |
    -+-------+---+---+---+---------------+---+---+---+
     y

    where the hashes '<' ((0, 0), (2, 12)), '>' ((1, 1), (4, 4)),
    '.' ((5, 6), (9, 12)), and '`' ((8, 0), (12, 8)) indicate the
    bounding boxes of the obstacles.

    6 locally-maximal bonding boxes which don't overlap any thereof can
    be found:

    0
    -+---+---+---+---+---+---+---+---+---+---+---+---+ x
     |:::::::|   1   |145|   1   5   |:::::::::::::::|
    -+:::::::+-------+---+-----------+:::::::::::::::|
     |:::::::::::::::|   |           |:::::::::::::::|
    -+:::::::::::::::| 4 |           |:::::::::::::::|
     |:::::::::::::::|   |     5     |:::::::::::::::|
    -+:::::::::::::::| 5 |           |:::::::::::::::|
     |:::::::::::::::|   |           |:::::::::::::::|
    -+:::::::+-------+---+-----------+:::::::::::::::|
     |:::::::|       |2 3|           |:::::::::::::::|
    -+:::::::| 2   3 |   |   3   5   |:::::::::::::::|
     |:::::::|       |4 5|           |:::::::::::::::|
    -+:::::::+-------+---+-----------+:::::::::::::::|
     |:::::::|       |   |:::::::::::::::::::::::::::|
    -+:::::::|       |   |:::::::::::::::::::::::::::|
     |:::::::|       |   |:::::::::::::::::::::::::::|
    -+:::::::|       | 2 |:::::::::::::::+-----------+
     |:::::::|       |   |:::::::::::::::|           |
    -+:::::::|   2   |   |:::::::::::::::|           |
     |:::::::|       |   |:::::::::::::::|           |
    -+:::::::|       | 4 |:::::::::::::::|     6     |
     |:::::::|       |   |:::::::::::::::|           |
    -+:::::::|       |   |:::::::::::::::|           |
     |:::::::|       |   |:::::::::::::::|           |
    -+-------+-------+---+---------------+-----------+
     y

    Excluding strips thinner than 2 units, 4 such boxes remain:

    0
    -+---+---+---+---+---+---+---+---+---+---+---+---+ x
     |:::::::::::::::|               |:::::::::::::::|
    -+:::::::::::::::|               |:::::::::::::::|
     |:::::::::::::::|               |:::::::::::::::|
    -+:::::::::::::::|       3       |:::::::::::::::|
     |:::::::::::::::|               |:::::::::::::::|
    -+:::::::::::::::|               |:::::::::::::::|
     |:::::::::::::::|               |:::::::::::::::|
    -+:::::::+-------+---+-----------+:::::::::::::::|
     |:::::::|       |1  |           |:::::::::::::::|
    -+:::::::| 1   2 | 2 |   2   3   |:::::::::::::::|
     |:::::::|       |  3|           |:::::::::::::::|
    -+:::::::+-------+---+-----------+:::::::::::::::|
     |:::::::|           |:::::::::::::::::::::::::::|
    -+:::::::|           |:::::::::::::::::::::::::::|
     |:::::::|           |:::::::::::::::::::::::::::|
    -+:::::::|           |:::::::::::::::+-----------+
     |:::::::|           |:::::::::::::::|           |
    -+:::::::|     1     |:::::::::::::::|           |
     |:::::::|           |:::::::::::::::|           |
    -+:::::::|           |:::::::::::::::|     4     |
     |:::::::|           |:::::::::::::::|           |
    -+:::::::|           |:::::::::::::::|           |
     |:::::::|           |:::::::::::::::|           |
    -+-------+-----------+---------------+-----------+
     y

    In code:

    >>> from matplotlib.transforms import Bbox
    >>> bounds = Bbox.from_extents(0, 0, 12, 12)
    >>> obs_extents = (
    ...     (0, 0, 2, 12), (1, 1, 4, 4), (5, 6, 9, 12), (8, 0, 12, 8),
    ... )
    >>> obstacles = [Bbox.from_extents(e) for e in obs_extents]
    >>> bboxes = find_locally_maximal_bboxes(bounds, *obstacles)
    >>> print(bboxes[:2], bboxes[2:4], bboxes[4:], sep='\n')
    [Bbox([[2.0, 0.0], [8.0, 1.0]]), Bbox([[2.0, 4.0], [5.0, 12.0]])]
    [Bbox([[2.0, 4.0], [8.0, 6.0]]), Bbox([[4.0, 0.0], [5.0, 12.0]])]
    [Bbox([[4.0, 0.0], [8.0, 6.0]]), Bbox([[9.0, 8.0], [12.0, 12.0]])]
    >>> bboxes = find_locally_maximal_bboxes(
    ...     bounds, *obstacles, min_width=2, min_height=2,
    ... )
    >>> print(bboxes[:2], bboxes[2:], sep='\n')
    [Bbox([[2.0, 4.0], [5.0, 12.0]]), Bbox([[2.0, 4.0], [8.0, 6.0]])]
    [Bbox([[4.0, 0.0], [8.0, 6.0]]), Bbox([[9.0, 8.0], [12.0, 12.0]])]

    (Also works with extents instead of `Bbox` objects):

    >>> bboxes = find_locally_maximal_bboxes(
    ...     bounds, *obs_extents, min_width=2, min_height=2,
    ... )
    >>> print(bboxes[:2], bboxes[2:], sep='\n')
    [Bbox([[2.0, 4.0], [5.0, 12.0]]), Bbox([[2.0, 4.0], [8.0, 6.0]])]
    [Bbox([[4.0, 0.0], [8.0, 6.0]]), Bbox([[9.0, 8.0], [12.0, 12.0]])]

    Caveats
    -------
    - The `min_{width|height|area}` arguments are experimental.
    - Computing time may grow quickly with the number of `obstacles`.
    """
    all_bboxes = [bounds, *obstacles]
    for i, bbox in enumerate(all_bboxes):
        if isinstance(bbox, Bbox):
            all_bboxes[i] = _utils.to_extents(bbox)
    bounds, *obstacles = all_bboxes
    bboxes_as_extents, _ = _clip_bboxes(
        [bounds], {}, obstacles,
        min_width=min_width,
        min_height=min_height,
        min_area=min_area,
    )
    return [_utils.to_bbox(extents) for extents in sorted(bboxes_as_extents)]
