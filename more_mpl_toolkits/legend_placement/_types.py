"""
Typing stuff
"""
import numbers
import typing
import warnings
try:
    from typing import Literal
except ImportError:
    from typing_extensions import Literal

from matplotlib.figure import Figure
from matplotlib.transforms import Bbox
try:
    from matplotlib.figure import FigureBase
except ImportError:  # Pre matplotlib v3.4
    FigureBase = Figure

AXIS_SPECIFICATIONS = True, False, 'x', 'y', 'xy'


class BboxFitWarning(UserWarning):
    """
    Warning class for questionable bbox fits.
    """
    @classmethod
    def warn(
        cls,
        msg: str,
        level: numbers.Integral = 1,
        **kwargs,
    ) -> None:
        """
        Issue a warning.

        Parameters
        ----------
        msg
            String message
        level
            Number of stack level above this method the warning is
            considered to be issued from;
            1 (the default) means the calling frame
        **kwargs
            Passed to `warnings.warn()` if within
            `cls.allowed_warn_kwargs`, else ignored

        Side effects
        ------------
        Warning of this class issued
        """
        kwargs = {
            key: value for key, value in kwargs.items()
            if key in cls.allowed_warn_kwargs
        }
        warnings.warn(msg, cls, level + 1, **kwargs)

    allowed_warn_kwargs = (
        'source',
        'skip_file_prefixes',  # python 3.12+
    )


Keyword = typing.TypeVar('Keyword', bound=str)
Keywords = typing.Mapping[Keyword, typing.Any]
NonnegNum = typing.TypeVar('NonnegativeNumber', bound=numbers.Real)
NonnegNumUnit = typing.TypeVar('NonnegativeNumberWithUnit', bound=str)
NonnegQuantity = typing.Union[NonnegNum, NonnegNumUnit, None]
Extents = typing.TypeVar(  # Left, bottom, right, top
    'Extents', bound=typing.Tuple[(numbers.Real,) * 4],
)
ExtentsOrBbox = typing.Union[Extents, Bbox]
Interval = typing.TypeVar(
    'Interval', bound=typing.Tuple[numbers.Real, numbers.Real],
)
AxisSpecifications = Literal[AXIS_SPECIFICATIONS]
ResolvedAxisSpecs = Literal['x', 'y', 'xy', '']
LegendLocation = Literal[
    0,  # Left/lower
    1,  # Right/upper
    2,  # Center
]
SortedItem = typing.TypeVar('Item')
SortingKey = typing.TypeVar('SortingKey')
