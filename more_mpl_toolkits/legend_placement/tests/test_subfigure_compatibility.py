"""
Test for compatibility with subfigures.

Requirements
------------
pytest
    Should be installed if installed with the <MODULE>[dev] dependencies
matplotlib.figure.SubFigure
    Matplotlib 3.4+
"""
import functools
import numbers
import typing
from contextlib import nullcontext, ExitStack

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pytest
try:
    from pytest import MonkeyPatch  # Need pytest v6.2+
except ImportError:
    from _pytest.monkeypatch import MonkeyPatch

from ..mpl_interface import (
    _resolve_flmb_kwargs, _get_bbox_in_fig_fracs, avoid_figure_elements
)

if not hasattr(mpl.figure, 'SubFigure'):
    mpl.figure.SubFigure = typing.TypeVar('SubFigure')
    SUBFIGURE = False

MODULE_WIDE_FIXTURES = True

BACKEND = mpl.get_backend()  # 'agg'
RC_PARAMS = {}

FIGURE = dict(
    figsize=(10., 6.),  # Inches
)
SUBFIGURE = dict(
    linewidth=1,  # Points
    edgecolor='red',
)

GRID_SIZE = 2, 2
GRIDSPEC_KWARGS = dict(
    left=.1,  # = 1 in
    right=.8,  # = 2 in -> 7 in within grid span
    top=(5 / 6),  # = 1 in
    bottom=(1 / 6),  # = 1 in
    height_ratios=[1, 2],
    wspace=(1 / 3),  # = 1 in (<- 3 in plot + 1 in space + 3 in plot)
    hspace=(2 / 3),  # = 1 in (<- 1 in plot + 1 in space + 2 in plot)
)

SUBPLOT_ID = 1  # Upper right corner
SUBFIG_ID = -2  # Lower left corner

LEFTS = [.1, .5]
RIGHTS = [.4, .8]
TOPS = [5 / 6, .5]
BOTTOMS = [2 / 3, 1 / 6]

subfigure_test = pytest.mark.skipif(
    not (
        isinstance(mpl.figure.SubFigure, type) and
        issubclass(
            mpl.figure.SubFigure,
            getattr(mpl.figure, 'FigureBase', mpl.figure.Figure),
        )
    ),
    reason='Matplotlib too old, `SubFigure` not available',
)
module_fixture = (
    pytest.fixture(scope='module')
    if MODULE_WIDE_FIXTURES else
    pytest.fixture
)


if MODULE_WIDE_FIXTURES:
    @module_fixture
    def monkeypatch() -> MonkeyPatch:
        with MonkeyPatch.context() as mp:
            yield mp


@module_fixture
def fig() -> mpl.figure.Figure:
    """
    <10 in x 6 in figure>
    """
    with ExitStack() as stack:
        stack.enter_context(plt.ioff() or nullcontext())
        stack.enter_context(mpl.rc_context(RC_PARAMS))
        plt.switch_backend(BACKEND)
        fig = plt.figure(**FIGURE)
        yield fig
    plt.close(fig)


@module_fixture
def subplot(fig: mpl.figure.Figure) -> mpl.axes.Axes:
    """
    y / in
    6 +---------------------------------------+
      |                                       |
    5 +   +-----------+   +-----------+       |
      |   |###########|   |  subplot  |       |
    4 +   +-----------+   +-----------+       |
      |                                       |
    3 +   +-----------+   +-----------+       |
      |   |###########|   |###########|       |
    2 +   |###########|   |###########|       |
      |   |###########|   |###########|       |
    1 +   +-----------+   +-----------+       |
      |                                       |
      +---+---+---+---+---+---+---+---+---+---+ x / in
     0    1   2   3   4   5   6   7   8   9  10
    """
    subplots = fig.subplots(
        *GRID_SIZE, gridspec_kw=GRIDSPEC_KWARGS,
    ).flatten()
    ax = subplots[SUBPLOT_ID]
    for subplot in subplots:
        if subplot is ax:
            continue
        subplot.tick_params(labelsize=0, labelcolor=[0, 0, 0, 0])
    return ax


@module_fixture
def gridspec(subplot: mpl.axes.Axes) -> mpl.figure.GridSpec:
    """
    y / in
    6 +---------------------------------------+
      |                                       |
    5 +   +-----------+   +-----------+       |
      |   |gridspec[0]|   |gridspec[1]|       |
    4 +   +-----------+   +-----------+       |
      |                                       |
    3 +   +-----------+   +-----------+       |
      |   |           |   |           |       |
    2 +   |gridspec[2]|   |gridspec[3]|       |
      |   |           |   |           |       |
    1 +   +-----------+   +-----------+       |
      |                                       |
      +---+---+---+---+---+---+---+---+---+---+ x / in
     0    1   2   3   4   5   6   7   8   9  10
    """
    return subplot.get_subplotspec().get_gridspec()


@module_fixture
def subfig(
    fig: mpl.figure.Figure,
    gridspec: mpl.figure.GridSpec,
    monkeypatch: MonkeyPatch,
) -> mpl.figure.SubFigure:
    """
    y / in
    6 +---------------------------------------+
      |                                       |
    5 +   +-----------+   +-----------+       |
      |   |###########|   |###########|       |
    4 +   +-----------+   +-----------+       |
      |                                       |
    3 +   +-----------+   +-----------+       |
      |   |           |   |###########|       |
    2 +   |  subfig   |   |###########|       |
      |   |           |   |###########|       |
    1 +   +-----------+   +-----------+       |
      |                                       |
      +---+---+---+---+---+---+---+---+---+---+ x / in
     0    1   2   3   4   5   6   7   8   9  10
    """
    if hasattr(fig, 'add_subfigure'):
        monkeypatch.setattr(
            mpl.figure.SubFigure,
            '_redo_transform_rel_fig',
            redo_transform_rel_fig_monkey_patch,
        )
        yield fig.add_subfigure(gridspec[SUBFIG_ID], **SUBFIGURE)
    else:
        pytest.skip('Matplotlib too old, `SubFigure` not available')


def force_draw(artist: mpl.artist.Artist) -> None:
    """
    Force a complete redraw.
    """
    fig = artist.figure
    seen_figs: typing.Set[int] = set()
    while True:
        if fig.figure in (fig, None):
            break
        assert id(fig) not in seen_figs, 'Loop of `.figure`'
        seen_figs.add(id(fig))
        fig = fig.figure
    fig.draw(fig.canvas.get_renderer())


def get_canvas_extent(
    artist: mpl.artist.Artist, redraw: bool = False,
) -> mpl.transforms.Bbox:
    """
    Return the extent of an artist in figure fractions.
    """
    if redraw:
        force_draw(artist)
    raw_bbox = artist.get_window_extent()
    screen_to_fig_frac = artist.figure.transFigure.inverted()
    return mpl.transforms.Bbox(
        screen_to_fig_frac.transform([raw_bbox.min, raw_bbox.max])
    )


redo_transform_rel_fig = getattr(
    mpl.figure.SubFigure,
    '_redo_transform_rel_fig',
    lambda self, bbox=None: None,
)


@functools.wraps(redo_transform_rel_fig)
def redo_transform_rel_fig_monkey_patch(
    self: mpl.figure.SubFigure,
    bbox: typing.Optional[mpl.transforms.Bbox] = None,
) -> None:
    """
    As of matplotlib v3.8 a `SubFigure` and a subplot sharing the
    same `SubplotSpec` doesn't in general share the same bbox;
    monkey-patch it
    """
    if bbox is None:
        bbox = self._subplotspec.get_position(self.figure)
    if self.bbox_relative is None:
        # Note: matplotlib 3.7 or earlier defaults to
        # .bbox_relative = None, which may result in a bug when
        # calling SubFigure._redo_transform_rel_fig() with an
        # explicit bbox
        self.bbox_relative = bbox
        return
    return redo_transform_rel_fig(self, bbox)


def get_extents(
    index: typing.Union[
        numbers.Integral,
        slice,
        typing.Tuple[
            typing.Union[numbers.Integral, slice],
            typing.Union[numbers.Integral, slice],
        ],
    ],
) -> typing.Tuple[numbers.Real, numbers.Real, numbers.Real, numbers.Real]:
    """
    Return the extents of a subplot plotted with `gridspec[index]`.
    """
    indices = np.arange(GRID_SIZE[0] * GRID_SIZE[1]).reshape(GRID_SIZE)
    try:  # 2D indexing
        i, j = index
    except TypeError:  # Cannot unpack -> treat as slice or integer
        subindices = indices.flatten()[index]
    else:
        subindices = indices[i, j]
    x_indices, y_indices = set(), set()
    for flattened_index in subindices.flatten():
        i, j = divmod(flattened_index, GRID_SIZE[0])
        y_indices.add(i)
        x_indices.add(j)
    return (
        LEFTS[min(x_indices)],
        BOTTOMS[max(y_indices)],
        RIGHTS[max(x_indices)],
        TOPS[min(y_indices)],
    )


@subfigure_test
def test_confirm_grid_layout(
    fig: mpl.figure.Figure,
    subfig: mpl.figure.SubFigure,
    subplot: mpl.axes.Axes,
) -> None:
    """
    Check if the layout looks ok.
    """
    def count_children(
        type: typing.Union[type, typing.Tuple[type, ...]],
    ) -> int:
        return sum(1 for child in children if isinstance(child, type))

    # force_draw(subfig)  # Apparently not needed

    # Check geometries
    fig_box = get_canvas_extent(subfig)
    ax_box = get_canvas_extent(subplot)
    assert fig_box.extents == pytest.approx(get_extents(SUBFIG_ID))
    assert ax_box.extents == pytest.approx(get_extents(SUBPLOT_ID))

    # Check ownership
    children = fig.get_children()
    assert subfig in children
    assert subplot in children
    assert count_children(mpl.axes.Axes) == GRID_SIZE[0] * GRID_SIZE[1]
    assert count_children(mpl.figure.SubFigure) == 1


@subfigure_test
def test_resolving_flmb_kwargs_from_subfigure(
    fig: mpl.figure.Figure, subfig: mpl.figure.SubFigure,
) -> None:
    """
    Test whether `..mpl_interface._resolve_flmb_kwargs()` correctly
    converts lengths and areas from different units into (sub-)figure
    fractions.
    """
    # Scaling factor from subfig fracs to fig fracs
    subfig_frac_to_fig_frac = (
        subfig.transSubfigure + subfig.transFigure.inverted()
    )
    (x1, y1), (x2, y2) = subfig_frac_to_fig_frac.transform([[0, 0], [1, 1]])
    sf2f_factors = dict(width=(x2 - x1), height=(y2 - y1))
    sf2f_factors['area'] = sf2f_factors['width'] * sf2f_factors['height']
    # Unit conversions (to inches)
    u2in_factors = {'cm': 1 / 2.54, 'in': 1, 'px': 1 / fig.dpi, 'pt': 1 / 72}
    # Conversion of absolute units consistent between fig and subfig
    for value, (unit, conv_factor) in zip([5, 6, 70, 8], u2in_factors.items()):
        quantity = '{} {}'.format(value, unit)
        from_fig, from_subfig = (
            _resolve_flmb_kwargs(
                obj, [],
                min_area=quantity, min_width=quantity, min_height=quantity,
            )
            for obj in (fig, subfig)
        )
        # Check internal consistency
        resolved_lengthes = np.array([  # In inches
            from_fig['min_' + key] * dimension
            for key, dimension in zip(['width', 'height'], FIGURE['figsize'])
        ])
        expected_length = value * conv_factor
        assert resolved_lengthes == pytest.approx(expected_length)
        resolved_area = (
            from_fig['min_area'] * FIGURE['figsize'][0] * FIGURE['figsize'][1]
        )
        expected_area = value * (conv_factor ** 2)
        assert resolved_area == pytest.approx(
            expected_area,
            # FIXME: this makes the test pass but it wasn't necessary
            # before...
            abs=1e-6,
        )
        # Check mutual consistency
        for key, factor in sf2f_factors.items():
            key = 'min_' + key
            assert from_fig[key] == pytest.approx(from_subfig[key] * factor)


@subfigure_test
def test_bbox_subfigure_fracs(
    subfig: mpl.figure.SubFigure, subplot: mpl.axes.Axes,
) -> None:
    """
    Test whether the bbox of `subplot` in `subfig`-fraction coordinates
    are correctly calculated.
    """
    subfig_bbox, subplot_bbox = (
        mpl.transforms.Bbox.from_extents(get_extents(i))
        for i in (SUBFIG_ID, SUBPLOT_ID)
    )
    expected_box = subplot_bbox.transformed(
        mpl.transforms.BboxTransformFrom(subfig_bbox),
    )
    result_bbox = _get_bbox_in_fig_fracs(subplot, subfig)
    assert result_bbox == pytest.approx(expected_box)


@subfigure_test
def test_find_bbox_outside_subfigure(
    fig: mpl.figure.Figure,
    subfig: mpl.figure.SubFigure,
    subplot: mpl.axes.Axes,
) -> None:
    """
    Test whether `..avoid_figure_elements()` correctly calculates the
    optimal legend placement (in the shaded area) for `subfig` given
    this setup.

    y / in
    6 +---------------------------------------+
      |                                       |
    5 +   + - - - - - - - +-----------+       |
      |   :/ / / / / / / /|  subplot  |       |
    4 +   :<- / / / /bounds-----------+       |
      |   :/ / / / / / / /:           :       |
    3 +   +-----------+ - - - - - - - +       |
      |   |           |                       |
    2 +   |  subfig   |                       |
      |   |           |                       |
    1 +   +-----------+                       |
      |                                       |
      +---+---+---+---+---+---+---+---+---+---+ x / in
     0    1   2   3   4   5   6   7   8   9  10
    """
    inches_to_subfig_fracs = (
        fig.dpi_scale_trans + subfig.transSubfigure.inverted()
    )
    expected_box = (
        mpl.transforms.Bbox([[1, 3], [5, 5]])
        .transformed(inches_to_subfig_fracs)
    )
    box_is_found, kwargs = avoid_figure_elements(
        subfig, [subplot],
        bounds=(
            mpl.transforms.Bbox([[1, 3], [8, 5]])
            .transformed(inches_to_subfig_fracs)
        ),
        tight=False,
    )
    assert box_is_found
    assert kwargs['loc'] == 'center left'
    assert (
        kwargs['bbox_to_anchor'].extents == pytest.approx(expected_box.extents)
    )


@pytest.mark.xfail(
    MODULE_WIDE_FIXTURES,
    reason=(
        "Module-wide fixtures used, teardown hasn't happened yet; "
        'see `MODULE_WIDE_FIXTURES`'
    ),
    raises=AssertionError,
    strict=True,
)
@subfigure_test
def test_monkeypatch_teardown():
    """
    Test if the teardown of the monkey-patched
    `SubFigure._redo_transform_rel_fig()` method has happened after
    we're doing with the tests.

    Caveats
    -------
    Should be run after all the tests using the `subfig` fixture.
    Maybe use `pytest-order` to ensure that in the future?
    """
    original = redo_transform_rel_fig
    monkeypatched = redo_transform_rel_fig_monkey_patch
    method = mpl.figure.SubFigure._redo_transform_rel_fig
    seen_methods: typing.Set[int] = set()
    while True:
        assert method is not monkeypatched, 'Teardown failed'
        if method is original:
            break
        assert id(method) not in seen_methods, 'Wrapping loop'
        seen_methods.add(id(method))
        next_method = getattr(method, '__wrapped__', None)
        if next_method is None:
            break
        method = next_method
