"""
Shared utilities
"""
import functools
import numbers
import typing

import tjekmate

from ._types import (
    Literal,
    Extents, ExtentsOrBbox, Bbox,
    Figure,
    NonnegNum, NonnegNumUnit,
    SortedItem, SortingKey,
)
from .._utils.length_conversions import UNITS_TO_INCHES

__all__ = (
    'MIN_DIM_RESOLVERS',
    'CompositeSorter',
    'get_nonneg_quantity',
    'is_extents', 'is_extents_or_bbox', 'to_extents', 'to_bbox',
    'format_arg_list',
)

DimensionResolver = typing.Callable[
    # Figure (NOT SubFigure)
    # bboxes in FIGURE fractions,
    # getter of quantity from bbox[,
    # power to raise the conversion factor to]
    [
        Figure,
        typing.Sequence[Bbox],
        typing.Callable[[Bbox], NonnegNum],
        typing.Optional[numbers.Real],
    ],
    # Multiplier to quantity (to convert into FIGURE fraction)
    NonnegNum
]

MIN_DIM_RESOLVERS: typing.Mapping[str, DimensionResolver] = {}


class CompositeSorter(typing.Generic[SortedItem, SortingKey]):
    """
    Sorting function combining the results of other sorting functions.

    Parameters
    ----------
    func, *funcs
        Sorting function taking an item and returning a sorting key
        (e.g. strings, numbers, sequences thereof, ...)

    Methods
    -------
    .__call__()
        Call all the `.funcs` in sequence, composing the result into a
        tuple of sorting keys.
    """
    def __init__(
        self,
        func: typing.Callable[[SortedItem], SortingKey],
        *funcs: typing.Callable[[SortedItem], SortingKey],
    ) -> None:
        self.funcs = (func, *funcs)

    def __repr__(self) -> str:
        return '{}({})'.format(
            type(self).__qualname__,
            ', '.join(repr(func) for func in self.funcs),
        )

    def __call__(self, item: SortedItem) -> typing.Tuple[SortingKey, ...]:
        return tuple(func(item) for func in self.funcs)

    __slots__ = 'funcs',


def _resolve_conversion_factor_from_bboxes(
    fig: Figure,
    bboxes: typing.Sequence[Bbox],
    getter: typing.Callable[[Bbox], NonnegNum],
    _=None,
) -> NonnegNum:
    """
    Calculate the conversion factor from (percentage) fractions of the
    minimal bbox to figure fractions.
    """
    min_dim = min(getter(b) for b in bboxes)
    return min_dim / 100


def _resolve_conversion_factor_from_inches(
    fig: Figure,
    bboxes: typing.Sequence[Bbox],
    getter: typing.Callable[[Bbox], NonnegNum],
    pow: numbers.Real = 1,
    post_conversion: NonnegNum = 1,
) -> NonnegNum:
    """
    Calculate the conversion factor from inches to figure fractions.
    The optional `post_conversion` factor can be used to scale the
    factor by `post_conversion ** pow`.
    """
    bbox_inches = Bbox.from_bounds(
        0, 0, fig.get_figwidth(), fig.get_figheight()
    )
    dim = getter(bbox_inches)
    return (post_conversion ** pow) / dim


def _resolve_conversion_factor_from_pixels(
    fig: Figure,
    bboxes: typing.Sequence[Bbox],
    getter: typing.Callable[[Bbox], NonnegNum],
    pow: numbers.Real = 1,
) -> NonnegNum:
    """
    Calculate the conversion factor from pixels to figure fractions.
    """
    return _resolve_conversion_factor_from_inches(
        fig, bboxes, getter, pow, post_conversion=(1 / fig.dpi),
    )


@typing.overload
def get_nonneg_quantity(
    obj: typing.Union[NonnegNum, NonnegNumUnit],
) -> typing.Tuple[numbers.Real, typing.Union[str, None]]:
    ...


@typing.overload
def get_nonneg_quantity(obj: typing.Any) -> None:
    ...


def get_nonneg_quantity(
    obj: typing.Any,
) -> typing.Union[
    typing.Tuple[numbers.Real, typing.Union[str, None]],
    None
]:
    """
    Example
    -------
    >>> [  # doctest: +NORMALIZE_WHITESPACE
    ...     get_nonneg_quantity(o)
    ...     for o in [-1, 0, '1', '10%', '1 cm', '-1 cm', 'cm', 'abc']
    ... ]
    [None, (0, None), (1.0, ''), (10.0, '%'), (1.0, 'cm'),
     None, None, None]
    """
    if tjekmate.is_nonnegative_real(obj):  # NonnegNum
        return obj, None
    if not isinstance(obj, str):
        return None
    for unit in sorted([*MIN_DIM_RESOLVERS, ''], key=len)[::-1]:
        # NonnegNumUnit
        if not obj.endswith(unit):
            continue
        if unit:
            obj = obj[:-len(unit)]
        try:
            value = float(obj.strip())
        except ValueError:
            return None
        if tjekmate.is_nonnegative_real(value):
            return value, unit
        else:
            return None
    return None


@typing.overload
def is_extents(obj: Extents) -> Literal[True]:
    ...


@typing.overload
def is_extents(obj: typing.Any) -> Literal[False]:
    ...


def is_extents(obj: typing.Any) -> bool:
    """
    Check if `obj` looks like a set of extents
    `left, bottom, right, top`.

    Example
    -------
    >>> is_extents([0, 0, 1, 1])
    True
    >>> is_extents([0, 0, -1, 1])  # right < left
    False
    >>> [is_extents(obj) for obj in (None, 1, 'string')]  # Wrong types
    [False, False, False]
    >>> is_extents([1, 2])  # Wrong length
    False
    """
    if not tjekmate.is_sequence(
        obj, length=4, item_checker=tjekmate.is_finite_real,
    ):
        return False
    left, bottom, right, top = obj
    return right >= left and top >= bottom


def is_extents_or_bbox(obj: typing.Any) -> bool:
    return is_extents(obj) or isinstance(obj, Bbox)


def to_extents(bbox_or_extents: ExtentsOrBbox) -> Extents:
    return tuple(getattr(bbox_or_extents, 'extents', bbox_or_extents))


def to_bbox(bbox_or_extents: ExtentsOrBbox) -> Bbox:
    return Bbox.from_extents(*to_extents(bbox_or_extents))


@typing.overload
def format_arg_list(
    arguments: typing.Mapping[str, typing.Any],
    *,
    values: typing.Optional[typing.Mapping[str, typing.Any]] = None,
    **kwargs
) -> str:  # Format all arg-name--value pairs in arguments
    ...


@typing.overload
def format_arg_list(
    arguments: typing.Sequence[str],
    *,
    values: typing.Mapping[str, typing.Any],
    **kwargs
) -> str:
    # Format all arg-name--value pairs in values present in arguments
    ...


def format_arg_list(
    arguments: typing.Union[
        typing.Sequence[str],
        typing.Mapping[str, typing.Any],
    ],
    *,
    equals: str = ' = ',
    values: typing.Optional[typing.Mapping[str, typing.Any]] = None,
    original_values: typing.Optional[
        typing.Mapping[str, typing.Any]
    ] = None,
    sources: typing.Optional[
        typing.Mapping[str, typing.Sequence[str]]
    ] = None,
    aliases: typing.Optional[typing.Mapping[str, str]] = None,
) -> str:
    """
    Format a list of arguments.

    Call signatures
    ---------------
    >>> format_arg_list(  # noqa # doctest: +SKIP
    ...     Mapping[arg_name, value], **kwargs,
    ... )
    >>> format_arg_list(  # noqa # doctest: +SKIP
    ...     List[arg_name], values=Mapping[arg_name, value], **kwargs,
    ... )

    Parameters
    ----------
    arguments
        Mapping from argument names to values, or sequence of argument
        names;
        if not a mapping, all argument names must have a corresponding
        entry in `values`
    equals
        String for representing the equal sign
    values
        Optional mapping from argument names to values
    original_values
        Optional mapping from argument names to their original values;
        if an argument is found here, it is formatted differently, and
        should not be found in `sources`
    sources
        Optional mapping from resolved argument names to their sources;
        if an argument is found here, it is formatted differently, and
        should not be found in `original_values`
    aliases
        Optional mapping from argument names to the preferred formatting

    Return
    ------
    String representing the list of arguments

    Notes
    -----
    If an argument is found in both `original_values` and `sources`, the
    latter takes formatting priority.

    Example
    -------
    >>> from pprint import pprint
    >>> foo, bar, baz = 1, 2, 'string'
    >>> args = dict(foo=foo, bar=bar, baz=baz)
    >>> original_values = dict(baz='STRING')
    >>> format_arg_list(args)
    "foo = 1, bar = 2, baz = 'string'"
    >>> format_arg_list(['foo', 'bar'], values=args, equals='=')
    'foo=1, bar=2'
    >>> format_arg_list(args, original_values=original_values)
    "foo = 1, bar = 2, baz = 'STRING' (-> 'string')"
    >>> arg_list = format_arg_list(
    ...     ['foo', 'foobar', 'eggs'],
    ...     values=dict(args, foobar=foo + bar, eggs=baz * 2),
    ...     sources=dict(foobar=('foo', 'bar'), eggs=('baz',)),
    ...     original_values=original_values,
    ... )
    >>> pprint(arg_list.partition(' -> '), width=68)
    ("foo = 1, (foo, bar=2, baz='STRING')",
     ' -> ',
     "(foobar=3, eggs='stringstring')")
    >>> format_arg_list(args, aliases=dict(bar='<bar>'))
    "foo = 1, <bar> = 2, baz = 'string'"
    """
    def apply_aliases(arg_name: str) -> str:
        return aliases.get(arg_name, arg_name)

    base_format = '{}{}{!r}'.format
    if not original_values:
        original_values = {}
    if not aliases:
        aliases = {}
    sources = dict(sources or {})
    if isinstance(arguments, typing.Mapping):
        values = dict(arguments)
    else:
        arguments, _args = {}, arguments
        for arg_name in _args:  # Handle duplicates gracefully
            arguments.setdefault(arg_name, values[arg_name])
    formatted_standard_args: typing.List[str] = []
    standard_args: typing.List[str] = []
    resolved_args: typing.List[str] = []
    # Primary formatting for all the easy stuff
    for arg_name, value in arguments.items():
        has_origin = arg_name in original_values
        is_resolved = arg_name in sources
        if is_resolved:
            resolved_args.append(arg_name)
            if arg_name in original_values:
                # Make sure that the original value is credited as a
                # source
                sources[arg_name] = arg_name, *sources[arg_name]
            continue
        else:
            standard_args.append(arg_name)
        if has_origin:
            orig_value = original_values[arg_name]
            formatted = '{} (-> {!r})'.format(
                base_format(
                    apply_aliases(arg_name), equals, orig_value,
                ),
                value,
            )
        else:
            formatted = base_format(apply_aliases(arg_name), equals, value)
        formatted_standard_args.append(formatted)
    # Formatting for the resolved args
    input_args: typing.Dict[str, typing.Union[str, None]] = {}
    formatted_source_args: typing.List[str]
    formatted_resolved_args: typing.List[str] = [
        base_format(apply_aliases(arg_name), '=', arguments[arg_name])
        for arg_name in resolved_args
    ]
    seen_names: typing.Set[str] = set()  # Suppress duplicates
    for source_name, _ in (
        (source_name, seen_names.add(source_name))
        for arg_name in resolved_args
        for source_name in sources[arg_name]
        if source_name not in seen_names
    ):
        if source_name in resolved_args:  # Flatten resolution levels
            continue
        if source_name in standard_args:  # Already shown, don't repeat
            input_value = None
        elif source_name in values:
            input_value = repr(
                original_values.get(source_name, values[source_name])
            )
        else:
            input_value = '<UNKNOWN>'
        input_args.setdefault(source_name, input_value)
    formatted_source_args = [
        (
            '{arg_name}'
            if formatted_value is None else
            '{arg_name}={formatted_value}'
        ).format(
            arg_name=apply_aliases(arg_name),
            formatted_value=formatted_value,
        )
        for arg_name, formatted_value in input_args.items()
    ]
    # Finalize
    arg_list = ', '.join(formatted_standard_args)
    if formatted_resolved_args:
        arg_list += ', ({}) -> ({})'.format(
            (
                ', '.join(formatted_source_args)
                if formatted_source_args else
                '...'
            ),
            ', '.join(formatted_resolved_args),
        )
    return arg_list


# Add the resolvers here
MIN_DIM_RESOLVERS.update({
    '%': _resolve_conversion_factor_from_bboxes,
    'in': _resolve_conversion_factor_from_inches,
    'px': _resolve_conversion_factor_from_pixels,
})
MIN_DIM_RESOLVERS.update(
    (
        unit,
        functools.partial(
            _resolve_conversion_factor_from_inches, post_conversion=in_inches,
        )
    )
    for unit, in_inches in UNITS_TO_INCHES.items()
    if unit not in MIN_DIM_RESOLVERS
)
