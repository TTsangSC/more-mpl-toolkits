`more_mpl_toolkits.legend_placement`
====================================

(Alias(es): `~.lp`)

In `matplotlib`, the optimal placement of the legend (via
[`.legend(loc='best')`][axes-legend]) is only possible when the legend
is tied to an `Axes` object instead of a `Figure` object.
This helper module looks at a figure and its children to try and figure
out where the legend would best fit without intersecting its siblings: 
```python
import matplotlib.pyplot as plt

from more_mpl_toolkits.legend_placement import avoid_figure_elements

fig = plt.figure()
gridspec = fig.add_gridspec(2, 2)
ax0 = fig.add_subplot(gridspec[0, 0])  # Top left
ax1 = fig.add_subplot(gridspec[0, 1])  # Top right
ax2 = fig.add_subplot(gridspec[1, 0])  # Bottom left

...  # Do your plotting stuff here

fit_found, legend_kwargs = avoid_figure_elements(fig)
assert fit_found
fig.legend(**legend_kwargs)  # Placed in bottom right
```

[axes-legend]: https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.legend.html
