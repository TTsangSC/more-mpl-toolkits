"""
Utilities for optimally placing a legend in a figure.

API
---
BboxFitWarning
    Warning subclass for warnings arising when calculating fitting
    bboxes

find_locally_maximal_bboxes()
    List bboxes which don't overlap the given list of obstacle bboxes

avoid_figure_elements()
    Compute the appropriate keyword arguments with which to call
    `Figure.legend()`

resolve_kwargs_from_bbox()
resolve_kwargs_from_bbox_and_dims()
    Helper functions for `avoid_figure_elements()` implementing the
    strategies for adjusting bboxes and alignments
"""
from ._types import BboxFitWarning
from .find_boxes import find_locally_maximal_bboxes
from .mpl_interface import (
    avoid_figure_elements,
    resolve_kwargs_from_bbox,
    resolve_kwargs_from_bbox_and_dims
)

__all__ = (
    'BboxFitWarning',
    'find_locally_maximal_bboxes',
    'avoid_figure_elements',
    'resolve_kwargs_from_bbox',
    'resolve_kwargs_from_bbox_and_dims',
)
