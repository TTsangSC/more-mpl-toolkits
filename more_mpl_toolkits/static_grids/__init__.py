"""
Create grid layouts with static (fixed) margins.

API
---
Wrappers:

gridspec_update()
    Wrapper around `matplotlib.gridspec.GridSpec.update()`
make_gridspec()
    Wrapper around `matplotlib.gridspec.GridSpec()`
fig_add_gridspec()
    Wrapper around `matplotlib.figure.FigureBase.add_gridspec()`
fig_subplots()
    Wrapper around `matplotlib.figure.FigureBase.subplots()`
fig_subplot_mosaic()
    Wrapper around `matplotlib.figure.FigureBase.subplot_mosaic()`

Helpers:

@process_static_grid_kwargs
    Helper decorator for creating `GridSpec` objects with static margins
convert_length()
    Helper function for converting static lengths

Fixed margins
-------------
To be specified with strings of the format '<number><unit>', where the
unit can be any of:

'mm', 'cm', 'm', 'in', 'ft', 'pt', 'px'
    Physical units
'%', '%h', '%w'
    Percentage fractions of the figure

Notes
-----
As opposed to the figure-fraction coordinate system of the vanilla
`GridSpec` where e.g. `right=.9` means to put the right edge of the grid
at 10% of the figure's width away from the figure's side, fixed margins
(even those defined with '%') are already referenced to their respective
edges of the figure, meaning that e.g. setting `left`, `right`,
`bottom`, and `top` all to '1 cm' puts a margin of 1 cm around all sides
of the grid spec.
"""
import functools
import inspect
import numbers
import types
import typing
try:  # python3.8+
    from typing import Literal, TypedDict
except ImportError:
    from typing_extensions import Literal, TypedDict
try:  # python3.10+
    from typing import Concatenate, ParamSpec
except ImportError:
    from typing_extensions import Concatenate, ParamSpec

import matplotlib
import numpy as np
try:
    from matplotlib.figure import FigureBase
except ImportError:
    from matplotlib.figure import Figure as FigureBase

from tjekmate import (
    check_call, ends_with, identical_to, is_keyword, is_in, is_instance,
    is_sequence, is_strict_boolean,
    takes_positional_args, check,
)
from .._utils.decorators import (
    replace_in_docs, partial_sole_positional_arg,
)
from .._utils.introspection import get_argument_value
from .._utils.length_conversions import UNITS_TO_INCHES
from .._utils.string_operations import strip_suffix

__all__ = (
    'convert_length',
    'process_static_grid_kwargs',
    'gridspec_update',
    'make_gridspec',
    'fig_add_gridspec',
    'fig_subplots',
    'fig_subplot_mosaic',  # May be absent for matplotlib < 3.3
)

# *************************** Typing stuff *************************** #

EMPTY = inspect.Parameter.empty
ADJUSTED_GRIDSPEC_KWARGS = (
    'left', 'right', 'top', 'bottom', 'wspace', 'hspace',
)
SPECIAL_UNITS = '%', '%w', '%h', 'px'
ALLOWED_UNITS = tuple(sorted({*UNITS_TO_INCHES, *SPECIAL_UNITS}))

PS = ParamSpec('PS')
T = typing.TypeVar('T')

Keyword = typing.TypeVar('Keyword', bound=str)
StringLength = typing.TypeVar('StringLength', bound=str)
AllowedUnit = Literal[ALLOWED_UNITS]
AdjustedGridspecKwarg = Literal[ADJUSTED_GRIDSPEC_KWARGS]
GridSpecFunction = typing.TypeVar(
    'GridSpecFunction',
    typing.Callable[PS, T],
    typing.Callable[PS, matplotlib.gridspec.GridSpec],
)

# *************** Lower-level API and helper functions *************** #


@check(fig=is_instance(FigureBase))
def _get_size_inches(
    fig: FigureBase,
) -> typing.Tuple[numbers.Real, numbers.Real]:
    """
    Example
    -------
    >>> from contextlib import nullcontext
    >>>
    >>> import pytest
    >>> from matplotlib import pyplot as plt
    >>> from numpy import allclose
    >>>
    >>> with (plt.ioff() or nullcontext()):
    ...     grid_size = 2, 3  # nrows, ncols
    ...     fig_size = 6.4, 4.8  # width, height
    ...     fig = plt.figure(figsize=fig_size)
    ...     try:
    ...         gridspec = fig.add_gridspec(*grid_size)
    ...         assert allclose(_get_size_inches(fig), fig_size)
    ...         if not hasattr(fig, 'add_subfigure'):
    ...             pytest.skip(
    ...                 'No SubFigure in this matplotlib version'
    ...             )
    ...         subfig = fig.add_subfigure(gridspec[0])
    ...         assert allclose(
    ...             _get_size_inches(subfig),
    ...             tuple(
    ...                 dim / ndiv
    ...                 for dim, ndiv in zip(fig_size, grid_size[::-1])
    ...             ),
    ...         )
    ...     finally:
    ...         plt.close(fig)
    """
    try:
        width, height = fig.get_size_inches()
    except AttributeError:  # Subfigure don't have get_size_inches()
        bottom_left, top_right = fig.transSubfigure.transform(
            [(0, 0), (1, 1)],
        )
        width, height = abs(top_right - bottom_left) / fig.dpi
    return width, height


def _length_helper(
    obj: typing.Any,
    recogized_units: typing.Collection[AllowedUnit] = ALLOWED_UNITS,
) -> typing.Union[typing.Tuple[float, AllowedUnit], None]:
    if not isinstance(obj, str):
        return None
    quantity, unit = strip_suffix(obj.strip(), ALLOWED_UNITS)
    if not unit:
        return None
    quantity = quantity.strip()
    if quantity:
        return float(quantity), unit
    return 1., unit


@check(
    length=is_instance(str) & check_call(('rstrip', ends_with(ALLOWED_UNITS))),
    to=is_in(ALLOWED_UNITS),
    fig=is_instance((FigureBase, type(None))),
)
@replace_in_docs({
    '<UNITS>': ', '.join(repr(u) for u in sorted(UNITS_TO_INCHES)),
    '<SPECIAL_UNITS>': ', '.join(repr(u) for u in SPECIAL_UNITS),
})
def convert_length(
    length: StringLength,
    to: AllowedUnit = 'in',
    fig: typing.Optional[FigureBase] = None,
) -> numbers.Real:
    """
    Convert a length into the required unit.

    Parameters
    ----------
    length
        String representation of a length, of format '[<num>]<unit>';
        the following units are understood:
          <UNITS>
        in addition, the following special units are understood if `fig`
        is provided:
          <SPECIAL_UNITS>
    to
        Unit to convert into
    fig
        Optional `matplotlib.figure.Figure[Base]` to use for conversions
        pertaining to the special units

    Return
    ------
    The length in `to`

    Examples
    --------
    >>> from numpy import isclose
    >>>
    >>> seventytwo_pts_in_in = convert_length('72 pt')
    >>> inch_in_cm = convert_length('in', to='cm')
    >>> assert isclose(inch_in_cm, 2.54), inch_in_cm
    >>> assert isclose(seventytwo_pts_in_in, 1), seventytwo_pts_in_in

    With a `fig`, it is possible to convert the special units:

    >>> from contextlib import nullcontext
    >>>
    >>> import pytest
    >>> from matplotlib import pyplot as plt
    >>>
    >>> with (plt.ioff() or nullcontext()):
    ...     grid_size = 2, 3  # nrows, ncols
    ...     fig_size = 6.4, 4.8  # width, height
    ...     dpi = 300
    ...     fig = plt.figure(figsize=fig_size)
    ...     fig.set_dpi(dpi)
    ...     try:
    ...         gridspec = fig.add_gridspec(*grid_size)
    ...         ten_percent_in_pixels = convert_length(
    ...             '10%', 'px', fig=fig,
    ...         )
    ...         one_percent_width_as_height_percentage = convert_length(
    ...             '1%w', '%h', fig=fig,
    ...         )
    ...         assert isclose(
    ...             ten_percent_in_pixels,
    ...             .1 * ((fig_size[0] * fig_size[1]) ** .5) * dpi,
    ...         ), ten_percent_in_pixels
    ...         assert isclose(
    ...             one_percent_width_as_height_percentage,
    ...             1 * fig_size[0] / fig_size[1]
    ...         ), one_percent_width_as_height_percentage
    ...         if not hasattr(fig, 'add_subfigure'):
    ...             pytest.skip(
    ...                 'No SubFigure in this matplotlib version'
    ...             )
    ...         subfig = fig.add_subfigure(gridspec[0])
    ...         hundred_pixels_fig, hundred_pixels_subfig = (
    ...             convert_length('100 px', fig=f)
    ...             for f in (fig, subfig)
    ...         )
    ...         assert isclose(
    ...             hundred_pixels_fig, 100 / dpi,
    ...         ), hundred_pixels_fig
    ...         assert isclose(
    ...             hundred_pixels_subfig, hundred_pixels_fig,
    ...         ), hundred_pixels_subfig
    ...     finally:
    ...         plt.close(fig)

    Notes
    -----
    '%' is taken to be a fraction of the geometric mean of the figure
    dimensions.
    """
    units_to_inches = dict(UNITS_TO_INCHES)
    if fig:
        width, height = _get_size_inches(fig)
        units_to_inches.update({
            'px': 1 / fig.dpi,
            '%w': width * 1e-2,
            '%h': height * 1e-2,
            '%': ((width * height) ** .5) * 1e-2,
        })
    real_len, unit = _length_helper(length.strip(), units_to_inches)
    multiplier = units_to_inches[unit]
    return real_len * multiplier / units_to_inches[to]


@partial_sole_positional_arg(placeholder=None)
@check(
    func=callable,
    postprocess=(identical_to(None) | takes_positional_args(1)),
    gridspec_kwargs_loc=(identical_to(None) | is_keyword),
    install=is_strict_boolean,
)
def process_static_grid_kwargs(
    func: GridSpecFunction,
    *,
    postprocess: typing.Optional[
        typing.Union[
            typing.Callable[[T], matplotlib.gridspec.GridSpec],
            typing.Callable[
                [T], typing.Tuple[matplotlib.gridspec.GridSpec, FigureBase]
            ]
        ]
    ] = None,
    gridspec_kwargs_loc: typing.Optional[Keyword] = None,
    install: bool = True,
) -> GridSpecFunction:
    """
    Decorator around a gridspec-making/-modifying function, enabling it
    to understand absolute margins (e.g. `left='1 in', hspace='3 cm'`)
    and to resolve them live with `GridSpec.update()`.

    Parameters
    ----------
    func()
        Callable taking (at least one of) the margin-specifying (e.g.
        `left`, `bottom`, `hspace`) `matplotlib.gridspec.GridSpec`
        instantiation/`.update()` arguments, and returning either the
        gridspec or an object from which the gridspec can be retrieved
    postprocess()
        Optional callable taking the return value of `func()` and
        returning either (1) the relevant gridspec, or (2) the gridspec
        and its figure in a tuple;
        default is to not perform post-processing, assuming that its
        return value is already the gridspec, and the figure is already
        available at `.figure` of said gridspec
    gridspec_kwargs_loc
        Optional argument name (e.g. `gridspec_kw` for
        `matplotlib.figure.Figure.subplots()`) under which the margin-
        specifying arguments can be retrieved;
        default is to take them from the bound arguments
    install
        Whether to install the bound method
        `gridspec_update.__get__(gridspec)` at `gridspec.update`

    Return
    ------
    func() passed
        Decorated `func()` which understands the static margin-
        specifying arguments `left`, `right`, `top`, `bottom`, `wspace`,
        and `hspace`
    else
        Decorator

    Notes
    -----
    - Require the gridspec to be associated with a figure.
    - Prior to `matplotlib` v3.3.0, it is possible for a figure to have
      created a gridspec without setting its `.figure` attribute (via
      `Figure.subplots()`);
      care is thus needed for the figure to be recovered.
    """
    StaticMarginsParams = TypedDict(
        'StaticMarginsParams',
        dict(
            {
                arg_name: typing.Union[StringLength, None]
                for arg_name in ADJUSTED_GRIDSPEC_KWARGS
            },
            callbacks=typing.Union[
                typing.Tuple[numbers.Integral, numbers.Integral], None
            ],
        ),
        total=False,
    )

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        ba = sig.bind(*args, **kwargs)
        # Intercept static margin-specifying args
        static_margin_args: typing.Dict[
            AdjustedGridspecKwarg, typing.Union[StringLength, None]
        ] = {}
        for arg_name in compatible_gridspec_kwargs:
            arg_value = get_gridspec_arg_value(ba, arg_name)
            if arg_value in (EMPTY, None):
                # Absent value, dont' overwrite the existing value
                continue
            if isinstance(arg_value, numbers.Real):
                # Value overridden with a native one (i.e. in figure-
                # fraction)
                static_margin_args[arg_name] = None
                continue
            try:
                _length_helper(arg_value)
            except Exception:
                # Only deal with stuff that we know how to, defer back
                # to the normal margin-handling system otherwise
                static_margin_args[arg_name] = None
                continue
            # Set the passed value (the one that the gridspec remembers)
            # to None, only handling that separately in the callbacks
            set_gridspec_arg_value(ba, arg_name, None)
            assert get_gridspec_arg_value(ba, arg_name) in (None, EMPTY)
            static_margin_args[arg_name] = arg_value
        # Call func() with vanilla arguments, then retrieve the gridspec
        result = func(*ba.args, **ba.kwargs)
        if postprocess is None:
            gridspec = result
            fig = gridspec.figure
        else:
            gridspec = postprocess(result)
            if is_sequence(gridspec, length=2):
                gridspec, fig = gridspec
            else:
                fig = gridspec.figure
        if not isinstance(gridspec, matplotlib.gridspec.GridSpec):
            raise TypeError(
                f'func = {func!r}, postprocess = {postprocess!r} -> '
                f'postprocess(func(...)) = {gridspec!r}: '
                'expected a gridspec'
            )
        # Update the stored static margin args, the re-resolve the
        # margins
        existing_static_margin_args = get_stored_static_margins(gridspec)
        existing_static_margin_args.update(static_margin_args)
        adjust_margins(gridspec, fig)
        has_callbacks = (
            existing_static_margin_args.setdefault('callbacks', None)
            is not None
        )
        needs_callbacks = any(
            value is not None
            for key, value in existing_static_margin_args.items()
            if key != 'callbacks'
        )
        # Manipulate the callbacks when necessary
        callbacks = fig.canvas.callbacks
        if has_callbacks and not needs_callbacks:
            for i in existing_static_margin_args.pop('callbacks'):
                callbacks.disconnect(i)
        elif not has_callbacks and needs_callbacks:
            # Note: canvas resizing is not triggered by
            # `fig.set_size_inches()`, `fig.set_figwidth()`, etc. with
            # some backends, so we also need to listen for the draw
            # event
            callback_func = functools.partial(adjust_margins, gridspec, fig)
            existing_static_margin_args['callbacks'] = tuple(
                callbacks.connect(event, callback_func)
                for event in ('resize_event', 'draw_event')
            )
        # Install the bound method for updating the gridspec at the
        # gridspec
        if install:
            gridspec.update = types.MethodType(gridspec_update, gridspec)
        return result

    def get_stored_static_margins(
        gridspec: matplotlib.gridspec.GridSpec,
    ) -> StaticMarginsParams:
        try:
            static_margin_args = getattr(gridspec, static_margins_key)
        except AttributeError:
            static_margin_args = {}
            setattr(gridspec, static_margins_key, static_margin_args)
        return static_margin_args

    def recalculate_bounds_from_margins(
        gridspec: matplotlib.gridspec.GridSpec, fig: FigureBase,
    ) -> typing.Dict[AdjustedGridspecKwarg, numbers.Real]:
        """
        Resolve the absolute margins to relative values (in figure
        fractions).
        """
        def get_bound(
            bound: Literal['left', 'right', 'bottom', 'top'],
        ) -> numbers.Real:
            if bound in results:
                return results[bound]
            return getattr(params, bound)

        if fig is None:
            raise TypeError(f'fig = {fig!r}: expected a figure, got none')
        params = gridspec.get_subplot_params()
        abs_margins: typing.Dict[AdjustedGridspecKwarg, numbers.Real] = {}
        unit_mapping: typing.Dict[
            AdjustedGridspecKwarg, typing.Dict[AllowedUnit, AllowedUnit]
        ] = {
            # Note: for convenience, we map `left='3%'` to `left='3%w'`,
            # etc.
            **dict.fromkeys(['left', 'right', 'wspace'], {'%': '%w'}),
            **dict.fromkeys(['bottom', 'top', 'hspace'], {'%': '%h'}),
        }
        for key, value in get_stored_static_margins(gridspec).items():
            if key == 'callbacks' or value is None:
                continue
            quantity, unit = _length_helper(value)
            abs_margins[key] = quantity * convert_length(
                unit_mapping.get(key, {}).get(unit, unit), fig=fig,
            )
        results = {}
        try:
            width, height = fig.get_size_inches()
        except AttributeError:  # Subfigure don't have get_size_inches()
            bottom_left, top_right = fig.transSubfigure.transform(
                [(0, 0), (1, 1)],
            )
            dpi = fig.get_dpi()
            width, height = abs(top_right - bottom_left) / dpi
        # Recalculate margins
        for length, lower, upper in [
            (width, 'left', 'right'), (height, 'bottom', 'top'),
        ]:
            if lower in abs_margins:
                results[lower] = abs_margins[lower] / length
            if upper in abs_margins:
                results[upper] = 1 - abs_margins[upper] / length
        # Recalculate spacing
        # rel_space*len_mean*(n - 1) + len_mean*n = len_grid    ...(1)
        #                      rel_space*len_mean = abs_space ...(2)
        # ->     len_grid/n - abs_space*(1 - 1/n) = len_mean
        # ->           len_grid/abs_space + 1 - n = n/rel_space
        # Compat note:
        # gridspec.{ncols|nrows} wasn't available before matplotlib
        # 3.2.0
        nrows, ncols = gridspec.get_geometry()
        for space, n, length, lower, upper in [
            ('wspace', ncols, width, 'left', 'right'),
            ('hspace', nrows, height, 'bottom', 'top'),
        ]:
            if space not in abs_margins:
                continue
            grid_length = (get_bound(upper) - get_bound(lower)) * length
            results[space] = (
                n / (grid_length / abs_margins[space] - n + 1)
            )
        return results

    def adjust_margins(
        gridspec: matplotlib.gridspec.GridSpec, fig: FigureBase,
        *_,
    ) -> None:
        """
        The change in the gridspec isn't automatically propagated to the
        subplots sometimes;
        do something about it here by manually calling `.set_position()`
        on the axes whose gridspec is `gridspec`.
        """
        type(gridspec).update(
            gridspec, **recalculate_bounds_from_margins(gridspec, fig),
        )
        for ax in fig.axes:
            try:
                ax_sps = ax.get_subplotspec()
                ax_gs = ax_sps.get_gridspec()
            except AttributeError:  # Not a subplot, no gridspec, etc.
                continue
            if ax_gs is not gridspec:
                continue
            ax.set_position(ax_sps.get_position(ax.figure))

    def set_gridspec_arg_value_raw(
        ba: inspect.BoundArguments,
        arg_name: AdjustedGridspecKwarg,
        arg_value: typing.Any,
    ) -> None:
        ba_partial = ba.signature.bind_partial(**{arg_name: arg_value})
        (bound_name, bound_value), = ba_partial.arguments.items()
        bound_param = ba.signature.parameters[bound_name]
        if bound_param.kind == bound_param.VAR_KEYWORD:
            # arg_name is mapped to the kwargs
            ba.arguments.setdefault(bound_name, {}).update(bound_value)
        else:  # arg_name is mapped to a named arg
            assert bound_name == arg_name
            assert bound_value == bound_value
            ba.arguments[bound_name] = bound_value

    def set_gridspec_arg_value_relocated(
        ba: inspect.BoundArguments,
        arg_name: AdjustedGridspecKwarg,
        arg_value: typing.Any,
    ) -> None:
        ba_partial = ba.signature.bind_partial(**{
            gridspec_kwargs_loc: {arg_name: arg_value},
        })
        (bound_name, bound_value), = ba_partial.arguments.items()
        bound_param = ba.signature.parameters[bound_name]
        if bound_param.kind == bound_param.VAR_KEYWORD:
            # gridspec_kwargs_loc is mapped to the kwargs
            kwargs = ba.arguments.setdefault(bound_name, {})
            loc = dict(kwargs.setdefault(gridspec_kwargs_loc, {}))
            kwargs[gridspec_kwargs_loc] = loc
            loc.update(bound_value[gridspec_kwargs_loc])
        else:  # gridspec_kwargs_loc is mapped to a named arg
            assert bound_name == gridspec_kwargs_loc
            assert bound_value[arg_name] == arg_value
            loc = dict(ba.arguments.setdefault(bound_name, {}))
            ba.arguments[bound_name] = loc
            loc.update(bound_value)

    static_margins_key: Keyword = '__gridspec_static_margin_args__'

    # How do we retrieve/set arguments with BoundArguments?
    sig = inspect.signature(func)
    get_gridspec_arg_value: typing.Callable[
        [inspect.BoundArguments, AdjustedGridspecKwarg],
        typing.Union[typing.Any, Literal[EMPTY]]
    ]
    if gridspec_kwargs_loc:
        def get_gridspec_arg_value(
            ba: inspect.BoundArguments, arg_name: AdjustedGridspecKwarg,
        ) -> typing.Union[typing.Any, Literal[EMPTY]]:
            gridspec_kwargs = get_argument_value(ba, gridspec_kwargs_loc)
            if gridspec_kwargs in (EMPTY, None):
                return EMPTY
            return gridspec_kwargs.get(arg_name, EMPTY)

        set_gridspec_arg_value = set_gridspec_arg_value_relocated
        sig.bind_partial(
            # Just to check that `gridspec_kwargs_loc` is an accepted
            # keyword argument
            **{gridspec_kwargs_loc: None}
        )
    else:
        get_gridspec_arg_value = get_argument_value
        set_gridspec_arg_value = set_gridspec_arg_value_raw

    # Pre-determine the available margin-specifying arguments
    compatible_gridspec_kwargs: typing.List[AdjustedGridspecKwarg] = []
    for arg_name in ADJUSTED_GRIDSPEC_KWARGS:
        try:
            get_gridspec_arg_value(sig.bind_partial(), arg_name)
        except TypeError:  # Cannot bind arg_name
            continue
        else:
            compatible_gridspec_kwargs.append(arg_name)
    if not compatible_gridspec_kwargs:
        msg = (
            'func = {}{}: '
            'cannot take any of these arguments for gridspec margins: {}'
        ).format(
            func.__name__,
            sig,
            ', '.join(f'`{a}`' for a in ADJUSTED_GRIDSPEC_KWARGS),
        )
        raise TypeError(msg)
    return wrapper


def _make_wrapped_fig_gridspec_method(
    method: typing.Callable[Concatenate[FigureBase, PS], T],
    *,
    use_instance_attr: bool = True,
    **kwargs
) -> typing.Callable[Concatenate[FigureBase, PS], T]:
    """
    Parameters
    ----------
    method()
        `Figure[Base]` method
    use_instance_attr
        Control what method object is actually called:
        True
            `getattr(fig, method.__name__)(*args, **kwargs)`
        False
            `getattr(type(fig), method.__name__)(fig, *args, **kwargs)`
    **kwargs
        See `process_static_grid_kwargs`

    Return
    ------
    Decorated `method()` with appropriate deferrence to subclass
    implementations

    Notes
    -----
    If one wants to hack `fig.<method>` by overriding the bound method
    created by the class-level descriptors with a callable object
    directly assigned to the figure's `.__dict__`, note that one MUST
    use `use_instance_attr = False` so that the returned wrapper method
    knows not to get the implementation from the instance directly
    (which would result in recursion).

    >>> from contextlib import nullcontext
    >>> from types import MethodType
    >>>
    >>> from matplotlib import pyplot as plt
    >>> from matplotlib.figure import Figure
    >>>
    >>> add_gridspec_1, add_gridspec_2 = (
    ...     _make_wrapped_fig_gridspec_method(
    ...         Figure.add_gridspec,
    ...         use_instance_attr=use_instance_attr,
    ...     )
    ...     for use_instance_attr in (False, True)
    ... )
    >>> with (plt.ioff() or nullcontext()):
    ...     for i, add_gs_impl in [
    ...         (1, add_gridspec_1), (2, add_gridspec_2),
    ...     ]:
    ...         try:
    ...             fig = plt.figure()
    ...             fig.add_gridspec = MethodType(add_gs_impl, fig)
    ...             try:
    ...                 gs = fig.add_gridspec(2, 2, left='3 cm')
    ...             except RecursionError:
    ...                 print('fig', i, 'recursion error')
    ...                 plt.close(fig)
    ...                 continue
    ...             except Exception as e:
    ...                 looks_like_recursion_error = False
    ...                 while True:
    ...                     msg, *_ = e.args
    ...                     if (
    ...                         isinstance(e, RecursionError)
    ...                         or 'RecursionError' in msg
    ...                     ):
    ...                         print('fig', i, 'recursion error')
    ...                         looks_like_recursion_error = True
    ...                         break
    ...                     e = getattr(e, '__cause__', None)
    ...                     if e is None:
    ...                         break
    ...                 if not looks_like_recursion_error:
    ...                     raise
    ...             else:
    ...                 print('fig', i, 'ok')
    ...         finally:
    ...             plt.close(fig)
    fig 1 ok
    fig 2 recursion error
    """
    @process_static_grid_kwargs(**kwargs)
    @functools.wraps(method)
    @check(fig=is_instance(FigureBase))
    def wrapper(fig: FigureBase, *args, **kwargs):
        if use_instance_attr:
            return getattr(fig, method_name)(*args, **kwargs)
        return getattr(type(fig), method_name)(fig, *args, **kwargs)

    method_name = method.__name__
    return wrapper


def _get_gridspec_and_figure_from_subplot_array(
    subplots: np.ndarray,
) -> typing.Tuple[matplotlib.gridspec.GridSpec, FigureBase]:
    # Note: pre-v3.3, `Figure.subplots()` may create a gridspec with
    # `.figure` set to none, so we need to explicitly return also the
    # figure in postprocess()
    if callable(getattr(subplots, 'flatten', None)):
        ax, *_ = subplots.flatten()
    else:  # For dealing with fig_subplots(1, 1, squeeze=True)
        ax = subplots
    return ax.get_subplotspec().get_gridspec(), ax.figure


def _get_gridspec_from_subplot_dict(
    subplots: typing.Mapping[typing.Hashable, matplotlib.axes.Axes],
) -> matplotlib.gridspec.GridSpec:
    ax, *_ = subplots.values()
    return ax.get_subplotspec().get_gridspec()


# ** Higher-level API (drop-in replacements of matplotlib methods) *** #

# Note: not an exhaustive list of all the methods which create/use a
# gridspec, but these should cover the common use-cases;
# users should take these as inspirations and define their own functions
# if needed


@functools.wraps(matplotlib.gridspec.GridSpec.update)
@check(gridspec=is_instance(matplotlib.gridspec.GridSpec))
def gridspec_update(
    gridspec: matplotlib.gridspec.GridSpec, *args, **kwargs
):
    GridSpecSubclass = type(gridspec)

    @process_static_grid_kwargs
    @functools.wraps(GridSpecSubclass.update)
    def inner_update(self, *args, **kwargs) -> matplotlib.gridspec.GridSpec:
        """
        We need this extra layer of redirection because
        `GridSpec.update()` doesn't return anything.
        """
        GridSpecSubclass.update(self, *args, **kwargs)
        return self

    inner_update(gridspec, *args, **kwargs)


make_gridspec = process_static_grid_kwargs(matplotlib.gridspec.GridSpec)
fig_add_gridspec = (
    _make_wrapped_fig_gridspec_method(FigureBase.add_gridspec)
)
fig_subplots = _make_wrapped_fig_gridspec_method(
    FigureBase.subplots,
    postprocess=_get_gridspec_and_figure_from_subplot_array,
    gridspec_kwargs_loc='gridspec_kw',
)
if hasattr(FigureBase, 'subplot_mosaic'):
    fig_subplot_mosaic = _make_wrapped_fig_gridspec_method(
        FigureBase.subplot_mosaic,
        postprocess=_get_gridspec_from_subplot_dict,
        gridspec_kwargs_loc='gridspec_kw',
    )
else:
    __all__ = tuple(name for name in __all__ if name != 'fig_subplot_mosaic')
