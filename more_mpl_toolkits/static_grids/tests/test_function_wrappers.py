"""
Tests for the function wrappers defined in the main file
"""
import inspect
import typing

import matplotlib
import pytest
from matplotlib import pyplot as plt

from .. import (
    convert_length,
    gridspec_update, make_gridspec,
    fig_add_gridspec, fig_subplots,
)
try:
    from .. import fig_subplot_mosaic
except ImportError:
    fig_subplot_mosaic = None

StringLength = typing.TypeVar('StringLength', bound=str)

DPI = 200
WIDTH = 8  # Inches
HEIGHT = 6  # Inches


def force_redraw(fig: matplotlib.figure.Figure) -> None:
    fig.draw(fig.canvas.get_renderer())


def check_adjacent_axes_hspacing(
    ax1: matplotlib.axes.Axes,
    ax2: matplotlib.axes.Axes,
    hspacing: StringLength,
) -> None:
    """
    Check the horizontal spacing (i.e. `wspace`) between two axes
    objects adjacent on a grid.
    """
    fig = ax1.figure
    assert ax2.figure is fig
    force_redraw(fig)
    box1, box2 = (ax.get_window_extent() for ax in (ax1, ax2))
    left_ax_right_edge = min(box.intervalx.max() for box in (box1, box2))
    right_ax_left_edge = max(box.intervalx.min() for box in (box1, box2))
    hspacing_in_px = convert_length(
        hspacing + ('w' if hspacing.endswith('%') else ''), 'px', fig=fig,
    )
    assert pytest.approx(hspacing_in_px) == (
        right_ax_left_edge - left_ax_right_edge
    )


def check_adjacent_axes_vspacing(
    ax1: matplotlib.axes.Axes,
    ax2: matplotlib.axes.Axes,
    vspacing: StringLength,
) -> None:
    """
    Check the vertical spacing (i.e. `hspace`) between two axes
    objects adjacent on a grid.
    """
    fig = ax1.figure
    assert ax2.figure is fig
    force_redraw(fig)
    box1, box2 = (ax.get_window_extent() for ax in (ax1, ax2))
    bottom_ax_top_edge = min(box.intervaly.max() for box in (box1, box2))
    top_ax_bottom_edge = max(box.intervaly.min() for box in (box1, box2))
    vspacing_in_px = convert_length(
        vspacing + ('h' if vspacing.endswith('%') else ''), 'px', fig=fig,
    )
    assert pytest.approx(vspacing_in_px) == (
        top_ax_bottom_edge - bottom_ax_top_edge
    )


def check_gridspec(
    gridspec: matplotlib.gridspec.GridSpec,
    fig: typing.Optional[matplotlib.figure.Figure] = None,
    *,
    left: typing.Optional[StringLength] = None,
    right: typing.Optional[StringLength] = None,
    bottom: typing.Optional[StringLength] = None,
    top: typing.Optional[StringLength] = None,
    wspace: typing.Optional[StringLength] = None,
    hspace: typing.Optional[StringLength] = None,
) -> None:
    """
    Check the grid spec.
    """
    lower_margins = 'left', 'bottom'
    upper_margins = 'right', 'top'
    inner_margins = 'wspace', 'hspace'
    horizontal_specs = {
        key: value + ('w' if value.endswith('%') else '')
        for key, value in dict(
            left=left, right=right, wspace=wspace,
        ).items()
        if value is not None
    }
    vertical_specs = {
        key: value + ('h' if value.endswith('%') else '')
        for key, value in dict(
            bottom=bottom, top=top, hspace=hspace,
        ).items()
        if value is not None
    }
    if fig is None:
        fig = gridspec.figure
    nrows, ncols = gridspec.get_geometry()
    force_redraw(fig)
    gridspec_params = gridspec.get_subplot_params()
    for specs, fig_dim_inches, subplots_frac_space, n in [
        (
            horizontal_specs,
            fig.get_figwidth(),
            gridspec_params.right - gridspec_params.left,
            ncols,
        ),
        (
            vertical_specs,
            fig.get_figheight(),
            gridspec_params.top - gridspec_params.bottom,
            nrows,
        ),
    ]:
        for attr, value in specs.items():
            frac_value = getattr(gridspec_params, attr)
            value_inches = convert_length(value, 'in', fig=fig)
            if attr in lower_margins:
                assert pytest.approx(
                    frac_value * fig_dim_inches
                ) == value_inches
            elif attr in upper_margins:
                assert pytest.approx(
                    (1 - frac_value) * fig_dim_inches
                ) == value_inches
            elif attr in inner_margins:
                subplots_space_inches = subplots_frac_space * fig_dim_inches
                spacing_inches = (
                    subplots_space_inches * frac_value
                    / (n + (n - 1) * frac_value)
                )
                assert pytest.approx(spacing_inches) == value_inches
            else:
                assert False


@pytest.fixture
def fig() -> matplotlib.figure.Figure:
    fig = plt.figure(figsize=(WIDTH, HEIGHT))
    fig.set_dpi(DPI)
    yield fig
    plt.close(fig)


def test_check_gridspec_update_installation(
    fig: matplotlib.figure.Figure,
) -> None:
    init_margins, new_margins = (
        dict.fromkeys(('left', 'right'), value)
        for value in ('.5 in', '2 cm')
    )
    gridspec = make_gridspec(2, 3, fig, **init_margins)
    check_gridspec(gridspec, fig, **init_margins)
    gridspec.update(**new_margins)
    check_gridspec(gridspec, fig, **new_margins)


def test_gridspec_update(fig: matplotlib.figure.Figure) -> None:
    margins = dict(left='1 cm', top='50 px', wspace='.5 in')
    gridspec = fig.add_gridspec(3, 4)
    gridspec_update(gridspec, **margins)
    check_gridspec(gridspec, fig, **margins)


def test_make_gridspec(fig: matplotlib.figure.Figure) -> None:
    margins = dict(bottom='100 pt', right='20 mm', hspace='100 px')
    gridspec = make_gridspec(2, 3, fig, **margins)
    check_gridspec(gridspec, fig, **margins)


def test_fig_add_gridspec(fig: matplotlib.figure.Figure) -> None:
    margins = dict(top='15%', bottom='20%', hspace='5%')
    gridspec = fig_add_gridspec(fig, 2, 2, **margins)
    check_gridspec(gridspec, fig, **margins)


def test_fig_subplots(fig: matplotlib.figure.Figure) -> None:
    margins = {
        **dict.fromkeys(('left', 'right', 'bottom', 'top'), '.5 in'),
        **dict.fromkeys(('hspace', 'wspace'), '.01 m'),
    }
    orig_margins = dict(margins)
    subplots = fig_subplots(fig, 2, 2, gridspec_kw=margins)
    gridspec = subplots[0, 0].get_subplotspec().get_gridspec()
    check_adjacent_axes_hspacing(
        subplots[0, 0], subplots[0, 1], margins['wspace'],
    )
    check_adjacent_axes_vspacing(
        subplots[0, 0], subplots[1, 0], margins['hspace'],
    )
    check_gridspec(gridspec, fig, **margins)
    # Check again after resizing
    fig.set_figwidth(10)
    fig.set_figheight(7)
    check_gridspec(gridspec, fig, **margins)
    check_adjacent_axes_hspacing(
        subplots[0, 0], subplots[0, 1], margins['wspace'],
    )
    check_adjacent_axes_vspacing(
        subplots[0, 0], subplots[1, 0], margins['hspace'],
    )
    # Check that we haven't inadvertantly clobbered the values in
    # `margins`
    assert margins == orig_margins
    # Addition: test that fig_subplots(squeeze=True) is working as
    # expected
    fig.clear()
    new_subplot = fig_subplots(
        fig, 1, 1, gridspec_kw=margins, squeeze=True,
    )
    assert isinstance(new_subplot, matplotlib.axes.Axes)
    new_gridspec = new_subplot.get_subplotspec().get_gridspec()
    check_gridspec(new_gridspec, fig, **margins)


@pytest.mark.skipif(
    fig_subplot_mosaic is None,
    reason='`matplotlib` too old, `fig.subplot_mosaic()` not available',
)
def test_fig_subplot_mosaic(fig: matplotlib.figure.Figure) -> None:
    margins = dict(left='1.5 in', hspace='.6 cm', wspace='5%')
    orig_margins = dict(margins)
    # Note: the API of `subplot_mosaic()` was being fleshed out over the
    # `matplotlib` versions, so some arguments are absent from earlier
    # versions
    sig = inspect.signature(fig.subplot_mosaic)
    kwargs = dict(gridspec_kw=margins)
    for arg_name, arg_value in dict(
        width_ratios=[1, 1, 2],
    ).items():
        try:
            sig.bind_partial(**{arg_name: arg_value})
        except TypeError:
            continue
        kwargs[arg_name] = arg_value
    layout = """
    AAB
    CDB
    ED.
    """
    subplots = fig_subplot_mosaic(fig, layout, **kwargs)
    gridspec = subplots['A'].get_subplotspec().get_gridspec()
    assert gridspec.get_geometry() == (3, 3)
    check_adjacent_axes_hspacing(
        subplots['A'], subplots['B'], margins['wspace'],
    )
    check_adjacent_axes_vspacing(
        subplots['C'], subplots['E'], margins['hspace'],
    )
    check_gridspec(gridspec, fig, **margins)
    # Check that we haven't inadvertantly clobbered the values in
    # `margins`
    assert margins == orig_margins
