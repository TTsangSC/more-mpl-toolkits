`more_mpl_toolkits.static_grids`
================================

(Alias(es): `~.sg`)

Tools for creating and maintaining layout grids with
explicitly-specified physical margins,
serving as an alternative to:

- The vanilla [`matplotlib.gridspec.GridSpec`][gridspec],
  which works in fractions of the figure (`left`, `right`, `bottom`,
  `top`) and subplot (`hspace`, `wspace`) dimensions; and
- [Layout-engine-based][layout-engine] approaches.

Available units
---------------

- Physical units
  - 'mm', 'cm', 'm', 'in', 'ft', 'pt'
    - 1 in = 2.54 cm = 72 pt
  - 'px': number of dots/pixels
- Special units
  - '%h', '%w': % height/width of the figure
  - '%' is automatically aliased to either when appropriate

Example
-------

```python
import matplotlib.pyplot as plt

from more_mpl_toolkits.static_grids import fig_subplots, convert_length

fig = plt.figure(figsize=(8., 6.))  # Inches
fig.set_dpi(DPI)

subplots = fig_subplots(
    fig, 3, 2,
    gridspec_kw={
        # Ext. margins
        **dict.fromkeys('left right top bottom'.split(), '1. in'),
        # Int. margins
        **dict.fromkeys(('wspace', 'hspace'), '2 cm'),
    },
)

# Even after resizing the figure, the margins stay the same sizes
fig.set_figwidth(10)
fig.set_figheight(8)
```

[gridspec]: https://matplotlib.org/stable/api/_as_gen/matplotlib.gridspec.GridSpec.html#matplotlib.gridspec.GridSpec
[layout-engine]: https://matplotlib.org/stable/api/layout_engine_api.html
