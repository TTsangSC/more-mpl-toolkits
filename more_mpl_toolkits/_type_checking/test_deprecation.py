"""
Test whether the deprecation is working correctly.
"""
import importlib

import pytest

import tjekmate

REAL_ATTR = 'is_instance'
FAKE_ATTR = 'is_foo'
TC_MODULE = (lambda: None).__module__.rpartition('.')[0]


def test_import_real_attr() -> None:
    with pytest.warns(FutureWarning, match='deprecated.+migrate to tjekmate'):
        exec(f'from {TC_MODULE} import {REAL_ATTR}')
        value_1 = locals()[REAL_ATTR]
        value_2 = getattr(importlib.import_module(TC_MODULE), REAL_ATTR)
    value_3 = getattr(tjekmate, REAL_ATTR)
    assert value_1 is value_2 is value_3


def test_import_fake_attr() -> None:
    with pytest.raises(ImportError, match=FAKE_ATTR):
        exec(f'from {TC_MODULE} import {FAKE_ATTR}')
    with pytest.raises(AttributeError, match=FAKE_ATTR):
        getattr(importlib.import_module(TC_MODULE), FAKE_ATTR)
