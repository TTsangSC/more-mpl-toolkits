"""
Type checking utilities (deprecated).

Notes
-----
To be removed by version v1.10 or v2.0, whichever is sooner.
"""
import importlib
import typing
import warnings
try:
    from importlib.metadata import distribution
except ImportError:
    from importlib_metadata import distribution

import tjekmate


def __getattr__(attr: str) -> typing.Any:
    """
    Import the required name from `tjekmate` and show a `FutureWarning`.
    """
    this_module = __getattr__.__module__
    src_module = tjekmate.__name__
    if attr not in tjekmate.__all__:
        module_obj = importlib.import_module(this_module)
        msg = 'module {!r} has no attribute {!r}'.format(module_obj, attr)
        error = AttributeError(msg)
        for xc_attr, xc_value in dict(name=attr, obj=this_module).items():
            try:
                setattr(error, xc_attr, xc_value)
            except Exception:
                pass
        raise error
    value = getattr(tjekmate, attr)
    msg = (
        '{0}.{1}: Loading type-checking utilities from `{0}` is deprecated and '
        'will result in errors in the future; '
        'migrate to {2} (URL(s): {3}) to avoid this'
    ).format(
        __getattr__.__module__,
        attr,
        src_module,
        distribution(src_module).metadata.get_all('Project-URL', 'n/a'),
    )
    warnings.warn(msg, FutureWarning)
    return value
