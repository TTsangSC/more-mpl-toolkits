`more_mpl_toolkits.aligned_annotations`
=======================================

(Alias(es): `~.aa`)

[`Axes.annotate()`][axes-annotate] with helpful defaults and shorthands
for the annotation position:
```python
import matplotlib.pyplot as plt

from more_mpl_toolkits import aligned_annotations as aa, rays

ax = plt.gca()
ax.set_xlim((-1, 1))
ax.set_ylim((-1, 1))

fixed_annotation = aa.annotate_aligned(
    ax,
    'This annotation\nis fixed to the right of the plot',
    'center right',
    rotation=90,
    # # The arguments below (among others) are automatically set:
    # rotation_mode='anchor',
    # # Rotated text vertically centered
    # ha='center',
    # # Right edge of the rotated text lies flush w/right spine
    # # (w/padding)
    # va='bottom',
)

line = rays.axline(  # Intersects the axes bounds at (-1, 1), (1, 0)
    ax, (0, .5), slope=-.5,
)
moving_annotation = aa.annotate_aligned(
    ax,
    'This annotation\nmoves with the line',
    xy='line',  # Keep above center of the line segment in the viewlims
    va='bottom',
    rotation='line',  # Keep parallel with the line
    axline=line,
)

ax.set_xlim((1, 2))  # Intercepts now at (1, .5), (2, 0)
# `moving_annotation` still visible because it is dynamically
# repositioned
```

[axes-annotate]: https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.annotate.html
