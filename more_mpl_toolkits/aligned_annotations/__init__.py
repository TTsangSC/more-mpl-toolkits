"""
Create annotations that are aligned against the plot area or a line.

API
---
resolve_position_defaults()
    Helper function for generating suitable arguments for
    `matplotlib.axes.Axes.annotate()`

annotate_aligned()
    Conveniently draw (rotated) annotations that are aligned against
    plot-area edges, data lines, etc.
"""
import functools
import inspect
import itertools
import math
import numbers
import operator
import typing
import warnings
try:
    from typing import Literal
except ImportError:
    from typing_extensions import Literal

import matplotlib.axes
import matplotlib.cbook
import matplotlib.lines
import matplotlib.text
import numpy as np

from tjekmate import (
    identical_to, is_in, is_instance, items_fulfill,
    check_attr, check_call, takes_positional_args, check, logical,
)

from .._utils.decorators import replace_in_docs
from ..rays import Ray

__all__ = (
    'resolve_position_defaults',
    'annotate_aligned',
)

_THIS_MODULE_ = (lambda: None).__module__
if _THIS_MODULE_.endswith('.__init__'):
    _THIS_MODULE_, *_ = _THIS_MODULE_.rpartition('.')
_PACKAGE_, *_ = _THIS_MODULE_.rpartition('.')

BORDER_PADDING = .25  # Font-size fraction
_BORDER_PADDINGS = [BORDER_PADDING, 0, -BORDER_PADDING]

SPECIAL_POSITIONS = {
    f'{vname}_{hname}': dict(
        xy=(i_x*.5, i_y*.5),
        xycoords='axes fraction',
        ha=hname,
        va=vname,
        # Add some padding so that the annotation doesn't lie flush
        # against the plot area bounds
        xytext=(_BORDER_PADDINGS[i_x], _BORDER_PADDINGS[i_y]),
        textcoords='offset fontsize',
    )
    for (i_x, hname), (i_y, vname) in itertools.product(
        enumerate(('left', 'center', 'right')),
        enumerate(('bottom', 'center', 'top')),
    )
}
for _name, _alias in dict(
    center_center='center',
    top_center='top',
    bottom_center='bottom',
    center_left='left',
    center_right='right',
).items():
    SPECIAL_POSITIONS[_alias] = SPECIAL_POSITIONS[_name]
for _name, _value in list(SPECIAL_POSITIONS.items()):
    if '_' not in _name:
        continue
    chunks = _name.split('_')
    # 'top_right' = 'right_top' = 'top right' = 'right top', etc.
    SPECIAL_POSITIONS['_'.join(chunks[::-1])] = _value
    SPECIAL_POSITIONS[' '.join(chunks)] = _value
    SPECIAL_POSITIONS[' '.join(chunks[::-1])] = _value
del _name, _value

ALIGN_LINE_DEFAULTS = dict(
    rotation=dict(rotation_mode='anchor'),
    xy=dict(ha='center', va='center'),
)

_ALLOWED_AXES_DESIGNATIONS = 'x', 'y'
ALLOWED_AXES_DESIGNATIONS = (
    *_ALLOWED_AXES_DESIGNATIONS, 'xy', 'none', None, True, False,
)

AnnotationPosition = Literal[tuple(sorted(SPECIAL_POSITIONS))]
Point2D = typing.Tuple[numbers.Real, numbers.Real]
AxesWithFigure = typing.TypeVar('AxesWithFigure', bound=matplotlib.axes.Axes)
Line2DTwoDataPoints = typing.TypeVar(
    'Line2DTwoDataPoints', bound=matplotlib.lines.Line2D,
)
Axline = typing.Union[
    Line2DTwoDataPoints,
    typing.Tuple[Point2D, Point2D],
    typing.Tuple[Point2D, numbers.Real],
]
_AllowedAxesDesignations = Literal[_ALLOWED_AXES_DESIGNATIONS]
AllowedAxesDesignations = Literal[ALLOWED_AXES_DESIGNATIONS]

AnnotateLike = typing.Callable[
    [matplotlib.axes.Axes, str, typing.Tuple[numbers.Real, numbers.Real]],
    matplotlib.text.Annotation
]

_is_real = is_instance(numbers.Real) & ~logical(math.isnan)
_is_real_pair = items_fulfill(_is_real, _is_real)
_is_line2d_with_two_points = (
    check_call(('get_xdata', _is_real_pair))
    & check_call(('get_ydata', _is_real_pair))
)


_replace_in_docs = replace_in_docs(
    _THIS_MODULE_=_THIS_MODULE_, _PACKAGE_=_PACKAGE_,
)


def _rotate(
    xy: Point2D,
    degree: typing.Optional[numbers.Real] = None,
) -> np.ndarray:
    """Simple 2-d rotation."""
    if degree:
        rad = degree * math.pi / 180
        c, s = math.cos(rad), math.sin(rad)
    else:
        c, s = 1., 0.
    return np.array([[c, -s], [s, c]]).dot(xy)


@check(
    axline=(
        items_fulfill(
            _is_real_pair, (is_instance(numbers.Real) | _is_real_pair),
        )
        | _is_line2d_with_two_points
    ),
)
@_replace_in_docs
def _resolve_slope_and_anchors_from_axline(
    ax: matplotlib.axes.Axes,
    axline: Axline,
) -> typing.Tuple[numbers.Real, Point2D, Point2D]:
    """
    Example
    -------
    >>> from matplotlib import pyplot as plt
    >>> from numpy import isclose, allclose
    >>> from _PACKAGE_.rays import Ray

    Resolution from `matplotlib.lines.Line2D`:

    >>> try:
    ...     ax = plt.gca()
    ...     line = ax.add_line(Ray.make((0, 1), (2, 2)))
    ...     slope, *_ = _resolve_slope_and_anchors_from_axline(ax, line)
    ...     assert isclose(slope, .5)
    ... finally:
    ...     plt.close(ax.figure)

    Resolution from two points:

    >>> slope, (x1, y1), (x2, y2) = (
    ...     _resolve_slope_and_anchors_from_axline(ax, [(3, 5), (2, 7)])
    ... )
    >>> assert allclose([slope, (y2 - y1) / (x2 - x1)], -2.)

    Resolution from a point and a slope:

    >>> slope, (x1, y1), (x2, y2) = (
    ...     _resolve_slope_and_anchors_from_axline(ax, [(3, 5), -2])
    ... )
    >>> assert allclose([slope, (y2 - y1) / (x2 - x1)], -2.)
    """
    if isinstance(axline, matplotlib.lines.Line2D):
        ax.figure.draw(
            # Note: force draw to consolidate the dimensions, or the
            # resolved values will be wonky
            ax.figure.canvas.get_renderer()
        )
        get_anchors_in_data_coords = (
            axline.get_transform() + ax.transData.inverted()
        ).transform
        xy1, xy2 = get_anchors_in_data_coords(
            list(zip(*axline.get_data()))
        )
        slope = None
    else:
        xy1, xy2 = axline
        if isinstance(xy2, numbers.Real):
            xy2, slope = None, xy2
        else:
            slope = None
    ray = Ray.make(
        tuple(xy1), (None if xy2 is None else tuple(xy2)), slope=slope,
    )
    return (ray.slope, *ray.anchors)


@check(
    xy=(_is_real_pair | is_in(SPECIAL_POSITIONS)),
    rotation=(identical_to(None) | is_instance(numbers.Real)),
    rotation_mode=is_in((None, 'default', 'anchor')),
    outer=is_in(ALLOWED_AXES_DESIGNATIONS),
    annotate_method=(identical_to(None) | takes_positional_args(3)),
)
def resolve_position_defaults(
    xy: typing.Union[Point2D, AnnotationPosition],
    *args,
    rotation: typing.Optional[numbers.Real] = None,
    rotation_mode: typing.Optional[Literal['default', 'anchor']] = None,
    outer: AllowedAxesDesignations = None,
    annotate_method: typing.Optional[AnnotateLike] = None,
    **kwargs
) -> typing.Dict[str, typing.Any]:
    """
    Helper function for placing an annotation at a predefined position.

    Parameters
    ----------
    xy
        Coordinates to annotate;
        alternatively, a string specification for where to put the
        annotation (e.g. 'center', 'bottom right');
        if used, the arguments `xy`, `xytext`, `ha`, `va`, and
        `textcoords` are replaced by sensible, `rotation`-sensitive
        defaults aligning the text against said corner, edge, or center
    rotation
        Optional angle in degrees to rotate the text by (default 0)
    rotation_mode
        Optional rotation mode (see
        `matplotlib.text.Annotation.set_rotation_mode()`);
        note that the default is now 'anchor' and other values may mess
        with the alignment
    outer
        Whether to draw a plot-area-aligned annotation (i.e.
        `xy = <special_str_spec>`) on the inside (if false) or the
        outside (if true) of the plot area:
        True or 'xy'
            Put the annotation on the outside in all directions
        None or False or 'none' (default)
            Put the annotation on the inside in all directions
        'x', 'y'
            Only put the annotation on the outside in the specified
            direction
    annotate_method()
        Callable with a signature compatible with
        `matplotlib.axes.Axes.annotate(ax, text, xy[, ...])`
        (also the default), returning the appropriate
        `matplotlib.text.Annotation` object
    *args, **kwargs
        Same meaning as in `annotate_method()`

    Return
    ------
    Dictionary of keyword arguments for `annotate_method()`

    Example
    -------
    >>> get_ha_va = lambda processed_kwargs: (
    ...     processed_kwargs['ha'], processed_kwargs['va']
    ... )
    >>> get_ha_va(resolve_position_defaults((0, 0)))  # Defaults
    ('left', 'baseline')
    >>> get_ha_va(resolve_position_defaults('right'))  # Special pos.
    ('right', 'center')
    >>> get_ha_va(  # Special position with rotated defaults
    ...     resolve_position_defaults('bottom', rotation=90)
    ... )
    ('left', 'center')
    >>> get_ha_va(  # Special position + put on the outside
    ...     resolve_position_defaults('bottom', rotation=90, outer=True)
    ... )
    ('right', 'center')

    Notes
    -----
    `outer` is only applied when `xy` specifies an edge or a corner;
    it is otherwise silently ignored.
    """
    rotation90: Literal[0, 1, 2, 3]
    text_defaults = dict(
        ha='left',
        va='baseline',
    )
    is_default_position = (
        isinstance(xy, str) and xy in SPECIAL_POSITIONS
    )
    # Bind arguments
    default_annotate = matplotlib.axes.Axes.annotate
    dummy_bound_annotate = functools.partial(
        default_annotate if annotate_method is None else annotate_method,
        object(),
    )
    ba = inspect.signature(dummy_bound_annotate).bind('', xy, *args, **kwargs)
    # Backwards compatibility: they changed the param name of the first
    # param between 3.2 (s) and 3.3 (text)
    text_param_name, *_ = ba.signature.parameters
    kwargs = {
        # Named args (except for `text`)
        varname: value for varname, value in ba.arguments.items()
        if varname not in (text_param_name,)
        if (
            ba.signature.parameters[varname].kind is not
            inspect.Parameter.VAR_KEYWORD
        )
    }
    *_, last_param = ba.signature.parameters.values()
    if last_param.kind is last_param.VAR_KEYWORD:
        kwargs.update(ba.arguments.get(last_param.name, {}))
    if is_default_position:
        kwargs.pop('xy')
    # Apply defaults for alignments and position
    if rotation_mode is None:
        rotation_mode = 'anchor'
    if is_default_position:
        more_kwargs = dict(SPECIAL_POSITIONS.get(xy, {}))
        overlap = set(more_kwargs) & set(kwargs)
        if overlap:
            msg = '{}: overridden by xy = {!r}'.format(
                ', '.join(f'{k} = {kwargs[k]!r}' for k in overlap),
                xy,
            )
            warnings.warn(msg)
    else:
        more_kwargs = {}
        for key, value in text_defaults.items():
            kwargs.setdefault(key, value)
    for abbr_name, long_name in (
        ('va', 'verticalalignment'), ('ha', 'horizontalalignment'),
    ):
        if long_name in kwargs:
            kwargs[abbr_name] = kwargs.pop(long_name)
    # Figure out how to align the annotations
    if rotation:
        rotation90 = round((rotation / 90) % 4)
        if rotation90 < 0:
            rotation90 += 4
        assert rotation90 in range(4)
        rotation90 = int(rotation90)  # Compatibility with older numpy
    else:
        rotation90 = 0
    new_orientations: typing.Dict[str, str] = dict(center='center')
    orientations = 'right', 'top', 'left', 'bottom'
    new_orientations.update(
        (old, new)
        for old, new in zip(
            orientations, np.roll(orientations, rotation90),
        )
    )
    ha, va = np.roll(['ha', 'va'], rotation90)
    if outer in (None, False, 'none'):
        reflect_hori, reflect_vert = False, False
    elif outer in (True, 'xy'):
        reflect_hori, reflect_vert = True, True
    else:
        reflect_hori, reflect_vert = (xy in outer for xy in 'xy')
    if is_default_position:  # Rotate the positional presets
        # 1. Reflect `outer`-ed alignments and offsets
        for i, (
            is_reflected, alignment_key, opposite_alignments,
        ) in enumerate([
            (reflect_hori, 'ha', ('left', 'right')),
            (reflect_vert, 'va', ('bottom', 'top')),
        ]):
            if not is_reflected:
                continue
            alignment = more_kwargs.get(alignment_key)
            if alignment not in opposite_alignments:
                continue
            more_kwargs[alignment_key] = next(
                oa for oa in opposite_alignments if oa != alignment
            )
            if 'xytext' in more_kwargs:
                xytext = list(more_kwargs['xytext'])
                xytext[i] *= -1
                more_kwargs['xytext'] = tuple(xytext)
        # 2. Resolve the rotated alignments
        more_kwargs[ha], more_kwargs[va] = (
            new_orientations[more_kwargs[a]] for a in ('ha', 'va')
        )
    kwargs.update(
        more_kwargs,
        rotation=rotation,
        rotation_mode=rotation_mode,
    )
    return kwargs


@check(
    **dict.fromkeys(('slope', 'fraction'), is_instance(numbers.Real)),
    anchor=_is_real_pair,
    xylims=(
        is_instance(matplotlib.axes.Axes)
        | items_fulfill(_is_real_pair, _is_real_pair)
    ),
)
def get_line_center_in_frame(
    xylims: typing.Union[
        matplotlib.axes.Axes, typing.Tuple[Point2D, Point2D],
    ],
    slope: numbers.Real,
    anchor: Point2D,
    fraction: numbers.Real = .5,
) -> Point2D:
    """
    Get the center of the segment of a line within the frame.

    Parameters
    ----------
    xylims
        2-by-2 real array in the form `[[xmin, xmax], [ymin, ymax]]` or
        an `Axes` object (from which the axis limits are taken)
    slope, anchor
        Real parameters (in data coordinates) for the line in question
    fraction
        Real fraction along the line segment to go down:
        Line vertical (i.e. `slope` infinite)
            0, 1 = 'bottom', 'top'
        Otherwise
            0, 1 = 'left', 'right'

    Return
    ------
    Midpoint of (or any other specified point along) the line segment in
    data coordinates

    Notes
    -----
    If the line doesn't intersect the bounding box, said point along the
    lower-left--upper-right diagonal is returned as a fallback.
    """
    T = typing.TypeVar('T')
    Sortable = typing.TypeVar('Sortable')

    def get_min_and_max(
        items: typing.Collection[T],
        *,
        key: typing.Optional[typing.Callable[[T], Sortable]] = None,
    ) -> typing.Tuple[T, T]:
        try:
            min_value, *_, max_value = sorted(items, key=key)
        except ValueError:  # Not enough value to unpack
            pass
        else:
            return min_value, max_value
        value, = items
        return value, value

    if isinstance(xylims, matplotlib.axes.Axes):
        (xl1, xl2), (yl1, yl2) = xylims.get_xlim(), xylims.get_ylim()
    else:
        (xl1, xl2), (yl1, yl2) = xylims
    if xl1 > xl2:
        xl1, xl2 = xl2, xl1
    if yl1 > yl2:
        yl1, yl2 = yl2, yl1
    xa, ya = anchor
    points: typing.List[Point2D] = []
    if np.isfinite(slope):
        points.extend((x, slope * (x - xa) + ya) for x in (xl1, xl2))
        pair_sort_order = 1
    else:  # Vertical line, order points by the y-coordinates
        pair_sort_order = -1
    if slope:
        points.extend(((y - ya) / slope + xa, y) for y in (yl1, yl2))
    points = [(x, y) for x, y in points if xl1 <= x <= xl2 if yl1 <= y <= yl2]
    if points:
        (x_start, y_start), (x_end, y_end) = get_min_and_max(
            points, key=operator.itemgetter(slice(None, None, pair_sort_order)),
        )
        return (
            x_start + (x_end - x_start) * fraction,
            y_start + (y_end - y_start) * fraction,
        )
    # Fallback
    return (xl1 + (xl2 - xl1) * fraction, yl1 + (yl2 - yl1) * fraction)


@check(ax=is_instance(matplotlib.axes.Axes), slope=is_instance(numbers.Real))
def get_on_screen_angle(
    ax: matplotlib.axes.Axes,
    slope: numbers.Real,
) -> numbers.Real:
    """
    Get the on-screen angle of a line of a given slope (in data
    coordinates).

    Parameters
    ----------
    ax
        Axes
    slope
        Real slope in data coordinates

    Return
    ------
    Angle in degrees (in display coordinates) the line is at from the
    horizontal
    """
    if np.isinf(slope):
        xy = (0, np.copysign(1, slope))
    else:
        xy = (1, slope)
    return _get_on_screen_angle_from_anchors((0, 0), xy)


def _get_on_screen_angle_from_anchors(
    ax: matplotlib.axes.Axes, anchor1: Point2D, anchor2: Point2D,
) -> numbers.Real:
    dx, dy = operator.sub(*ax.transData.transform([anchor2, anchor1]))
    return np.arctan2(dy, dx) * 180 / np.pi


@check(
    ax=(
        is_instance(matplotlib.axes.Axes)
        & check_attr('figure', checker=~identical_to(None))
    ),
    text=is_instance(str),
    xy=(_is_real_pair | is_in((*SPECIAL_POSITIONS, 'line'))),
    fraction=is_instance(numbers.Real),
    rotation=(is_in(('line', None)) | is_instance(numbers.Real)),
    annotate_method=(identical_to(None) | takes_positional_args(3)),
)
@_replace_in_docs
def annotate_aligned(
    ax: AxesWithFigure,
    text: str,
    xy: typing.Union[Point2D, AnnotationPosition, Literal['line']],
    *,
    axline: typing.Optional[Axline] = None,
    fraction: numbers.Real = .5,
    rotation: typing.Optional[
        typing.Union[numbers.Real, Literal['line']]
    ] = None,
    annotate_method: typing.Optional[AnnotateLike] = None,
    **kwargs
) -> matplotlib.text.Annotation:
    r"""
    Create an aligned annotation.

    Parameters
    ----------
    ax
        Axes with a `.figure`
    text
        String
    xy
        Real coordinate pair
            (Default `ax.annotate()` behavior)
        'center', 'bottom right', etc.
            Align the text against said corner, edge, or center;
            the arguments `xy`, `xytext`, `ha`, `va`, and `textcoords`
            are replaced by sensible, `rotation`-sensitive values
            (using `_THIS_MODULE_
            .resolve_position_defaults()` to resolve for the suitable
            values)
        'line'
            Place the text along an `ax.axline()`;
            requires `axline`
    axline
        Optional specification of a line against which to align the
        annotation:
        `matplotlib.lines.Line2D` object
            Align against `axline`;
            the line should consist of exactly two data points
        Two real coordinate pairs
            Align against `ax.axline(axline[0], axline[1])`
        Real coordinate pair and a real number
            Align against `ax.axline(axline[0], slope=axline[1])`
    fraction
        Fractional position along the line segment visible within the
        plot limits to place the annotation(s);
        0 is the leftmost end and 1 the rightmost;
        ignored if `xy != 'line'`
    rotation
        Optional angle by which the annotation is rotated:
        Real number
            Rotate by this many degrees from the horizontal
        'line'
            Align parallel to `axline`;
            requires `axline`
        Default is 'line' if `xy == 'line'` and 0 otherwise
    outer
        Whether to draw a plot-area-aligned annotation (i.e.
        `xy = <special_str_spec>`) on the inside (if false) or the
        outside (if true) of the plot area:
        True or 'xy'
            Put the annotation on the outside in all directions
        None or False or 'none' (default)
            Put the annotation on the inside in all directions
        'x', 'y'
            Only put the annotation on the outside in the specified
            direction
    annotate_method()
        Callable with a signature compatible with
        `matplotlib.axes.Axes.annotate(ax, text, xy[, ...])`
        (also the default), returning the appropriate
        `matplotlib.text.Annotation` object
    **kwargs
        Passed to `annotate_method()`

    Return
    ------
    (`matplotlib.text.Annotation`) Object returned by
    `annotate_method()`

    Side effects
    ------------
    {xy|rotation} = 'line'
        Callbacks attached to `ax` and `ax.figure.canvas` for updating
        the locations of the annotations
    axline = <Line2D>
        `ax.figure.draw()` called and `ax.figure.canvas.renderer`
        allocated, to ensure the correct resolution of line dimensions

    Caveats
    -------
    - If the line does not intersect the plot limits, the annotation is
      drawn at the corresponding point along the main diagonal but with
      the correct slope as a fallback.

    - If the axis scaling is non-linear, the behavior is undefined.

    See also
    --------
    _THIS_MODULE_.resolve_position_defaults()
    _THIS_MODULE_.get_on_screen_angle()
    _THIS_MODULE_.get_line_center_in_frame()

    Examples
    --------
    Create rotated annotation aligned to the top right corner:

    >>> from matplotlib import pyplot as plt
    >>> from numpy import allclose, array, isclose
    >>>
    >>> ax = plt.gca()
    >>> redraw = lambda: ax.figure.draw(ax.figure.canvas.renderer)
    >>> ann_fixed = annotate_aligned(  # Defaults set for alignments
    ...     ax, 'Top-right\ncorner', 'top right', rotation=-90,
    ... )
    >>> (
    ...     ann_fixed.get_horizontalalignment(),
    ...     ann_fixed.get_verticalalignment(),
    ...     ann_fixed.get_rotation_mode(),
    ... )
    ('left', 'top', 'anchor')

    Create "floating" annotation following a line:

    >>> ax.clear()
    >>> ax.set_aspect('equal', adjustable='box')
    >>> _ = ax.set_xlim((-1, 1))
    >>> _ = ax.set_ylim((-1, 1))
    >>> x = array([-2, 2])
    >>> line, = ax.plot(  # Intersect axis limits at (-1, 0), (0, 1)
    ...     x, x + 1,
    ... )
    >>> ann_floating = annotate_aligned(
    ...     ax, 'With line', 'line', axline=line,
    ... )
    >>> assert allclose(ann_floating.xy, (-.5, .5))
    >>> assert isclose(ann_floating.get_rotation(), 45)
    >>> ax.set_aspect('auto')
    >>> _ = ax.set_ylim((-.5, .5))  # Upper intercept now at (-.5, .5)
    >>> redraw()
    >>> assert allclose(ann_floating.xy, (-.75, .25))
    >>> ann_floating.remove()
    >>> del ann_floating

    Create annotations at a specific position relative to a line:

    >>> ann_end_of_line = annotate_aligned(
    ...     ax, 'End of line', 'line',
    ...     axline=line,
    ...     ha='center',
    ...     va='bottom',
    ...     fraction=1,  # Draw annotation at endpoint of line segment
    ...     rotation=0,
    ... )
    >>> assert allclose(ann_end_of_line.xy, (-.5, .5))
    >>> _ = ax.set_ylim(  # Upper intercept -> right int. at (1, 2)
    ...     (0, 3),
    ... )
    >>> redraw()
    >>> assert allclose(ann_end_of_line.xy, (1, 2))
    >>> plt.close(ax.figure)
    """
    setup_callbacks: bool = False
    slope: typing.Union[numbers.Real, None] = None
    anchor1: typing.Union[Point2D, None] = None
    anchor2: typing.Union[Point2D, None] = None
    get_angle: typing.Callable[[], typing.Union[numbers.Real, None]]
    get_xy: typing.Callable[[], typing.Union[Point2D, None]]
    T = typing.TypeVar('T')
    PS = typing.ParamSpec('PS')
    RemoveMethod = typing.Callable[[matplotlib.artist.Artist], T]

    # In older matplotlib versions:
    # - We can't use Text(transform=...) to translate + rotate the text
    #   live
    #   -> use callbacks to set the rotation angle and reposition the
    #      text
    def adjust_annotation(
        ax: matplotlib.axes.Axes,
        annotation: matplotlib.text.Annotation,
        get_xy: typing.Callable[[], typing.Union[Point2D, None]],
        get_angle: typing.Callable[[], typing.Union[numbers.Real, None]],
        hard_redraw: bool = False,
    ) -> None:
        """Helper function for adjusting the annotation position."""
        angle, xy, adjusted = get_angle(), get_xy(), False
        if angle is not None:
            annotation.set_rotation(angle)
            adjusted = True
        # Notes:
        # - annotation.get_position() is occupied by .xyann, so
        #   annotation.set_position(xy) doesn't work
        # - Some methods look at .get_x() and .get_y() and some others
        #   at .xy, so we need to take care of both
        if xy is not None:
            annotation.xy = (x, y) = xy
            annotation.set_x(x)
            annotation.set_y(y)
            adjusted = True
        # TODO: work on blitting; right now it's too laggy in
        # interactive mode, esp. the blind callbacks
        # if ax.figure.canvas.supports_blit: ...
        if adjusted:
            try:
                ax.draw_artist(ax if hard_redraw else annotation)
            except Exception:
                pass

    def make_new_remove_method(
        old_method: typing.Optional[RemoveMethod] = None,
        *cr_id_pairs: typing.Tuple[
            matplotlib.cbook.CallbackRegistry, numbers.Integral,
        ],
    ) -> RemoveMethod:
        """
        Make a wrapper around `._remove_method` which deals with the
        callbacks we've created.
        """
        if old_method is None:
            def old_method(self, *args, **kwargs): return

        @functools.wraps(old_method)
        def new_remove_method(self, *args, **kwargs):
            for registry, i in cr_id_pairs:
                registry.disconnect(i)
            return old_method(self, *args, **kwargs)

        return new_remove_method

    def make_blind_callback(
        func: typing.Callable[PS, T], *args: PS.args, **kwargs: PS.kwargs,
    ) -> typing.Callable[..., T]:
        """
        Return a callable which returns `func(*args, **kwargs)` no
        matter what arguments are passed.
        """
        def blind_callback(*a, **k):
            return func(*args, **kwargs)

        for attr, value in dict(
            __name__=getattr(func, '__name__', None),
            __module__=getattr(func, '__module__', None),
            args=tuple(args),
            kwargs=dict(kwargs),
        ).items():
            if value is None:
                continue
            try:
                setattr(blind_callback, attr, value)
            except AttributeError:
                pass
        return blind_callback

    # Massage/Resolve arguments, create callbacks, annotate
    if rotation is None and xy == 'line':
        rotation = 'line'
    if 'line' in (xy, rotation) and axline is None:
        raise TypeError(
            f'xy = {xy!r}, axline = {axline!r}, rotation = {rotation!r}: '
            '`axline` must be provided for alignment against a line'
        )
    if annotate_method is None:
        annotate_bound_method = ax.annotate
    else:
        annotate_bound_method = functools.partial(annotate_method, ax)
    # Note: type checks to be skipped in get_angle() and get_xy()
    # because these functions will be repeatedly called in the callback
    if 'line' in (xy, rotation):
        slope, anchor1, anchor2 = _resolve_slope_and_anchors_from_axline(
            ax, axline,
        )
        setup_callbacks = True
    if 'line' == rotation:
        assert anchor1 is not None
        assert anchor2 is not None
        get_angle = functools.partial(
            _get_on_screen_angle_from_anchors, ax, anchor1, anchor2,
        )
        for key, value in ALIGN_LINE_DEFAULTS.get('rotation', {}).items():
            kwargs.setdefault(key, value)
    else:
        def get_angle(): return rotation

    if 'line' == xy:
        assert slope is not None
        assert anchor1 is not None
        get_xy = functools.partial(
            get_line_center_in_frame.__wrapped__,
            ax,
            slope=slope,
            anchor=anchor1,
            fraction=fraction,
        )
        for key, value in ALIGN_LINE_DEFAULTS.get('xy', {}).items():
            kwargs.setdefault(key, value)
    else:
        def get_xy(): return xy

    if xy in SPECIAL_POSITIONS:
        kwargs = resolve_position_defaults(xy, rotation=get_angle(), **kwargs)
    kwargs.setdefault('rotation', get_angle())
    xy = kwargs.pop('xy', get_xy())
    # Note: older mpl (~3.3) don't have textcoords='offset fontsize',
    # implement workaround
    if kwargs.get('textcoords') == 'offset fontsize':
        # Note: fontsize is not necassarily a real number, so use a
        # dummy annotation to resolve it
        dummy_ann = annotate_bound_method(
            '', xy,
            **{
                key: value for key, value in kwargs.items()
                if key in ('size', 'fontsize')
            },
        )
        fontsize = dummy_ann.get_fontsize()
        dummy_ann.remove()
        if 'xytext' not in kwargs:
            warnings.warn('`textcoords` used but not `xytext`')
            xytext = (0, 0)  # Neutered
        else:
            xytext = kwargs['xytext']  # In fractions of the font size
        kwargs.update(
            xytext=tuple(rel_size * fontsize for rel_size in xytext),
            textcoords='offset points',
        )
    annotation = annotate_bound_method(text, xy, **kwargs)

    # Set up callbacks for:
    # - Triggering recalcs from viewing-window changes and figure
    #   resizing
    # - Removal of callbacks from callback registries on object removal
    if not setup_callbacks:
        return annotation
    base_callback = functools.partial(
        adjust_annotation,
        get_xy=get_xy,
        get_angle=get_angle,
        annotation=annotation,
    )
    # Note: callback of canvas too messy, just pre-bound ax and call it
    # a day
    cr_id_pairs = [
        (registry, registry.connect(event, callback))
        for registry, event, callback in [
            (ax.callbacks, 'xlim_changed', base_callback),
            (ax.callbacks, 'ylim_changed', base_callback),
            (
                ax.figure.canvas.callbacks, 'resize_event',
                make_blind_callback(base_callback, ax),
            ),
            # Note: also trigger a re-calc on drawing since the above
            # events don't capture "passive" resizing with e.g.
            # `ax.apply_aspect()`
            # FIXME: this still has a bug in that the resultant
            # annotation may show as being doubled in an interactive
            # backend, but at least the saved image looks normal
            (
                ax.figure.canvas.callbacks, 'draw_event',
                make_blind_callback(base_callback, ax, hard_redraw=True),
            ),
        ]
    ]
    annotation._remove_method = make_new_remove_method(
        annotation._remove_method, *cr_id_pairs,
    )
    return annotation
