"""
Dummy module for testing `~._utils.monkeypatching`
"""
import typing

__all__ = ('register', 'deregister', 'clear_registry', 'show_registry')

_REGISTRY: typing.List[int] = []

register: typing.Callable[[int], None] = _REGISTRY.append
deregister: typing.Callable[[int], None] = _REGISTRY.remove
clear_registry: typing.Callable[[], None] = _REGISTRY.clear


def show_registry() -> typing.List[int]:
    """
    Return a copy of the registry.
    """
    return list(_REGISTRY)
