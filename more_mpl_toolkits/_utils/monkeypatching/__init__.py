"""
Utilities helping with monkey-patching.
"""
import importlib.util
import types

from ..decorators import replace_in_docs

__all__ = ('clone_module',)

_MODULE_ = (lambda: None).__module__
if _MODULE_.endswith('.__init__'):
    _MODULE_ = _MODULE_.rpartition('.')[0]


@replace_in_docs(_MODULE_=_MODULE_)
def clone_module(module: types.ModuleType) -> types.ModuleType:
    """
    Create a clone of a module which is independent of the original --
    perfect for when you need a monkeypatched version of the module,
    changing its functionalities by replacing some entities in it.

    Example
    -------
    Individuality:

    >>> from _MODULE_ import _test_dummy_module as original
    >>> clone = clone_module(original)
    >>> assert clone is not original
    >>> try:
    ...     original.register(1)
    ...     original.register(2)
    ...     original.register(3)
    ...     clone.register(2)
    ...     clone.register(3)
    ...     clone.register(4)
    ...     print(original.show_registry(), clone.show_registry())
    ...     original.deregister(2)
    ...     clone.deregister(3)
    ...     print(original.show_registry(), clone.show_registry())
    ... finally:
    ...     original.clear_registry()
    ...     clone.clear_registry()
    [1, 2, 3] [2, 3, 4]
    [1, 3] [2, 4]

    Monkeypatched:

    >>> _old_clone_register = clone.register
    >>>
    >>> def register(x: int) -> None:
    ...     '''When registering, double the registered number'''
    ...     _old_clone_register(x * 2)
    ...
    >>> clone.register = register
    >>> try:
    ...     for i in range(1, 4):
    ...         clone.register(i)
    ...     clone.show_registry()
    ... finally:
    ...     clone.clear_registry()
    [2, 4, 6]
    """
    spec = module.__spec__
    clone = importlib.util.module_from_spec(spec)
    clone.__spec__.loader.exec_module(clone)
    return clone
