"""
Common utilities for length conversion.
"""
import numbers
import typing
try:
    from typing import Literal
except ImportError:
    from typing_extensions import Literal

from tjekmate import is_finite_real, is_in, is_sequence, check
from . import string_operations

__all__ = (
    'UNITS_TO_INCHES',
    'convert_length', 'convert_figure_dimensions',
)

UNITS_TO_INCHES = {
    'in': 1.,
    'ft': 12.,
    'mm': 3.93701E-2,
    'cm': 3.93701E-1,
    'm': 3.93701E+1,
    'pt': 1 / 72,
}

BaseLengthLike = typing.TypeVar(
    'BaseLengthLike', bound=typing.Union[numbers.Real, str],
)
LengthLike = typing.TypeVar('LengthLike', bound=BaseLengthLike)
DimensionsString = typing.TypeVar('DimensionsString', bound=str)
PositiveFiniteNumber = typing.TypeVar(
    'PositiveFiniteNumber', bound=numbers.Real,
)
RecognizedUnit = Literal[tuple(UNITS_TO_INCHES)]


@typing.overload
def is_positive_finite_number(x: PositiveFiniteNumber) -> Literal[True]:
    ...


@typing.overload
def is_positive_finite_number(x: typing.Any) -> Literal[False]:
    ...


def is_positive_finite_number(x: typing.Any) -> bool:
    return is_finite_real(x) and x > 0


@typing.overload
def is_length_like(x: BaseLengthLike) -> Literal[True]:
    ...


@typing.overload
def is_length_like(x: typing.Any) -> Literal[False]:
    ...


def is_length_like(x: typing.Any) -> bool:
    """
    Example
    -------
    >>> [
    ...     is_length_like(l)
    ...     for l in [0, 1., -1., '3 in', 'ft', '10 cm', '10 ftt', None]
    ... ]
    [True, True, True, True, True, True, False, False]
    """
    if isinstance(x, numbers.Real):
        return True
    if not isinstance(x, str):
        return False
    return x.strip().endswith(tuple(UNITS_TO_INCHES))


@typing.overload
def is_fig_dimensions_like(x: DimensionsString) -> Literal[True]:
    ...


@typing.overload
def is_fig_dimensions_like(
    x: typing.Tuple[PositiveFiniteNumber, PositiveFiniteNumber]
) -> Literal[True]:
    ...


@typing.overload
def is_fig_dimensions_like(x: typing.Any) -> Literal[False]:
    ...


def is_fig_dimensions_like(x: typing.Any) -> bool:
    if isinstance(x, str):  # '<width> x <height>'
        width, sep, height = (chunk.strip() for chunk in x.partition('x'))
        if not (width and sep and height):
            return False
        return is_fig_dimensions_like((width, height))
    return is_sequence(x, length=2, item_checker=is_length_like)


@check(length=is_length_like, to=is_in(frozenset(UNITS_TO_INCHES)))
def convert_length(
    length: BaseLengthLike, to: RecognizedUnit = 'in',
) -> numbers.Real:
    """
    Convert a length into inches.

    Parameters
    ----------
    length
        String or numeric representation of a length;
        if a string of format '[<num>]<unit>', the following units are
        understood:
          'in', 'ft', 'mm', 'cm', 'm', 'pt';
        if a (unitless) number, it is assumed to be in inches
    to
        Unit to convert into

    Return
    ------
    The length in `to`

    Example
    -------
    >>> from numpy import isclose
    >>>
    >>> seventytwo_pts_in_in = convert_length('72 pt')
    >>> inch_in_cm = convert_length('in', to='cm')
    >>> assert isclose(inch_in_cm, 2.54), inch_in_cm
    >>> assert isclose(seventytwo_pts_in_in, 1), seventytwo_pts_in_in
    """
    if isinstance(length, numbers.Real):
        return length
    length = length.strip()
    length, unit = string_operations.strip_suffix(length, UNITS_TO_INCHES)
    multiplier = UNITS_TO_INCHES[unit]
    if length.strip():
        real_len = float(length)
    else:  # Nothing left after stripping the unit -> one unit
        real_len = 1
    return real_len * multiplier / UNITS_TO_INCHES[to]


@check(dim=is_fig_dimensions_like)
def convert_figure_dimensions(
    dim: typing.Union[
        str, typing.Tuple[LengthLike, LengthLike],
    ],
) -> typing.Tuple[PositiveFiniteNumber, PositiveFiniteNumber]:
    """
    Convert dimensions to inches.

    Parameters
    ----------
    dim
        String or numeric representation of two positive numbers;
        if strings of format '[<num>]<unit>', the following units are
        understood:
          'in', 'ft', 'mm', 'cm', 'm', 'pt';
        if a string in the form '<num><unit>x<num><unit>', the two
        numbers are separately parsed into the two-tuple of dimensions

    Return
    ------
    Two-tuple of positive numbers

    Example
    -------
    >>> from numpy import allclose
    >>> width = '2.54 cm'  # 1 in
    >>> height = '3.81 cm'  # 1.5 in
    >>> assert allclose(
    ...     convert_figure_dimensions((width, height)), (1, 1.5),
    ... )
    >>> assert allclose(
    ...     convert_figure_dimensions('{}x{}'.format(width, height)),
    ...     (1, 1.5),
    ... )
    >>> convert_figure_dimensions((5, 0))
    Traceback (most recent call last):
      ...
    ValueError: dim = (5, 0) -> ...: expected positive reals
    """
    if isinstance(dim, str):  # 'width x height'
        return convert_figure_dimensions(dim.split('x'))
    width, height = (convert_length(item) for item in dim)
    if not all(is_positive_finite_number(length) for length in (width, height)):
        raise ValueError(
            f'dim = {dim!r} -> ({width!r}, {height!r}): expected positive reals'
        )
    return width, height
