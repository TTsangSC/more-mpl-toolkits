"""
Common operations for strings
"""
import typing

from tjekmate import (
    identical_to, is_collection, is_instance, type_checked,
)

common_string_operation = type_checked(
    checkers=dict(
        string=(identical_to(None) | is_instance(str)),
        prefix=is_collection(item_type=str),
        suffix=is_collection(item_type=str),
    ),
    annotations=dict(
        string=str,
        prefix=typing.Union[str, typing.Collection[str]],
        suffix=typing.Union[str, typing.Collection[str]],
    ),
)


def _strip_suffix(string, suffix) -> typing.Tuple[str, str]:
    if isinstance(suffix, str):
        suffix = (suffix,)
    suffix = next(
        s for s in sorted(set(suffix) | {''}, key=len, reverse=True)
        if string.endswith(s)
    )
    if suffix:
        return string[:-len(suffix)], suffix
    return string, ''


strip_suffix = common_string_operation(_strip_suffix)
strip_suffix.__doc__ = (
    """
    Strip the suffix(es) if found.

    Parameters
    ----------
    string
        String
    suffix
        String suffix(es)

    Return
    ------
    If any of the suffixes found
        Tuple `stripped_string, longest_matching_suffix`
    else
        Tuple `string, ''`

    Example
    -------
    >>> strip_suffix('abc', 'c')
    ('ab', 'c')
    >>> strip_suffix('abc', ('c', 'bc'))
    ('a', 'bc')
    >>> strip_suffix('abc', 'ab')
    ('abc', '')
    >>> strip_suffix('abc', 'abc')
    ('', 'abc')
    >>> strip_suffix('abc', '')
    ('abc', '')
    """
)


@common_string_operation
def strip_prefix(string, prefix) -> typing.Tuple[str, str]:
    """
    Strip the prefix(es) if found.

    Parameters
    ----------
    string
        String
    prefix
        String prefix(es)

    Return
    ------
    If any of the prefixes found
        Tuple `longest_matching_prefix, stripped_string`
    else
        Tuple `'', string`

    Example
    -------
    >>> strip_prefix('abc', 'a')
    ('a', 'bc')
    >>> strip_prefix('abc', ('a', 'ab'))
    ('ab', 'c')
    >>> strip_prefix('abc', 'bc')
    ('', 'abc')
    >>> strip_prefix('abc', 'abc')
    ('abc', '')
    >>> strip_prefix('abc', '')
    ('', 'abc')
    """
    if isinstance(prefix, str):
        prefix = (prefix,)
    rev_suffixes = tuple(p[::-1] for p in prefix)
    rev_string, rev_suffix = _strip_suffix(string[::-1], rev_suffixes)
    return rev_suffix[::-1], rev_string[::-1]
