"""
Common decorators
"""
import collections
import functools
import inspect
import itertools
import keyword
import numbers
import typing
import warnings
try:
    from typing import Literal
except ImportError:  # < python 3.8
    from typing_extensions import Literal
try:
    from typing import ParamSpec, Concatenate
except ImportError:  # < python3.10
    from typing_extensions import ParamSpec, Concatenate

__all__ = (
    'partial_sole_positional_arg',
    'partial_leading_args',
    'replace_in_docs',
    'insert_kwargs_from',
    'copy_func',
    'copy_method_signature_from',
    'copy_annotations_from',
    'deprecate_positionals',
)

T = typing.TypeVar('T')
T1 = typing.TypeVar('T1')
T2 = typing.TypeVar('T2')
PS = ParamSpec('PS')
PS1 = ParamSpec('PS1')
PS2 = ParamSpec('PS2')

Keyword = typing.TypeVar('Keyword', bound=str)
KeywordOrKeywords = typing.Union[Keyword, typing.Collection[Keyword]]
Annotation = typing.TypeVar('Annotation')
ReturnType = typing.TypeVar('ReturnType')
FunctionLike = typing.TypeVar(
    'FunctionLike', typing.Callable, staticmethod, classmethod,
)
FunctionLikeNonConstrained = typing.Union[
    typing.Callable, staticmethod, classmethod,
]
FunctionLikeOrType = typing.TypeVar('FunctionLikeOrType', FunctionLike, type)

EMPTY = inspect.Parameter.empty
OMITTABLE = type('_Omittable', (), dict(__repr__=lambda self: '<OMITTABLE>'))()

# ************ functools.partial-related functionalities ************* #


def partial_sole_positional_arg(
    func: typing.Callable[[T1], T2],
    *,
    placeholder: typing.Any = OMITTABLE,
) -> typing.Callable[..., typing.Union[T2, typing.Callable[[T1], T2]]]:
    """
    Make the single positional argument in a callable omittable;
    if omitted, a `functools.partial` object is returned.

    Parameters
    ----------
    func()
        Callable with:
        - EXACTLY ONE positional(-only) parameter, without a default
        - NO variadic positional parameter
        - OPTIONAL (variadic) keyword-only arguments
    placeholder
        Placeholder value to be spliced into the signature of the
        returned callable;
        should NOT be a value normally passed to the positional
        parameter

    Notes
    -----
    - `placeholder` isn't really explicitly used in the function
      wrapper, but beware when `func()` uses introspection to resolve
      its own arguments.

    - If a function is to use this decorator, its annotations should
      apply to the UN-decorated version;
      the annotations for the return type and the first parameter are
      transformed here.

    - This decorator handles the signature, but the user is responsible
      for writing the docs suitably.

    Example
    -------
    >>> import inspect
    >>> import typing
    >>>
    >>> @partial_sole_positional_arg(placeholder=None)
    ... def foo(x: int, *, y: int = 10) -> int: return x + y
    ...
    >>> sig = inspect.signature(foo)
    >>> assert sig.parameters['x'].annotation == typing.Union[int, None]
    >>> assert sig.return_annotation == typing.Union[
    ...     int, typing.Callable[[int], int],
    ... ]
    >>> foo(1, y=2)
    3
    >>> foo(y=5)(4)
    9
    """
    sig = inspect.signature(func)
    param0, *kw_params = sig.parameters.values()
    has_one_pos_arg = param0.kind in (
        param0.POSITIONAL_OR_KEYWORD, param0.POSITIONAL_ONLY,
    )
    pos_arg_no_default = param0.default is EMPTY
    other_args_kwonly = all(
        param.kind in (param.KEYWORD_ONLY, param.VAR_KEYWORD)
        for param in kw_params
    )
    if not (has_one_pos_arg and pos_arg_no_default and other_args_kwonly):
        raise TypeError(
            f'func: {func.__name__}{sig}: '
            'expected exactly one positional parameter without a default, and '
            'all others to be keyword-only'
        )

    @typing.overload
    def wrapper(**kwargs) -> typing.Callable[
        [param0.annotation], sig.return_annotation
    ]:
        ...

    @typing.overload
    def wrapper(arg: param0.annotation, **kwargs) -> sig.return_annotation: ...

    @functools.wraps(func)
    def wrapper(*arg, **kwargs):
        if not arg:
            return functools.partial(func, **kwargs)
        return func(*arg, **kwargs)

    # Fix the signature
    if sig.return_annotation is EMPTY:
        ReturnType = typing.Any
    else:
        ReturnType = sig.return_annotation
    if param0.annotation is EMPTY:
        ArgType = typing.Any
    else:
        ArgType = param0.annotation
    if ArgType is typing.Any:
        ApparentArgType = ArgType
    else:
        ApparentArgType = typing.Union[
            ArgType, (None if placeholder is None else Literal[placeholder]),
        ]
    new_param0 = param0.replace(annotation=ApparentArgType, default=placeholder)
    wrapper.__signature__ = sig.replace(
        parameters=[new_param0, *kw_params],
        return_annotation=typing.Union[
            ReturnType, typing.Callable[[ArgType], ReturnType],
        ],
    )
    return wrapper


partial_sole_positional_arg = partial_sole_positional_arg(
    partial_sole_positional_arg, placeholder=None,
)


@partial_sole_positional_arg
def partial_leading_args(
    func: typing.Callable[Concatenate[PS1, PS2], T],
    *,
    placeholder: typing.Any = OMITTABLE,
    min_nargs: typing.Optional[numbers.Integral] = None,
) -> typing.Callable[
    ..., typing.Union[T, typing.Callable[PS1, T]]
    # Note: either
    # >>> typing.Callable[Concatenate[PS1, PS2], T]
    # or
    # >>> typing.Callable[PS2, typing.Callable[PS1, T]]
]:
    """
    Make the leading positional arguments in a callable omittable;
    if omitted a callable is returned which is the inverse of
    `functools.partial`, appending instead of prepending the passed
    positional arguments.

    Parameters
    ----------
    func()
        Callable with:
        - AT LEAST ONE positional(-only) parameter(s), ALL without
          defaults
        - NO variadic positional parameter
        - OPTIONAL (variadic) keyword-only arguments
    placeholder
        Placeholder value to be spliced into the signature of the
        returned callable;
        should NOT be a value normally passed to the positional
        parameters
    min_nargs
        Optional non-negative number;
        if provided, require at least this many arguments to trigger
        partial processing

    Notes
    -----
    - `placeholder` isn't really explicitly used in the function
      wrapper, but beware when `func()` uses introspection to resolve
      its own arguments.

    - If a function is to use this decorator, its annotations should
      apply to the UN-decorated version;
      the annotations for the return type and the positional
      parameter(s) are transformed here.

    - This decorator handles the signatures (of both the decorated
      method and of the returned partial-like callable), but the user is
      responsible for writing the docs suitably.

    Example
    -------
    >>> import inspect
    >>> import typing
    >>>
    >>> @partial_leading_args(placeholder=None, min_nargs=1)
    ... def foo(x: int, y: float, z: str, *, a: float = 1.) -> None:
    ...     print('{:{}}'.format(a * (x - y), z))
    ...

    Automatic re-resolution of signatures:

    >>> sig = inspect.signature(foo)
    >>> assert [sig.parameters[xyz].annotation for xyz in 'xyz'] == [
    ...     typing.Union[int, float, str],
    ...     typing.Union[float, str, None],
    ...     typing.Union[str, None],
    ... ]
    >>> assert [sig.parameters[xyz].default for xyz in 'xyz'] == [
    ...     inspect.Parameter.empty, None, None,
    ... ]
    >>> assert sig.return_annotation == typing.Union[
    ...     None,
    ...     typing.Callable[[int], None],
    ...     typing.Callable[[int, float], None],
    ... ]

    >>> foo_with_default_yz = foo(1.5, '.2f')
    >>> new_sig = inspect.signature(foo_with_default_yz)
    >>> assert new_sig.parameters['y'].default == 1.5
    >>> assert new_sig.parameters['z'].default == '.2f'

    Invocation of partial-like callable:

    >>> foo_with_default_yz(5)  # = '{:.2f}'.format(1. * ((5) - 1.5))
    3.50
    >>> foo_with_default_yz(5, 6)  # = '{:2f}'.format(1. * ((5) - (6)))
    -1.00
    >>> foo_with_default_yz(
    ...     3, 5, a=2,  # = '{:2f}'.format((2.) * ((3) - (5)))
    ... )
    -4.00
    """
    def get_error_msg():
        return (
            f'func: {func.__name__}{sig}: '
            'expected (1) at least one positional parameter(s) all without '
            'defaults, (2) no variadic positional parameter, and (3) all others'
            'to be keyword-only'
        )

    # Handling the signature
    sig = inspect.signature(func)
    pos_params: typing.List[inspect.Parameter] = []
    kw_params: typing.List[inspect.Parameter] = []
    for param in sig.parameters.values():
        if param.kind in (param.POSITIONAL_OR_KEYWORD, param.POSITIONAL_ONLY):
            pos_params.append(param)
        elif param.kind == param.VAR_POSITIONAL:
            raise TypeError(get_error_msg())
        else:
            kw_params.append(param)
    if not pos_params:
        raise TypeError(get_error_msg())
    if any(param.default is not EMPTY for param in pos_params):
        raise TypeError(get_error_msg())

    # Handling number of args passed
    nargs_pos = len(pos_params)
    if min_nargs is None:
        min_nargs = 0
    if not isinstance(min_nargs, numbers.Integral) and min_nargs >= 0:
        raise TypeError(
            f'min_nargs = {min_nargs!r}: expected a nonnegative integer'
        )
    if min_nargs > nargs_pos:
        raise ValueError(
            (
                'func: {}{}, min_nargs = {!r}: '
                'more arguments required than `func` has positional parameters'
            ).format(func.__name__, sig, min_nargs)
        )

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        ba = sig.bind_partial(*args, **kwargs)
        if min_nargs <= len(ba.args) < nargs_pos:
            # Note: we can't fully reuse the partial-binding results
            # because the bindings on the positional args are wrong

            @functools.wraps(func)
            def inner_wrapper(*args, **kwargs):
                ba = inner_wrapper.__signature__.bind(*args, **kwargs)
                ba.apply_defaults()
                return func(*ba.args, **ba.kwargs)

            # Fix the signature
            ba_kwargs_only = sig.bind_partial(**kwargs)
            new_pos_params, new_kw_params = (
                collections.OrderedDict((param.name, param) for param in params)
                for params in (pos_params, kw_params)
            )
            # Insert new params and add/fix defaults from kwargs
            for arg_name, arg_value in kwargs.items():
                if arg_name in ba_kwargs_only.kwargs:
                    param = new_kw_params.get(arg_name)
                    if not param:
                        _, kwargs_param = new_kw_params.popitem()
                        assert kwargs_param.kind is kwargs_param.VAR_KEYWORD
                        # (Mapped to kwargs, insert a new named keyword-
                        # only param for it)
                        new_kw_params[arg_name] = inspect.Parameter(
                            arg_name, inspect.Parameter.KEYWORD_ONLY,
                            default=arg_value,
                        )
                        new_kw_params[kwargs_param.name] = kwargs_param
                    elif param.kind is not param.VAR_KEYWORD:
                        # Update default of keyword-only param
                        new_kw_params[arg_name] = param.replace(
                            default=arg_value,
                        )
                    else:  # Clash with kwargs, move kwargs elsewhere
                        _, kwargs_param = new_kw_params.popitem()
                        assert kwargs_param.kind is kwargs_param.VAR_KEYWORD
                        new_kw_params[arg_name] = inspect.Parameter(
                            arg_name, inspect.Parameter.KEYWORD_ONLY,
                            default=arg_value,
                        )
                        for new_name in itertools.chain(
                            [
                                'kwargs', 'keywords', 'kw',
                                'more_kwargs', 'more_keywords', 'more_kw',
                            ],
                            ('_' for i in itertools.count(1)),
                        ):
                            if new_name not in {
                                *new_kw_params, *new_pos_params,
                            }:
                                new_kw_params[new_name] = kwargs_param.replace(
                                    name=new_name,
                                )
                                break
                else:
                    # Positional args with new defaults supplied by
                    # kwargs
                    new_pos_params[arg_name] = (
                        new_pos_params[arg_name].replace(default=arg_value)
                    )
            # Sanity check: can we unambiguously do something like
            # func(*incoming_args, *args, **kwargs)?
            # (This requires all the keyword-supplied pos args to trail
            # the list of pos args)
            pos_args_have_no_defaults = [
                param.default is EMPTY for param in new_pos_params.values()
            ]
            if not all(
                pos_args_have_no_defaults[:sum(pos_args_have_no_defaults)]
            ):
                msg = (
                    'func: {}{}, func(..., {}): '
                    'positional arguments supplied via keywords should '
                    'trail other positional arguments'
                ).format(
                    func.__name__,
                    sig,
                    ', '.join('{}={!r}'.format(*kvp) for kvp in kwargs.items()),
                )
                raise TypeError(msg)
            # Add defaults to the pos args from args
            remaining_pos_param_names_reversed = (
                param.name for param in list(new_pos_params.values())[::-1]
                if param.default is EMPTY
            )
            for name, pos_value in zip(
                remaining_pos_param_names_reversed, args[::-1],
            ):
                new_pos_params[name] = new_pos_params[name].replace(
                    default=pos_value,
                )
            inner_wrapper.__signature__ = sig.replace(
                parameters=[*new_pos_params.values(), *new_kw_params.values()],
            )
            return inner_wrapper
        return func(*args, **kwargs)

    # Fix the signature
    base_annotations: typing.List[Annotation] = [
        typing.Any if param.annotation is EMPTY else param.annotation
        for param in pos_params
    ]
    revised_annotations: typing.List[typing.List[Annotation]] = [
        [] for _ in base_annotations
    ]
    return_annotations: typing.List[Annotation] = []
    new_pos_params: typing.List[inspect.Parameter] = []
    Placeholder = None if placeholder is None else Literal[placeholder]
    # E.g.
    # For the function
    # >>> @partial_leading_args(placeholder=None, min_nargs=1)
    # ... def foo(x: str, y: int, z: float) -> int: ...
    # The following signatures should be supported:
    # - (x: str, y: int, z: float) -> int
    # - (x: int, y: float) -> Callable[[str], int]
    # - (x: float) -> Callable[[str, int], int]
    if sig.return_annotation is EMPTY:
        ReturnType: Annotation = typing.Any
    else:
        ReturnType: Annotation = sig.return_annotation
    for i, nargs_passed in enumerate(range(min_nargs, nargs_pos + 1)[::-1]):
        passed_annotations = base_annotations[-nargs_passed:]
        remaining_annotations = base_annotations[:-nargs_passed]
        if i:  # Partial call signature -> partial-like method
            return_annotations.append(
                typing.Callable[remaining_annotations, ReturnType]
            )
        else:  # Full call signature -> function-call result
            return_annotations.append(ReturnType)
        for ann_list, annotation in zip(
            revised_annotations, passed_annotations,
        ):
            ann_list.append(annotation)
    assert all(revised_annotations)
    for i, (param, annotations) in enumerate(
        zip(pos_params, revised_annotations)
    ):
        if i >= min_nargs:
            param = param.replace(default=placeholder)
            annotations.append(Placeholder)
        new_pos_params.append(
            param.replace(annotation=typing.Union[tuple(annotations)])
        )
    wrapper.__signature__ = sig.replace(
        parameters=[*new_pos_params, *kw_params],
        return_annotation=typing.Union[tuple(return_annotations)],
    )
    return wrapper


# ************************* Doc manipulation ************************* #


@typing.overload
def replace_in_docs(
    replacements: typing.Optional[typing.Mapping[str, str]] = None,
    **more_replacements: str,
) -> typing.Callable[[FunctionLikeOrType], FunctionLikeOrType]:
    ...


@typing.overload
def replace_in_docs(
    # Note: python3.7 doesn't have positional-onlys ('/')
    method_or_cls: FunctionLikeOrType,
    replacements: typing.Optional[typing.Mapping[str, str]] = None,
    **more_replacements: str,
) -> FunctionLikeOrType:
    ...


def replace_in_docs(*args, **kwargs: str) -> None:
    r"""
    Replace occurrences of the specified patterns in the `.__doc__` of
    the argument (method or class) with target values.

    Call signatures
    ---------------
    (moc[, {pat_str: tar_str}][, **{pat_str: tar_str}]) -> moc
    ([{pat_str: tar_str}][, **{pat_str: tar_str}]) -> (deco(moc) -> moc)

    Examples
    --------
    >>> import textwrap
    >>>
    >>> def foo():
    ...     '''
    ...     This is a FOO function.
    ...     '''
    ...     pass
    ...
    >>> class Foo:
    ...     '''
    ...     This is a FOO class.
    ...     '''
    ...     def bar(self):
    ...         '''
    ...         This is a FOO instance method.
    ...         '''
    ...         pass
    ...
    ...     @classmethod
    ...     def baz(cls):
    ...         '''
    ...         This is a FOO class method.
    ...         '''
    ...
    ...     @staticmethod
    ...     def foobar(cls):
    ...         '''
    ...         This is a FOO static method.
    ...         '''

    Single-word replacement:

    >>> assert foo is replace_in_docs(foo, FOO='foo')

    Multi-target replacements (done sequentially):

    >>> assert Foo is replace_in_docs(
    ...     Foo, {'FOO': 'Foo', 'Foo class': 'Foo'}
    ... )

    Usage as a decorator:

    >>> @replace_in_docs({'replaced': 'decorated'})
    ... def print_doc(obj):
    ...     '''Show how the doc is replaced.'''
    ...     print(textwrap.dedent(obj.__doc__).strip())
    ...
    >>> @replace_in_docs({'ham': 'eggs'}, function='method')
    ... def spam():
    ...     '''
    ...     This function works on ham.
    ...     '''
    ...     pass

    Results:

    >>> print_doc(foo)
    This is a foo function.
    >>> print_doc(Foo)  # "FOO class" -> "Foo class" -> "Foo"
    This is a Foo.
    >>> print_doc(Foo.bar)
    This is a Foo instance method.
    >>> print_doc(Foo.baz)  # See print_doc(Foo)
    This is a Foo method.
    >>> print_doc(Foo.foobar)
    This is a Foo static method.
    >>> print_doc(print_doc)
    Show how the doc is decorated.
    >>> print_doc(spam)
    This method works on eggs.
    """
    def decorator(
        method_or_cls: FunctionLikeOrType,
        replacements: typing.Mapping[str, str],
    ) -> FunctionLikeOrType:
        for method_wrapper in staticmethod, classmethod:
            if isinstance(method_or_cls, method_wrapper):
                return method_wrapper(
                    decorator(method_or_cls.__func__, replacements),
                )
        if isinstance(method_or_cls, type):
            for method in vars(method_or_cls).values():
                if not (
                    # Static/class methods
                    isinstance(method, (staticmethod, classmethod)) or
                    # Instance methods
                    callable(method) and
                    callable(getattr(method, '__get__', None))
                ):
                    continue
                try:
                    decorator(method, replacements)
                except Exception:
                    pass
        if isinstance(getattr(method_or_cls, '__doc__', None), str):
            for pattern, replacement in replacements.items():
                try:
                    method_or_cls.__doc__ = (
                        method_or_cls.__doc__.replace(pattern, replacement)
                    )
                except Exception:
                    pass
        return method_or_cls

    nargs = len(args)
    assert nargs < 3
    if nargs == 2:
        method_or_cls, replacements = args
        assert callable(method_or_cls)
        return decorator(method_or_cls, dict(replacements, **kwargs))
    if nargs == 1:
        if callable(args[0]):
            return decorator(*args, kwargs)
        return functools.partial(
            decorator, replacements=dict(args[0], **kwargs),
        )
    return functools.partial(decorator, replacements=kwargs)


# ********************** Signature manipulation ********************** #


@typing.overload
def is_identifier(obj: Keyword) -> Literal[True]:
    ...


@typing.overload
def is_identifier(obj: typing.Any) -> Literal[False]:
    ...


def is_identifier(obj: typing.Any) -> bool:
    """
    Return
    ------
    Whether `obj` is a string identifier which can e.g. be an attribute
    or argument name

    Example
    -------
    >>> [is_identifier(word) for word in '1 word is return _'.split()]
    [False, True, False, False, True]
    """
    return (
        isinstance(obj, str) and
        obj.isidentifier() and
        not keyword.iskeyword(obj)
    )


def get_sig_inner_func_and_converter(
    func: FunctionLike,
) -> typing.Tuple[
    inspect.Signature,
    typing.Callable,
    Literal[classmethod, staticmethod, None],
]:
    """
    Parameters
    ----------
    func()
        Instance, class, or static method

    Return
    ------
    In a tuple:

    - Signature of the (inner) function

    - (Inner if a class/static method) Function

    - Class converting the inner function back to the correct type of
      method OR None
    """
    if type(func) in (classmethod, staticmethod):
        func, converter = func.__func__, type(func)
    else:
        func, converter = func, None
    return inspect.signature(func), func, converter


def copy_func(
    func: typing.Callable[PS, ReturnType],
) -> typing.Callable[PS, ReturnType]:
    """
    Create a wrapper around the function.
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)
    return wrapper


def insert_kwargs(
    func1: FunctionLike,
    func2: FunctionLikeNonConstrained,
    *funcs: typing.Sequence[FunctionLikeNonConstrained],
    copy: bool = False,
    order: typing.Optional[typing.Sequence[Keyword]] = None,
) -> FunctionLike:
    """
    Insert keyword-only arguments from other functions.

    Example
    -------
    >>> import functools, inspect
    >>> copy_w_kwargs = functools.partial(insert_kwargs, copy=True)
    >>>
    >>> def f1(x, y=2, *, z=5): None
    ...
    >>> def f2(a, b, *, c=6, **k): None
    ...
    >>> f = copy_w_kwargs(f1, f2)
    >>> print(inspect.signature(f))
    (x, y=2, *, z=5, c=6, **k)

    If `copy` is true, a copy with an altered signature is created;
    else, the function signature itself is updated:

    >>> f is f1
    False
    >>> f1_copy = (
    ...     lambda f: functools.wraps(f)(lambda *a, **k: f(*a, **k))
    ... )(f1)
    >>> f_update = insert_kwargs(f1_copy, f2)
    >>> f_update is f1_copy
    True

    Use `order` to reorder the keyword-only arguments:

    >>> print(inspect.signature(copy_w_kwargs(f1, f2, order='cz')))
    (x, y=2, *, c=6, z=5, **k)

    This function "falls through" ordinary static and class methods:

    >>> c = copy_w_kwargs(classmethod(f1), f2)
    >>> isinstance(c, classmethod)
    True
    >>> print(inspect.signature(c.__func__))
    (x, y=2, *, z=5, c=6, **k)

    Notes
    -----
    If annotations for the same keyword-only arguments exist in more
    than one of the provided methods, the EARLIER one takes precedence.
    """
    if funcs:
        # Only make one wrapper (if needed), and only reorder the kwargs
        # once
        return insert_kwargs(
            insert_kwargs(func1, func2, copy=copy), *funcs,
            order=order,
        )

    sig1, func1_inner, converter = get_sig_inner_func_and_converter(func1)
    sig2, *_ = get_sig_inner_func_and_converter(func2)
    # Get original signature and treat any trailing vakwargs separately
    params = sig1.parameters.copy()
    vakwargs_param: typing.Union[inspect.Parameter, None]
    if params:
        _, vakwargs_param = params.popitem()
        if vakwargs_param.kind is not vakwargs_param.VAR_KEYWORD:
            params[vakwargs_param.name], vakwargs_param = vakwargs_param, None
    else:
        vakwargs_param = None
    # Append kwargs from the other signature
    for param in sig2.parameters.values():
        if param.kind is param.KEYWORD_ONLY:
            params.setdefault(param.name, param)
        elif param.kind is param.VAR_KEYWORD:
            if param.name in params:  # Name clash
                pass
            else:
                vakwargs_param = param
    # Compute new signature
    non_kw_params: typing.List[inspect.Parameter] = []
    kw_params: typing.List[inspect.Parameter] = []
    for param in params.values():
        if param.kind in (
            param.POSITIONAL_ONLY,
            param.POSITIONAL_OR_KEYWORD,
            param.VAR_POSITIONAL,
        ):
            non_kw_params.append(param)
        else:
            kw_params.append(param)
    if order is not None:  # Reorder keyword arguments
        order_dict = {name: i for i, name in enumerate(order)}
        orig_order_dict = {param.name: i for i, param in enumerate(kw_params)}
        kw_params.sort(
            key=lambda param: (
                order_dict.get(param.name, float('inf')),
                orig_order_dict[param.name]
            )
        )
    if vakwargs_param:
        kw_params.append(vakwargs_param)
    new_sig = sig1.replace(parameters=(non_kw_params + kw_params))
    # Do we update the object itself?
    if copy:
        new_func1_inner = copy_func(func1_inner)
        new_func1_inner.__signature__ = new_sig
        return converter(new_func1_inner) if converter else new_func1_inner
    if converter:
        func1.__func__.__signature__ = new_sig
    else:
        func1.__signature__ = new_sig
    return func1


def insert_kwargs_from(
    func: FunctionLikeNonConstrained,
    *funcs: typing.Sequence[FunctionLikeNonConstrained],
    copy: bool = False,
    order: typing.Optional[typing.Sequence[Keyword]] = None,
) -> typing.Callable[[FunctionLike], FunctionLike]:
    """
    Decorator taking the keyword parameters of the supplied callables(s)
    and adding them to the signature of the decorated method.

    Parameters
    ----------
    func(), *funcs
        Callables or class-/static-method objects from which keyword
        parameters should be collected
    copy
        Whether to create a copy (wrapper function) around the decorated
        method;
        if not, the signature of the decorated method is directly
        modified
    order
        Optional sequence of keywords according to which the keyword-
        only parameters (both native and inserted) are to be ordered in
        the final signature;
        if a keyword-only parameter is not found in `order`, it is put
        after all the parameters which are

    Return
    ------
    Decorator on instance, class, or static method which either creates
    a copy with an edited signature (if `copy = True`) or edits its
    signature directly (otherwise)

    Notes
    -----
    If annotations for the same keyword-only arguments exist in more
    than one of the provided methods, the EARLIER one takes precedence.

    Example
    -------
    >>> import inspect
    >>> import typing
    >>>
    >>> def foo(
    ...     a: int, b: int,
    ...     *,
    ...     y: typing.Optional[int] = None, z: bool = True,
    ... ) -> None:
    ...     ...
    ...
    >>> @insert_kwargs_from(foo)
    ... def bar(x: int, **kwargs) -> int:
    ...     ...
    ...
    >>> params = inspect.signature(bar).parameters
    >>> assert list(params) == ['x', 'y', 'z', 'kwargs']
    """
    def decorator(func: FunctionLike, **kwargs) -> FunctionLike:
        return insert_kwargs(func, *funcs, **{**default_kwargs, **kwargs})

    funcs = [func, *funcs]
    if not (
        order is None or
        isinstance(order, typing.Sequence) and
        all(isinstance(item, str) and item.isidentifier() for item in order)
    ):
        raise TypeError(f'order = {order!r}: expected a sequence of keywords')
    default_kwargs = dict(copy=copy, order=order)
    return decorator


def copy_method_signature_from(
    func: FunctionLikeNonConstrained,
    *,
    copy_return: typing.Optional[bool] = None,
    copy: bool = False,
    drop: typing.Optional[
        typing.Union[Keyword, typing.Collection[Keyword]]
    ] = None,
) -> typing.Callable[[FunctionLike], FunctionLike]:
    """
    Create a decorator for copying the signature of one method to
    another.

    Parameters
    ----------
    func()
        Instance, class, or static method
    copy_return
        True
            Alway copy the return annotation of `func()`
        False
            Never copy the return annotation of `func()`
        None (default)
            Only copy the return annotation of `func()` if it has one
            and the decorated function doesn't
    copy
        Whether to create a new callable based on the decorated method
        (if true) or to directly update its signature (if false)
    drop
        Optional parameter name(s) to exclude from the signature

    Return
    ------
    Decorator for methods setting its signature

    Example
    -------
    >>> from inspect import signature
    >>> def func(foo: int = 1, bar: str = '') -> None:
    ...     ...
    ...
    >>> another_func = copy_method_signature_from(func)(
    ...     lambda *a, **k: None,
    ... )
    >>> print(signature(another_func))
    (foo: int = 1, bar: str = '') -> None

    Return annotation is preserved by default if found (use
    `copy_return` to control this behavior):

    >>> @copy_method_signature_from(func)
    ... def yet_another_func(*a, **k) -> int:
    ...     return 1
    ...
    >>> print(signature(yet_another_func))
    (foo: int = 1, bar: str = '') -> int

    Parameters can be removed by `drop`:

    >>> @copy_method_signature_from(func, drop='bar')
    ... def more_func(*a, **k) -> str:
    ...     return ''
    ...
    >>> print(signature(more_func))
    (foo: int = 1) -> str

    Notes
    -----
    The decorator knows how to manage parameters like `cls` or `self`
    even if `func()` and the decorated function are different kinds of
    methods;
    for instance and class methods, these parameters may be prepended to
    the final signature if absent (e.g. the method starts with a
    var-arg).
    """
    # Helper functions
    def get_truncated_sig(func: FunctionLike) -> typing.Tuple[
        typing.Union[inspect.Parameter, None],
        inspect.Signature,
        typing.Callable,
        Literal[classmethod, staticmethod, None],
    ]:
        """
        Truncate the first named positional argument of an instance or
        class method (if present)

        Return
        ------
        >>> (  # noqa # doctest: +SKIP
        ...     first_named_arg_or_none,
        ...     truncated_signature,
        ...     inner_func,
        ...     method_class_or_none
        ... )

        Example
        -------
        >>> from inspect import signature
        >>> class Foo:
        ...     def instance_method(self, x: int) -> float:
        ...         ...
        ...
        ...     @copy_method_signature_from(instance_method)
        ...     def another_instance_method(*args, **kwargs):
        ...         ...
        ...
        ...     def inst_mthd_with_return_annotation(*a, **kw) -> int:
        ...         ...
        ...
        ...     @copy_method_signature_from(instance_method)
        ...     @classmethod
        ...     def class_method(cls, *args, **kwargs):
        ...         ...
        ...
        ...     @copy_method_signature_from(instance_method)
        ...     @staticmethod
        ...     def static_method(*args, **kwargs):
        ...         ...
        ...
        >>> print(signature(Foo.another_instance_method))
        (self, x: int) -> float
        >>> print(signature(Foo.inst_mthd_with_return_annotation))
        (self, x: int) -> int
        >>> print(signature(vars(Foo)['class_method']))
        (cls, x: int) -> float
        >>> print(signature(vars(Foo)['static_method']))
        (x: int) -> float

        Notes
        -----
        Should be used AFTER the `classmethod` and `staticmethod`
        decorators.
        """
        sig, func, method_cls = get_sig_inner_func_and_converter(func)
        if not sig.parameters:  # No arguments
            return None, sig, func, method_cls
        # Manage the first argument: is it the class/instance bound
        # argument?
        first_arg: typing.Union[inspect.Parameter, None] = next(
            iter(sig.parameters.values()), None,
        )
        if first_arg:
            is_named_positional = first_arg.kind in (
                first_arg.POSITIONAL_ONLY, first_arg.POSITIONAL_OR_KEYWORD,
            )
            # Static method: don't do anything
            if method_cls is staticmethod:
                first_arg = None
            # Class/instance method with named first argument: truncate
            elif is_named_positional:
                sig = sig.replace(
                    parameters=list(sig.parameters.values())[1:],
                )
            # Else: create an appropriately named first argument
            else:
                first_arg = inspect.Parameter(
                    name=('cls' if method_cls else 'self'),
                    kind=first_arg.POSITIONAL_OR_KEYWORD,
                )
        elif method_cls is not staticmethod:
            raise TypeError(
                f'func = {func!r} -> {func.__qualname__}{sig}: '
                'class/instance method without a parameter'
            )
        return first_arg, sig, func, method_cls

    def get_return_annotation_from_func1(sig: inspect.Signature) -> Annotation:
        return src_trunc_sig.return_annotation

    def get_default_return_annotation_from_func1(
        sig: inspect.Signature,
    ) -> Annotation:
        return (
            src_trunc_sig if sig.return_annotation is EMPTY else sig
        ).return_annotation

    def get_return_annotation_from_sig(sig: inspect.Signature) -> Annotation:
        return sig.return_annotation

    def filter_dropped_names(
        param: inspect.Parameter, drop: typing.Collection[Keyword],
    ) -> bool:
        return param.name not in drop

    def filter_always(*args, **kwargs) -> Literal[True]:
        return True

    # Actual decorator
    def update_sig(
        func: FunctionLike,
        param_filter: typing.Callable[[inspect.Parameter], bool],
        return_annotation_getter: typing.Callable[
            [inspect.Signature], Annotation,
        ],
    ) -> FunctionLike:
        """
        Update the signature of `func`.

        Parameters
        ----------
        func()
            Callable or static/class method
        param_filter()
            Callable returning for a `Parameter` object whether it
            should be kept in the finalized signature
        return_annotation_getter()
            Callable returning for a `Signature` object its appropriate
            return annotation
        """
        first_arg, sig, func_inner, method_cls = get_truncated_sig(func)
        return_annotation = return_annotation_getter(sig)
        params: typing.Iterable[inspect.Parameter]
        if method_cls is staticmethod:
            # Static method -> use truncated sig
            params = src_trunc_sig.parameters.values()
        else:  # Class/instance method -> use appropriate first arg
            params = (
                src_first_arg if src_method_cls == method_cls else first_arg,
                *src_trunc_sig.parameters.values()
            )
        sig = src_trunc_sig.replace(
            parameters=[p for p in params if param_filter(p)],
            return_annotation=return_annotation,
        )
        if copy:
            func_inner = copy_func(func_inner)
            func_inner.__signature__ = sig
            return method_cls(func_inner) if method_cls else func_inner
        if method_cls:
            func.__func__.__signature__ = sig
        else:
            func.__signature__ = sig
        return func

    # Resolve the suitable strats
    src_first_arg, src_trunc_sig, _, src_method_cls = get_truncated_sig(func)
    return_annotation_getter = (
        get_return_annotation_from_func1
        if copy_return else
        get_default_return_annotation_from_func1
        if (
            copy_return is None and src_trunc_sig.return_annotation is not EMPTY
        ) else
        get_return_annotation_from_sig
    )
    if drop is None:
        param_filter = filter_always
    elif is_identifier(drop):
        param_filter = functools.partial(filter_dropped_names, drop=[drop])
    elif all(is_identifier(name) for name in drop):
        param_filter = functools.partial(filter_dropped_names, drop=drop)
    else:
        raise TypeError(f'drop = {drop!r}: expected parameter name(s)')
    return functools.partial(
        update_sig,
        param_filter=param_filter,
        return_annotation_getter=return_annotation_getter,
    )


def copy_annotations_from(
    func: FunctionLikeNonConstrained,
    *funcs: typing.Sequence[FunctionLikeNonConstrained],
    copy_params: typing.Optional[bool] = None,
    copy_return: typing.Optional[bool] = None,
    copy: bool = False,
) -> typing.Callable[[FunctionLike], FunctionLike]:
    """
    Create a decorator for copying the annotations of one method to
    another.

    Parameters
    ----------
    func, *funcs
        Instance, class, or static methods
    copy_params, copy_return
        True
            Alway copy the parameter/return annotation from the methods
        False
            Never copy the parameter/return annotation of the methods
        None (default)
            Only copy the parameter/return annotation if one exists and
            the decorated function doesn't have it
    copy
        Whether to create a new callable based on the decorated method
        (if true) or to directly update its signature (if false)

    Return
    ------
    Decorator for methods setting its signature

    Example
    -------
    >>> from inspect import signature
    >>> def func(foo: int = 1, bar: str = '') -> None:
    ...     ...
    ...
    >>> another_func = copy_annotations_from(func)(
    ...     lambda foo, bar: None,
    ... )
    >>> print(signature(another_func))
    (foo: int, bar: str) -> None

    Default is to only supply an annotation when absent (set
    `copy_return` and `copy_params` to change):

    >>> @copy_annotations_from(func)
    ... def yet_another_func(foo, bar: int, *, baz: int = 3) -> int:
    ...     return 1
    ...
    >>> print(signature(yet_another_func))
    (foo: int, bar: int, *, baz: int = 3) -> int

    Annotations can be inherited from multiple methods:

    >>> @copy_annotations_from(func, yet_another_func)
    ... def more_func(foo, bar, baz):
    ...     ...
    ...
    >>> print(  # bar and return annotation using func's
    ...     signature(more_func)
    ... )
    (foo: int, bar: str, baz: int) -> None

    Notes
    -----
    If annotations for the same parameter/return type exist in more than
    one of the provided methods, the EARLIER one takes precedence.
    """
    # Helper functions
    def get_return_annotation_from_funcs(sig: inspect.Signature) -> Annotation:
        return return_annotation

    def get_default_return_annotation_from_funcs(
        sig: inspect.Signature,
    ) -> Annotation:
        if sig.return_annotation is EMPTY:
            return return_annotation
        return sig.return_annotation

    def get_return_annotation_from_sig(sig: inspect.Signature) -> Annotation:
        return sig.return_annotation

    def get_param_annotation_from_table(param: inspect.Parameter) -> Annotation:
        return annotations.get(param.name, param.annotation)

    def get_default_param_annotation_from_table(
        param: inspect.Parameter,
    ) -> Annotation:
        if param.annotation is EMPTY:
            return annotations.get(param.name, EMPTY)
        return param.annotation

    def get_param_annotation_from_param(param: inspect.Parameter) -> Annotation:
        return param.annotation

    def update_annotation(name: str, annotation: Annotation) -> None:
        if annotations.setdefault(name, EMPTY) is EMPTY:
            annotations[name] = annotation

    def update_sig(
        func: FunctionLike,
        param_annotation_getter: typing.Callable[
            [inspect.Parameter], Annotation,
        ],
        return_annotation_getter: typing.Callable[
            [inspect.Signature], Annotation,
        ],
    ) -> FunctionLike:
        """
        Update the signature of `func`.

        Parameters
        ----------
        func()
            Callable or static/class method
        param_annotation_getter()
            Callable returning for a `Parameter` object its appropriate
            annotation
        return_annotation_getter()
            Callable returning for a `Signature` object its appropriate
            return annotation
        """
        sig, func_inner, method_cls = get_sig_inner_func_and_converter(func)
        sig = sig.replace(
            parameters=[
                param.replace(annotation=param_annotation_getter(param))
                for param in sig.parameters.values()
            ],
            return_annotation=return_annotation_getter(sig),
        )
        if copy:
            func_inner = copy_func(func_inner)
            func_inner.__signature__ = sig
            return method_cls(func_inner) if method_cls else func_inner
        if method_cls:
            func.__func__.__signature__ = sig
        else:
            func.__signature__ = sig
        return func

    # Gather annotations
    annotations: typing.Dict[
        typing.Union[Keyword, Literal['return']],
        typing.Any
    ] = {}
    for func in (func, *funcs):
        sig, *_ = get_sig_inner_func_and_converter(func)
        update_annotation('return', sig.return_annotation)
        for param in sig.parameters.values():
            update_annotation(param.name, param.annotation)
    return_annotation = annotations.pop('return', EMPTY)
    # Manage annotation-getting strats
    return functools.partial(
        update_sig,
        param_annotation_getter=(
            get_param_annotation_from_table
            if copy_params else
            get_default_param_annotation_from_table
            if copy_params is None else
            get_param_annotation_from_param
        ),
        return_annotation_getter=(
            get_return_annotation_from_funcs
            if copy_return else
            get_default_return_annotation_from_funcs
            if copy_return is None and return_annotation is not EMPTY else
            get_return_annotation_from_sig
        ),
    )


@typing.overload
def deprecate_positionals(
    func: typing.Callable[PS, T], *args, **kwargs
) -> typing.Callable[PS, T]:
    ...


@typing.overload
def deprecate_positionals(
    func: None = None, *args, **kwargs
) -> typing.Callable[[typing.Callable[PS, T]], typing.Callable[PS, T]]:
    ...


def deprecate_positionals(
    func: typing.Optional[typing.Callable[PS, T]] = None,
    *,
    deprecate: typing.Optional[KeywordOrKeywords] = None,
    message: str = (
        'supplying these arguments/this argument as positional(s) is '
        'deprecated and will become invalid for a future version; '
        'use the keyword-argument form instead'
    ),
) -> typing.Union[
    typing.Callable[[typing.Callable[PS, T]], typing.Callable[PS, T]],
    typing.Callable[PS, T],
]:
    r"""
    Deprecate the use of parameters with defaults as positional
    arguments, urging the user to use the keyword-argument form instead

    Parameters
    ----------
    func()
        Optional function to decorate
    deprecate
        Optional name or names of the positional parameters with
        defaults which should be deprecated;
        default is to take all parameters (1) with defaults and (2) can
        be passed both positionally and by keywords
    message
        String message (e.g. deprecation schedule) that should be
        attached to the issued `FutureWarning`

    Example
    -------
    >>> import warnings
    >>> from re import escape
    >>>
    >>> import pytest

    Normal use:

    >>> @deprecate_positionals
    ... def func(
    ...     a: int, b: int, c: int = 3, d: int = 4, *, e: int = 5,
    ... ) -> int:
    ...     return a + b + c + d + e
    ...
    >>> with warnings.catch_warnings():
    ...     warnings.simplefilter('error', FutureWarning)
    ...     func(1, 2)  # This is okay
    ...     func(1, 2, d=3)  # ... and this
    ...     func(1, 2, c=4, d=5)  # ...
    ...     func(1, 2, c=5, d=6, e=7)  # and this too
    15
    14
    17
    21
    >>> with pytest.warns(
    ...     FutureWarning,
    ...     match=(
    ...         r'func\(\.\.\.\): c = 4: .* positional.* deprecated .*'
    ...     ),
    ... ):
    ...     func(1, 2, 4, d=5)  # This is deprecated
    17
    >>> with pytest.warns(
    ...     FutureWarning,
    ...     match=(
    ...         r'func\(\.\.\.\): c = 4, d = 3: '
    ...         '.* positional.* deprecated .*'
    ...     ),
    ... ):
    ...     func(6, 5, 4, 3)  # ... and this too
    23

    Selective deprecation:

    >>> func2 = deprecate_positionals(deprecate='d')(func.__wrapped__)
    >>> with warnings.catch_warnings():
    ...     warnings.simplefilter('error', FutureWarning)
    ...     func2(1, 2, 4, d=5)  # Okay since `c` isn't deprecated
    17
    >>> with pytest.warns(
    ...     FutureWarning,
    ...     match=(
    ...         r'func\(\.\.\.\): d = 3: .* positional.* deprecated .*'
    ...     ),
    ... ):
    ...     func2(6, 5, 4, 3)  # ... and this too
    23

    Notes
    -----
    - The decorated function should not have any variadic positional
      parameter, since values can't be supplied to them without also
      passing every preceding argument as positionals.
      If found, a `TypeError` is raised.

      >>> with pytest.raises(
      ...       TypeError,
      ...       match=escape(
      ...           'foo(*args): '
      ...           'cannot have a variadic positional parameter'
      ...       ),
      ... ):
      ...     @deprecate_positionals
      ...     def foo(a: int = 1, *args: int, b: int = 2) -> None:
      ...         ...

    - It is also a `TypeError` to deprecate a positional parameter and
      not every positional parameter after it:

      >>> with pytest.raises(
      ...     TypeError,
      ...     match=escape(
      ...         "bar(b), deprecate = {'b'}: "
      ...         '`b` cannot be deprecated as a positional parameter; '
      ...         'it is followed by the non-deprecated positional '
      ...         'parameter(s) `c`, `d`'
      ...     ),
      ... ):
      ...     @deprecate_positionals(deprecate=['b'])
      ...     def bar(
      ...           a: int, b: int = 1, c: int = 2, d: int = 3,
      ...     ) -> None:
      ...         ...

    - If `deprecate` is explicitly passed, it is required to be name(s)
      of (an) actual positional parameter(s) with default(s), or it will
      again be a `TypeError`:

      >>> with pytest.raises(
      ...     TypeError,
      ...     match=escape(
      ...         r"baz(...), deprecate = {'a'}: "
      ...         'this is/these are not positional parameters with '
      ...         'defaults: `a`'
      ...     ),
      ... ):
      ...     @deprecate_positionals(deprecate='a')
      ...     def baz(x: float, y: float) -> None:
      ...         ...
    """
    # Input verification
    if func is None:
        return functools.partial(
            deprecate_positionals, deprecate=deprecate, message=message,
        )
    if isinstance(deprecate, str):
        deprecate = {deprecate}
    sig = inspect.signature(func)
    try:
        func_name = func.__name__
    except AttributeError:
        func_name = '<func>'
    if any(
        param.kind is param.VAR_POSITIONAL for param in sig.parameters.values()
    ):  # Check that we don't have a variadic positional param
        offending_param = next(
            param for param in sig.parameters.values()
            if param.kind is param.VAR_POSITIONAL
        )
        raise TypeError(
            f'{func_name}(*{offending_param.name}): '
            'cannot have a variadic positional parameter'
        )
    pos_params = [
        param for param in sig.parameters.values()
        if param.kind in (param.POSITIONAL_ONLY, param.POSITIONAL_OR_KEYWORD)
    ]
    opt_pos_param_names = [
        param.name for param in pos_params
        if param.default is not param.empty
        if param.kind is param.POSITIONAL_OR_KEYWORD
    ]
    if deprecate is None:
        deprecate = set(opt_pos_param_names)
    else:
        deprecate = set(deprecate)
        absent_param_names = (  # Check that the params are present
            deprecate - set(opt_pos_param_names)
        )
        if absent_param_names:
            raise TypeError(
                (
                    '{}(...), deprecate = {!r}: '
                    'this is/these are not positional parameters with '
                    'defaults: {}'
                ).format(
                    func_name,
                    deprecate,
                    ', '.join(f'`{name}`' for name in absent_param_names),
                ),
            )
    # Check that the deprecated pos params are trailing
    opp_deprecated = [name in deprecate for name in opt_pos_param_names]
    if not any(opp_deprecated):  # Nothing to deprecate -> early exit
        return func
    if any(opp_deprecated[:-opp_deprecated.count(True)]):
        i_depr = opp_deprecated.index(
            # First erroneous occurrance of a deprecated param (which is
            # followed by at least one non-deprecated one)
            True,
        )
        raise TypeError(
            (
                '{0}({1}), deprecate = {2!r}: '
                '`{1}` cannot be deprecated as a positional parameter; '
                'it is followed by the non-deprecated positional '
                'parameter(s) {3}'
            ).format(
                func_name,
                opt_pos_param_names[i_depr],
                deprecate,
                ', '.join(
                    f'`{name}`' for name, is_depr in zip(
                        opt_pos_param_names[i_depr:], opp_deprecated[i_depr:],
                    ) if not is_depr
                ),
            ),
        )

    # Actual work
    max_posargs = len(pos_params) - len(deprecate)
    deprecated_param_names = [
        name for name, is_depr in zip(opt_pos_param_names, opp_deprecated)
        if is_depr
    ]

    @functools.wraps(func)
    def deprecation_wrapper(*args, **kwargs):
        ndeprecated = len(args) - max_posargs
        if ndeprecated > 0:
            msg = '{}(...): {}: {}'.format(
                func_name,
                ', '.join(
                    f'{key} = {value!r}' for key, value in zip(
                        deprecated_param_names[:ndeprecated],
                        args[-ndeprecated:],
                    )
                ),
                message,
            )
            warnings.warn(msg, FutureWarning, stacklevel=2)
        return func(*args, **kwargs)

    return deprecation_wrapper
