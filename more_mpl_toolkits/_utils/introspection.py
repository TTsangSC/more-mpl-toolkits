"""
Introspection utilities
"""
import inspect
import typing
try:
    from typing import Literal
except ImportError:
    from typing_extensions import Literal

from tjekmate import is_instance, is_keyword, check

__all__ = ('get_argument_value',)

EMPTY = inspect.Parameter.empty


@typing.overload
def get_argument_value(
    sig: inspect.Signature, argname: str, *args, **kwargs
) -> typing.Union[typing.Any, Literal[EMPTY]]:
    ...


@typing.overload
def get_argument_value(
    sig: inspect.BoundArguments, argname: str,
) -> typing.Union[typing.Any, Literal[EMPTY]]:
    ...


@check(
    sig=is_instance((inspect.Signature, inspect.BoundArguments)),
    argname=is_keyword,
)
def get_argument_value(
    sig: typing.Union[inspect.Signature, inspect.BoundArguments],
    argname: str,
    *args, **kwargs
) -> typing.Union[typing.Any, Literal[EMPTY]]:
    """
    Resolve the value of the argument.

    Parameters
    ----------
    sig
        Signature or BoundArguments;
        if the latter, the variable arguments must not be provided
    argname
        String name for the argument
    *args, **kwargs
        Arguments for binding to `sig`

    Return
    ------
    Value of the argument (if bound) or `inspect.Parameter.empty`
    (otherwise)

    Example
    -------
    >>> import inspect
    >>>
    >>> def foo(x: int, y: float = 2., **kw) -> None:
    ...     ...
    ...
    >>> sig = inspect.signature(foo)
    >>> ba = sig.bind(1, 3, z=5)
    >>> get_argument_value(sig, 'y', *ba.args)
    3
    >>> get_argument_value(ba, 'y')
    3
    >>> get_argument_value(ba, 'z')
    5
    >>> get_argument_value(ba, 'alpha') is inspect.Parameter.empty
    True
    """
    if isinstance(sig, inspect.BoundArguments):
        if args or kwargs:
            raise TypeError(
                f'sig = {sig!r}, argname = {argname!r}, '
                f'*args = {args!r}, **kwargs = {kwargs!r}: '
                'variable arguments not accepted when `sig` is a '
                '`BoundArguments` object'
            )
        ba, sig = sig, sig.signature
    else:  # Signature
        ba = sig.bind(*args, **kwargs)
    try:
        *_, varkwargs_param = sig.parameters.values()
    except ValueError:  # Unpacking error
        varkwargs_name = None
    else:
        if varkwargs_param.kind is varkwargs_param.VAR_KEYWORD:
            varkwargs_name = varkwargs_param.name
        else:
            varkwargs_name = None
    if argname in ba.arguments:
        return ba.arguments[argname]
    param = sig.parameters.get(argname)
    default = getattr(param, 'default', EMPTY)
    if default is not EMPTY:
        return default
    if varkwargs_name:
        return ba.arguments.get(varkwargs_name, {}).get(argname, EMPTY)
    return EMPTY
