"""
Code examples for `more_mpl_toolkits`.

.aligned_annotations
    Draw annotations aligned against the plot area or lines.
.periodic_table
    Draw the periodic table.
"""
