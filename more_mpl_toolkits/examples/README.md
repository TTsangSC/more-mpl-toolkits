`more_mpl_toolkits.examples`
============================

(Alias(es): `~.ex`)

[`.aligned_annotations`][ex-aligned-annotations]
------------------------------------------------

Showcase for how [`~.aligned_annotations`][aa] helps with constructing
annotations aligned against other objects, particularly:

- when (rotated) annotations are to be aligned against the axes bounds,
  and
- when lines are to be annotated.

[`.periodic_table`][ex-periodic-table]
--------------------------------------

Showcase in the form of drawing a periodic table,
for how [`~.categorized_legends`][cl] can be
used for plotting artists and generating a summarizing ("categorized")
legend thereof,
where the various attributes of the artists (shape, style, size, etc.)
depend on the argument values passed to the separate parameters of a
data formatter function.

[aa]: more_mpl_toolkits/aligned_annotations
[cl]: more_mpl_toolkits/categorized_legends
[ex-aligned-annotations]: more_mpl_toolkits/examples/aligned_annotations
[ex-periodic-table]: more_mpl_toolkits/examples/periodic_table
