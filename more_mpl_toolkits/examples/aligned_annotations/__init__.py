"""
Demo for aligning annotations against axes bounds a/o lines.

                                        bar
+---------+---------+   -+-------------------+
|         |         |    |  o  data  b/      |
|   (A)   |   (B)   |   -+ --- fit  +/       |
|         |         |    |         x/ ##     |
+---------+---------+   -+        m/ =       |
|         |         |    |       =/R^2       |
|   (C)   |   (D)   |   -+      y/           |
|         |         |    |      /            |
+---------+---------+   -+----+----+----+----+
                         '   foo   '    '    '
Left panel
----------
The subplots (A--D) host 13 annotations each, aligned against the 4
corners (2 for each corner), 4 edges, and center of the subplot;
annotations in (A) are upright, those in (B) are rotated by 90 degrees,
etc.

Right panel
-----------
Some data points are plotted and a fitting line through them is drawn.
The line is then annotated with its equation and R-squared value, where
the annotation is always rotated to be parallel to the line, event as
the plot limit changes.
In addition, the annotations 'foo' and 'bar' are anchored to where the
line intersects the axes bounds.
"""
