import itertools
import os
import typing

import matplotlib as mpl
import matplotlib.patheffects as mpe
import matplotlib.pyplot as plt
import numpy as np

from more_mpl_toolkits import aligned_annotations as aa, rays

Keyword = typing.TypeVar('Keyword', bound=str)
Keywords = typing.Mapping[Keyword, typing.Any]

RC_PARAMS = {
    'figure.figsize': (10, 5),
    'figure.dpi': 300,
}
GRIDSPEC_KWARGS = dict(
    # 4 in by 4 in subplots
    top=.9,
    bottom=.1,
    left=.05,
    right=.95,
    # Center spacing = 4 in * (2 grid squares) + .5 in * (2 margins))
    wspace=.25,  # Relative to subplot width
)
SUBGRIDSPEC_KWARGS = dict(wspace=0, hspace=0)

FIXED_ANN_POSITIONS = [
    hori if vert == 'center' else vert if hori == 'center' else f'{vert} {hori}'
    for hori, vert in itertools.product(
        ('left', 'right', 'center'), ('top', 'bottom', 'center')
    )
]
FIXED_ANN_COLORS = 'rgby'
FIXED_ANN_ANGLES = np.arange(4)*90
FIXED_ANN_KWARGS = dict(size=6)

RNG = np.random.default_rng(42)
FITTING_N, FITTING_M, FITTING_B = 10, 3., -1.
FITTING_ERROR_X, FITTING_ERROR_Y = .05, .1
FITTING_TOLERANCE = .1
FITTING_X = np.linspace(0, 1, FITTING_N)
FITTING_Y = (
    FITTING_M*FITTING_X + FITTING_B + RNG.normal(0, FITTING_ERROR_Y, FITTING_N)
)
FITTING_X += RNG.normal(0, FITTING_ERROR_X, FITTING_N)

# Notes on the expected errors:
#
# m        = s_xy / s_xx
#          = (<xy> - <x> <y>) / (<x^2> - <x>^2)
# m_x      = (y - <y>) / (n <x^2> - n <x>^2)
#            - (<xy> - <x> <y>) / (<x^2> - <x>^2)^2 * 2 (x - <x>) / n
#          = (dy - 2 m dx) / ((n - ddof) * s_xx)
# m_y      = dx / ((n - ddof) s_xx)
# Err(m)^2 = Err(x)^2 * |m_x|^2 + Err(y)^2 * |m_y|^2
#          = ((n - ddof) s_xx)^-2
#            * (Err(x)^2 * (n - ddof) s_yy + Err(y)^2 * (n - ddof) s_xx)
#          = (m^2 / R^2) / (n - ddof)
#            * (Err(x)^2 / s_xx + Err(y)^2 / s_yy)
#
# b        = <y> - m <x>
# Err(b)^2 = Err(<y>)^2 + m^2 Err(<x>)^2 + <x>^2 Err(m)^2
#          = Err(m)^2 <x>^2 + (Err(x)^2 + Err(y)^2) / n
#          = Err(m)^2 <x>^2
#            + (Err(x)^2 / s_xx * R^2 + Err(y)^2 / s_yy) * s_yy / n

FITTING_PLOT_KWARGS = dict(
    marker='o',
    linestyle='none',
    color='tab:orange',
    label='data',
)
FITTING_AXLINE_KWARGS = dict(
    linestyle='--',
    color='black',
    resolve_defaults=True,
    label='fit',
)

FITTING_VIEWS = (
    # Intercepts: (-.16..., -1.5) -> bottom, (1.16..., 2.5) -> top
    dict(xlim=(-.5, 1.5), ylim=(-1.5, 2.5)),
    # Intercepts: (.25, -.25) -> bottom, (.5, .5) -> right
    dict(xlim=(-.25, .5), ylim=(-1, 2)),
)

FITTING_ANN_OFFSET = .3  # Relative to font size
FITTING_ANN_KWARGS = dict(
    rotation_mode='anchor',
    path_effects=[
        # 2 pt edge width + overlay = 1 pt edge strictly on exterior
        mpe.PathPatchEffect(edgecolor='white', linewidth=2),
        mpe.Normal(),
    ],
)
FITTING_LINE_END_ANNOTATIONS = (
    (
        'foo',
        dict(
            xytext=(0, -FITTING_ANN_OFFSET),
            textcoords='offset fontsize',
            ha='center',
            va='top',
            rotation=0,
        )
    ),
    (
        'bar',
        dict(
            xytext=(FITTING_ANN_OFFSET, FITTING_ANN_OFFSET),
            textcoords='offset fontsize',
            ha='left',
            va='bottom',
            rotation=0,
        )
    ),
)


def main(
    outputs: typing.Tuple[str, str] = tuple(
        os.path.join(
            os.path.dirname(__file__),
            f'aligned_annotations_{suffix}.pdf',
        )
        for suffix in ('before', 'after')
    ),
    *,
    fig: typing.Optional[Keywords] = None,
    gridspec_kwargs: typing.Optional[Keywords] = None,
    subgridspec_kwargs: typing.Optional[Keywords] = None,
    legend: typing.Union[bool, Keywords] = True,
    rc: typing.Optional[typing.Mapping[str, typing.Any]] = None,
) -> None:
    """
    Make two versions of the figure.

    Parameters
    ----------
    outputs
        String paths to write the figure versions to
    fig
        Optional keyword arguments to `matplotlib.plt.figure()` to
        initialize a figure
    gridspec_kwargs
        Optional keyword arguments for generating the primary 1-by-2
        gridspec
    subgridspec_kwargs
        Optional keyword arguments for generating the 2-by-2 sub-
        gridspec
    legend
        Whether to draw a legend;
        if a keyword mapping, use as the kwargs for `Axes.legend()`
    rc
        Optional mapping for `matplotlib.rcParams` entries

    Side effects
    ------------
    The two `outputs` written to
    """
    if rc is not None:
        with mpl.rc_context(rc):
            return main(
                outputs,
                fig=fig,
                gridspec_kwargs=gridspec_kwargs,
                subgridspec_kwargs=subgridspec_kwargs,
                legend=legend,
            )
    figure = plt.figure(**(fig or {}))
    gridspec = figure.add_gridspec(1, 2, **(gridspec_kwargs or {}))
    if legend in (True,):
        legend = {}

    # Draw the panels with axes-aligned annotations
    subgridspec = gridspec[0].subgridspec(2, 2, **(subgridspec_kwargs or {}))
    left_subplots = [figure.add_subplot(subgridspec[i]) for i in range(4)]
    for ax in left_subplots:
        ax.set_xticks([])
        ax.set_yticks([])
    assert (
        len(left_subplots)
        == len(FIXED_ANN_COLORS)
        == len(FIXED_ANN_COLORS)
        == 4
    )
    annotations_set_1: typing.List[mpl.text.Annotation] = []
    annotations_set_2: typing.List[mpl.text.Annotation] = []
    for (
        (edge_flips, corner_flips, annotations_set),
        (ax, color, rotation),
        position,
    ) in itertools.product(
        [
            ([None], ['x', 'y'], annotations_set_1),
            (['xy'], ['xy', None], annotations_set_2),
        ],
        zip(left_subplots, FIXED_ANN_COLORS, FIXED_ANN_ANGLES),
        FIXED_ANN_POSITIONS,
    ):
        if position == 'center':
            flips = [None]
        elif position in ('top', 'bottom', 'left', 'right'):
            flips = edge_flips
        else:
            flips = corner_flips
        for side in flips:
            text = '{}\n($\\theta = {}\\degree$)'.format(
                ('{}\n(outer = {})' if side else '{0}').format(
                    position, side,
                ),
                rotation,
            )
            annotation = aa.annotate_aligned(
                ax, text,
                xy=position, color=color, rotation=rotation, outer=side,
                **FIXED_ANN_KWARGS,
            )
            annotation.set_visible(False)  # Only turn on when needed
            annotations_set.append(annotation)

    # Draw a panel with the results of a fit
    x, y = FITTING_X, FITTING_Y
    (xx, xy), (_, yy) = np.cov(x, y)
    slope = xy / xx
    intercept = (y - slope * x).mean()
    rsq = (xy * xy) / (xx * yy)
    rel_x_sqe = FITTING_ERROR_X**2 / xx
    rel_y_sqe = FITTING_ERROR_Y**2 / yy
    slope_sqe = (slope*slope / rsq) * (rel_x_sqe + rel_y_sqe) / FITTING_N
    intercept_sqe = (
        slope_sqe * x.mean()**2 + yy * (rsq * rel_x_sqe + rel_y_sqe) / FITTING_N
    )
    assert (slope - FITTING_M)**2 < slope_sqe * (1 + FITTING_TOLERANCE)
    assert (intercept - FITTING_B)**2 < intercept_sqe * (1 + FITTING_TOLERANCE)

    right_subplot = figure.add_subplot(gridspec[1])
    right_subplot.plot(x, y, **FITTING_PLOT_KWARGS)
    line = rays.axline(
        right_subplot, (0, intercept), slope=slope, **FITTING_AXLINE_KWARGS,
    )
    line_annotation = (
        '$y = {0:{fmt}}x + {1:{fmt}}$\n($R^2 = {2:{fmt}}$)'.format(
            slope, intercept, rsq, fmt='.2f',
        )
    )
    aa.annotate_aligned(
        right_subplot,
        line_annotation,
        'line',
        axline=line,
        **FITTING_ANN_KWARGS,
    )
    assert len(FITTING_LINE_END_ANNOTATIONS) == 2
    for frac, (end_text, kwargs) in enumerate(FITTING_LINE_END_ANNOTATIONS):
        kwargs = {**FITTING_ANN_KWARGS, **kwargs}
        aa.annotate_aligned(
            right_subplot,
            end_text,
            xy='line',
            axline=line,
            fraction=frac,
            **kwargs,
        )
    if isinstance(legend, typing.Mapping):
        right_subplot.legend(**legend)

    # Save the different views
    assert len(FITTING_VIEWS) == len(outputs) == 2
    for view, annotations_set, output in zip(
        FITTING_VIEWS, (annotations_set_1, annotations_set_2), outputs,
    ):
        # Note: for each set in each subplot we draw 2 annotations per
        # corner, 1 per edge, and 1 per center
        assert len(annotations_set) == 52, len(annotations_set)
        # Axes-bound annotations may be eclipsed by the neighboring
        # axes;
        # remove them and add to the figure instead to circumvent
        for annotation in annotations_set:
            # Note: matplotlib 3.7 made .texts immutable, so
            # .texts.pop() and .texts.remove() wouldn't work;
            # however, since the position of the annotations are
            # dependent on their .axes, we can't completely dissociate
            # annotations from axes
            ax = annotation.axes
            annotation.set_visible(True)
            annotation.remove()
            assert annotation.axes is None
            assert annotation not in ax.texts
            figure.add_artist(annotation)
            annotation.axes = ax
        right_subplot.set(**view)
        figure.savefig(output)
        print(output)
        for annotation in annotations_set:
            annotation.set_visible(False)


if __name__ == '__main__':
    main(
        rc=RC_PARAMS,
        gridspec_kwargs=GRIDSPEC_KWARGS,
        subgridspec_kwargs=SUBGRIDSPEC_KWARGS,
    )
