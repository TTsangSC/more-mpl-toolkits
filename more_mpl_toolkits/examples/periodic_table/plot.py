"""
Plotting code.
"""
import functools
import numbers
import os
import typing

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patheffects as mpe
import numpy as np

import more_mpl_toolkits.categorized_legends as cl

from . import data, type_annotations

# ************************* Plotting params ************************** #

_PATH_CODES = [
    getattr(mpl.path.Path, name)
    for name in 'MOVETO LINETO LINETO CLOSEPOLY'.split()
]
LL_TRIANGLE = mpl.path.Path(
    [(-.5, -.5), (.5, -.5), (-.5, .5), (0, 0)], _PATH_CODES,
)
UR_TRIANGLE = mpl.path.Path(
    [(.5, .5), (-.5, .5), (.5, -.5), (0, 0)], _PATH_CODES,
)

STATE_COLORS: typing.Dict[
    type_annotations.State, type_annotations.Color
] = dict(
    solid='chocolate',
    liquid='turquoise',
    gas='lavender',
    unknown='silver',
)
METALLICITY_COLORS: typing.Dict[
    type_annotations.Metallicity, type_annotations.Color
] = dict(
    metal='gold',
    metalloid='mediumseagreen',
    nonmetal='dimgray',
    unknown='silver',
)
DEFAULT_ATOM_COLOR = 'black'
MEAN_ATOM_RADIUS = np.mean([r for r in data.COVALENT_RADII if r is not None])
MAX_ATOM_RADIUS = max(r for r in data.COVALENT_RADII if r is not None)
BACKGROUND_SIZE = 1
ATOM_SIZE = .8
LABEL_SIZE = .9
COLOR_CALC_ATOM_BIAS = 2
ATOM_EDGE_WIDTH = .6
F_BLOCK_ANNOTATION_OFFSET = 4

# Defaults for the demo
FIG_PADDING = .5
FIG_HEIGHT, FIG_WIDTH = 4, 11
FIGURE_KWARGS = dict(figsize=(FIG_WIDTH, FIG_HEIGHT))
RC_PARAMS = {
    'lines.markersize': 21.5,
    'font.size': 8,
    'hatch.linewidth': 1.25,
    'hatch.color': 'white',
}
PLOT_CATEGORIZED_KWARGS = dict(
    gridspec_kwargs=dict(
        left=FIG_PADDING / FIG_WIDTH,
        right=.6,
        top=1 - FIG_PADDING / FIG_HEIGHT,
        bottom=FIG_PADDING / FIG_HEIGHT,
    ),
)
CATEGORIZED_LEGEND_KWARGS = dict(
    # Show up to 4 categorization values per legend line
    values_per_line=4,
    # Show 2 values out of the range of values of the CKA `atomic_mass`,
    # etc.
    range_samples=dict(atomic_mass=2, stable_mass=2, covalent_radius=3),
    # Only shows the names of these CKAs
    show_category_names=dict(
        atomic_mass=True, stable_mass=True, covalent_radius=True,
    ),
    # Exclude the CKA `element` from the legend
    suppressed_categories='element',
    # Order the CKAs in this way
    category_name_sorter=(
        'atomic_mass',
        'stable_mass',
        'covalent_radius',
        'metallicity',
        'state',
    ).index,
    # For the CKAs whose names are shown in their legend entries, use
    # the mapping from CKA names to typeset strings to typeset the names
    category_name_typesetter=dict(
        atomic_mass='Abridged atomic weight\n',
        stable_mass='Most-stable-isotope relative atomic mass\n',
        covalent_radius=r'Covalent radius ($\mathrm{\AA}$)' '\n',
    ),
    # For each of the named CKAs, use the provided format string or
    # callable to typeset the values
    category_value_typesetters=dict(
        atomic_mass='{:.1f}',
        stable_mass='{:.1f}',
        covalent_radius='{:.2f}',
        metallicity=(
            lambda m: 'Non-metal' if m == 'nonmetal' else m.capitalize()
        ),
        state=str.capitalize,
    ),
    # Don't include whole-line-height extra whitespace between CKAs
    gap_between_categories=False,
    # Vanilla `Axes.legend()` arguments
    loc='center right',
    bbox_to_anchor=(1 - FIG_PADDING / FIG_WIDTH, .5),
    handlelength=8,  # Width of the legend-handle area
    handleheight=4,  # Height of each legend-handle
    labelspacing=3,  # Vertical spacing between lines
    handletextpad=3,  # Hori. spacing between legend handles and labels
    frameon=False,  # No fancy patch around the legend
)

# **************************** Functions ******************************#


def get_abs_brightness_difference(
    c1: type_annotations.Color,
    c2: type_annotations.Color,
) -> numbers.Real:
    """
    Compute the difference between the brightness of two colors.

    Parameters
    ----------
    c1, c2
        Colors (RGB string, normalized RGB arrays, named colors, etc.)

    Return
    ------
    Abs. difference in brightness (normalized)
    """
    # See https://en.wikipedia.org/wiki/Luma_(video)\
    # #Rec._601_luma_versus_Rec._709_luma_coefficients
    luma_weights = .299, .587, .114
    c1, c2 = mpl.colors.to_rgba_array([c1, c2])[:, :-1]
    return abs((c1 - c2).dot(luma_weights))


def get_element_marker_kwargs(
    *,
    element: typing.Optional[numbers.Integral] = None,
    atomic_mass: typing.Optional[numbers.Real] = None,
    stable_mass: typing.Optional[numbers.Real] = None,
    covalent_radius: typing.Optional[numbers.Real] = None,
    metallicity: typing.Optional[type_annotations.Metallicity] = None,
    state: typing.Optional[type_annotations.State] = None,
) -> typing.List[typing.Dict[str, typing.Any]]:
    """
    The DATA FORMATTER which maps CATEGORIZED KEYWORD ARGUMENTS (CKAs)
    provided
    to a CATEGORIZED-LEGEND (CL) METHOD to a sequence of
    keyword-argument mappings.

    Parameters
    ----------
    element
        Zero-indexed element index (= `<atomic number> - 1`)
    atomic_mass
        Abriged atomic weight, based on the naturally-occuring isotopic
        average
    stable_mass
        Averaged/Abriged atomic weight, based on the most-stable
        isotope
    covalent_radius
        Covalent radius of the element (in aangstroem)
    metallicity
        Whether the element is a metal, metalloid, or non-metal
    state
        The standard state of the element (solid, etc.)

    Return
    ------
    List of keyword dictionaries
    """
    base_markersize = mpl.rcParams['lines.markersize'] ** 2
    assert isinstance(base_markersize, numbers.Real)
    base_kwargs = dict(edgecolor='none')
    # Background
    state_kwargs, metallicity_kwargs = {}, {}
    background_size = BACKGROUND_SIZE * base_markersize
    for kwargs, triangle_path, color_map, value in (
        (state_kwargs, UR_TRIANGLE, STATE_COLORS, state),
        (metallicity_kwargs, LL_TRIANGLE, METALLICITY_COLORS, metallicity),
    ):
        if value is not None:
            kwargs.update(
                base_kwargs,
                marker=triangle_path,
                color=color_map[value],
                s=background_size,
            )
        if value == 'unknown':
            kwargs.update(hatch='+++')
    # Foreground and label
    if all(
        param is None
        for param in (element, atomic_mass, stable_mass, covalent_radius)
    ):
        # Hide atoms and labels if no atomic quantities supplied
        atom_kwargs, label_kwargs = {}, {}
    else:
        # Foreground
        if element is None:
            atom_facecolor = DEFAULT_ATOM_COLOR
        else:
            try:
                atom_facecolor = data.JMOL_COLORS[element]
            except IndexError:
                atom_facecolor = DEFAULT_ATOM_COLOR
            if atom_facecolor is None:
                atom_facecolor = DEFAULT_ATOM_COLOR
        atom_radius = covalent_radius or MEAN_ATOM_RADIUS
        atom_size = (
            ATOM_SIZE
            * base_markersize
            * (atom_radius / MAX_ATOM_RADIUS) ** 2
        )
        atom_kwargs = dict(
            base_kwargs,
            marker='o',
            color=atom_facecolor,
            s=atom_size,
            linewidths=ATOM_EDGE_WIDTH,
        )
        # Choose atom edgecolor to have high contrast with the facecolor
        if get_abs_brightness_difference(atom_facecolor, 'black') < .5:
            atom_kwargs.update(edgecolor='white')
        else:
            atom_kwargs.update(edgecolor='black')
        # Choose label facecolor to have high contrast with the
        # surrounding colors
        label_size = LABEL_SIZE * base_markersize
        colors_to_compare = [
            atom_facecolor,
            *(
                kwargs.get('color', mpl.rcParams['figure.facecolor'])
                for kwargs in (state_kwargs, metallicity_kwargs)
            )
        ]
        assert atom_size <= background_size
        atom_real_size = np.pi * atom_size
        half_background_real_size = 2 * background_size
        label_real_size = .5 * label_size
        atom_apparent_size = max(atom_real_size - .2 * label_real_size, 0)
        half_background_apparent_size = (
            half_background_real_size
            - .5 * max(atom_apparent_size, label_real_size)
        )
        weights = (
            [COLOR_CALC_ATOM_BIAS * atom_apparent_size]
            + [half_background_apparent_size] * 2
        )
        label_facecolor = max(
            ('white', 'black'),
            key=lambda color: sum(
                weight * get_abs_brightness_difference(color, bg_color)
                for weight, bg_color in zip(weights, colors_to_compare)
            ),
        )
        label_edgecolor = 'white' if label_facecolor == 'black' else 'black'
        symbol = 'X' if element is None else data.CHEMICAL_SYMBOLS[element]
        if len(symbol) < 2:
            # Pad between symbol and sub-/superscripts so that the
            # overall size of the mathtext-rendered marker is more
            # consistent
            symbol += r'\;' * (2 - len(symbol))
        assert atomic_mass is None or stable_mass is None
        am = (  # Formatted atomic mass
            '{:.1f}'.format(atomic_mass)
            if atomic_mass is not None else
            r'\;[{:.0f}]'.format(stable_mass)
            if stable_mass is not None else
            r'\mathrm{{{}}}'.format(r'\#' * 4)
        )
        cr = (  # Formatted covalent radius
            r'\mathrm{{{}}}'.format(r'\#' * 3)
            if covalent_radius is None else
            '{:.2f}'.format(covalent_radius)
        )
        if len(am) < 5:
            am = r'\;'*(5 - len(am)) + am
        if len(cr) < 4:
            cr = r'\;'*(4 - len(cr)) + cr
        label_kwargs = dict(
            base_kwargs,
            marker=rf'$\mathbf{{{symbol}}}^{{{am}}}_{{{cr}}}$',
            s=label_size,
            # Use path effect to make sure that the edge doesn't eat
            # into the text
            path_effects=[
                mpe.PathPatchEffect(
                    edgecolor=label_edgecolor,
                    linewidth=2 * ATOM_EDGE_WIDTH,
                    joinstyle='round',
                ),
                mpe.PathPatchEffect(
                    facecolor=label_facecolor,
                    edgecolor='none',
                ),
            ],
        )
    return [
        kwargs
        for kwargs in (
            state_kwargs, metallicity_kwargs, atom_kwargs, label_kwargs,
        )
        if kwargs
    ]


def get_element_data(element: numbers.Integral) -> type_annotations.Keywords:
    """
    The CATEGORIZER, which gathers the data pertaining to an element in
    the form of keyword arguments to be passed to a CL METHOD.

    Parameters
    ----------
    element
        Zero-indexed element index (= `<atomic number> - 1`)

    Return
    ------
    Dictionary with the keys `element`, `atomic_mass`, `stable_mass`,
    `covalent_radius`, `metallicity`, and `state`
    """
    data_tables = dict(
        atomic_mass=data.AVERAGE_MASSES,
        stable_mass=data.STABLE_MASSES,
        covalent_radius=data.COVALENT_RADII,
        state=data.STATES,
        metallicity=data.METALLICITIES,
    )
    results: type_annotations.Keywords = dict(element=element)
    for quantity, table in data_tables.items():
        try:
            value = table[element]
        except IndexError:
            continue
        if value is not None:
            results[quantity] = value
    return results


def get_xy_grid_position(
    element: numbers.Integral,
) -> typing.Tuple[numbers.Integral, numbers.Integral]:
    """
    Compute the grid point on which to place the element;
    lanthanides and actinides (the f-block) are placed beneath the
    others.

    Parameters
    ----------
    element
        Zero-indexed element index (= `<atomic number> - 1`)

    Return
    ------
    Tuple `(x_coordinate, y_coordinate)`
    """
    period, group = data.PERIODS_GROUPS[element]
    assert isinstance(group, numbers.Integral)
    if isinstance(period, numbers.Integral):
        x = group - (3 if group < 3 else 2)
        y = data.MAX_PERIOD - period + 1
    elif period in ('La', 'Ac'):
        x, y = group - 1, dict(La=-1, Ac=-2)[period]
    else:
        raise AssertionError(f'period; {period!r}')
    return x, y


def make_periodic_table(
    fig: typing.Optional[type_annotations.Keywords] = None,
    **kwargs
) -> typing.Tuple[cl.Figure, cl.Axes]:
    """
    Plot the periodic table onto a figure.

    Parameters
    ----------
    fig
        Optional keyword arguments to `matplotlib.plt.figure()` to
        initialize a figure
    **kwargs
        Keyword arguments for
        `<MODULE>.categorized_legend.plot_categorized()`

    Return
    ------
    Tuple of `(figure, axes)`
    """
    base_markersize = mpl.rcParams['lines.markersize']
    figure = cl.Figure(instantiator=plt.figure, **(fig or {}))
    elements = range(data.MAX_NUMBER)
    x_coords, y_coords = zip(*(
        get_xy_grid_position(element) for element in elements
    ))
    _fig, ax = cl.plot_categorized(
        # This is our (rows of) data
        elements,
        # These two callables take each row to a positional argument to
        # be passed to the CL method
        data_getters=(x_coords.__getitem__, y_coords.__getitem__),
        # This callable takes each row to a keyword mapping of CKAs
        categorizer=get_element_data,
        # This callable takes CKAs to sets of arguments for the Axes
        # method underlying the CL emthod
        formatter=get_element_marker_kwargs,
        # Axes method to use
        method='scatter',
        fig=figure,
        # Make sure that we don't split the data points betwen subplots
        **{k: v for k, v in kwargs.items() if not k.startswith('split_')},
    )
    assert figure is _fig
    for series in 'lanthanides', 'actinides':
        x, y = get_xy_grid_position(getattr(data, series.upper())[0])
        ax.annotate(
            series.capitalize() + ':',
            xy=(x - 1, y),
            xytext=(-(base_markersize + F_BLOCK_ANNOTATION_OFFSET), 0),
            textcoords='offset points',
            ha='right',
            va='center',
        )
    ax.set_axis_off()
    return figure, ax


def main(
    output: str = os.path.join(os.path.dirname(__file__), 'periodic_table.pdf'),
    *,
    fig: typing.Optional[type_annotations.Keywords] = None,
    rc: typing.Optional[typing.Mapping[str, typing.Any]] = None,
    plot_categorized: typing.Optional[type_annotations.Keywords] = None,
    categorized_legend: typing.Union[
        type_annotations.Keywords, bool, None,
    ] = None,
) -> None:
    """
    Make the full figure.

    Parameters
    ----------
    output
        String path to write the figure to
    fig
        Optional keyword arguments to `matplotlib.plt.figure()` to
        initialize a figure
    rc
        Optional mapping for `matplotlib.rcParams` entries
    plot_categorized
        Optional keyword arguments for
        `<MODULE>.categorized_legend.plot_categorized()` for drawing the
        main table
    categorized_legend
        Optional keyword arguments for
        `<MODULE>.categorized_legend.Figure.categorized_legend()` for
        drawing the categorized legend (CL);
        can also be a boolean controlling whether the CL is drawn

    Side effects
    ------------
    `output` written to
    """
    if rc is not None:
        with mpl.rc_context(rc):
            return main(
                output,
                fig=fig,
                plot_categorized=plot_categorized,
                categorized_legend=categorized_legend,
            )
    if categorized_legend in (True, None):
        categorized_legend = {}
    figure, ax = make_periodic_table(fig, **(plot_categorized or {}))
    ax.set_aspect('equal')
    if isinstance(categorized_legend, typing.Mapping):
        figure.categorized_legend(**categorized_legend)
    figure.savefig(output)
    print(output)


main_demo = functools.partial(
    main,
    fig=FIGURE_KWARGS,
    rc=RC_PARAMS,
    plot_categorized=PLOT_CATEGORIZED_KWARGS,
    categorized_legend=CATEGORIZED_LEGEND_KWARGS,
)
