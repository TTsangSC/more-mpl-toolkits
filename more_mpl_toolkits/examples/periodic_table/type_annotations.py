import numbers
import typing
try:
    from typing import Literal
except ImportError:
    from typing_extensions import Literal

import numpy as np


Symbol = typing.TypeVar('Symbol', bound=str)
Metallicity = Literal['metal', 'metalloid', 'nonmetal', 'unknown']
State = Literal['gas', 'liquid', 'solid', 'unknown']

RGB = typing.TypeVar('RGB', bound=str)
NamedColor = typing.TypeVar('NamedColor', bound=str)
Color = typing.Union[np.ndarray, typing.Sequence[numbers.Real], RGB, NamedColor]

Keyword = typing.TypeVar('Keyword', bound=str)
Keywords = typing.Mapping[Keyword, typing.Any]
