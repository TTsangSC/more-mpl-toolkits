"""
Data tables

Notes
-----
All items in `.__all__` are lists indexed by `atomic_number - 1`, i.e.
0 -> H, 1 -> He, etc.
"""
import numbers
import typing

from . import type_annotations


__all__ = (
    'CHEMICAL_SYMBOLS',
    'PERIODS_GROUPS',
    'METALLICITIES',
    'STATES',
    'COVALENT_RADII',
    'AVERAGE_MASSES',
    'STABLE_MASSES',
    'JMOL_COLORS',
)


def get_atomic_number(
    element: typing.Union[type_annotations.Symbol, numbers.Integral],
) -> int:
    try:
        return CHEMICAL_SYMBOLS.index(element) + 1
    except ValueError:
        return int(element)


# ************************** Atomic symbols ************************** #
# (See https://en.wikipedia.org/wiki/Periodic_table)

CHEMICAL_SYMBOLS: typing.List[type_annotations.Symbol] = [
    symbol
    for symbol in """
    H                                                     He
    Li Be                                  B  C  N  O  F  Ne
    Na Mg                                  Al Si P  S  Cl Ar
    K  Ca    Sc Ti V  Cr Mn Fe Co Ni Cu Zn Ga Ge As Se Br Kr
    Rb Sr    Y  Zr Nb Mo Tc Ru Rh Pd Ag Cd In Sn Sb Te I  Xe
    Cs Ba *  Lu Hf Ta W  Re Os Ir Pt Au Hg Tl Pb Bi Po At Rn
    Fr Ra ** Lr Rf Db Sg Bh Hs Mt Ds Rg Cn Nh Fl Mc Lv Ts Og
    """.split()
    if symbol.isidentifier()
]
_lu = CHEMICAL_SYMBOLS.index('Lu')
_lr = CHEMICAL_SYMBOLS.index('Lr')
# Lanthanides and actinides
CHEMICAL_SYMBOLS = [
    *CHEMICAL_SYMBOLS[:_lu],
    *'La Ce Pr Nd Pm Sm Eu Gd Tb Dy Ho Er Tm Yb'.split(),
    *CHEMICAL_SYMBOLS[_lu:_lr],
    *'Ac Th Pa U Np Pu Am Cm Bk Cf Es Fm Md No'.split(),
    *CHEMICAL_SYMBOLS[_lr:],
]
del _lu, _lr

# ************************ Positions on grid ************************* #

# Alkali (earth) metals, noble gases
GROUPS: typing.Dict[int, typing.Tuple[typing.Union[int, None]]] = {
    # dict[group_num][period_num - 1] = atomic_num or None
    1: (1, 3, 11, 19, 37, 55, 87),
    2: (None, 4, 12, 20, 38, 56, 88),
    18: (2, 10, 18, 36, 54, 86, 118),
}
# Boron group -> halogens
GROUPS.update(
    (group, (None, *(offset + group for offset in (-8, 0, 18, 36, 68, 100))))
    for group in range(13, 18)
)
# d-block metals
GROUPS.update(
    (group, (*((None,) * 3), *(offset + group for offset in (18, 36, 68, 100))))
    for group in range(3, 13)
)
# Per tradition, take lanthanides and actinides out and place at the
# bottom
LANTHANIDES, ACTINIDES = (tuple(range(first, first + 14)) for first in (57, 89))

# Lay the elements out on a 2D grid: normal groups
_p_g = {  # dict[atomic_num] = (i_grid_x, i_grid_y)
    elem: (offset + 1, group)
    for group, elems in GROUPS.items() for offset, elem in enumerate(elems)
    if elem
}
_p_g.update(
    (elem, (label, offset + 1))
    for label, special_group in dict(La=LANTHANIDES, Ac=ACTINIDES).items()
    for offset, elem in enumerate(special_group)
)
PERIODS_GROUPS: typing.List[typing.Tuple[typing.Union[int, str], int]] = [
    pg for _, pg in sorted(_p_g.items())
]
del _p_g

MAX_PERIOD = max(
    p for p, _ in PERIODS_GROUPS if isinstance(p, numbers.Integral)
)
MAX_NUMBER = len(PERIODS_GROUPS)

# ************************** Metallicities *************************** #

METALLOIDS = {
    get_atomic_number(e)
    for e in """
    B
       Si
       Ge As
       Sb Te
          Po
    """.split()
}
NONMETALS = {
    get_atomic_number(e)
    for e in """
    H              He
       C  N  O  F  Ne
          P  S  Cl Ar
             Se Br Kr
                I  Xe
                   Rn
    """.split()
}
UNKNOWN_CLASSIFICATIONS = {
    get_atomic_number(e) for e in ('Fr', 'At', *range(100, MAX_NUMBER + 1))
}
_m: typing.Dict[int, type_annotations.Metallicity] = dict.fromkeys(
    range(1, MAX_NUMBER + 1), 'metal',
)
_m.update(
    (e, label)
    for label, elements in dict(
        metalloid=METALLOIDS,
        nonmetal=NONMETALS,
        unknown=UNKNOWN_CLASSIFICATIONS,
    ).items()
    for e in elements
)
METALLICITIES: typing.List[type_annotations.Metallicity] = [
    metallicity for _, metallicity in sorted(_m.items())
]
del _m

# ****************************** States ****************************** #

GASES = {
    get_atomic_number(e)
    for e in """
    H           He
       N  O  F  Ne
             Cl Ar
                Kr
                Xe
                Rn
    """.split()
}
LIQUIDS = {get_atomic_number(e) for e in ('Br', 'Hg')}
UNKNOWN_STATES = {get_atomic_number(e) for e in range(100, MAX_NUMBER + 1)}
_st: typing.Dict[int, type_annotations.State] = dict.fromkeys(
    range(1, MAX_NUMBER + 1), 'solid',
)
_st.update(
    (e, label)
    for label, elements in dict(
        gas=GASES,
        liquid=LIQUIDS,
        unknown=UNKNOWN_STATES,
    ).items()
    for e in elements
)
STATES: typing.List[type_annotations.State] = [
    st for _, st in sorted(_st.items())
]
del _st

# ************************** Covalent radii ************************** #
# (See also:
# https://en.wikipedia.org/wiki/Atomic_radii_of_the_elements_(data_page)
# https://jmol.sourceforge.net/jscolors/
# )

COVALENT_RADII: typing.List[typing.Union[float, None]]
AVERAGE_MASSES: typing.List[typing.Union[float, None]]
STABLE_MASSES: typing.List[typing.Union[float, None]]
JMOL_COLORS: typing.List[typing.Union[type_annotations.RGB, None]]

_cr_am_sm_c = zip(
    # Period 1 (s: H-He)
    (.32, 1.0080, None, '#FFFFFF'),
    (.46, 4.0026, None, '#D900FF'),
    # Period 2 (s + p: Li-Ne)
    (1.33, 6.94, None, '#CC80FF'),
    (1.02, 9.0122, None, '#C2FF00'),
    (.85, 10.81, None, '#FFB5B5'),
    (.75, 12.011, None, '#909090'),
    (.71, 14.007, None, '#3050F8'),
    (.63, 15.999, None, '#FF0D0D'),
    (.64, 18.998, None, '#90E050'),
    (.67, 20.180, None, '#B3E3F5'),
    # Period 3 (s + p: Na-Ar)
    (1.55, 22.990, None, '#AB5CF2'),
    (1.39, 24.305, None, '#8AFF00'),
    (1.26, 26.982, None, '#BFA6A6'),
    (1.16, 28.085, None, '#f0C8A0'),
    (1.11, 30.974, None, '#FF8000'),
    (1.03, 32.06, None, '#FFFF30'),
    (.99, 35.45, None, '#1FF01F'),
    (.96, 39.95, None, '#80D1E3'),
    # Period 4 (s: K-Ca)
    (1.96, 30.098, None, '#8F40D4'),
    (1.71, 40.078, None, '#3DFF00'),
    # Period 4 (d: Sc-Zn)
    (1.48, 44.956, None, '#E6E6E6'),
    (1.36, 47.867, None, '#BFC2C7'),
    (1.34, 50.962, None, '#A6a6AB'),
    (1.22, 51.996, None, '#8A99C7'),
    (1.19, 54.938, None, '#9C7AC7'),
    (1.16, 55.845, None, '#E06633'),
    (1.11, 58.933, None, '#F090A0'),
    (1.10, 58.693, None, '#50D050'),
    (1.12, 63.546, None, '#C88033'),
    (1.18, 65.38, None, '#7D80B0'),
    # Period 4 (p: Ga-Kr)
    (1.24, 69.723, None, '#C28F8F'),
    (1.21, 72.630, None, '#668F8F'),
    (1.21, 74.922, None, '#BD80E3'),
    (1.16, 78.971, None, '#FFA100'),
    (1.14, 79.904, None, '#A62929'),
    (1.17, 83.798, None, '#5CB8D1'),
    # Period 5 (s: Rb-Sr)
    (2.10, 85.468, None, '#703EB0'),
    (1.85, 87.62, None, '#00FF00'),
    # Period 5 (d: Y-Cd)
    (1.63, 88.906, None, '#94FFFF'),
    (1.54, 91.224, None, '#94E0E0'),
    (1.47, 92.906, None, '#73C2C9'),
    (1.38, 95.95, None, '#54B5B5'),
    (1.28, None, 97, '#3B9E9E'),
    (1.25, 101.07, None, '#248F8F'),
    (1.25, 102.91, None, '#0A7D8C'),
    (1.20, 106.42, None, '#006985'),
    (1.28, 107.87, None, '#C0C0C0'),
    (1.36, 112.41, None, '#FFD98F'),
    # Period 5 (p: In-Xe)
    (1.42, 114.82, None, '#A67573'),
    (1.40, 118.71, None, '#668080'),
    (1.40, 121.76, None, '#9E63B5'),
    (1.36, 127.60, None, '#D47A00'),
    (1.33, 126.90, None, '#940094'),
    (1.31, 131.29, None, '#429EB0'),
    # Period 6 (s: Cs-Ba)
    (2.32, 132.91, None, '#5717BF'),
    (1.96, 137.33, None, '#00C900'),
    # Period 6 (f: La-Yb)
    (1.80, 138.91, None, '#70D4FF'),
    (1.63, 140.12, None, '#FFFFC7'),
    (1.76, 140.91, None, '#D9FFC7'),
    (1.74, 144.24, None, '#C7FFC7'),
    (1.73, None, 145, '#A3FFC7'),
    (1.72, 150.36, None, '#8FFFC7'),
    (1.68, 151.96, None, '#61FFC7'),
    (1.69, 157.25, None, '#45FFC7'),
    (1.68, 158.93, None, '#30FFC7'),
    (1.67, 162.50, None, '#1FFFC7'),
    (1.66, 164.93, None, '#00FF9C'),
    (1.65, 167.26, None, '#00E675'),
    (1.64, 168.93, None, '#00D452'),
    (1.70, 173.05, None, '#00BF38'),
    # Period 6 (d: Lu-Hg)
    (1.62, 174.97, None, '#00AB24'),
    (1.52, 178.49, None, '#4DC2FF'),
    (1.46, 180.95, None, '#4DA6FF'),
    (1.37, 183.84, None, '#2194D6'),
    (1.31, 186.21, None, '#267DAB'),
    (1.29, 190.23, None, '#266696'),
    (1.22, 192.22, None, '#175487'),
    (1.23, 195.08, None, '#D0D0E0'),
    (1.24, 196.97, None, '#FFD123'),
    (1.33, 200.59, None, '#B8B8D0'),
    # Period 6 (p: Tl-Rn)
    (1.44, 204.38, None, '#A6544D'),
    (1.44, 207.2, None, '#575961'),
    (1.51, 208.98, None, '#9E4FB5'),
    (1.45, None, 209, '#AB5C00'),
    (1.47, None, 210, '#754F45'),
    (1.42, None, 222, '#428296'),
    # Period 7 (s: Fr-Ra)
    (None, None, 223, '#420066'),
    (2.01, None, 226, '#007D00'),
    # Period 7 (f: Ac-Cm)
    (1.86, None, 227, '#70ABFA'),
    (1.75, 232.04, None, '#00BAFF'),
    (1.69, 231.04, None, '#00A1FF'),
    (1.70, 238.03, None, '#008FFF'),
    (1.71, None, 237, '#0080FF'),
    (1.72, None, 244, '#006BFF'),
    (1.66, None, 243, '#545CF2'),
    (1.66, None, 247, '#785CE3'),
    (None, None, 247, '#8A4FE3'),
    (None, None, 251, '#A136D4'),
    (None, None, 252, '#B31FD4'),
    (None, None, 257, '#B31FBA'),
    (None, None, 258, '#B30DA6'),
    (None, None, 259, '#BD0D87'),
    # Period 7 (d: Lr-Cn)
    (None, None, 266, '#C70066'),
    (None, None, 267, '#CC0059'),
    (None, None, 268, '#D1004F'),
    (None, None, 269, '#D90045'),
    (None, None, 270, '#E00038'),
    (None, None, 269, '#E6002E'),
    (None, None, 278, '#EB0026'),
    (None, None, 281, None),
    (None, None, 282, None),
    (None, None, 285, None),
    # Period 7 (p: Nh-Og)
    (None, None, 286, None),
    (None, None, 289, None),
    (None, None, 290, None),
    (None, None, 293, None),
    (None, None, 294, None),
    (None, None, 294, None),
)

COVALENT_RADII, AVERAGE_MASSES, STABLE_MASSES, JMOL_COLORS = (
    list(items) for items in _cr_am_sm_c
)
