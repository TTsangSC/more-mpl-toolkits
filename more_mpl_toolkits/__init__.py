"""
Miscellaneous utilities for working with `matplotlib`.

Subpackages
-----------
.aligned_annotations
    Alignment of annotations to other plot elements.
    Alias(es): `aa`

.categorized_legends
    Visualization of categorized data.
    Alias(es): `categorized_legend` (for backwards compatibility), `cl`

.legend_placement
    Optimal placement of figure legends.
    Alias(es): `lp`

.rays
    Drawing of rays (lines extending indefinitely), as a replacement/
    backport/augmentation of `Axes.axline()`.
    Alias(es): `ra`

.static_grids
    Figure grid layouts with fixed margins.
    Alias(es): `sg`

.examples
    Various code examples.
    Alias(es): `ex`

Aliasing support
----------------
DEFAULT_PACKAGE_ALIASES
    Default aliases for subpackages

enable_aliases(), disable_aliases()
    En-/Disable the use of aliases

set_aliases()
    Create, override, and/or disable specific aliases

reset_aliases()
    Restore the default aliases

Environment variables
---------------------
${MPL_ENABLE_ALIASES}, ${MPL_DISABLE_ALIASES}
    En-/Disable the use of aliases at launch;
    if available and non-empty, their values are `ast.literal_eval()`-ed
    to truey/falsy values at package import, which determines if
    `enable_aliases()` is called during the import;
    if none are provided, default to using aliases;
    if both are provided and conflicts one another (e.g. both true),
    defer to ${MPL_ENABLE_ALIASES}
"""
import importlib
import pkgutil
import types
import typing
import warnings
from ._api_aliases import (  # Import alias handling
    DEFAULT_PACKAGE_ALIASES,
    enable_aliases, disable_aliases, set_aliases, reset_aliases,
)

__version__ = '1.6.1'
__all__ = (
    'DEFAULT_PACKAGE_ALIASES',
    'enable_aliases', 'disable_aliases', 'set_aliases', 'reset_aliases',
)

# Convenience auto-imports


def __getattr__(submodule: str) -> types.ModuleType:
    """
    Automatically import subpackages as requested.
    """
    def error(addendum: typing.Optional[str] = None) -> AttributeError:
        try:
            this_package = importlib.import_module(pkg_spec.name)
            if vars(this_package) is not globals():
                msg = (
                    'imported module object {!r} is not identical to the '
                    'global namespace'
                ).format(pkg_spec.name)
                warnings.warn(msg, ImportWarning)
                raise ImportError(msg)
        except Exception:
            obj = None
        else:
            obj = this_package
        msg = 'module {!r} has no attribute {!r}{}'.format(
            pkg_spec.name, submodule, (f' ({addendum})' if addendum else ''),
        )
        kwargs = dict(name=submodule)
        if obj:
            kwargs.update(obj=obj)
        # Note: keyword-argument support for exceptions is wonky, the
        # .name and .obj attributes for AttributeError seems to be a
        # 3.10 addition
        # So add the attributes post-hoc
        exc = AttributeError(msg)
        for attr_name, attr_value in kwargs.items():
            setattr(exc, attr_name, attr_value)
        return exc

    pkg_spec = __spec__
    available_submodules = {
        info.name
        for info in pkgutil.iter_modules(pkg_spec.submodule_search_locations)
    }
    if submodule in {*available_submodules, *DEFAULT_PACKAGE_ALIASES}:
        try:
            return importlib.import_module(f'{pkg_spec.name}.{submodule}')
        except ImportError:
            if submodule in DEFAULT_PACKAGE_ALIASES:  # Aliases disabled
                addendum = (
                    '`.{}` is an alias for `.{}`; '
                    'use `.enable_aliases()` to (re-)enable aliases'
                ).format(submodule, DEFAULT_PACKAGE_ALIASES[submodule])
            else:
                addendum = None
            raise error(addendum) from None
    else:
        raise error()
