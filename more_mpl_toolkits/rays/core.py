"""
Core functionalities.
"""
import math
import numbers
import operator
import typing
import warnings
try:
    from typing import Literal
except ImportError:
    from typing_extensions import Literal

import matplotlib.axes
import matplotlib.colors
import matplotlib.lines
import matplotlib.patches
import matplotlib.transforms
import numpy as np


from tjekmate import (
    identical_to,
    is_collection, is_in, is_instance, is_sequence, is_strict_boolean,
    check,
)

__all__ = (
    'Ray',
    'axline',
    'draw_parity_line',
    'get_line_defaults',
)

Point = typing.Tuple[numbers.Real, numbers.Real]
Ray = typing.TypeVar('Ray', bound='Ray')
AxlineDefaultsSource = typing.Union[
    Literal['spines', 'grid'],
    matplotlib.lines.Line2D,
    matplotlib.patches.Patch,
    typing.Collection[
        typing.Union[matplotlib.lines.Line2D, matplotlib.patches.Patch]
    ],
]


def _is_patch_or_line(obj: typing.Any) -> bool:
    return isinstance(obj, (matplotlib.patches.Patch, matplotlib.lines.Line2D))


def _is_real(x: typing.Any) -> bool:
    """
    Example
    -------
    >>> [_is_real(x) for x in [None, 1, float('inf'), float('nan')]]
    [False, True, True, False]
    """
    return isinstance(x, numbers.Real) and not math.isnan(x)


_is_point = is_sequence(length=2, item_checker=_is_real)

# Drawing rays


class Ray(matplotlib.lines.Line2D):
    """
    Helper `Line2D` class for recalculating the transform when plotting.

    Attributes
    ----------
    anchors
        Two points
    slope
        Slope

    Notes
    -----
    - Somehow changing the axes bounds doesn't invalidate the chained
      transform by default, so we have to do it manually in
      `.get_transform()` and `.draw()`.

    - To be instantiated via `.make()`;
      using `.__new__()` directly may result in undefined behavior.
    """
    anchors: typing.Tuple[Point, Point]
    slope: numbers.Real

    # Overridden methods
    def __init__(self, *args, **kwargs) -> None:
        msg = (
            f'using {type(self).__name__}() to directly instantiate is '
            'deprecated; use {type(self).__name__}.make() instead'
        )
        # Note: this is intentionally left as a `UserWarning` instead of
        # a `DeprecationWarning` so that it is by default visible to
        # end-users; `FutureWarning` doesn't seem a good fit either,
        # so...
        warnings.warn(msg)
        matplotlib.lines.Line2D.__init__(self, *args, **kwargs)

    def __repr__(self) -> str:
        return ''.join((
            type(self).__name__,
            '(',
            ', '.join(
                f'{name}={getattr(self, name)!r}'
                for name in ('anchors', 'slope')
            ),
            ')',
        ))

    def get_transform(self) -> matplotlib.transforms.Transform:
        if getattr(self, 'axes', None):
            self.set_transform(self._get_ray_transform())
        return matplotlib.lines.Line2D.get_transform(self)

    def draw(self, renderer: typing.Any) -> None:
        self.get_transform()
        matplotlib.lines.Line2D.draw(self, renderer)

    # Instantiator
    @typing.overload
    @classmethod
    def make(
        cls, xy1: Point, xy2: Point, **kwargs
    ) -> matplotlib.lines.Line2D:
        ...

    @typing.overload
    @classmethod
    def make(
        cls, xy1: Point, xy2: None = None, *, slope: numbers.Real, **kwargs
    ) -> matplotlib.lines.Line2D:
        ...

    @classmethod
    def make(
        cls: typing.Type[Ray],
        xy1: Point,
        xy2: typing.Optional[Point] = None,
        *,
        slope: typing.Optional[numbers.Real] = None,
        **kwargs
    ) -> Ray:
        """
        Create a new instance.

        Parameters
        ----------
        xy1
            x- and y-coordinates (in data coordinates)
        xy2
            Optional x- and y-coordinates !- `xy1` for defining the
            line with two points (in data coordinates);
            either this or `slope` should be provided
        slope
            Optional slope of the line for defining the line with a
            single point (in data coordinates);
            either this or `xy2` should be provided
        **kwargs
            Passed to `matplotlib.lines.Line2D()`

        Return
        ------
        Instance
        """
        inst = object.__new__(cls)
        if not ((xy2 is None) ^ (slope is None)):
            raise TypeError(
                f'xy1 = {xy1!r}, slope = {slope!r}: '
                'either (but not both) must be `None`'
            )
        inst.anchors = xy1, xy2
        inst.slope = slope
        # The code will complain if we directly use the initializer
        matplotlib.lines.Line2D.__init__(
            inst, xdata=(0, 1), ydata=(0, 1), **kwargs,
        )
        return inst

    # Transforms
    def _get_ray_transform(self) -> matplotlib.transforms.Transform:
        """
        Assuming that the anchor points have data coordinates
          (x1, y1), (x2, y2)
        and defining
          dx := x2 - x1, dy := y2 - y1,
        and that the axes bounds have data coordinates
          (X1, Y1), (X2, Y2)
          dX := X2 - X1, dY := Y2 - Y1,
        we have for the desired transform (from fraction to data
        coords.)
          T: (u, v)
          -> (X1 + dX*u, (y1*x2 - y2*x1)/dx + dy/dx*(X1 + dX*v))
        Consider
          T1: (u, v) -> (X1 + dX*u, X1 + dX*v)
          T2: (u', v') -> (u', (y1*x2 - y2*x1)/dx + dy/dx*v')
        (we can also by symmetry write)
          T1: (u, v) -> (Y1 + dY*u, Y1 + dY*v)
          T2: (u', v') -> ((x1*y2 - x2*y1)/dy + dx/dy*u', v')
        We then have
          T = T1 + T2

        Return
        ------
        Transform taking the points (0, 0), (1, 1) to two points on the
        ray passing though both `.anchors` at the axes limits, in
        display coordinates

        Notes
        -----
        Requires `.axes`.
        """
        tr = matplotlib.transforms
        a1, a2 = self.anchors
        m = self.slope
        dx, dy = (a2[i] - a1[i] for i in (0, 1))
        ax = self.axes
        axes2data = ax.transAxes - ax.transData
        swap_axes = tr.Affine2D.from_values(0, 1, 1, 0, 0, 0)
        if math.isfinite(m) and (m == 0 or abs(m) <= abs(1 / m)):
            transform_1 = tr.blended_transform_factory(
                axes2data, (swap_axes + axes2data + swap_axes),
            )
            transform_2 = tr.Affine2D.from_values(
                1, 0, 0, m, 0, (a1[1] * a2[0] - a1[0] * a2[1]) / dx,
            )
        else:
            transform_1 = tr.blended_transform_factory(
                (swap_axes + axes2data + swap_axes), axes2data,
            )
            transform_2 = tr.Affine2D.from_values(
                1 / m, 0, 0, 1, (a1[0] * a2[1] - a1[1] * a2[0]) / dy, 0,
            )
        return transform_1 + transform_2 + ax.transData

    # Bookkeeping
    def _resolve_anchors_from_slope(self) -> None:
        dx: numbers.Real
        dy: numbers.Real
        m = self.slope
        first = self.anchors[0]
        if math.isfinite(m):
            dx, dy = 1, m
        else:  # Infinities
            dx, dy = 0, math.copysign(1, m)
        self._anchors = (first, (first[0] + dx, first[1] + dy))

    def _resolve_slope_from_anchors(self) -> None:
        dx: numbers.Real
        dy: numbers.Real
        a1, a2 = self.anchors
        dx, dy = (a2[i] - a1[i] for i in (0, 1))
        if dx:
            self._slope = dy / dx
        else:
            self._slope = (
                float('inf') * math.copysign(1, dy) * math.copysign(1, dx)
            )

    @property
    def anchors(self) -> typing.Tuple[Point, typing.Union[Point, None]]:
        return self._anchors

    @anchors.setter
    def anchors(
        self, anchors: typing.Tuple[Point, typing.Union[Point, None]],
    ) -> None:
        if not (
            is_sequence(anchors, length=2) and
            _is_point(anchors[0]) and
            (anchors[1] is None or _is_point(anchors[1]))
        ):
            raise TypeError(
                f'anchors = {anchors!r}: '
                'expected `(point, point)` or `(point, None)`'
            )
        anchors = tuple(tuple(a) if a else None for a in anchors)
        if anchors[0] == anchors[1]:
            raise ValueError(
                f'.anchors[0] = {anchors[0]!r} = .anchors[1]: '
                'points must be distinct'
            )
        self._anchors = anchors
        if anchors[1] is None:
            if hasattr(self, 'slope'):
                if self.slope is None:
                    raise TypeError(
                        f'anchors = {anchors!r}, .slope = {self.slope!r}: '
                        '`.anchors[1]` and `.slope` cannot both be `None`'
                    )
                self._resolve_anchors_from_slope()
        else:
            self._resolve_slope_from_anchors()

    @property
    def slope(self) -> typing.Union[numbers.Real, None]:
        return self._slope

    @slope.setter
    def slope(
        self, slope: typing.Union[numbers.Real, None],
    ) -> None:
        if not (slope is None or _is_real(slope)):
            raise TypeError(
                f'.slope = {slope!r}: expected real number or `None`'
            )
        self._slope = slope
        if not hasattr(self, 'anchors'):
            return
        if slope is None:
            if self.anchors[1] is None:
                raise TypeError(
                    f'.anchors = {self.anchors!r}, slope = {slope!r}: '
                    '`.anchors[1]` and `.slope` cannot both be `None`'
                )
            self._resolve_slope_from_anchors()
        else:
            self._resolve_anchors_from_slope()


@check(
    ax=(identical_to(None) | is_instance(matplotlib.axes.Axes)),
    default_zorder_offset=_is_real,
    use=(
        is_instance(str) & is_in(('spines', 'grid'))
        | _is_patch_or_line
        | is_collection(item_checker=_is_patch_or_line)
    ),
)
def get_line_defaults(
    ax: typing.Optional[matplotlib.axes.Axes] = None,
    *,
    default_zorder_offset: numbers.Real = .1,
    use: AxlineDefaultsSource = 'spines',
) -> typing.Dict[str, typing.Any]:
    r"""
    Get appropriate default values for drawing an axline.

    Parameters
    ----------
    ax
        Optional `matplotlib.axes.Axes`
    default_zorder_offset
        Default real offset in zorder from that of `ax.patch`
    use
        Which object(s) to base the defaults on:
        'spines' (default)
            Use the axes spines (i.e. borders) that are visible
        'grid'
            Use the axes gridlines that are visible
        line(s)
            Use the line(s)
        patch(es)
            Use the patch edge(s)

    Return
    ------
    Dictionary with defaults for the following keyword arguments:
    - alpha
      use = 'spines'
          The mean of the `.alpha` of the visible spines
      use = 'grid'
          The mean of the `.alpha` of the visible gridlines
      use = line(s) and/or patch(es)
          The mean of the `.alpha` of the objects
      (default fallback)
          None

    - color
      use = 'spines'
          The mean of the `.edgecolor` of the visible spines, and
          `matplotlib.rcParams['axes.edgecolor']` if that is not
          available
      use = 'grid'
          The mean of the `.color` of the visible gridlines, and
          `matplotlib.rcParams['grid.color']` if that is not available
      use = line(s) and/or patch(es)
          The mean of the `.[edge]color` of the objects
      (default fallback)
          `matplotlib.rcParams['axes.edgecolor']`

    - linestyle
      use = 'spines'
          The `.linestyle` of (one of) the visible spines, and
          '-' (i.e. solid lines) if that is not available
      use = 'grid'
          The `.linestyle` of (one of) the visible gridlines, and
          `matplotlib.rcParams['grid.linestyle']` if that is not
          available
      use = line(s) and/or patch(es)
          The `.linestyle` of (one of) the objects
      (default fallback)
          None

    - linewidth
      use = 'spines'
          The mean of the `.linewidth` of the visible spines, and
          `matplotlib.rcParams['axes.linewidth']` if that is not
          available
      use = 'grid'
          The mean of the `.linewidth` of the visible gridlines, and
          `matplotlib.rcParams['grid.linewidth']` if that is not
          available
      use = line(s) and/or patch(es)
          The mean of the `.linewidth` of the objects
      (default fallback)
          None

    - zorder
      ax = <Axes>
          The midpoint between that of `ax.patch`, and the minimum of
          those of `ax`'s other children's;
          if that is not available, use the zorder of `ax.patch` plus
          `default_zorder_offset`
      (default fallback)
          None

    Examples
    --------

    >>> import matplotlib.pyplot as plt
    >>> from matplotlib import rcParams, rcParamsDefault
    >>> from matplotlib.colors import to_rgb
    >>> from matplotlib.patches import Circle, Rectangle
    >>> from numpy import allclose, isclose
    >>>
    >>> rcParams.update(rcParamsDefault)
    >>> same_color = lambda c1, c2: allclose(to_rgb(c1), to_rgb(c2))
    >>> ax = plt.gca()

    Defaults based on spines:

    >>> resolved_defaults = get_line_defaults(ax)
    >>> assert same_color(
    ...     resolved_defaults['color'], rcParams['axes.edgecolor'],
    ... )

    Defaults based on gridlines:

    >>> grid_defaults = {
    ...     key[len('grid.'):]: value
    ...     for key, value in rcParams.find_all(r'^grid\.').items()
    ... }
    >>> custom_linestyle = '--'
    >>> ax.grid(linestyle=custom_linestyle)
    >>> resolved_defaults = get_line_defaults(ax, use='grid')
    >>> assert same_color(
    ...     resolved_defaults['color'], grid_defaults['color'],
    ... )
    >>> assert (
    ...     resolved_defaults['linestyle']
    ...     == custom_linestyle
    ...     != grid_defaults['linestyle']
    ... )

    Defaults based on other objects:

    >>> lw1, lw2, lw3 = .5, 1, 3
    >>> circle = Circle((0, 0), linewidth=lw1)
    >>> rect1 = Rectangle((0, 0), 1, 1, linewidth=lw2)
    >>> rect2= Rectangle((0, 0), 1, 1, linewidth=lw3)
    >>> resolved_defaults = get_line_defaults(
    ...     use=[circle, rect1, rect2],
    ... )
    >>> assert isclose(
    ...     resolved_defaults['linewidth'], np.mean([lw1, lw2, lw3]),
    ... )

    (Close the spawned figure to be nice to `matplotlib`:)

    >>> plt.close(ax.figure)
    """
    Source = typing.Union[matplotlib.lines.Line2D, matplotlib.patches.Patch]
    LineAttribute = typing.TypeVar('LineAttribute')

    def get_color_from_source(obj: Source) -> LineAttribute:
        for method in 'get_edgecolor', 'get_color':
            try:
                bound_method = getattr(obj, method)
            except AttributeError:
                continue
            result = bound_method()
            if result is not None:
                return result
        return None

    def aggregate_alphas(alphas: typing.List[LineAttribute]) -> LineAttribute:
        if all(a is None for a in alphas):
            return None
        return np.mean([1 if a is None else a for a in alphas])

    def aggregate_colors(colors: typing.List[LineAttribute]) -> LineAttribute:
        if all(isinstance(c, str) for c in colors) and len(set(colors)) == 1:
            # Same named color -> keep name
            return colors[0]
        mean_color = matplotlib.colors.to_rgba_array(colors).mean(axis=0)
        if mean_color[-1] == 1:  # Opaque -> drop alpha channel
            return mean_color[:-1]
        return mean_color

    line_attributes = (
        'alpha', 'color', 'linestyle', 'linewidth',  # zorder
    )
    fallback_defaults = dict(
        color=matplotlib.rcParams['axes.edgecolor'],
    )
    spines_defaults = dict(
        color=matplotlib.rcParams['axes.edgecolor'],
        linestyle='-',
        linewidth=matplotlib.rcParams['axes.linewidth'],
    )
    grid_defaults = dict(
        alpha=matplotlib.rcParams['grid.alpha'],
        color=matplotlib.rcParams['grid.color'],
        linestyle=matplotlib.rcParams['grid.linestyle'],
        linewidth=matplotlib.rcParams['grid.linewidth'],
    )
    value_getters: typing.Dict[
        str,
        typing.Callable[
            [typing.Union[matplotlib.lines.Line2D, matplotlib.patches.Patch]],
            LineAttribute
        ]
    ] = dict(
        alpha=operator.methodcaller('get_alpha'),
        color=get_color_from_source,
        linestyle=operator.methodcaller('get_linestyle'),
        linewidth=operator.methodcaller('get_linewidth'),
    )
    value_aggregators: typing.Dict[
        str,
        typing.Callable[[typing.List[LineAttribute]], LineAttribute]
    ] = dict(
        alpha=aggregate_alphas,
        color=aggregate_colors,
        linestyle=operator.itemgetter(0),
        linewidth=np.mean,
    )

    # Load the correct sets of defaults
    if use == 'spines':
        defaults = spines_defaults
        use = [
            spine
            for spine in (ax.spines.values() if ax else [])
            if spine.get_visible()
        ]
    elif use == 'grid':
        defaults = grid_defaults
        use = [
            line
            for lines in (
                (ax.get_xgridlines(), ax.get_ygridlines()) if ax else ()
            )
            for line in lines
            if line.get_visible()
        ]
    elif _is_patch_or_line(use):
        defaults = {}
        use = [use]
    elif is_collection(use, item_checker=_is_patch_or_line):
        defaults = {}
        use = list(use)
    else:
        assert False, f'use = {use!r} passed type check'

    # Resolving
    resolved_defaults: typing.Dict[str, typing.Any] = {}
    if use:
        for attr in line_attributes:
            getter = value_getters[attr]
            aggregator = value_aggregators[attr]
            resolved_defaults[attr] = aggregator(
                [getter(artist) for artist in use]
            )
    else:
        resolved_defaults.update(
            (attr, defaults.get(attr, fallback_defaults.get(attr)))
            for attr in line_attributes
        )
    if ax:
        patch = ax.patch
        other_children = [
            child for child in ax.get_children() if child is not patch
        ]
        if other_children:
            resolved_defaults['zorder'] = .5 * (
                patch.get_zorder()
                + min(child.get_zorder() for child in other_children)
            )
        else:
            resolved_defaults['zorder'] = (
                patch.get_zorder() + default_zorder_offset
            )
    else:
        resolved_defaults['zorder'] = default_zorder_offset
    return resolved_defaults


@typing.overload
def axline(
    ax: matplotlib.axes.Axes, xy1: Point, xy2: Point, **kwargs
) -> matplotlib.lines.Line2D:
    ...


@typing.overload
def axline(
    ax: matplotlib.axes.Axes,
    xy1: Point,
    xy2: None = None,
    *,
    slope: numbers.Real,
    **kwargs
) -> matplotlib.lines.Line2D:
    ...


def axline(
    ax: matplotlib.axes.Axes,
    xy1: Point,
    xy2: typing.Optional[Point] = None,
    *,
    slope: typing.Optional[numbers.Real] = None,
    resolve_defaults: typing.Union[bool, AxlineDefaultsSource] = False,
    **kwargs
) -> matplotlib.lines.Line2D:
    """
    Like `Axes.axline()`, but with useful defaults for the line drawn.

    Parameters
    ----------
    ax
        Axes
    xy1
        x- and y-coordinates (in data coordinates)
    xy2
        Optional x- and y-coordinates != `xy1` for defining the line
        with two points (in data coordinates);
        either this or `slope` should be provided
    slope
        Optional slope of the line for defining the line with a single
        point (in data coordinates);
        either this or `xy2` should be provided
    resolve_defaults
        Whether to use `<MODULE>.get_line_defaults()`
        to resolve the appropriate defaults for line attributes (e.g.
        color and width);
        if not a boolean value, it is passed to
        `<MODULE>.get_line_defaults(use=...)`
    **kwargs
        Passed to `matplotlib.lines.Line2D()`

    Return
    ------
    `Line2D`

    See also
    --------
    `<MODULE>.get_line_defaults()`
    """
    # Check arguments
    # (Other checks are done in Ray.make())
    if not isinstance(ax, matplotlib.axes.Axes):
        raise TypeError(f'ax = {ax!r}: expected a `matplotlib.axes.Axes`')
    # Resolving defaults
    if is_strict_boolean(resolve_defaults):
        resolved_defaults = get_line_defaults(ax) if resolve_defaults else {}
        kwargs = dict(resolved_defaults, **kwargs)
    else:
        kwargs = dict(get_line_defaults(ax, use=resolve_defaults), **kwargs)
    # Create ray
    ray = Ray.make(xy1=xy1, xy2=xy2, slope=slope, **kwargs)
    # Fallbacks to the older simple method where appropriate
    if not ray.slope:
        return ax.axhline(ray.anchors[0][1], **kwargs)
    elif math.isinf(ray.slope):
        return ax.axvline(ray.anchors[0][0], **kwargs)
    # Use ray
    if not all(axis.get_scale() == 'linear' for axis in (ax.xaxis, ax.yaxis)):
        xscale, yscale = (axis.get_scale() for axis in (ax.xaxis, ax.yaxis))
        raise ValueError(
            f'<slope> = {ray.slope!r}, ax.xaxis.get_scale() = {xscale!r}, '
            f'ax.yaxis.get_scale() = {yscale!r}: '
            'sloped line can only be specified where the axis scales are linear'
        )
    return ax.add_line(ray)


# Actual handling of parity line
def draw_parity_line(
    ax: matplotlib.axes.Axes,
    *,
    resolve_defaults: typing.Optional[
        typing.Union[bool, AxlineDefaultsSource]
    ] = None,
    **kwargs
) -> matplotlib.lines.Line2D:
    """
    Draw a parity line.

    Parameters
    ----------
    ax
        Axes
    resolve_defaults
        Whether to use `<MODULE>.get_line_defaults()`
        to resolve the appropriate defaults for line attributes (e.g.
        color and width);
        if not a boolean value, it is passed to
        `<MODULE>.get_line_defaults(use=...)`
        default is to use the axes' spines to resolve the defaults,
        plus using a dashed line style
    **kwargs
        Passed to `<MODULE>.axline()`

    Return
    ------
    `matplotlib.lines.Line2D`

    See also
    --------
    `<MODULE>.axline()`
    `<MODULE>.get_line_defaults()`

    Notes
    -----
    If `resolve_defaults` is any value but `None`, the default of a
    dashed line style is not applied over what is resolved from
    `<MODULE>.get_line_defaults()`.
    """
    # Checking
    if not isinstance(ax, matplotlib.axes.Axes):
        raise TypeError(f'ax = {ax!r}: expected a `matplotlib.axes.Axes`')
    # Resolving defaults
    if resolve_defaults is None:
        resolve_defaults = 'spines'
        kwargs.setdefault('linestyle', '--')
    # Actual plotting
    return axline(
        ax,
        (0, 0),
        slope=1,
        resolve_defaults=resolve_defaults,
        **kwargs
    )


# TODO: functions for drawing fitting lines/annotated lines
# (split to another submodule)
