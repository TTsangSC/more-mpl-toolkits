"""
Utilities for drawing rays (unbounded lines).

API
---
Ray
    `matplotlib.lines.Line2D` subclass helping in drawing rays

axline()
    Drop-in replacement for/extension to `matplotlib.axes.Axes.axline()`

draw_parity_line()
    Draw a parity line with helpful defaults

get_line_defaults()
    Resolve sensible default arguments befitting rays

Notes
-----
More recent `matplotlib` versions (v.3.3+) already ship with the
`matplotlib.axes.Axes.axline()` function, which offer similar
functionalities;
however, the resolution of defaults are unique to this module.
"""
from .core import Ray, axline, draw_parity_line, get_line_defaults

__all__ = (
    'Ray',
    'axline',
    'draw_parity_line',
    'get_line_defaults',
)

for _entity in (
    lambda namespace: (namespace[name] for name in __all__ if name in namespace)
)(locals()):
    try:
        _entity.__doc__ = (
            _entity.__doc__.replace('<MODULE>', _entity.__module__)
        )
    except AttributeError:
        pass
del _entity
