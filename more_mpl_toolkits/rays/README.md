`more_mpl_toolkits.rays`
========================

(Alias(es): `~.ra`)

This is essentially a backport of [`Axes.axline()`][axes-axline],
introduced in `matplotlib` v.3.3,
primarily motivated by the fact that I couldn't be bothered to swap out
some old [`module load`s][lmod] in some workspaces.
Of course there are also enhancements like optimized z-ordering of the
drawn line relative to the other plot elements,
and the resolution of some useful default properties:
```python
import matplotlib.pyplot as plt
import numpy as np

from more_mpl_toolkits import rays

x: np.ndarray = np.array(...)  # noqa
y: np.ndarray = np.array(...)  # noqa

(s_xx, s_xy), _ = np.cov(x, y)
slope = s_xy/s_xx
intercept = (y - slope*x).mean()

fig, ax = plt.subplots(1, 1, squeeze=True)
ax.plot(x, y, marker='+')

rays.draw_parity_line(ax, label='parity')  # Draw a parity line
rays.axline(  # Draw a fitting line
    ax,
    (0, intercept),
    slope=slope,
    resolve_defaults=True,
    linestyle='-',
    label='fit: $y = {:.2f} x + {:.2f}$'.format(slope, intercept),
)
```

[axes-axline]: https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.axline.html
[lmod]: https://lmod.readthedocs.io/en/latest/#
