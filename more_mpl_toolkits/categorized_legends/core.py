"""
`matplotlib` wrapper classes facilitating creating CATEGORIZED LEGENDS.
"""
from .axes import CategorizedLegendAxes
from .data_classification import DataCatClass
from .figure import CategorizedLegendFigure
from .recipes import CategorizedLegendRecipe
from .mixin import _CLSuperHelper
from ._utils import (
    CATEGORIZED_LEGEND_KWARGS_ALIASES,
    resolve_cl_aliased_args,
    set_subpackage_name_in_docs as _set_module_name,
)

__all__ = (
    # Data
    'CATEGORIZED_LEGEND_KWARGS_ALIASES',
    # Functions
    'resolve_cl_aliased_args',
    # Classes
    'CategorizedLegendAxes',
    'CategorizedLegendFigure',
    'CategorizedLegendRecipe',
    'DataCatClass',
)

# Process callables: fix docstrings


assert _CLSuperHelper  # To get around F401

for _name in [*__all__, '_CLSuperHelper']:
    _value = locals().get(_name)
    if callable(_value):
        _set_module_name(locals()[_name])
    del _name, _value
del _set_module_name
