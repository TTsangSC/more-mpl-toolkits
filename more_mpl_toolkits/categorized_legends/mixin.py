"""
Mixin class for common methods between axes and figure objects.
"""
import abc
import functools
import inspect
import numbers
import operator
import types
import typing
import warnings
from collections import OrderedDict

import numpy as np
from matplotlib.axes import Axes
from matplotlib.legend import Legend
from matplotlib.lines import Line2D

import tjekmate

from . import utils, wrapper_proxy, _types, _utils
from .data_classification import DataCatClass
from ._recipes import CategorizedLegendRecipe

__all__ = ('KEYWORD_ARGUMENT_ORDER', 'CategorizedLegendMixin')

KEYWORD_ARGUMENT_ORDER: typing.Sequence[_types.Keyword] = """
max_level

category_value_classifications
order_base max_linear_orders max_discrete_numbers

category_name_sorter category_value_sorters

category_name_typesetter category_value_typesetters
show_category_names suppressed_categories conflate
range_samples log_range_samples linear_range_samples
values_per_line

ncol fill_columns_first fill_rows_first gap_between_categories
legend_method
""".split()

CategorizedLegendAxes = typing.TypeVar(
    'CategorizedLegendAxes', bound='CategorizedLegendAxes',
)
CategorizedLegendFigure = typing.TypeVar(
    'CategorizedLegendFigure', bound='CategorizedLegendFigure',
)
Wrappee = typing.TypeVar('Wrappee')
Wrapper = typing.TypeVar('Wrapper', bound=Wrappee)

LegendLike = typing.Callable[
    [
        typing.Self,
        typing.Sequence[_types.LegendHandle],
        typing.Sequence[str],
    ],
    Legend
]


class _CLSuperHelper:
    """
    Private helper class for the emulation for `super()` functionalities
    in classes making use of `mixin._get_base_class_implementations()`
    (`mixin = <MODULE>.core.CategorizedLegendMixin`).

    Example
    -------
    >>> from _MODULE_ import wrapper_proxy
    >>> class Foo:
    ...     def __init__(self, num: int = 0) -> None:
    ...         self.num = num
    ...
    ...     def foo(self) -> int:
    ...         return self.num
    ...
    >>> class Foobar(Foo):
    ...     pass
    ...
    >>> @_CLSuperHelper.add_super
    ... class FooWrapper(Foo, metaclass=wrapper_proxy.WrapperProxyMeta):
    ...     def foo_with_super_helper(self) -> int:
    ...         return self.super().foo()
    ...
    ...     def foo_without_super_helper(self) -> int:
    ...         return super().foo()

    `super()` works for concrete instances/subclasses of `FooWrapper`:

    >>> foo = FooWrapper(1)
    >>> isinstance(foo, FooWrapper)
    True
    >>> FooWrapper in type(foo).mro()
    True
    >>> foo.foo_with_super_helper() == 1
    True
    >>> foo.foo_without_super_helper() == 1
    True

    But not for "virtual" instances/subclasses automatically created by
    wrapping an instance of a strict subclass of the base classes of
    `FooWrapper`:

    >>> foobar = FooWrapper(Foobar(2))
    >>> isinstance(foobar, FooWrapper)
    True
    >>> FooWrapper in type(foobar).mro()
    False
    >>> foobar.foo_with_super_helper() == 2
    True
    >>> foobar.foo_without_super_helper(
    ... ) == 2  # doctest: +NORMALIZE_WHITESPACE
    Traceback (most recent call last):
      ...
    TypeError: super(type, obj):
      obj must be an instance or subtype of type

    Which however is circumvented by the use of the super-helper as
    shown above.

    Like `super()`, `.super()` works for both class and instance
    attribute lookups:

    >>> @_CLSuperHelper.add_super
    ... class Bar:
    ...     @classmethod
    ...     def bar(cls) -> str:
    ...         return cls.__name__
    ...
    >>> class Baz(Bar):
    ...     @classmethod
    ...     def bar(cls) -> str:
    ...         parent_bar = cls.super().bar
    ...         return '{}(): {!r}'.format(
    ...             parent_bar.__func__.__qualname__, parent_bar(),
    ...         )
    ...
    >>> Baz.bar()
    "Bar.bar(): 'Baz'"

    The single- and double-argument form of `super()` are likewise
    emulated:

    >>> @_CLSuperHelper.add_super
    ... class Spam:
    ...     foobar = 1
    ...
    >>> class Ham(Spam):
    ...     foobar = 2
    ...
    >>> class Eggs(Ham):
    ...     foobar = 3
    ...
    >>> eggs = Eggs()
    >>> eggs.foobar, eggs.super().foobar, eggs.super(Ham, eggs).foobar
    (3, 2, 1)

    >>> class OtherFood(Eggs):
    ...     before_ham = Eggs.super(Ham)
    ...
    >>> OtherFood().before_ham.foobar  # Using `Spam`'s value
    1

    See also
    --------
    <MODULE>.wrapper_proxy.WrapperProxyMeta
        For a discussion of why this helper is needed.
    """
    @staticmethod
    def resolve_for_instance(
        self: Wrapper,
        attr: _types.Keyword,
        parent: typing.Optional[typing.Type[Wrappee]] = None,
    ) -> typing.Any:
        """
        Emulation for `super()` for instance attributes, roughly
        equivalent to

        >>> getattr(  # noqa # doctest: +SKIP
        ...     super(parent or type(self), self), attr,
        ... )

        Parameters
        ----------
        self
            Instance of a wrapper proxy class
        attr
            Identifier name of the instance attribute
        parent
            Optional base class of the type of the instance;
            default `type(self)`

        Return
        ------
        Instance attribute of `self` (descriptors, bound methods, etc.)
        """
        if not utils.is_identifier(attr):
            raise TypeError(f'attr = {attr!r}: expected an identifier')
        # Look through base classes
        base_implementations = self._get_base_class_implementations(
            attr, bound_desc=True, instance=self, parent=parent,
        )
        if base_implementations:
            _, base_impl = base_implementations[0]
            return base_impl
        # Use attribute-lookup method of the base classes
        for lookup in '__getattribute__', '__getattr__':
            base_getters = self._get_base_class_implementations(
                lookup, bound_desc=True, instance=self, parent=parent,
            )
            if not base_getters:
                continue
            _, base_getter = base_getters[0]
            try:
                return base_getter(attr)
            except AttributeError:
                pass
        # Lookup failed
        raise AttributeError(
            'base classes of {!r} has no attribute {!r}'
            .format(parent or type(self), attr)
        )

    @staticmethod
    def resolve_for_class(
        cls: typing.Type[Wrapper],
        attr: _types.Keyword,
        parent: typing.Optional[typing.Type[Wrappee]] = None,
    ) -> typing.Any:
        """
        Emulation for `super()` for class attributes, roughly equivalent
        to

        >>> getattr(  # noqa # doctest: +SKIP
        ...     super(parent or cls, cls), attr,
        ... )

        Parameters
        ----------
        cls
            Wrapper proxy class
        attr
            Identifier name of the class attribute
        parent
            Optional base class of the class;
            default `cls`

        Return
        ------
        Class attribute, bound class method, etc.
        """
        if not utils.is_identifier(attr):
            raise TypeError(f'attr = {attr!r}: expected an identifier')
        # Look through base classes
        base_implementations = cls._get_base_class_implementations(
            attr, bound_desc=True, instance=None, parent=parent,
        )
        if base_implementations:
            _, base_impl = base_implementations[0]
            return base_impl
        # Lookup failed
        raise AttributeError(
            'base classes of {!r} has no attribute {!r}'
            .format(parent or cls, attr)
        )

    @classmethod
    def _super0(
        desc_cls,
        *,
        inst: typing.Optional[Wrapper] = None,
        cls: typing.Optional[typing.Type[Wrapper]] = None,
    ) -> typing.Any:
        """
        Zero-argument form of `super()`:
        - The starting type is implicitly resolved.
        - The binding target is taken to be the `inst` (if given) or the
          `cls` (otherwise).
        """
        return desc_cls._super2(None, cls if inst is None else inst)

    @classmethod
    def _super1(
        desc_cls,
        parent: typing.Type[Wrappee],
        **_
    ) -> typing.Any:
        """
        Single-argument form of `super()`:
        - The starting type is chosen to be `parent`.
        - The object is unbound and needs to be bounded via
          `.__get__()`.
        """
        class SuperHelper:
            desc_cls: type
            parent: typing.Type[Wrappee]
            __slots__ = 'desc_cls', 'parent',

            def __init__(self, desc_cls, parent) -> None:
                self.desc_cls = desc_cls
                self.parent = parent

            def __repr__(self) -> str:
                return '{}({!r})'.format(type(self).__name__, parent)

            def __get__(
                self,
                inst: typing.Union[Wrapper, None],
                cls: typing.Optional[typing.Type[Wrapper]] = None,
            ) -> typing.Any:
                return self.desc_cls._super2(
                    self.parent, cls if inst is None else inst,
                )

        if not isinstance(parent, type):
            raise TypeError(
                'parent = {!r}: super() argument 1 must be a type, not {}'
                .format(parent, type(parent).__qualname__)
            )
        return SuperHelper(desc_cls, parent)

    @classmethod
    def _super2(
        desc_cls,
        parent: typing.Union[typing.Type[Wrappee], None],
        target: typing.Union[Wrapper, typing.Type[Wrapper]],
        **_
    ) -> typing.Any:
        """
        Two-argument form of `super()`:
        - The starting type is chosen to be `parent`.
        - The binding target is taken to be the `target`.
        """
        class SuperHelper:
            parent: typing.Union[typing.Type[Wrappee], None]
            __slots__ = 'method', 'parent', 'target',

            def __init__(self, method, parent, target) -> None:
                self.method = method
                self.parent = parent
                self.target = target

            def __repr__(self) -> str:
                parent, target = (
                    object.__getattribute__(self, attr)
                    for attr in ('parent', 'target')
                )
                return '{}({!r}, {!r})'.format(
                    type(self).__name__,
                    type(target) if parent is None else parent,
                    target,
                )

            def __getattribute__(self, attr: _types.Keyword) -> typing.Any:
                method, parent, target = (
                    object.__getattribute__(self, attr)
                    for attr in ('method', 'parent', 'target')
                )
                return method(target, attr, parent=parent)

        # Resolve whether we're doing class or instance lookup
        if not (parent is None or isinstance(parent, type)):
            raise TypeError(
                'parent = {!r}: super() argument 1 must be a type, not {}'
                .format(parent, type(parent).__qualname__)
            )
        is_class_lookup: bool
        if parent is None:
            is_class_lookup = isinstance(target, type)
            checker = utils.always()
        else:
            if isinstance(target, parent):
                is_class_lookup, checker = False, utils.always()
            elif isinstance(target, type):
                is_class_lookup, checker = True, issubclass
            else:
                is_class_lookup, checker = False, isinstance
        # Sanity check
        if not checker(target, parent):
            raise TypeError(
                'super(type, obj): obj must be an instance or subtype of type'
            )
        # Fix doc, signature, and annotation of helper class
        SuperHelper.__doc__ = __doc__
        if is_class_lookup:
            method, ObjType = desc_cls.resolve_for_class, typing.Type[Wrapper]
        else:
            method, ObjType = desc_cls.resolve_for_instance, Wrapper
        SuperHelper.__annotations__.update(
            target=ObjType,
            method=typing.Callable[
                [
                    ObjType, _types.Keyword,
                    typing.Optional[typing.Type[Wrappee]],
                ],
                typing.Any
            ],
        )
        sig = inspect.signature(SuperHelper.__init__)
        SuperHelper.__init__.__signature__ = sig.replace(
            parameters=[
                param.replace(
                    annotation=SuperHelper.__annotations__.get(
                        name, param.annotation,
                    ),
                )
                for name, param in sig.parameters.items()
            ],
        )
        return SuperHelper(method, parent, target)

    def __get__(
        self,
        inst: typing.Union[Wrapper, None],
        cls: typing.Optional[typing.Type[Wrapper]] = None,
    ) -> typing.Any:
        """
        Return
        ------
        Object which when called emulates the behavior of `super()`
        """
        @typing.overload
        def super_helper() -> typing.Any: ...

        @typing.overload
        def super_helper(parent: typing.Type[Wrappee]) -> typing.Any:
            ...

        @typing.overload
        def super_helper(
            parent: typing.Type[Wrappee],
            target: typing.Union[Wrapper, typing.Type[Wrapper]],
        ) -> typing.Any:
            ...

        def super_helper(*args) -> typing.Any:
            super_implementations = (self._super0, self._super1, self._super2)
            try:
                super_method = super_implementations[len(args)]
            except IndexError:
                pass
            else:
                return super_method(*args, inst=inst, cls=cls)
            raise TypeError(
                '*args = {!r}: {}() takes {} positional arguments but {} given'
                .format(
                    args,
                    super_helper.__name__,
                    f'0 to {len(super_implementations) - 1}',
                    (lambda n: '1 was' if n == 1 else f'{n} were')(len(args)),
                )
            )

        # Check if the requisite method is present at the host/inst. as
        # a test
        getattr(
            cls if inst is None else inst,
            '_get_base_class_implementations',
        )
        super_helper.__doc__ = type(self).__doc__
        return super_helper

    @classmethod
    def add_super(
        desc_cls,
        cls: typing.Optional[type] = None,
        *,
        attr: _types.Keyword = 'super',
    ) -> typing.Union[
        typing.Callable[[type], type],
        type,
    ]:
        """
        Add a `.super()` method (i.e. an instance of this helper class)
        to the decorated class.

        Parameters
        ----------
        cls
            Decorated class
        attr
            Name to add the helper instance to;
            should not be equal to '_get_base_class_implementations'

        Return
        ------
        If `cls` provided:
            `cls`
        Else
            Decorator expecting a class

        Side effects
        ------------
        Addition/overwriting of the attribute `attr`:

        >>> setattr(cls, attr, <instance>)  # noqa # doctest: +SKIP

        In addition, if `cls` doesn't have a
        `._get_base_class_implementations()` method, the one from
        `<MODULE>.core.CategorizedLegendMixin`
        is automatically added.
        """
        impl_getter_name = '_get_base_class_implementations'
        if cls is None:
            return functools.partial(
                desc_cls.add_super,
                attr=attr,
            )
        if not isinstance(cls, type):
            raise TypeError(f'cls = {cls!r}: expected a class')
        if not utils.is_identifier(attr):
            raise TypeError(f'attr = {attr!r}: expected an identifier')
        if attr == impl_getter_name:
            raise ValueError(
                f'attr = {attr!r}: cannot equal {impl_getter_name!r}'
            )
        if not callable(getattr(cls, impl_getter_name, None)):
            default = inspect.getattr_static(
                CategorizedLegendMixin, impl_getter_name,
            )
            setattr(cls, impl_getter_name, default)
        setattr(cls, attr, desc_cls())
        return cls


# ******** IMPORTANT: Mix-in class for common functionalities ******** #


class CategorizedLegendMixin(abc.ABC):
    """
    Mix-in class for methods and descriptors common to
    `<MODULE>.Figure` and
    `<MODULE>.Axes`.
    For an index of names suitable for mixing in, see `.injectables`.
    Use `.inject(cls)` to inject those names into the class namespace.

    Notes
    -----
    - Since `.__slots__` is consumed at class instantiation, it must be
      explicitly supplied in the class dict;
      using `.inject()` to decorate the class will NOT properly set
      `.__slots__` up.

    - Classes using the methods listed here should provide the following
      methods:

        .get_cl_categories(), get_cl_children();

      it is also recommended that the class overrides the implementation
      of `._get_cl_handles_labels()` provided here with another;
      it should have the same return type but the call signature can be
      different.
    """

    # ****** Methods to be defined in classes using this mixin ******* #

    @abc.abstractmethod
    @typing.overload
    def get_cl_categories(
        self,
        *args,
        concise: typing.Literal[True] = True,
        **kwargs
    ) -> typing.Dict[
        _types.CategorizedKeyword, typing.Set[_types.Categorization]
    ]:
        ...

    @abc.abstractmethod
    @typing.overload
    def get_cl_categories(
        self,
        *args,
        concise: typing.Literal[False] = False,
        **kwargs
    ) -> typing.Dict[
        typing.Tuple[
            _types.CategorizedKeyword,
            _types.CLMethodName,
            _types.CategoryFormatter,
        ],
        typing.Set[_types.Categorization]
    ]:
        ...

    @abc.abstractmethod
    def get_cl_categories(
        self, *args, concise: bool = True, **kwargs
    ) -> typing.Union[
        typing.Dict[
            _types.CategorizedKeyword,
            typing.Set[_types.Categorization]
        ],
        typing.Dict[
            typing.Tuple[
                _types.CategorizedKeyword,
                _types.CLMethodName,
                _types.CategoryFormatter,
            ],
            typing.Set[_types.Categorization]
        ],
    ]:
        """
        Search for and get all the values encountered so far for the
        categories.

        Parameters
        ----------
        *args, concise, **kwargs
            To be implemented by classes using this mixin

        Return
        ------
        concise = True

            >>> dict[  # noqa # doctest: +SKIP
            ...     category, set[value_encountered],
            ... ];

        else

            >>> dict[  # noqa # doctest: +SKIP
            ...     tuple[category, cl_method, formatter],
            ...     set[value_encountered]
            ... ]
        """
        ...

    @abc.abstractmethod
    def get_cl_children(
        self,
        *args,
        max_level: typing.Optional[
            typing.Union[numbers.Integral, bool]
        ] = None,
        **kwargs
    ) -> typing.List[
        typing.List[
            typing.Union[CategorizedLegendFigure, CategorizedLegendAxes]
        ]
    ]:
        """
        List all the child figures and/or axes relevant to making a
        categorized legend.

        Parameters
        ----------
        *args, **kwargs
            To be implemented by classes using this mixin
        max_level
            Optional limit for how many levels of children to descend
            into;
            the default `None` (equivalent to `True`) indicates that
            there is no limit

        Result
        ------
        >>> list[  # noqa # doctest: +SKIP
        ...     # Each list represents a level
        ...     list[axes_or_figure]
        ... ]
        """
        ...

    # ************************ Mixin methods ************************* #

    def __eq__(
        self, other: typing.Any,
    ) -> typing.Union[bool, typing.Literal[NotImplemented]]:
        """
        Allow a wrapper to compare equal with its wrapped instance.
        """
        if self.super().__eq__(other) not in (False, NotImplemented):
            return True
        return wrapper_proxy._get_wrapped_instance(self) == other

    def __hash__(self) -> int:
        # Just use the base-class implementation
        return self.super().__hash__()

    @typing.overload
    def register_cl_method(
        self,
        cl_method: typing.Union[
            _types.CLMethodName, typing.Collection[_types.CLMethodName],
        ],
        formatter: None = None,
    ) -> typing.Callable[[_types.CategoryFormatter], _types.CategoryFormatter]:
        ...

    @typing.overload
    def register_cl_method(
        self,
        cl_method: typing.Union[
            _types.CLMethodName, typing.Collection[_types.CLMethodName],
        ],
        formatter: _types.CategoryFormatter,
    ) -> _types.CategoryFormatter:
        ...

    def register_cl_method(
        self,
        cl_method: typing.Union[
            _types.CLMethodName, typing.Collection[_types.CLMethodName],
        ],
        formatter: typing.Optional[_types.CategoryFormatter] = None,
    ) -> typing.Union[
        _types.CategoryFormatter,
        typing.Callable[[_types.CategoryFormatter], _types.CategoryFormatter]
    ]:
        """
        Register a CL METHOD and optionally specify a DATA FORMATTER to
        it;
        upon registration, the method becomes available on the axes
        object (if an axes object) or any child axes spawned therefrom
        (if a figure).

        Parameters
        ----------
        cl_method
            Name or names of the CL METHOD(s);
            must be of the format `.cl_method_prefix + method_name`,
            where `method_name` is the name of a method acting on a
            `matplotlib.axes.Axes` object, with a known recipe to get a
            legend handle from the return value thereof (see Notes)
        formatter()
            Optional DATA FORMATTER for the CL METHOD

        Return
        ------
        formatter callable
            `formatter()` itself
        else
            Decorator which can be used to perform the registration:

            >>> from _MODULE_ import Figure
            >>>
            >>> fig = Figure()
            >>>
            >>> @fig.register_cl_method('cl_plot')
            ... def plot_formatter(
            ...     *,
            ...     cat1: typing.Optional[int] = None,
            ...     cat2: typing.Optional[str] = None,
            ... ) -> typing.Dict:
            ...     return {}
            ...
            >>> fig.cl_method_formatters['cl_plot'] == plot_formatter
            True

        Side effects
        ------------
        formatter callable
            `formatter()` registered to the prescribed CL METHOD
        else
            CL METHOD registered, but without an assigned DATA
            FORMATTER:

            >>> _ = fig.register_cl_method('cl_scatter')
            >>> assert fig.cl_method_formatters['cl_scatter'] is None

        Notes
        -----
        - If a specific formatter function has not been assigned to a CL
          METHOD when it is called, `.default_cl_formatter()` is used.

        - Only `Axes` methods with known recipes can be registered;
          see
          `<MODULE>.register_cl_recipe()`
          and
          `<MODULE>.get_cl_recipes()`.
        """
        # Get list of supported values
        prefix = type(self).cl_method_prefix
        supported = CategorizedLegendRecipe.get_cl_compatible_methods()
        if isinstance(cl_method, str):
            cl_method = cl_method,
        if not (
            isinstance(cl_method, typing.Collection) and
            all(
                utils.is_identifier(name) and name.startswith(prefix)
                for name in cl_method
            )
        ):
            raise TypeError(
                f'cl_method = {cl_method!r}: '
                f'expected identifier(s) starting with {prefix!r}'
            )
        method_names = [name[len(prefix):] for name in cl_method]
        for name in method_names:
            if not callable(getattr(Axes, name, None)):
                raise TypeError(f'Axes.{name}(): not a method')
            if name not in supported:
                raise NotImplementedError(
                    f'Axes.{name}(): '
                    f'unsupported `Axes` method; '
                    f'supported values are {supported!r} '
                    f'(use `{self._get_cl_module_name()}.register_cl_recipe()` '
                    'to add support for other methods)'
                )
        # Make sure that we have a ._cl_mthd_fmtrs
        self.cl_method_formatters
        formatter = self._check_formatter(formatter)
        self._cl_mthd_fmtrs.update(dict.fromkeys(cl_method, formatter))
        # Propagate to one level of children at a time
        for child in (
            child for children in self.get_cl_children(max_level=1)
            for child in children
        ):
            child.register_cl_method(cl_method, formatter)
        # Return a decorator if formatter() not provided
        if formatter is None:
            def registerer(
                formatter: _types.CategoryFormatter,
            ) -> _types.CategoryFormatter:
                """
                Register CL METHOD(s) associated with the object `{}`
                using `formatter()` as the formatting function.

                Method(s) registered
                --------------------
                {}
                """
                if formatter is None:
                    raise TypeError(
                        f'formatter = {formatter!r}: expected a callable'
                    )
                self.register_cl_method(cl_method, formatter)
                return formatter

            name = f'register_{"_".join(cl_method)}_formatter'
            registerer.__name__ = registerer.__qualname__ = name
            registerer.__doc__ = registerer.__doc__.format(
                repr(self), ', '.join(f'.{name}()' for name in cl_method)
            )
            return registerer
        # Otherwise, just return the formatter itself
        return formatter

    def deregister_cl_method(
        self,
        cl_method: typing.Union[
            _types.CLMethodName, typing.Collection[_types.CLMethodName],
        ],
    ) -> None:
        """
        Deregister a CL METHOD.

        Parameters
        ----------
        cl_method
            Name or names of the CL METHOD(s);
            must be of the format `.cl_method_prefix + method_name`

        Effect
        ------
        if `cl_method` is registered to the figure or axes
            `cl_method` removed from the object registry and the `dir()`
            of all associated `matplotlib.axes.Axes` objects
        else
            No-op

        Notes
        -----
        It is an error if `cl_method` isn't an identifier starting with
        `cls.cl_method_prefix` to begin with.
        """
        prefix = type(self).cl_method_prefix
        if isinstance(cl_method, str):
            cl_method = cl_method,
        if not (
            isinstance(cl_method, typing.Collection) and
            all(
                utils.is_identifier(name) and name.startswith(prefix)
                for name in cl_method
            )
        ):
            raise TypeError(
                f'cl_method = {cl_method!r}: '
                f'expected identifier(s) starting with {prefix!r}'
            )
        for name in cl_method:
            self._cl_mthd_fmtrs.pop(name, None)
        # Propagate to one level of children at a time
        for child in (
            child for children in self.get_cl_children(max_level=1)
            for child in children
        ):
            child.deregister_cl_method(cl_method)

    def set_default_cl_formatter(
        self,
        formatter: typing.Union[_types.CategoryFormatter, None],
    ) -> typing.Union[_types.CategoryFormatter, None]:
        """
        Set the default data formatter of the object.

        Parameters
        ----------
        formatter()
            DATA FORMATTER for CL methods without one specifically
            registered

        Return
        ------
        `formatter()` itself

        Example
        -------

        >>> from _MODULE_ import Figure
        >>>
        >>> fig = Figure()
        >>>
        >>> @fig.set_default_cl_formatter
        ... def plot_formatter(
        ...     *,
        ...     cat1: typing.Optional[int] = None,
        ...     cat2: typing.Optional[str] = None,
        ... ) -> typing.Dict:
        ...     return {}
        ...
        >>> fig.default_cl_formatter == plot_formatter
        True

        Notes
        -----
        For historic reasons, this method depends on the
        `.default_cl_formatter` property, instead of the other way
        round;
        it is implemented as an afterthought so that like the CL-method-
        specific formatters the default formatter can also be set via
        decoration.
        """
        self.default_cl_formatter = formatter
        return formatter

    @classmethod
    def _get_base_class_implementations(
        cls: typing.Type[Wrapper],
        attr: _types.Keyword,
        *,
        bound_desc: bool = True,
        instance: typing.Optional[Wrapper] = None,
        parent: typing.Optional[typing.Type[Wrappee]] = None,
    ) -> typing.List[typing.Tuple[typing.Type[Wrappee], typing.Any]]:
        """
        Propose synonymous implementations of the attribute from base
        classes.

        Parameters
        ----------
        attr
            Identifier name of the attr
        bound_desc
            Whether to use the descriptor protocol to bind the results
        instance
            Optional instance to bind to (ignored if `bound_desc` is
            false)
        parent
            Optional base class after which (exclusive) to start the
            lookup

        Return
        ------
        List of two-tuples of `(defining_class, attr_value)` in MRO
        order
        """
        if parent is None:
            parent, resolve_special = cls, False
        else:
            resolve_special = isinstance(parent, wrapper_proxy.WrapperProxyMeta)
        if not (isinstance(parent, type) and issubclass(cls, parent)):
            raise TypeError(
                f'parent = {parent!r}: expected a base class of {cls!r}'
            )
        mro_seq = cls.mro()
        # Note: since WrapperProxyMeta instances has special resolution
        # of class relationships, we also have to take that into account
        # here
        is_strict_superclass: typing.Callable[[type], bool]
        mro_map = {base_cls: i for i, base_cls in enumerate(mro_seq)}
        if resolve_special and parent not in mro_map:
            def is_strict_superclass(cls: type) -> bool:
                """
                For wrapper classes which doesn't explicitly have
                `parent` in the MRO, defer the check to the next
                immediate superclasses (inclusive)
                """
                return (cls in mro_after_parent) and (mro_map[cls] >= min_mro)

            mro_after_parent = {
                base_cls for base_cls in mro_map if issubclass(parent, base_cls)
            }
            min_mro = min(mro_map[cls] for cls in mro_after_parent)
        else:
            def is_strict_superclass(cls: type) -> bool:
                """
                For regular inheritance, just look for classes higher up
                in the MRO than `parent` (excclusive)
                """
                return mro_map[cls] > min_mro

            min_mro = mro_map[parent]
        all_origins_and_attrs: typing.List[
            typing.Tuple[typing.Type[Wrappee], typing.Any]
        ] = []
        for base_cls in mro_seq[::-1]:  # Start from base classes
            if base_cls is cls:
                break
            cls_dict = vars(base_cls)
            if attr not in cls_dict:
                continue
            attribute = cls_dict[attr]
            # Bind descriptors
            if callable(getattr(attribute, '__get__', None)) and bound_desc:
                try:
                    bound_attribute = attribute.__get__(instance, cls)
                except Exception:  # Binding failed
                    continue
            else:
                bound_attribute = attribute
            if is_strict_superclass(base_cls):
                all_origins_and_attrs.append((base_cls, bound_attribute))
        return all_origins_and_attrs[::-1]

    super = _CLSuperHelper()

    @_utils.super_recursion_guard
    def clear(self, *args, **kwargs) -> None:
        """
        Propagate the `.clear()` to child objects, then invoke the
        `.clear()` method of the base class.
        """
        # Note: if the non-WrapperProxyMeta base class uses super()
        # in its .clear() method we may have an infinite loop;
        # hence we have to introspect to make sure that it isn't doing
        # anything stupid
        children = self.get_cl_children(max_level=1)
        if children:
            children, = children
        # Clear self
        super(type(self), self).clear(*args, **kwargs)
        # Clear children
        for child in children:
            child.clear(*args, **kwargs)

    @staticmethod
    def _check_formatter(
        formatter: typing.Union[_types.CategoryFormatter, None],
    ) -> typing.Union[_types.CategoryFormatter, None]:
        """
        Check if a formatter is valid (or is `None`), and return it if
        so.
        """
        if formatter is None:
            return None
        if not callable(formatter):
            raise TypeError(f'formatter = {formatter!r}: expected a callable')
        try:
            hash(formatter)
        except Exception:
            raise TypeError(f'formatter = {formatter!r}: expected a hash-able')
        # Check signature
        for name, arg in inspect.signature(formatter).parameters.items():
            if arg.kind == arg.KEYWORD_ONLY:
                if arg.default is inspect.Parameter.empty:
                    raise TypeError(
                        f'formatter = {formatter!r}: '
                        f'parameter {str(arg)!r} has no default'
                    )
            elif arg.kind == arg.VAR_KEYWORD:
                pass
            else:
                raise TypeError(
                    f'formatter = {formatter!r}: '
                    f'parameter {str(arg)!r} is neither a keyword-only '
                    'argument with a default nor a variable-length '
                    'keyword argument'
                )
        # Try calling it?
        kwargs = formatter()
        if utils.is_keyword_mapping(kwargs):
            pass
        elif (
            isinstance(kwargs, typing.Sequence) and
            all(utils.is_keyword_mapping(kw) for kw in kwargs)
        ):
            pass
        else:
            raise TypeError(
                f'formatter = {formatter!r}: '
                'does not generate keyword arguments or a sequence thereof'
            )
        return formatter

    def _replace_wrapped_instance_generic(
        self,
        obj: typing.Any,
        attr: typing.Optional[
            typing.Union[_types.Keyword, typing.Collection[_types.Keyword]]
        ] = None,
    ) -> None:
        """
        If `obj` contains a reference to the object instance wrapped by
        the wrapper object, replace it with the wrapper.

        Parameters
        ----------
        obj
            Generic object with a writable instance `.__dict__`
        attr
            Optional name(s) to check and replace;
            default is to replace all instances of the wrapped instance
            in `obj.__dict__`

        Side effects
        ------------
        `obj` updated
        """
        wrapped = wrapper_proxy._get_wrapped_instance(self)
        inst_dict = obj.__dict__
        if attr is None:
            attrs = inst_dict
        elif utils.is_identifier(attr):
            attrs = [attr]
        elif tjekmate.is_collection(
            attr, item_checker=utils.is_identifier,
        ):
            attrs = attr
        else:
            raise TypeError(
                f'attr = {attr!r}: expected attribute name(s) or none'
            )
        attrs = set(attrs)
        for attr in (attrs & set(inst_dict)):
            value = inst_dict[attr]
            if value is wrapped:
                inst_dict[attr] = self

    @classmethod
    def wrap(
        cls: typing.Type[Wrapper],
        wrapped: Wrappee,
        *,
        default_cl_formatter: typing.Optional[_types.CategoryFormatter] = None,
        **kwargs: typing.Mapping[
            _types.CLMethodName, typing.Union[_types.CategoryFormatter, None]
        ]
    ) -> Wrapper:
        """
        Instantiate from an object of a compatible base class.

        Parameters
        ----------
        wrapped
            Instance to be wrapped
        default_cl_formatter
            Optional default DATA FORMATTER for CL METHODS without its
            own
        **kwargs: Mapping[cl_method_name, Union[formatter, None]]
            Mapping from CL METHOD names to either `None` or DATA
            FORMATTERS;
            see Notes

        Return
        ------
        Wrapped instance

        Notes
        -----
        <cl_method_name> = None:
            Register `Axes.<cl_method_name>()` to be a CL METHOD without
            providing a formatter, for all axes associated with the
            object.
        <cl_method_name> = formatter():
            Register `Axes.<cl_method_name>()` to be a CL METHOD, and
            use `formatter()` as its DATA FORMATTER for all `Axes`
            objects associated with the object.
        """
        obj = cls(wrapped)
        if default_cl_formatter is not None:
            obj.default_cl_formatter = default_cl_formatter
        for cl_method, formatter in kwargs.items():
            obj.register_cl_method(cl_method, formatter)
        return obj

    @classmethod
    @tjekmate.check(
        suppressed_categories=(
            tjekmate.identical_to(None)
            | utils.is_identifier
            | tjekmate.is_collection(item_checker=utils.is_identifier)
        ),
        conflate=(
            tjekmate.identical_to(None) | tjekmate.is_instance(typing.Mapping)
        ),
    )
    def _cl_filter_categories(
        cls,
        cl_categories: typing.Mapping[
            typing.Tuple[
                _types.CategorizedKeyword,
                _types.CLMethodName,
                _types.CategoryFormatter,
            ],
            typing.Set[_types.Categorization]
        ],
        *,
        suppressed_categories: typing.Optional[
            typing.Union[
                _types.CategorizedKeyword,
                typing.Collection[_types.CategorizedKeyword],
                typing.Mapping[
                    _types.CategorizedKeyword,
                    typing.Union[
                        _types.CLMethodName,
                        _types.CategoryFormatter,
                        typing.Collection[
                            typing.Union[
                                _types.CLMethodName, _types.CategoryFormatter,
                            ]
                        ],
                    ]
                ],
            ]
        ] = None,
        conflate: typing.Optional[
            typing.Mapping[
                typing.Literal['categories', 'methods', 'formatters'],
                typing.Union[
                    typing.Sequence[str],
                    typing.Collection[typing.Sequence[str]],
                    typing.Sequence[_types.CategoryFormatter],
                    typing.Collection[
                        typing.Sequence[_types.CategoryFormatter]
                    ],
                ]
            ]
        ] = None,
        **kwargs
    ) -> typing.Dict[
        typing.Tuple[
            _types.CategorizedKeyword,
            _types.CLMethodName,
            _types.CategoryFormatter,
        ],
        typing.Set[_types.Categorization]
    ]:
        """
        Filter the categories and suppress certain entries accodring to
        the given specs.

        Parameters
        ----------
        cl_categories
            Return value of

            >>> ax_or_fig.get_cl_categories(  # noqa # doctest: +SKIP
            ...     ..., concise = False,
            ... )

        ...
            See documentation of
            `<MODULE>.Figure.categorized_legend()`
        **kwargs
            Ignored

        Return
        ------
        Dictionary with the same structure as `cl_categories` but
        filtered
        """
        # Helper functions
        prefix = cls.cl_method_prefix

        def is_cl_method_name_or_formatter(obj: typing.Any) -> bool:
            # Check first for CL method name
            if utils.is_identifier(obj) and obj.startswith(prefix):
                return True
            # Then check if it is a data formatter
            try:
                cls._check_formatter(obj)
            except Exception:
                return False
            return obj is not None

        def no_conflation(
            category: _types.CategorizedKeyword,
            method_name: _types.CLMethodName,
            formatter: _types.CategoryFormatter,
        ) -> typing.Tuple[
            _types.CategorizedKeyword,
            _types.CLMethodName,
            _types.CategoryFormatter,
        ]:
            return category, method_name, formatter

        def make_conflator(
            category: typing.Sequence[
                typing.Sequence[_types.CategorizedKeyword]
            ],
            method_name: typing.Sequence[typing.Sequence[_types.CLMethodName]],
            formatter: typing.Sequence[
                typing.Sequence[_types.CategoryFormatter]
            ],
        ) -> typing.Callable[
            [
                _types.CategorizedKeyword,
                _types.CLMethodName,
                _types.CategoryFormatter,
            ],
            typing.Tuple[
                _types.CategorizedKeyword,
                _types.CLMethodName,
                _types.CategoryFormatter,
            ]
        ]:
            Item = typing.TypeVar('Item', bound=typing.Hashable)

            def conflate(
                item: Item,
                conflations: typing.Sequence[typing.Sequence[Item]],
            ) -> Item:
                for conflated_items in conflations:
                    if item in conflated_items:
                        return conflated_items[0]
                return item

            def get_conflated_key(
                category: _types.CategorizedKeyword,
                method_name: _types.CLMethodName,
                formatter: _types.CategoryFormatter,
            ) -> typing.Tuple[
                _types.CategorizedKeyword,
                _types.CLMethodName,
                _types.CategoryFormatter,
            ]:
                return tuple(
                    conflator(value)
                    for conflator, value in zip(
                        conflators, (category, method_name, formatter),
                    )
                )

            conflators = tuple(
                functools.partial(conflate, conflations)
                for conflations in (category, method_name, formatter)
            )
            return get_conflated_key

        def suppress_categories_by_name(
            category: _types.CategorizedKeyword,
            *_,
            suppressed_categories: typing.Collection[_types.CategorizedKeyword],
        ) -> bool:
            return category in suppressed_categories

        def suppress_categories_by_matches(
            category: _types.CategorizedKeyword,
            method_name: _types.CLMethodName,
            formatter: _types.CategoryFormatter,
            suppressed_categories: typing.Mapping[
                _types.CategorizedKeyword,
                typing.Collection[
                    typing.Union[_types.CLMethodName, _types.CategoryFormatter]
                ]
            ],
        ) -> bool:
            matches = suppressed_categories.get(category, set())
            return bool({method_name, formatter} & matches)

        # Type checks
        # suppressed_categories
        if suppressed_categories is None:
            suppressed_categories = ()
        elif isinstance(suppressed_categories, str):
            suppressed_categories = suppressed_categories,
        suppressed_checker: typing.Callable[
            [
                _types.CategorizedKeyword,
                _types.CLMethodName,
                _types.CategoryFormatter
            ],
            bool
        ]
        # conflate
        conflate_keys = 'categories', 'methods', 'formatters'
        conflate_item_checkers = (
            utils.is_identifier, utils.is_identifier, callable,
        )
        conflate_item_names = (
            'string identifier', 'string identifier', 'formatter function',
        )
        if conflate is None:
            conflate = {}
        conflate = {
            key: value for key, value in conflate.items()
            if key in conflate_keys
        }
        for key, item_checker, item_name in zip(
            conflate_keys, conflate_item_checkers, conflate_item_names,
        ):
            if key not in conflate:
                continue
            value = conflate[key]
            sequence_checker = (
                tjekmate.is_sequence(item_checker=item_checker)
                & ~tjekmate.is_instance(str)
            )
            if tjekmate.is_collection(
                value, item_checker=sequence_checker,
            ):
                conflate[key] = tuple(tuple(item) for item in value)
            elif sequence_checker(value):
                conflate[key] = tuple(value),
            else:
                raise TypeError(
                    f'conflate[{key!r}] = {value!r}: '
                    f'expected sequence or sequences of {item_name}s'
                )
            if not value:
                del conflate[key]

        # Early exit
        if not (suppressed_categories or conflate):
            return dict(cl_categories)

        # Create filters and processors
        if isinstance(suppressed_categories, typing.Mapping):
            # Mapping: category name matches key, and (method name or
            # formatter) matches any of the items in value
            suppressed_categories = dict(suppressed_categories)
            for cat_name, suppressed in suppressed_categories.items():
                if is_cl_method_name_or_formatter(suppressed):
                    suppressed_categories[cat_name] = frozenset({suppressed})
                elif tjekmate.is_collection(
                    suppressed, item_checker=is_cl_method_name_or_formatter,
                ):
                    suppressed_categories[cat_name] = frozenset(suppressed)
                else:
                    raise TypeError(
                        f'suppressed_categories[{cat_name!r}] = '
                        f'{suppressed!r}: '
                        'expected a CL method name '
                        f'(beginning with {prefix!r}) or a DATA FORMATTER'
                    )
            suppressed_checker = functools.partial(
                suppress_categories_by_matches,
                suppressed_categories=suppressed_categories,
            )
        else:  # Collection: category name matches any item
            suppressed_checker = functools.partial(
                suppress_categories_by_name,
                suppressed_categories=suppressed_categories,
            )
        if conflate:
            conflator = make_conflator(*(
                conflate.get(coll_name, ()) for coll_name in conflate_keys
            ))
        else:
            conflator = no_conflation
        results: typing.Dict[
            typing.Tuple[
                _types.CategorizedKeyword,
                _types.CLMethodName,
                _types.CategoryFormatter,
            ],
            typing.Set[_types.Categorization]
        ] = {}
        for cat_mthd_fmtr, values in cl_categories.items():
            if suppressed_checker(*cat_mthd_fmtr):
                continue
            results.setdefault(conflator(*cat_mthd_fmtr), set()).update(values)
        return results

    @classmethod
    @tjekmate.check(
        **{
            arg_name: utils.is_keyword_mapping(
                value_checker=tjekmate.is_positive_integer,
                optional=allow_none,
                allow_value=True,
            )
            for arg_name, allow_none in dict(
                range_samples=False,
                log_range_samples=True,
                linear_range_samples=True,
            ).items()
        },
        category_value_sorters=functools.partial(
            utils.is_keyword_mapping,
            value_type=(typing.Callable, typing.Mapping),
            optional=True,
        ),
        category_value_typesetters=functools.partial(
            utils.is_keyword_mapping,
            value_type=(typing.Callable, typing.Mapping, str),
            optional=True,
        ),
        category_value_classifications=functools.partial(
            utils.is_keyword_mapping,
            value_checker=tjekmate.is_in(
                tuple(
                    value for dcc in DataCatClass for value in (dcc, dcc.name)
                ),
            ),
            optional=True,
        ),
    )
    @_utils.insert_kwargs_from(_cl_filter_categories)
    def _cl_gather_handles(
        cls,
        cl_categories: typing.Mapping[
            typing.Tuple[
                _types.CategorizedKeyword,
                _types.CLMethodName,
                _types.CategoryFormatter,
            ],
            typing.Set[_types.Categorization]
        ],
        cl_figure: CategorizedLegendFigure,
        *,
        # Category specifications
        category_value_sorters: typing.Optional[
            typing.Mapping[
                _types.CategorizedKeyword,
                typing.Union[
                    typing.Callable[[_types.Categorization], _types.Sortable],
                    typing.Mapping[_types.Categorization, _types.Sortable],
                ]
            ]
        ] = None,
        category_value_typesetters: typing.Optional[
            typing.Mapping[
                _types.CategorizedKeyword,
                typing.Union[
                    typing.Callable[[_types.Categorization], str],
                    _types.FormatString,
                    typing.Mapping[_types.Categorization, str],
                ]
            ]
        ] = None,
        category_value_classifications: typing.Optional[
            typing.Mapping[
                _types.CategorizedKeyword, typing.Union[DataCatClass, str]
            ]
        ] = None,
        # Category classification
        order_base: numbers.Real = 10,
        max_linear_orders: numbers.Integral = 3,
        max_discrete_numbers: numbers.Integral = 5,
        # Numeric category rendering options
        range_samples: typing.Union[
            typing.Mapping[_types.CategorizedKeyword, numbers.Integral],
            numbers.Integral,
        ] = 5,
        log_range_samples: typing.Optional[
            typing.Union[
                typing.Mapping[_types.CategorizedKeyword, numbers.Integral],
                numbers.Integral,
            ]
        ] = None,
        linear_range_samples: typing.Optional[
            typing.Union[
                typing.Mapping[_types.CategorizedKeyword, numbers.Integral],
                numbers.Integral,
            ]
        ] = None,
        # Misc
        **kwargs
    ) -> typing.Dict[
        typing.Tuple[_types.CategorizedKeyword, DataCatClass],
        typing.List[typing.List[typing.Tuple[_types.LegendHandle, str]]]
    ]:
        """
        Gather legend handles from the data.

        Parameters
        ----------
        cl_categories
            Return value of

            >>> ax_or_fig.get_cl_categories(  # noqa # doctest: +SKIP
            ...     ..., concise=False,
            ... )

        cl_figure
            Figure associated with the axes or figure
        ...
            See documentation of
            `<MODULE>.Figure.categorized_legend()`
        **kwargs
            Passed to `._cl_filter_categories()`

        Return
        ------
        Gathered data in the form

        >>> dict[  # noqa # doctest: +SKIP
        ...     tuple[category_name, classification],
        ...     list[
        ...         # One item for each combination of category name,
        ...         # CL method, and data formatter
        ...         list[
        ...             # One item for each resolved category value
        ...             tuple[handle, string_label]
        ...         ]
        ...     ]
        ... ];

        Notes
        -----
        Each item in the result of `.get_cl_categories()` is classified
        into a `DataCatClass`, which dictates how the `arguments` are
        to be transformed and used in building a legend entry.
        Currently the only transformations implemented are:

        DataCatClass.BOOLS
        -> `[True, False]`;

        DataCatClass.LINEAR_RANGE
        -> `linear_range_samples`-many numbers uniformly resampled from
           between the extrema (inclusive);

        DataCatClass.LOG_RANGE
        -> `log_range_samples`-many numbers geometrically resampled from
           between the extrema (inclusive).
        """
        # Samplers for category values
        N = typing.TypeVar('N', bound=numbers.Number)
        Sampler = typing.Callable[
            [typing.Set[N], typing.Optional[numbers.Integral]],
            typing.Collection[N]
        ]

        def sample_bools(
            arguments: typing.Set[bool],
            samples: typing.Optional[numbers.Integral] = None,
        ) -> typing.Collection[bool]:
            return [True, False]

        def sample_log_range(
            arguments: typing.Set[numbers.Real],
            samples: typing.Optional[numbers.Integral] = None,
        ) -> typing.Collection[numbers.Real]:
            if samples is None:
                samples = defaults['log_range_samples']
            lo, hi = min(arguments), max(arguments)
            assert (lo < hi < 0) or (0 < lo < hi)
            return np.geomspace(lo, hi, samples)

        def sample_linear_range(
            arguments: typing.Set[numbers.Real],
            samples: typing.Optional[numbers.Integral] = None,
        ) -> typing.Collection[numbers.Real]:
            if samples is None:
                samples = defaults['linear_range_samples']
            if all(isinstance(x, numbers.Integral) for x in arguments):
                result = np.linspace(
                    min(arguments), max(arguments), samples, dtype=int
                )
                return sorted(set(result))
            return np.linspace(min(arguments), max(arguments), samples)

        value_samplers: typing.Dict[DataCatClass, Sampler] = {
            DataCatClass.BOOLS: sample_bools,
            DataCatClass.LOG_RANGE: sample_log_range,
            DataCatClass.LINEAR_RANGE: sample_linear_range,
        }

        # Get defaults from the signature
        defaults = {
            name: param.default
            for name, param in (
                inspect.signature(cls._cl_gather_handles)
                .parameters.items()
            )
            if param.default is not param.empty
        }
        for name in 'log_range_samples', 'linear_range_samples':
            if defaults.get(name) is None:
                defaults[name] = defaults['range_samples']

        # Type checks/massaging
        # cl_figure
        fig_cls = _utils.load_cl_namespace('figure.CategorizedLegendFigure')
        if not isinstance(cl_figure, fig_cls):
            raise TypeError(
                f'cl_figure = {cl_figure!r}: '
                f'expected a `{cls._get_cl_module_name()}.Figure` object'
            )
        # category_value_*
        cv_maps = dict(
            sorters=dict(category_value_sorters or {}),
            typesetters=dict(category_value_typesetters or {}),
            classifications=dict(category_value_classifications or {}),
        )
        # category_value_classifications
        cv_maps['classifications'] = {
            category: (
                DataCatClass[enum_or_name]
                if isinstance(enum_or_name, str) else
                enum_or_name
            )
            for category, enum_or_name in cv_maps['classifications'].items()
        }
        # *_samples
        if log_range_samples is None:
            log_range_samples = range_samples
        if linear_range_samples is None:
            linear_range_samples = range_samples
        classification_num_samples = {
            DataCatClass.LOG_RANGE: log_range_samples,
            DataCatClass.LINEAR_RANGE: linear_range_samples,
        }

        # Create dummy axis
        dummy_ax = cl_figure.add_subplot(cl_figure.add_gridspec(1, 1)[:, :])

        # Filter and gather data
        cl_categories = cls._cl_filter_categories(cl_categories, **kwargs)
        gathered: typing.Dict[
            typing.Tuple[_types.CategorizedKeyword, DataCatClass],
            typing.List[typing.List[typing.Tuple[_types.LegendHandle, str]]]
        ] = {}
        try:
            # Gather handles from all categories and CL methods
            # formatted with various formatters
            prefix = cls.cl_method_prefix
            for (
                (category, cl_method, formatter), arguments
            ) in cl_categories.items():
                assert cl_method.startswith(prefix)
                classification = cv_maps['classifications'].get(
                    category,
                    DataCatClass.from_collection(
                        arguments,
                        order_base=order_base,
                        max_linear_orders=max_linear_orders,
                        max_discrete_numbers=max_discrete_numbers,
                    )
                )
                # Sample the argument space and sort the samples
                sampler = value_samplers.get(classification)
                sorting = cv_maps['sorters'].get(category)
                if sampler:
                    samples: typing.Any = classification_num_samples.get(
                        classification
                    )
                    if isinstance(samples, typing.Mapping):
                        # Category-dependent
                        samples = samples.get(category)
                    if samples is None:
                        arguments = sampler(arguments)
                    elif isinstance(samples, numbers.Integral):
                        arguments = sampler(arguments, samples)
                    else:
                        raise AssertionError
                if sorting is None:
                    arguments = sorted(arguments)
                elif callable(sorting):
                    arguments = sorted(arguments, key=sorting)
                elif isinstance(sorting, typing.Mapping):
                    arguments = sorted(arguments, key=sorting.__getitem__)
                else:
                    raise AssertionError
                arguments = tuple(arguments)
                # Create handle for each sample
                handles = tuple(
                    (
                        CategorizedLegendRecipe.get(cl_method[len(prefix):])
                        .get_handle(dummy_ax, formatter, **{category: argument})
                    )
                    for argument in arguments
                )
                # Generate labels to go with the artists
                typesetting = cv_maps['typesetters'].get(category)
                typesetter: typing.Callable[[_types.Sortable], str]
                if typesetting is None:
                    typesetter = str
                elif callable(typesetting):
                    typesetter = typesetting
                elif isinstance(typesetting, str):
                    typesetter = typesetting.format
                elif isinstance(typesetting, typing.Mapping):
                    def typesetter(arg: _types.Sortable) -> str:
                        return typesetting.get(arg, str(arg))
                else:
                    raise AssertionError
                labels = tuple(typesetter(argument) for argument in arguments)
                if not all(isinstance(label, str) for label in labels):
                    raise TypeError(
                        f'category_value_typesetters[{category!r}] = '
                        f'{typesetting!r}: '
                        'typeset labels not all strings'
                    )
                # Store the generated handles and labels
                gathered.setdefault(
                    (category, classification), []
                ).append(list(zip(handles, labels)))
        finally:  # Remove all trace of the dummy axes
            for method in dummy_ax.clear, dummy_ax.set_axis_off:
                try:
                    method()
                except Exception:
                    pass
            else:
                cl_figure._axstack.remove(dummy_ax)
        return gathered

    @classmethod
    @tjekmate.check(
        category_name_sorter=(
            callable | utils.is_keyword_mapping(optional=True)
        ),
        category_name_typesetter=(
            callable
            | tjekmate.is_instance(str)
            | utils.is_keyword_mapping(optional=True, value_type=str)
        ),
        show_category_names=utils.is_keyword_mapping(
            value_checker=tjekmate.is_strict_boolean, allow_value=True,
        ),
        values_per_line=utils.is_keyword_mapping(
            value_checker=tjekmate.is_positive_integer, allow_value=True,
        ),
    )
    def _cl_collate_handles(
        cls,
        gathered_data: typing.Dict[
            typing.Tuple[_types.CategorizedKeyword, DataCatClass],
            typing.List[typing.List[typing.Tuple[_types.LegendHandle, str]]]
        ],
        *,
        category_name_sorter: typing.Optional[
            typing.Union[
                typing.Callable[[_types.CategorizedKeyword], _types.Sortable],
                typing.Mapping[_types.CategorizedKeyword, _types.Sortable],
            ]
        ] = None,
        category_name_typesetter: typing.Optional[
            typing.Union[
                typing.Callable[[_types.CategorizedKeyword], str],
                _types.FormatString,
                typing.Mapping[_types.CategorizedKeyword, str]
            ]
        ] = None,
        show_category_names: typing.Union[
            typing.Mapping[_types.CategorizedKeyword, bool], bool
        ] = False,
        values_per_line: typing.Union[
            typing.Mapping[_types.CategorizedKeyword, numbers.Integral],
            numbers.Integral,
        ] = 1,
        **kwargs
    ) -> typing.OrderedDict[
        _types.CategorizedKeyword,
        typing.List[typing.Tuple[_types.LegendHandle, str]]
    ]:
        """
        Fold the handles so that each legend line lists the required
        number of values.

        Parameters
        ----------
        gathered_data
            Return value of `ax_or_fig._cl_gather_handles()`
        ...
            See documentation of
            `<MODULE>.Figure.categorized_legend()`
        **kwargs
            Ignored

        Return
        ------
        >>> OrderedDict[  # noqa # doctest: +SKIP
        ...     category_name,
        ...     list[  # One item per legend line
        ...         tuple[handle, string_label]
        ...     ]
        ... ]

        where each `handle` and `string_label` may be composed of the
        combination of several thereof from `gathered_data`
        """
        # Utility functions
        Item = typing.TypeVar('Item')

        def split_long_list(
            seq: typing.Sequence[Item], lmax: numbers.Integral,
        ) -> typing.List[typing.Sequence[Item]]:
            """
            Split `seq` into a list of subsequences, each being at most
            `lmax` long.
            """
            assert lmax > 0
            results = []
            while len(seq) > lmax:
                truncated, seq = seq[:lmax], seq[lmax:]
                results.append(truncated)
            if seq:
                results.append(seq)
            return results

        # Get defaults from the signature
        defaults = {
            name: param.default
            for name, param in (
                inspect.signature(cls._cl_collate_handles)
                .parameters.items()
            )
            if param.default is not param.empty
        }

        # Type checking/massaging
        # category_name_*
        if utils.is_keyword_mapping(category_name_sorter):
            category_name_sorter = functools.partial(
                operator.getitem, category_name_sorter,
            )
        if category_name_typesetter is None:
            category_name_typesetter = str
        elif isinstance(category_name_typesetter, typing.Mapping):
            # assert utils.is_keyword_mapping(
            #     category_name_typesetter, value_type=str
            # )
            category_name_typesetter = (
                lambda mapping: lambda c: mapping.get(c, c)
            )(category_name_typesetter)
        elif isinstance(category_name_typesetter, str):
            category_name_typesetter = category_name_typesetter.format

        # Collation
        collated: typing.Dict[
            _types.CategorizedKeyword,
            typing.List[typing.Tuple[_types.LegendHandle, str]]
        ] = {}
        for (category, classification), bundles in sorted(
            gathered_data.items(),
            # Note: enums can't be directly compared with one another,
            # so just look at its name
            key=lambda ccb: (ccb[0][0], ccb[0][1].name),
        ):
            if isinstance(values_per_line, numbers.Integral):
                legend_line_len = values_per_line
            elif isinstance(values_per_line, typing.Mapping):
                legend_line_len = values_per_line.get(
                    category, defaults['values_per_line']
                )
            else:
                raise AssertionError
            subbundles: typing.Sequence[
                typing.Sequence[typing.Tuple[_types.LegendHandle, str]]
            ]
            for bundle in bundles:
                # Each bundle represents the variety of arguments which
                # has been supplied to a given combination of Axes
                # method and formatter
                if classification in (
                    DataCatClass.LOG_RANGE, DataCatClass.LINEAR_RANGE,
                ):
                    # Format ranges as single legend entries
                    subbundles = bundle,
                    infix, comb_fmt = r'$,\,\cdots,\,$', '$[${}$]$'
                else:
                    # Format as discrete entries, keeping at most
                    # values_per_line many on a line
                    subbundles = split_long_list(bundle, legend_line_len)
                    infix, comb_fmt = ', ', '{}'
                for subbundle in subbundles:
                    handles, labels = zip(*subbundle)
                    comb_handle = _types.CLHandleCollection(handles)
                    comb_label = comb_fmt.format(infix.join(labels))
                    (
                        collated.setdefault(category, [])
                        .append((comb_handle, comb_label))
                    )

        # Handle categories: formatting and sorting
        if show_category_names:  # Prepend cat name where appropriate
            for category, entries in collated.items():
                if not (
                    isinstance(show_category_names, bool) or
                    show_category_names.get(
                        category, defaults['show_category_names']
                    )
                ):
                    continue
                typeset_category = category_name_typesetter(category)
                if not isinstance(typeset_category, str):
                    raise TypeError(
                        f'category_name_typesetter = '
                        f'{category_name_typesetter!r}: '
                        'typeset category '
                        f'({category!r} -> {typeset_category!r}) not a string'
                    )
                collated[category] = [
                    (handle, rf'{typeset_category}$\,=\,${label}')
                    for handle, label in entries
                ]
        # Sort categories
        sorted_categories = sorted(collated, key=category_name_sorter)
        return OrderedDict((cat, collated[cat]) for cat in sorted_categories)

    @classmethod
    def _cl_distribute_handles(
        cls,
        collated_data: typing.OrderedDict[
            _types.CategorizedKeyword,
            typing.List[typing.Tuple[_types.LegendHandle, str]]
        ],
        *,
        ncol: numbers.Integral = 1,
        fill_columns_first: typing.Optional[bool] = None,
        fill_rows_first: typing.Optional[bool] = None,
        gap_between_categories: bool = True,
        **kwargs
    ) -> typing.Tuple[np.ndarray, numbers.Integral]:
        """
        Lay out the legend items on a 2-D grid.

        Parameters
        ----------
        collated_data
            Return value of `ax_or_fig._cl_collate_handles()`
        ...
            See documentation of
            `<MODULE>.Figure.categorized_legend()`
        **kwargs
            Ignored

        Return
        ------
        Two-tuple of `array, final_ncol`, where:
        - array
            2-D numpy array of at most `ncol` columns (if
            `fill_columns_first`) or rows (otherwise);
            the first index iterates through the columns;
            each item will be a tuple of the form `(handle, label)`
        - final_ncol
            Resolved number of columns in the legend
        """
        # Type checking
        # ncol
        if not tjekmate.is_positive_integer(ncol):
            raise TypeError(f'ncol = {ncol!r}: expected a positive integer')
        # fill_{rows,columns}_first
        if fill_columns_first is None and fill_rows_first is None:
            # Default: use matplotlib default (fill columns first)
            transpose_handles = False
        elif fill_columns_first is None:  # fill_rows_first supplied
            transpose_handles = bool(fill_rows_first)
        elif fill_rows_first is None:  # fill_columns_first supplied
            transpose_handles = not fill_columns_first
        else:  # Both supplied
            if not bool(fill_columns_first) ^ bool(fill_rows_first):
                raise ValueError(
                    f'fill_columns_first = {fill_columns_first!r}, '
                    f'fill_rows_first = {fill_rows_first!r}: '
                    'conflicting values'
                )
            transpose_handles = bool(fill_rows_first)

        # Figure out the column to put each category in
        category_lengths: typing.Dict[_types.CategorizedKeyword, int] = {
            category: len(lines) for category, lines in collated_data.items()
            if lines
        }
        if len(category_lengths) < ncol:
            ncol = len(category_lengths)
        if not ncol:
            # Note: `ncol` must be at least one or `matplotlib` will
            # complain
            return np.array((), dtype=object).reshape((0, 0)), 1
        pad_length = 1 if gap_between_categories else 0
        column_assignments: typing.List[
            typing.List[_types.CategorizedKeyword]
        ] = [[] for _ in range(ncol)]
        column_nlines = np.zeros(ncol, dtype=int)
        if ncol > 1:
            # Multicolumn -> greedy split, start filling the columns
            # from the longet categories first
            for category, length in sorted(
                category_lengths.items(),
                key=(lambda cat_length: cat_length[1]),
                reverse=True,
            ):
                i = column_nlines.argmin()
                if column_nlines[i]:  # Add line to non-empty column
                    column_nlines[i] += pad_length + length  # + padding
                else:  # First category assigned to column
                    column_nlines[i] = length
                column_assignments[i].append(category)
            category_order = tuple(collated_data).index
            for cc in column_assignments:
                # Resort categories in each column by established order
                cc.sort(key=category_order)
        else:  # No split
            column_assignments[0].extend(collated_data)
            if category_lengths:  # Include padding between categories
                column_nlines[0] = (
                    sum(category_lengths.values()) +
                    pad_length*(len(category_lengths) - 1)
                )

        # Create columns
        # - Insert padding as breaks between categories
        # - Pad the columns so that each has the same number of entries
        nlines = column_nlines.max()
        padding = (cls.make_cl_empty_handle(), '')
        columns = np.empty(
            (ncol, nlines), dtype=[('handle', object), ('label', object)],
        )
        columns[...] = padding
        for i_col, categories in enumerate(column_assignments):
            if not categories:  # Empty column
                continue
            row_offset = 0
            for category in categories:
                handles_and_labels = collated_data[category]
                nitems = len(handles_and_labels)
                columns[
                    i_col, row_offset:(row_offset + nitems)
                ] = handles_and_labels
                row_offset += pad_length + nitems
        # Transpose where appropriate
        if transpose_handles:
            columns = columns.T
        if columns.size:
            ncol = columns.shape[0]
        else:  # Default
            ncol = 1
        return columns, ncol

    @_utils.insert_kwargs_from(_cl_gather_handles, _cl_collate_handles)
    def _get_cl_handles_labels(
        self,
        cl_categories: typing.Mapping[
            typing.Tuple[
                _types.CategorizedKeyword,
                _types.CLMethodName,
                _types.CategoryFormatter,
            ],
            typing.Set[_types.Categorization]
        ],
        cl_figure: CategorizedLegendFigure,
        **kwargs
    ) -> typing.OrderedDict[
        _types.CategorizedKeyword,
        typing.List[typing.Tuple[_types.LegendHandle, str]]
    ]:
        """
        Internal method for getting legend items.

        Parameters
        ----------
        cl_categories
            Result from calling `.get_cl_categories(concise=False)`,
            a mapping of form

            >>> dict[  # noqa # doctest: +SKIP
            ...     tuple[
            ...         category_name, cl_method_name, data_formatter,
            ...     ],
            ...     set[category_value]
            ... ]

        cl_figure
            `<MODULE>.Figure`
            the object is associated with;
            `self` for figures, `self.figure` for axes, etc.
        **kwargs
            See the documentations of `.get_cl_handles_labels()` and
            `.categorized_legend()`

        Notes
        -----
        Should be overridden by subclasses for providing different
        signatures.
        """
        # Drop suppressed categories
        # Classification of data type
        # Rendering of data (numerical ranges, boolean switches)
        # Generation of handles
        gathered_handles_and_labels = self._cl_gather_handles(
            cl_categories, cl_figure, **kwargs,
        )
        # Collation of handles
        # Concatenation of labels
        # Split categories with many entries into lines
        # Format labels with category names(if necessary)
        # Sort categories
        return self._cl_collate_handles(gathered_handles_and_labels, **kwargs)

    @_utils.copy_method_signature_from(_get_cl_handles_labels)
    def get_cl_handles_labels(
        self, *args, **kwargs
    ) -> typing.OrderedDict[
        _types.CategorizedKeyword,
        typing.List[typing.Tuple[_types.LegendHandle, str]]
    ]:
        """
        Retrieve handles and labels suitable for constructing a
        CATEGORIZED LEGEND (CL).

        Parameters
        ----------
        *arg, **kwargs
            See documentation for `.categorized_legend()`

        Return
        ------
        >>> OrderedDict[  # noqa # doctest: +SKIP
        ...     category_name,
        ...     list[  # One item per legend line
        ...         tuple[handle, string_label]
        ...     ]
        ... ]

        where each `handle` and `string_label` may be suitably collated
        from several individual ones
        """
        return self._get_cl_handles_labels(*args, **kwargs)

    @_utils.insert_kwargs_from(_cl_distribute_handles)
    @_utils.copy_method_signature_from(_get_cl_handles_labels)
    def _categorized_legend(
        self: typing.Self,
        *args,
        ncol: numbers.Integral = 1,
        fill_columns_first: typing.Optional[bool] = None,
        fill_rows_first: typing.Optional[bool] = None,
        gap_between_categories: bool = True,
        legend_method: typing.Optional[LegendLike] = None,
        **kwargs
    ) -> Legend:
        """
        Method called internally to return a CL;
        see the documentation for `.categorized_legend()` (to be
        implemented in classes using this mixin) for the meaning of
        individual parameters.

        Parameters
        ----------
        *args, **kwargs
            Passed to `.get_cl_handles_labels()`
        ...
            See documentation of
            `<MODULE>.Figure.categorized_legend()`

        Return
        ------
        Legend
        """
        # Create 'bound method'
        if legend_method is None:
            legend_bound_method = self.legend
        else:
            legend_bound_method = functools.partial(legend_method, self)
        # Get the anonymous kwargs, which are to be supplied to
        # .legend()
        sig = inspect.signature(self.categorized_legend)
        kwargs_param_name = next(
            name for name, param in sig.parameters.items()
            if param.kind is param.VAR_KEYWORD
        )
        legend_kwargs = (
            sig.bind(*args, ncol=ncol, **kwargs)
            .arguments.get(kwargs_param_name, {})
        )
        # Create and arrange handles and labels
        handles_and_labels = self._get_cl_handles_labels(
            *args, **kwargs
        )
        hndl_lbl_array, ncol = self._cl_distribute_handles(
            handles_and_labels,
            ncol=ncol,
            fill_columns_first=fill_columns_first,
            fill_rows_first=fill_rows_first,
            gap_between_categories=gap_between_categories,
            **kwargs,
        )
        # Actually making the Legend
        if hndl_lbl_array.size:
            handles, labels = zip(*hndl_lbl_array.flatten())
        else:
            handles, labels = (), ()
        legend = legend_bound_method(
            handles, labels, ncol=ncol, **legend_kwargs,
        )
        legend_has_title = 'title' in legend_kwargs
        # When the legend is empty, issue a warning
        if not len(handles):
            ba = sig.bind(*args, **kwargs)
            dropped_arg_names = [
                arg_name for arg_name in ba.arguments
                if ba.signature.parameters[arg_name].kind in (
                    inspect.Parameter.KEYWORD_ONLY,
                    inspect.Parameter.VAR_KEYWORD,
                )
            ]
            for arg_name in dropped_arg_names:
                del ba.arguments[arg_name]
            args_repr = ', '.join(
                '{}{}={!r}'.format(
                    (
                        '*'
                        if (
                            sig.parameters[name].kind
                            is inspect.Parameter.VAR_POSITIONAL
                        ) else
                        ''
                    ),
                    name,
                    value,
                ) for name, value in ba.arguments.items()
            )
            if legend_has_title:
                msg = 'the legend object is empty except its title'
            else:
                legend.set_visible(False)
                msg = (
                    'making the created legend object invisible '
                    'because it is empty (no handles, labels, titles)'
                )
            warnings.warn(f'{args_repr}: {msg}')
        return legend

    @_utils.copy_annotations_from(get_cl_children)
    def _find_cl_children_at(
        self,
        *args,
        _locations: typing.Collection[_types.Keyword],
        _immediate_child_parentage_loc: typing.Optional[_types.Keyword] = None,
        max_level=None,
        **kwargs,
    ):
        """
        Helper method for `.get_cl_children()`, to be called therein.

        Parameters
        ----------
        *args, **kwargs
            Passed to `.get_cl_children()` of child objects
        _locations
            Collection of attribute names at which collections of
            children can be found
        _immediate_child_parentage_loc
            Optional attribute name to check at the immediate children
            that their parent is indeed `self`
        max_level
            Optional limit for how many levels of children to descend
            into;
            the default `None` (equivalent to `True`) indicates that
            there is no limit

        Return
        ------
        Result for `.get_cl_children()`
        """
        CHILDREN_CLASSES = (
            _utils.load_cl_namespace('figure.CategorizedLegendFigure'),
            _utils.load_cl_namespace('axes.CategorizedLegendAxes'),
        )
        Level = typing.List[typing.Union[CHILDREN_CLASSES]]
        if tjekmate.is_collection(
            _locations, item_checker=utils.is_identifier,
        ):
            _locations = frozenset(_locations)
        else:
            raise TypeError(
                f'_locations = {_locations!r}: expected string identifiers'
            )
        if tjekmate.is_strict_boolean(max_level):
            max_level = None if max_level else 0
        if not (max_level is None or isinstance(max_level, numbers.Integral)):
            raise TypeError(
                f'max_level = {max_level!r}: expected `None` or an integer'
            )
        levels: typing.Dict[numbers.Integral, Level] = {}
        if max_level is not None and max_level < 1:
            return []
        next_level = [
            child for loc in _locations for child in getattr(self, loc, ())
            if isinstance(child, CHILDREN_CLASSES)
        ]
        if _immediate_child_parentage_loc:
            next_level = [
                child for child in next_level
                if getattr(child, _immediate_child_parentage_loc, None) is self
            ]
        if not next_level:
            return []
        levels[0] = next_level
        if max_level is not None:
            max_level -= 1
        for child in next_level:
            for n, children in enumerate(child.get_cl_children(
                *args, max_level=max_level, **kwargs,
            )):
                levels.setdefault(1 + n, []).extend(children)
        return [level for _, level in sorted(levels.items())]

    @staticmethod
    def _get_cl_module_name() -> str:
        """
        Return
        ------
        '<MODULE>'
        """
        return _utils.CL_MODULE

    @staticmethod
    def make_cl_empty_handle() -> _types.LegendHandle:
        """
        Return
        ------
        'Empty' legend handle which can be used for spacing
        """
        return Line2D([], [], linestyle='none')

    # ********************** Mixin descriptors *********************** #

    @property
    def cl_method_formatters(self) -> typing.Mapping[
        _types.CategorizedKeyword, _types.CategoryFormatter
    ]:
        """
        Mapping of the CL METHOD names to DATA FORMATTERS
        (readonly;
        manipulation happens via `.register_cl_method()` and
        `.deregister_cl_method()`)
        """
        if not hasattr(self, '_cl_mthd_fmtrs'):
            self._cl_mthd_fmtrs = {}
        return types.MappingProxyType(dict(self._cl_mthd_fmtrs))

    @cl_method_formatters.deleter
    def cl_method_formatters(self) -> None:
        if self.cl_method_formatters:
            self._cl_mthd_fmtrs.clear()

    @property
    def default_cl_formatter(
        self,
    ) -> typing.Union[_types.CategoryFormatter, None]:
        """
        Default data formatter for CL METHODS, used if the value of its
        entry in `.cl_method_formatters` is `None`
        """
        if not hasattr(self, '_default_cl_formatter'):
            self.default_cl_formatter = None
        return self._default_cl_formatter

    @default_cl_formatter.setter
    def default_cl_formatter(
        self, formatter: typing.Union[_types.CategoryFormatter, None],
    ) -> None:
        self._default_cl_formatter = self._check_formatter(formatter)
        # Propagate to one level of children at a time
        for child in (
            child for children in self.get_cl_children(max_level=1)
            for child in children
        ):
            child.default_cl_formatter = formatter

    @default_cl_formatter.deleter
    def default_cl_formatter(self) -> None:
        self.default_cl_formatter = None

    # ******************** Misc. mixin attributes ******************** #

    cl_method_prefix: str = 'cl_'

    # Need slots for the private variables backing the descriptors
    __slots__ = ('_default_cl_formatter', '_cl_mthd_fmtrs')

    # ********************* Attribute injection ********************** #
    # What to inject into other classes, and how?

    @classmethod
    def inject(
        cls,
        another_class: typing.Type[Wrapper],
        override: typing.Union[bool, None] = None,
        call_set_name: bool = False,
    ) -> typing.Type[Wrapper]:
        """
        Inject the class' `.injectables` into the namespace of another
        class.

        Parameters
        ----------
        another_class
            Class to inject the `.injectables` to
        override
            Behavior switch for whether to override preexisting names:
            True
                Inject even if the name already exists.
            False
                Never inject if the name already exists.
            None
                Inject even if the name already exists, but only when it
                is not local to the `another_class` (i.e. is inherited).
        call_set_name
            Whether the invoke an injectable's `.__set_name__()` method,
            if any

        Return
        ------
        `another_class` but with the names injected

        Notes
        -----
        - If the injectable is `.__slots__`, it is silently ignored
          (since changing the `.__slots__` of an already-instantiated
          class is futile).

        - The signature of the injected `.get_cl_handles_labels()`
          method is updated according to that of
          `another_class._get_cl_handles_labels()`.
        """
        if not isinstance(another_class, type):
            raise TypeError(
                f'another_class = {another_class!r}: expected a class'
            )
        if not (override is None or isinstance(override, bool)):
            raise TypeError(
                f'override = {override!r}: expected `None` or a boolean'
            )
        if not isinstance(call_set_name, bool):
            raise TypeError(
                f'call_set_name = {call_set_name!r}: expected a boolean'
            )
        # Inject names
        for name, attr in cls.injectables.items():
            if name == '__slots__':
                continue
            if (
                override or
                (override is None and name not in vars(another_class)) or
                not hasattr(another_class, name)
            ):
                setattr(another_class, name, attr)
                if (
                    call_set_name and
                    callable(getattr(attr, '__set_name__', None))
                ):
                    attr.__set_name__(another_class, name)
        # Update signatures of certain methods
        another_cls_dict = vars(another_class)
        underscore_gchl_injected, gchl_injected = (
            another_cls_dict[method] is vars(CategorizedLegendMixin)[method]
            for method in ('_get_cl_handles_labels', 'get_cl_handles_labels')
        )
        if gchl_injected and not underscore_gchl_injected:
            gchl = _utils.copy_method_signature_from(
                another_cls_dict['_get_cl_handles_labels'], copy=True,
            )(another_cls_dict['get_cl_handles_labels'])
            another_class.get_cl_handles_labels = gchl
        return another_class

    injectables = locals()
    injectables = {
        attr: value for attr, value in dict(injectables).items()
        if (
            attr not in (
                'injectables', 'inject',
                '__doc__', '__module__', '__annotations__',
                '__name__', '__qualname__',
            )
        )
    }
