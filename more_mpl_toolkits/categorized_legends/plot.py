"""
Utilities for making categorized plots.
"""
import functools
import inspect
import itertools
import numbers
import os
import textwrap
import typing
from tempfile import TemporaryDirectory
from warnings import warn

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import transforms
from matplotlib.axes import Axes
from matplotlib.axis import Axis
from matplotlib.figure import Figure
from matplotlib.gridspec import GridSpec
from matplotlib.legend import Legend
from matplotlib.text import Annotation, Text
from matplotlib.transforms import Bbox

import tjekmate

from .. import aligned_annotations, legend_placement
from . import core, recipes, utils, _types
from .._utils.decorators import copy_func

try:  # line_profiler compatibility
    profile
except NameError:
    def profile(func: typing.Callable) -> typing.Callable:
        """
        Dummy line-profiling decorator (no-op).
        """
        return func

__all__ = (
    'get_grid_dimensions',
    'plot_categorized',
    'place_legend_optimally',
)

# ************************ Typing and configs ************************ #

CLF, CLA = core.CategorizedLegendFigure, core.CategorizedLegendAxes

T = typing.TypeVar('T')
PS = typing.ParamSpec('PS')

Row = typing.TypeVar('Row')
PlottableData = typing.TypeVar('PlottableData')
Keyword = typing.TypeVar('Keyword', bound=str)
Keywords = typing.Mapping[Keyword, typing.Any]
CategoryIdentifier = typing.TypeVar('CategoryIdentifier', bound=Keyword)
CategoryValue = typing.TypeVar('CategoryValue', bound=typing.Hashable)
Categorization = typing.Mapping[CategoryIdentifier, CategoryValue]
PlenaryLabel = typing.TypeVar('PlenaryLabel', bound=str)
PlotCategorizedReturnValue = typing.Tuple[
    CLF,
    typing.Union[
        CLA,
        typing.Mapping[typing.Union[CategoryValue, PlenaryLabel], CLA],
        typing.Mapping[
            typing.Tuple[
                typing.Union[CategoryValue, PlenaryLabel],
                typing.Union[CategoryValue, PlenaryLabel],
            ],
            CLA
        ],
    ],
]

OptionalKeywords = typing.Optional[Keywords]
OptionalKeywordsWithBoolShorthand = typing.Optional[
    typing.Union[OptionalKeywords, bool]
]

_ALLOWED_AXES_DESIGNATIONS = 'x', 'y'
ALLOWED_AXES_DESIGNATIONS = (
    *_ALLOWED_AXES_DESIGNATIONS, 'xy', 'none', None, True, False,
)
_AllowedAxesDesignations = typing.Literal[_ALLOWED_AXES_DESIGNATIONS]
AllowedAxesDesignations = typing.Literal[ALLOWED_AXES_DESIGNATIONS]

ALLOWED_SHARE_XY_VALUES = ('all', 'none', 'row', 'col', True, False)
AXIS_SHARING_MODES = {'all': True, 'none': False}
AllowedShareXYValue = typing.Literal[ALLOWED_SHARE_XY_VALUES]

ALLOWED_PLENARY_VALUES = 'none', 'before', 'after', None, True, False
PLENARY_MODES = {'none': None, False: None, True: 'before'}
AllowedPlenary = typing.Literal[ALLOWED_PLENARY_VALUES]

AllowedSplitXY = typing.Union[
    CategoryIdentifier,
    typing.Tuple[CategoryIdentifier, numbers.Integral, numbers.Integral],
]
SplitDict = typing.TypedDict(
    'SplitDict',
    dict.fromkeys(('x', 'y', 'xy'), typing.Union[CategoryIdentifier, None]),
)
GridPoint = typing.Tuple[numbers.Integral, numbers.Integral]

UnifyAxLimDict = typing.TypedDict(
    'UnifyAxLimDict',
    dict.fromkeys('xy', typing.Union[bool, typing.Literal['row', 'col']]),
)

DropTickLabelDict = typing.TypedDict('DropTickLabelDict', dict(x=bool, y=bool))

_AnnotateXY = typing.TypeVar(
    'XY', typing.Tuple[numbers.Real, numbers.Real], str,
)

AnnotateAlignedLike = typing.Callable[[Axes, str, _AnnotateXY], Annotation]
FigureAddGridSpecLike = typing.Callable[
    [Figure, numbers.Integral, numbers.Integral], GridSpec,
]
BoundFigureAddGridSpecLike = typing.Callable[
    [numbers.Integral, numbers.Integral], GridSpec,
]

HORIZONTAL_TILINGS = 'left', 'right'
VERTICAL_TILINGS = 'top', 'bottom'
ALLOWED_TILINGS = dict(
    {
        tiling: tiling
        for hori_vert_tiling in itertools.product(
            HORIZONTAL_TILINGS, VERTICAL_TILINGS,
        )
        for tiling in (hori_vert_tiling, hori_vert_tiling[::-1])
    },
    left=('left', 'top'),
    top=('top', 'left'),
    right=('right', 'top'),
    bottom=('bottom', 'left'),
)
AllowedTilings = typing.Literal[tuple(ALLOWED_TILINGS)]

EMPTY = inspect.Signature.empty

CATEGORIZED_LEGEND_PARAMS = inspect.signature(CLF.categorized_legend).parameters
CATEGORIZED_LEGEND_DEFAULTS = {
    param.name: param.default
    for param in CATEGORIZED_LEGEND_PARAMS.values()
    if param.default is not param.empty
}
CATEGORIZED_LEGEND_TYPE_HINTS = {
    param.name: param.annotation
    for param in CATEGORIZED_LEGEND_PARAMS.values()
    if param.annotation is not param.empty
}

CL_ALIASED_ARGS = (
    'category_value_sorters',
    'category_value_typesetters',
    'category_name_typesetter',
    'show_category_names',
)
NON_CL_ALIASED_ARGS = (
    'categorizer',
)
ALIASED_ARGS = CL_ALIASED_ARGS + NON_CL_ALIASED_ARGS

PLOT_CATEGORIZED_KWARGS_ALIASES = dict(
    {
        alias: resolved_name
        for alias, resolved_name
        in core.CATEGORIZED_LEGEND_KWARGS_ALIASES.items()
        if resolved_name in CL_ALIASED_ARGS
    },
    category_getter='categorizer',
)

# ************************* Helper functions ************************* #

place_legend_optimally = legend_placement.avoid_figure_elements
get_grid_dimensions = utils.get_grid_dimensions

_is_single_arg_callable = tjekmate.takes_positional_args(1)
_is_optional_keywords = utils.is_keyword_mapping(optional=True)
_is_optional_keywords_or_bool = (
    _is_optional_keywords | tjekmate.is_strict_boolean
)
_is_none_or_identifier = tjekmate.identical_to(None) | utils.is_identifier


def _check_membership_strict_boolean(
    obj: typing.Any,
    items: typing.Collection[typing.Union[typing.Hashable, bool]],
) -> bool:
    """
    Membership check with strict constraints on booleans.

    Example
    -------
    >>> values = 'x', 'y', 'xy', 'none', None, True, False
    >>> [
    ...     _check_membership_strict_boolean(obj, values)
    ...     for obj in [0, 1, 'foo', *values]
    ... ]
    [False, False, False, True, True, True, True, True, True, True]
    """
    non_bool_items, bool_items = set(), set()
    for item in items:
        (
            bool_items
            if tjekmate.is_strict_boolean(item) else
            non_bool_items
        ).add(item)
    try:
        if obj in non_bool_items:
            return True
    except Exception:
        return False
    try:
        if obj in bool_items:
            return tjekmate.is_strict_boolean(obj)
    except Exception:
        return False
    return False


@tjekmate.check(
    aliases=tjekmate.is_mapping(
        key_checker=utils.is_identifier, value_checker=utils.is_identifier,
    ),
)
def _resolve_aliases(
    aliases: typing.Mapping[Keyword, Keyword],
) -> typing.Callable[[typing.Callable[..., T]], typing.Callable[..., T]]:
    """
    Convenience decorator for using
    `<CL_MODULE>.core.resolve_cl_aliased_args()`.

    Parameters
    ----------
    aliases
        Mapping from argument-name aliases to actual argument names

    Example
    -------
    >>> @_resolve_aliases(dict(c='b'))
    ... def foo(a: int, b: int = 5) -> int:
    ...     return a + b
    ...
    >>> foo(1, c=2)
    3
    >>> foo(2)
    7

    Notes
    -----
    - As opposed to `[...].resolve_cl_aliased_args()`, no aliases nor
      defaults are implicitly supplied in this decorator.

    - Users are responsible for documenting the use of aliases
      themselves;
      this decorator doesn't attempt to recompute the function signature
      nor manipulates the function docs of the decorated function.

    - No checks are done for loops at the moment and only single-step
      resolution is attempted, i.e. if `aliases = {'b': 'c', 'a': 'b'}`
      then `a` doesn't resolve to `c`.
      This may change in the future since name aliasing seems to be a
      common usecase within the repo and it may be worth refactoring all
      the snippets into a central utility subpackage.
    """
    def resolve_aliases(
        func: typing.Callable[..., T],
    ) -> typing.Callable[..., T]:
        sig = inspect.signature(func)
        defaults = {
            param.name: param.default for param in sig.parameters.values()
            if param.default is not param.empty
        }

        @functools.wraps(func)
        def arg_resolving_wrapper(*args, **kwargs):
            # Use a partial bind to resolve the positionals
            # Don't pass the kwargs to the signature yet, or it may trip
            # on aliases (unexpected argument ...)
            partial_ba = sig.bind_partial(*args)
            # Resolve aliases on the kwargs
            kwargs = core.resolve_cl_aliased_args(
                kwargs,
                defaults=defaults,
                resolve=aliases,
                update_defaults=False,
                update_resolve=False,
            )
            return func(*partial_ba.args, **kwargs)

        return arg_resolving_wrapper

    # Bookkeeping
    aliases = dict(aliases)
    aliases_by_targets: typing.Dict[Keyword, typing.Set[Keyword]] = {}
    for alias, unaliased in aliases.items():
        aliases_by_targets.setdefault(unaliased, set()).add(alias)
    # Prep docstring for the returned decorator
    if aliases:
        aliases_repr = '\n'.join(
            '{} -> {}'.format(', '.join(sorted(source_aliases)), unaliased)
            for unaliased, source_aliases in sorted(aliases_by_targets.items())
        )
    else:
        aliases_repr = '<No aliases defined>'
    resolve_aliases.__doc__ = textwrap.dedent("""
    Decorator adding aliases to function arguments.
    Aliases to add
    --------------
    {}
    """).format(aliases_repr)
    return resolve_aliases


def _force_render(fig: Figure, hard: bool = True) -> None:
    """
    Force objects in the figure to be rendered so that their positions
    are consolidated, etc.
    This is required for some manipulations in older matplotlib
    versions.

    Parameters
    ----------
    fig
        Figure
    hard = True
        Force rendering by saving to a physical tempfile
    hard = False
        Force rendering by manually invoking `fig.draw()`

    Side effects
    ------------
    Figure plotted to a .png file in a tempdir, which is promptly
    deleted (if `hard = True`)

    TODO
    ----
    Test if it suffices to do soft rendering
    """
    if not hard:
        fig.draw(fig.canvas.get_renderer())
        return
    with TemporaryDirectory() as tempdir:
        fig.savefig(os.path.join(tempdir, 'force_render.png'))


def _unify_axis_limits(
    axes_objs: typing.Iterable[Axes], axis: typing.Literal['x', 'y'],
) -> None:
    """
    Unify the axis limits of the `Axes` objects.

    Parameters
    ----------
    axes_objs
        `matplotlib.axes.Axes` objects
    axis
        'x' or 'y'

    Side effects
    ------------
    Axis limits unified along `axis` among the supplied `axes_objs`

    Notes
    -----
    Legacy method;
    use instead the `share{x|y}` arguments at axes creation time
    """
    axes_objs = list(axes_objs)
    if not axes_objs:
        return
    all_bounds = [
        value for ax in axes_objs for value in getattr(ax, f'get_{axis}lim')()
    ]
    lbound, ubound = (f(all_bounds) for f in (min, max))
    # Note: here we assume all the axes objects have compatible locators
    # May not be true in general but good enough for this kind of plots
    first_ax, *_ = axes_objs
    first_axis = getattr(first_ax, axis + 'axis')
    lbound, ubound = first_axis.get_major_locator().view_limits(lbound, ubound)
    for ax in axes_objs:
        getattr(ax, f'set_{axis}lim')(lbound, ubound)


def _make_dummy_axes_with_labels(
    fig: Figure,
    axes_objs: typing.Iterable[Axes],
    gridspec: GridSpec,
    xlabel: typing.Optional[str] = None,
    ylabel: typing.Optional[str] = None,
) -> Axes:
    """
    Create a dummy `Axes` object encompassing the entire grid spec with
    everything set invisible beside the labels.

    Parameters
    ----------
    fig
        `matplotlib.figure.Figure` object
    axes_objs
        `matplotlib.axes.Axes` objects
    gridspec
        `matplotlib.figure.GridSpec` object
    xlabel, ylabel
        Optional string axis labels;
        at least one of them must be provided

    Return
    ------
    Created dummy `Axes`
    """
    if not any(label is not None for label in (xlabel, ylabel)):
        raise TypeError(
            f'xlabel = {xlabel!r}, ylabel = {ylabel!r}: cannot both be none'
        )
    axes_objs: typing.List[Axes] = list(axes_objs)
    min_zorder = min(ax.get_zorder() for ax in axes_objs)
    dummy = fig.add_subplot(
        gridspec[:, :],
        zorder=min_zorder - 1,  # Place beneath other axes
    )
    # Hide plot elements
    for obj in (dummy.patch, *dummy.spines.values()):
        obj.set_visible(False)
    # Force tick labels to be consolidated
    _force_render(fig)
    for axis, label in dict(x=xlabel, y=ylabel).items():
        dummy_axis = getattr(dummy, axis + 'axis')
        if label is None:
            dummy_axis.set_ticks([])
            continue
        # Plot tick labels but make them transparent for spacing
        all_subplot_tick_labels = {
            tick_label.get_text()
            for ax in axes_objs
            for tick_label in getattr(ax, f'get_{axis}ticklabels')()
        }
        if all_subplot_tick_labels:
            n = len(all_subplot_tick_labels)
            getattr(dummy, f'set_{axis}lim')(0, n)
            dummy_axis.set_ticks(range(n))
            for tick_line in dummy_axis.get_ticklines():
                tick_line.set_alpha(0)
            dummy_axis.set_ticklabels(all_subplot_tick_labels, alpha=0)
        # Caveat: dummy_axis.set_label(label) doesn't work
        getattr(dummy, f'set_{axis}label')(label)
    return dummy


_UniqueItem = typing.TypeVar('_UniqueItem')
_HashedUniqueItem = typing.TypeVar('_HashedUniqueItem', bound=typing.Hashable)


@profile
def _resolve_ultimate_dependence(
    dependences: typing.Collection[typing.Tuple[_UniqueItem, _UniqueItem]],
    hasher: typing.Callable[[_UniqueItem], _HashedUniqueItem] = id,
) -> typing.List[typing.Tuple[_UniqueItem, _UniqueItem]]:
    """
    Determine the ultimate dependences between the items.

    Parameters
    ----------
    dependences
        Collection of two-tuples of items;
        each tuple is to be read as "`items[0]` is directly dependent
        on `items[1]`"
    hasher()
        Function uniquely identifying the items by mapping them to
        hashables

    Return
    ------
    List of resolved ultimate dependences

    Example
    -------

    >>> deps = [
    ...     ('A', 'B'),  # A -> B
    ...     ('C', 'D'),  # C -> D
    ...     ('B', 'C'),  # B -> C
    ...     ('E', 'F'),  # E -> F
    ... ]
    >>> sorted(
    ...     _resolve_ultimate_dependence(deps, lambda string: string)
    ... )
    [('A', 'D'), ('B', 'D'), ('C', 'D'), ('E', 'F')]

    Notes
    -----
    Legacy method for determining which `Axes` depends on which, so that
    only the axis limits of the most-upstream one is updated via
    callback when a downstream `Axes` object is updated by the user.
    Since now we use `share{x|y}` at axes creation, this is not needed.
    """
    # Tabulate the items and their direct dependences
    objects: typing.Dict[_HashedUniqueItem, _UniqueItem] = (
        {hasher(item): item for item_pair in dependences for item in item_pair}
    )
    direct_dependences: typing.Dict[_HashedUniqueItem, _HashedUniqueItem] = {}
    for item_dep, item_indep in dependences:
        id_dep, id_indep = hasher(item_dep), hasher(item_indep)
        if not (
            id_dep in direct_dependences and
            direct_dependences[id_dep] != id_indep
        ):
            direct_dependences[id_dep] = id_indep
            continue
        raise RuntimeError(
            (
                '{!r} cannot depend on both {!r} and {!r}'
            ).format(
                item_dep,
                objects[direct_dependences[id_dep]],
                item_indep,
            )
        )
    # Recursively resolve to the ultimate dependences
    ultimate_dependences: typing.Dict[
        _HashedUniqueItem, _HashedUniqueItem
    ] = {}
    for dependent, dependee in direct_dependences.items():
        if dependent in ultimate_dependences:  # Already resolved
            continue
        path: typing.Set[_HashedUniqueItem] = {dependent}
        while True:
            if dependee in path:
                raise RuntimeError(
                    'Loop detected: '
                    f'the dependence path of {objects[dependee]!r} '
                    'loops back on itself'
                )
            elif dependee in ultimate_dependences:
                # Ultimate dependence known from this point
                dependee = ultimate_dependences[dependee]
                break
            elif dependee in direct_dependences:
                # Continue to resolve upstream
                path.add(dependee)
                dependee = direct_dependences[dependee]
            else:
                # Arrived at independent axes object
                break
        ultimate_dependences.update(dict.fromkeys(path, dependee))
    # Sanity checks
    # All depedencies are resolved
    assert set(ultimate_dependences) == set(direct_dependences)
    # Destinations are not dependent on anything else
    assert not (set(ultimate_dependences.values()) & set(ultimate_dependences))
    return [
        (objects[id_dep], objects[id_indep])
        for id_dep, id_indep in ultimate_dependences.items()
    ]


@profile
def _set_axis_limit_update_hooks(
    dependences: typing.Collection[typing.Tuple[Axes, Axes]],
    axis: typing.Literal['x', 'y'],
) -> None:
    """
    Set up callback hooks for updating the axis limits of independent
    `Axes` objects when those of their dependents are changed.

    Parameters
    ----------
    dependences
        Collection of two-tuples of `Axes` objects;
        each tuple is to be read as "`ax_tup[0]` is ultimately dependent
        on `ax_tup[1]`"
    axis
        The axis ('x' or 'y') for which to set the callbacks

    Side effects
    ------------
    For each `Axes` object dependent on another for its `axis`-axis
    limits, a callback is added to the `axis + 'lim_changed'` event so
    that any change is back-propagated to the `Axes` object it depends
    on.

    Caveats
    -------
    - Kludgy fix for how the `<CL_MODULE>.Axes` wrapper seems to have
      botched the axis-limit sharing between subplots;
      fix the `Axes` class later.

    - Once "hard" limits are set for the dependent `Axes` object (e.g.
      via `.set_{x|y}lim()` or by dragging the GUI viewing window), the
      change is also reflected on the independent `Axes` object;
      this disables auto-scaling on both objects.

    Notes
    -----
    Legacy method;
    no longer used since child objects of the axes and figure wrappers
    are now properly referenced to the wrappers, not the inner wrapped
    objects
    """
    def update_axlims(
        ax_dep: Axes, ax_indep: Axes, axis: typing.Literal['x', 'y'],
    ) -> None:
        get_autoscale, set_autoscale, get_axlim, set_axlim = (
            getattr(ax, pattern.format(get_set, axis))
            for pattern in ('{}_autoscale{}_on', '{}_{}lim')
            for ax, get_set in ((ax_dep, 'get'), (ax_indep, 'set'))
        )
        if get_autoscale():
            # Change due to a change in data lim
            kwargs = {'scale' + xy: xy == axis for xy in 'xy'}
            set_autoscale(True)
            ax_indep.autoscale_view(**kwargs)
        else:
            # Change due to explicit set_{x|y}lim(), window dragging,
            # etc.
            set_axlim(get_axlim())

    for ax_dep, ax_indep in dependences:
        callback = (
            functools.partial(update_axlims, ax_indep=ax_indep, axis=axis)
        )
        ax_dep.callbacks.connect(axis + 'lim_changed', callback)


# TODO: should we remove the `fig` parameter? It isn't used anywhere
@profile
def _drop_interstitial_tick_labels(
    fig: Figure,
    subplots: typing.Iterable[Axes],
    axis: typing.Literal['x', 'y'],
) -> None:
    """
    Remove all axis tick labels sandwiched between two subplots.

    Parameters
    ----------
    fig
        `matplotlib.figure.Figure` object
    subplots
        `matplotlib.axes._subplots.AxesSubplot` objects;
        must be created from the same `matplotlib.Figure.GridSpec`
        object
    axis
        'x' or 'y'

    Side effects
    ------------
    For each `axis`-axis of a subplot in `subplots`, if the subplot has
    an immediate neighbor sandwiching the axis ticks with it, the tick
    labels are dropped.
    """
    # Helper functions for getting the label positions
    def get_axes_fraction(text: Text, ax: Axes) -> np.ndarray:
        to_axes_fraction = text.get_transform() + ax.transAxes.inverted()
        return to_axes_fraction.transform(text.get_position())

    def check_mean_label_position(
        ax: Axes,
        xy: typing.Literal['x', 'y'],
        comparator: typing.Callable[[numbers.Real], bool],
        minor: bool = False,
    ) -> bool:
        """
        Parameters
        ----------
        ax
            Axes
        xy
            Axis of labels
        comparator()
            Callable returning for the mean position in axes-fraction
            coordinates a number

        Return
        ------
        Return value of `comparator()`
        """
        # Check x-axis labels for their y- (vertical) coordinates, and
        # vice-versa
        xy_index = dict(x=1, y=0)[xy]
        get_ticklabels = getattr(ax, f'get_{xy}ticklabels')
        mean_position = np.mean([
            get_axes_fraction(label, ax)[xy_index]
            for label in get_ticklabels(minor=minor)
        ])
        return comparator(mean_position)

    labels_have_smaller_grid_indices_than_ax: typing.Callable[
        [Axes, bool], bool
    ]
    if axis == 'x':
        labels_have_smaller_grid_indices_than_ax = functools.partial(
            check_mean_label_position,
            xy=axis,
            # Since grid indices start from the top left, a bigger
            # y-coord means a smaller index
            comparator=tjekmate.gt(.5),
        )
        label_directions = 'labelbottom', 'labeltop',
    elif axis == 'y':
        labels_have_smaller_grid_indices_than_ax = functools.partial(
            check_mean_label_position,
            xy=axis,
            # Since grid indices start from the top left, a smaller
            # x-coord means a smaller index
            comparator=tjekmate.lt(.5),
        )
        label_directions = 'labelleft', 'labelright',
    else:
        raise TypeError(f'axis = {axis!r}: expected \'x\' or \'y\'')
    # Map out where the subplots are
    subplots = list(subplots)
    grid_occupancies = utils.get_gridspec_occupancies(subplots)
    nrows, ncols = grid_occupancies.shape
    # Check neighborhood of each subplot
    axis_objs_to_drop_labels_on: typing.Dict[
        typing.Literal['major', 'minor'], typing.List[Axis]
    ] = {}
    for (ax_sub, axis_obj), (ticker, is_minor) in itertools.product(
        ((ax, getattr(ax, axis + 'axis')) for ax in subplots),
        (('major', False), ('minor', True)),
    ):
        nlabels = len(axis_obj.get_ticklabels(minor=is_minor))
        if not nlabels:
            continue
        i_start, i_end, j_start, j_end = utils.get_subplot_geometry(ax_sub)
        # Where are its tick labels?
        labels_are_before_ax = labels_have_smaller_grid_indices_than_ax(
            ax_sub, minor=is_minor
        )
        # Are they sandwiched between ax_sub and a neighbor?
        if labels_are_before_ax and axis == 'x':  # Check i_start - 1
            if i_start == 0:  # Grid edge
                is_sandwiched = False
            else:
                is_sandwiched = grid_occupancies[
                    i_start - 1, j_start:(j_end + 1)
                ].any()
        elif labels_are_before_ax:  # y-axis -> check j_start - 1
            if j_start == 0:
                is_sandwiched = False
            else:
                is_sandwiched = grid_occupancies[
                    i_start:(i_end + 1), j_start - 1
                ].any()
        elif axis == 'x':
            # Labels after ax in grid ind. -> check i_end + 1
            if i_end == nrows - 1:
                is_sandwiched = False
            else:
                is_sandwiched = grid_occupancies[
                    i_end + 1, j_start:(j_end + 1)
                ].any()
        else:  # y-axis -> check j_end + 1
            if j_end == ncols - 1:
                is_sandwiched = False
            else:
                is_sandwiched = grid_occupancies[
                    i_start:(i_end + 1), j_end + 1
                ].any()
        # Unset labels if sandwiched
        if not is_sandwiched:
            continue
        # Remember which axis objects to operate on
        axis_objs_to_drop_labels_on.setdefault(ticker, []).append(axis_obj)
    # Suppress labels
    for ticker, axis_objs in axis_objs_to_drop_labels_on.items():
        for axis_obj in axis_objs:
            axis_obj.set_tick_params(
                which=ticker,
                **dict.fromkeys(label_directions, False),
            )


@profile
@tjekmate.add_annotations(
    # Note: pyflakes misinterprets the tabulated type hints accessed by
    # string-literal keys as nonexistent forward refs (see issue #373);
    # minimal example:
    # ```shell
    # $ {
    # >     echo "hints = dict(a=float)"
    # >     echo "def foo(a: hints['a']) -> None: pass"
    # > } | flake8 -
    # stdin:2:18: F821 undefined name 'a'
    # ```
    # To circumvent this, just use a decorator to apply the type
    # annotations
    **CATEGORIZED_LEGEND_TYPE_HINTS,
)
def _label_subplots(
    subplots_map: typing.Mapping[
        typing.Tuple[
            typing.Tuple[
                CategoryIdentifier,
                typing.Union[CategoryValue, PlenaryLabel],
                bool,
            ],
            ...
        ],
        CLA
    ],
    label_placer: typing.Callable[[Axes], typing.Tuple[_AnnotateXY, Keywords]],
    labeller: AnnotateAlignedLike = aligned_annotations.annotate_aligned,
    label_subplots: OptionalKeywords = None,
    show_category_names=CATEGORIZED_LEGEND_DEFAULTS['show_category_names'],
    category_name_typesetter=None,
    category_value_typesetters=None,
) -> None:
    """
    Draw the appropriate label (annotations) for each `Axes` object.

    Parameters
    ----------
    subplots_map
        Mapping of the format

        >>> Mapping[  # noqa # doctest: +SKIP
        ...     tuple[
        ...         tuple[category_name, category_value, is_plenary], ...
        ...     ],
        ...     ax
        ... ]

    label_placer()
        Function taking the subplot and returning

        >>> (xy, kwargs),  # noqa # doctest: +SKIP

        where `xy` are the coordinates to place the annotation and
        `kwargs` are keyword--value pairs to supply to `labeller()`;
        in case of clashes, `kwargs` are overridden by `label_subplots`
    labeller()
        Function taking the `Axes` object, text string, and a coordinate
        pair (plus optional keyword arguments) and returning an
        annotation (like `Axes.annotate()`)
    category_values
        Mapping of the format

        >>> Mapping[  # noqa # doctest: +SKIP
        ...     category_name, Sequence[category_value]
        ... ]

    label_subplots
        Optional keyword arguments for `labeller()`;
        if not provided, the function is a no-op.
    show_category_names
        Whether to show the category names in the subplot labels;
        can also be a mapping from specific category names to booleans
    category_name_typesetter
        Optional function, mapping or format string for typesetting
        category names
    category_value_typesetters
        Optional mapping of the format

        >>> Mapping[category_name, typesetter],  # noqa # doctest: +SKIP

        where typesetter can be a function, mapping, or format string
        for typesetting category values

    Side effects
    ------------
    Label drawn for each `Axes` object in `subplots_map.values()`
    """
    if label_subplots is None:  # Subplots not labeled
        return

    CatNameOrValue = typing.TypeVar(
        'CatNameOrValue', CategoryIdentifier, CategoryValue,
    )
    TwoFieldFormatString = typing.TypeVar(
        'TwoFieldFormatString', bound=_types.FormatString,
    )

    # Helper funcs
    def apply_typesetter(
        typesetter: typing.Union[
            typing.Callable[[CatNameOrValue], str],
            typing.Mapping[CatNameOrValue, str],
            str,
            typing.Any,
        ],
        obj: CatNameOrValue,
    ) -> typing.Union[str, CatNameOrValue]:
        if callable(typesetter):
            return typesetter(obj)
        if isinstance(typesetter, typing.Mapping):
            return typesetter.get(obj, obj)
        if isinstance(typesetter, str):
            return typesetter.format(obj)
        return obj  # Failback

    def get_typesetter(
        category_name: CategoryIdentifier,
    ) -> typing.Callable[[CategoryValue], typing.Union[str, CategoryValue]]:
        typesetter = (category_value_typesetters or {}).get(category_name)
        return functools.partial(apply_typesetter, typesetter)

    def get_format_string(
        category_name: CategoryIdentifier,
    ) -> TwoFieldFormatString:
        if isinstance(show_category_names, typing.Mapping):
            show = show_category_names.get(
                category_name,
                CATEGORIZED_LEGEND_DEFAULTS['show_category_names'],
            )
        else:
            show = show_category_names
        if show:  # Include category names
            return '{0}={1}'
        return '{1}'  # Only format the value

    def get_annotation_text(
        category_name: CategoryIdentifier, category_value: CategoryValue,
    ) -> str:
        format_str = get_format_string(category_name)
        typeset_cat_name = apply_typesetter(
            category_name_typesetter, category_name,
        )
        typeset_cat_value = get_typesetter(category_name)(category_value)
        return format_str.format(typeset_cat_name, typeset_cat_value)

    for categorization, ax in subplots_map.items():
        xy, kwargs = label_placer(ax)
        kwargs = dict(kwargs, **label_subplots)
        if all(is_plenary for *_, is_plenary in categorization):
            # Global plenary subplot: use the given label
            (_, subplot_label, _), *_ = categorization
        else:  # Format the non-plenary categories
            subplot_label = ', '.join(
                get_annotation_text(cat_name, cat_value)
                for cat_name, cat_value, is_plenary in categorization
                if not is_plenary
            )
        labeller(ax, subplot_label, xy, **kwargs)


@profile
def _draw_categorized_legend(
    fig: CLF,
    subplots: typing.Iterable[CLA],
    *,
    loc: typing.Optional[
        typing.Union[str, typing.Tuple[numbers.Real, numbers.Real], ]
    ] = None,
    bbox_to_anchor: typing.Optional[typing.Union[
        transforms.BboxBase,
        typing.Tuple[numbers.Real, numbers.Real],
        typing.Tuple[numbers.Real, numbers.Real, numbers.Real, numbers.Real],
    ]] = None,
    bbox_transform: typing.Optional[transforms.Transform] = None,
    **kwargs
) -> Legend:
    """
    Wrapper layer over `fig.categorized_legend()` intercepting certain
    call signatures and acting accordingly.

    Parameters
    ----------
    fig
        `<CL_MODULE>.Figure`
    subplots
        Iterable of `<CL_MODULE>.Axes` which the placed legend is to
        avoid
    loc, bbox_to_anchor, bbox_transform
        `matplotlib.figure.Figure.legend()` arguments
    min_height, min_width, min_area
        `<CL_MODULE>.plot.place_legend_optimally()`
        arguments
    **kwargs
        `matplotlib.figure.Figure.legend()` arguments

    Return
    ------
    `matplotlib.legend.Legend`

    Notes
    -----
    - This function implements `loc = 'best'` for figures if neither of
      `bbox_to_anchor` nor `bbox_transform` has a value supplied by
      using `<CL_MODULE>.plot.place_legend_optimally()`
      to place the legend away from the subplots.
    """
    def remove_legend(fig: CLF, legend: Legend) -> None:
        """
        Remove a legend.
        """
        try:
            legend.remove()
        except Exception:
            pass
        # Note: legend.remove() unreliable on CategorizedLegendFigures
        try:
            fig.legends.remove(legend)
        except ValueError:  # Already removed
            pass

    def check_gridspec(
        fig: CLF, subplots: typing.Collection[CLA],
    ) -> typing.Union[GridSpec, None]:
        """
        Return
        ------
        If grid has vacancies
            GridSpec
        Else
            None
        """
        vacancies = ~utils.get_gridspec_occupancies(subplots)
        if not vacancies.any():  # No vacancies
            return None
        ax = next(iter(subplots))
        return ax.get_gridspec()

    def get_recommended_alignment(
        bounds: Bbox, bbox_to_anchor: Bbox, tolerance: numbers.Real = 1E-6,
    ) -> str:
        """
        Parameters
        ----------
        bounds
            Bbox in fig-fraction coordinates
        bbox_to_anchor
            Bbox inside `bounds` to which the legend is to be anchored,
            in fig-fraction coordinates
        tolerance
            Maximum distance of an edge of `bbox_to_anchor` can be from
            the corresponding edge of `bounds` within which the legend
            is snapped to the edge

        Return
        ------
        String for `loc`
        """
        alignments: typing.List[str] = []  # y, x
        for axis, lo, hi in ('y', 'lower', 'upper'), ('x', 'left', 'right'):
            ba_lh: typing.Tuple[
                numbers.Real, numbers.Real, numbers.Real, numbers.Real,
            ] = tuple(
                getattr(box, axis + suffix)
                for box in (bounds, bbox_to_anchor) for suffix in ('min', 'max')
            )
            lo_on_edge, hi_on_edge = (
                abs(ba_lh[offset] - ba_lh[offset + 2]) <= tolerance
                for offset in (0, 1)
            )
            if not (lo_on_edge ^ hi_on_edge):
                # Touching both edges or none
                alignments.append('center')
            elif lo_on_edge:
                alignments.append(lo)
            else:
                alignments.append(hi)
        if all(a == 'center' for a in alignments):
            return 'center'
        else:
            return ' '.join(alignments)

    # *********************** Manage arguments *********************** #

    calculate_best = (
        all(arg is None for arg in (bbox_to_anchor, bbox_transform)) and
        isinstance(loc, str) and
        loc == 'best'
    )
    if calculate_best:
        loc = None
    # Roll everything back into kwargs
    kwargs = dict(kwargs)
    for varname in 'loc', 'bbox_to_anchor', 'bbox_transform':
        value = locals()[varname]
        if value is not None:
            kwargs[varname] = value
    # Extract place_legend_optimally() arguments
    minimize_overlap_kwargs: typing.Dict[Keyword, typing.Any] = {}
    for varname in 'min_height', 'min_width', 'min_area':
        if varname in kwargs:
            minimize_overlap_kwargs[varname] = kwargs.pop(varname)

    # **************** Get measurements from a legend **************** #

    # Get a legend
    legend = fig.categorized_legend(**kwargs)
    # Early exit
    if not calculate_best:
        return legend

    # *************** Update kwargs and replot legend **************** #

    subplots = list(subplots)
    # Try fitting the legend into the vacancies in the gridspec
    gridspec = check_gridspec(fig, subplots)
    legend_fits_in_grid = False
    if gridspec is not None:
        legend_fits_in_grid, more_kwargs = place_legend_optimally(
            fig, subplots,
            bounds=gridspec, legend=legend, **minimize_overlap_kwargs,
        )
        # Snap to edge if it fits
        if legend_fits_in_grid and 'bbox_to_anchor' in more_kwargs:
            gridspec_bounds = gridspec[:, :].get_position(fig)
            more_kwargs.update(
                loc=get_recommended_alignment(
                    gridspec_bounds, more_kwargs['bbox_to_anchor'],
                ),
                borderaxespad=0,
            )
    # Doesn't fit within the gridspec, try finding a fit on the whole
    # canvas
    if not legend_fits_in_grid:
        _, more_kwargs = place_legend_optimally(
            fig, subplots, legend=legend, **minimize_overlap_kwargs,
        )
    remove_legend(fig, legend)
    return fig.categorized_legend(**dict(more_kwargs, **kwargs))


def _check_cl_method(method: Keyword) -> bool:
    """
    Check if `method` is a name for which a CL recipe is registered.

    Notes
    -----
    This is in principle equivalent to

    >>> (  # noqa # doctest: +SKIP
    ...     utils.is_identifier
    ...     & tjekmate.is_in(recipes.get_cl_recipes())
    ... )(method),

    but using the above form directly in the
    `@tjekmate.check(method=...)` operator would freeze the list of
    permitted recipes to the ones available at the initialization time
    of this submodule.
    """
    if not utils.is_identifier(method):
        return False
    return method in recipes.get_cl_recipes()


# ********************** Main plotting function ********************** #


@typing.overload
def plot_categorized(
    *args,
    split_x: CategoryIdentifier, split_y: CategoryIdentifier,
    split_xy: None = None,
    **kwargs
) -> typing.Dict[typing.Tuple[CategoryValue, CategoryValue], CLA]:
    ...


@typing.overload
def plot_categorized(
    *args,
    split_x: None = None, split_y: None = None, split_xy: None = None,
    **kwargs
) -> CLA:
    ...


@typing.overload
def plot_categorized(
    *args,
    split_x: None = None,
    split_y: CategoryIdentifier,
    split_xy: None = None,
    **kwargs
) -> typing.Dict[CategoryValue, CLA]:
    ...


@typing.overload
def plot_categorized(
    *args,
    split_x: CategoryIdentifier,
    split_y: None = None,
    split_xy: None = None,
    **kwargs
) -> typing.Dict[CategoryValue, CLA]:
    ...


@typing.overload
def plot_categorized(
    *args,
    split_x: None = None, split_y: None = None,
    split_xy: AllowedSplitXY,
    **kwargs
) -> typing.Dict[CategoryValue, CLA]:
    ...


@profile
@_resolve_aliases(PLOT_CATEGORIZED_KWARGS_ALIASES)
@tjekmate.type_checked(
    checkers=dict(
        data_getters=tjekmate.is_sequence,
        categorizer=_is_single_arg_callable,
        formatter=callable,
        split_x=_is_none_or_identifier,
        split_y=_is_none_or_identifier,
        split_xy=(
            _is_none_or_identifier
            | tjekmate.items_fulfill(
                utils.is_identifier,
                tjekmate.is_positive_integer,
                tjekmate.is_positive_integer,
            )
        ),
        plenary_label=tjekmate.is_instance(str),
        tile_from=tjekmate.is_in(ALLOWED_TILINGS),
        category_value_sorters=(
            tjekmate.identical_to(None)
            | utils.is_keyword_mapping(
                value_checker=(
                    _is_single_arg_callable
                    | tjekmate.is_instance(typing.Mapping)
                ),
            )
        ),
        unify_axlims=(
            functools.partial(
                _check_membership_strict_boolean,
                items=ALLOWED_AXES_DESIGNATIONS,
            )
            | tjekmate.is_mapping(
                key_checker=tjekmate.is_in(('x', 'y')),
                value_checker=functools.partial(
                    _check_membership_strict_boolean,
                    items=ALLOWED_SHARE_XY_VALUES,
                ),
            )
        ),
        drop_ticklabels=functools.partial(
            _check_membership_strict_boolean, items=ALLOWED_AXES_DESIGNATIONS,
        ),
        gridspec_kwargs=_is_optional_keywords,
        add_gridspec_method=(
            tjekmate.identical_to(None)
            | tjekmate.takes_positional_args(3)
        ),
        label_subplots=_is_optional_keywords_or_bool,
        categorized_legend=_is_optional_keywords_or_bool,
        xlabel=(
            tjekmate.identical_to(None) | tjekmate.is_instance(str)
        ),
        ylabel=(
            tjekmate.identical_to(None) | tjekmate.is_instance(str)
        ),
        method=_check_cl_method,
        annotate_method=tjekmate.takes_positional_args(3),
    ),
    annotations={
        param_name: (
            lambda ann: typing.Optional[ann] if make_optional else ann
        )(CATEGORIZED_LEGEND_TYPE_HINTS[param_name])
        for param_name, make_optional in dict(
            category_value_sorters=False,
            show_category_names=True,
            category_name_typesetter=False,
            category_value_typesetters=False,
        ).items()
    },
)
def plot_categorized(
    rows: typing.Iterable[Row],
    data_getters: typing.Sequence[
        typing.Union[typing.Callable[[Row], PlottableData], typing.Any]
    ],
    categorizer: typing.Callable[[Row], Categorization],
    formatter: typing.Callable[
        ...,
        typing.Union[Keywords, typing.Sequence[Keywords]]
    ],
    *,
    split_x: typing.Optional[CategoryIdentifier] = None,
    split_y: typing.Optional[CategoryIdentifier] = None,
    split_xy: typing.Optional[AllowedSplitXY] = None,
    plenary_subplot: typing.Optional[
        typing.Union[
            AllowedPlenary,
            typing.Mapping[CategoryIdentifier, AllowedPlenary],
        ]
    ] = None,
    plenary_label: PlenaryLabel = '(all data)',
    tile_from: AllowedTilings = ('left', 'top'),
    category_value_sorters=None,
    gridspec_kwargs: OptionalKeywords = None,
    add_gridspec_method: typing.Optional[FigureAddGridSpecLike] = None,
    unify_axlims: typing.Union[
        AllowedAxesDesignations,
        typing.Mapping[_AllowedAxesDesignations, AllowedShareXYValue],
    ] = True,
    drop_ticklabels: AllowedAxesDesignations = True,
    label_subplots: OptionalKeywordsWithBoolShorthand = None,
    show_category_names=None,
    category_name_typesetter=None,
    category_value_typesetters=None,
    xlabel: typing.Optional[str] = None,
    ylabel: typing.Optional[str] = None,
    hide_split_categories: typing.Optional[bool] = None,
    categorized_legend: OptionalKeywordsWithBoolShorthand = None,
    fig: typing.Optional[typing.Union[Figure, Axes]] = None,
    method: Keyword = 'plot',
    annotate_method: AnnotateAlignedLike = aligned_annotations.annotate_aligned,
    **kwargs
) -> PlotCategorizedReturnValue:
    """
    Categorize and plot the data, optionally splitting it into subplots.

    Parameters (basic)
    ------------------
    rows
        Iterable of "data rows"
    data_getters
        Accessors for data, which when used on a "data row" result in
        the positional arguments for the plotting method (e.g. the x-
        and y-axis data);
        if single-argument callables, use as accessors on the "data
        row":

        >>> def call_plotting_method(rows):
        ...     for row in rows:
        ...         method_args = (acc(row) for acc in data_getters)
        ...         getattr(ax, method)(*method_args, ...);

        else, use for item access:

        >>> def call_plotting_method(rows):
        ...     for row in rows:
        ...         method_args = (row[key] for key in data_getters)
        ...         getattr(ax, method)(*method_args, ...);

    categorizer() (alias: category_getter())
        Callable taking a "data row" and returning a mapping of
        string-identifier category keys (CKAs;
        see section "CATEGORIZED KEYWORD ARGUMENTS" in
        `<CL_MODULE>.__doc__`)
        to their values; e.g.:

        >>> def categorizer(row):  # "foo" and "bar" are CKAs
        ...     return {
        ...         col: getattr(row, col, None)
        ...         for col in ('foo', 'bar')
        ...     }

        categorizes a "data row" according to its `.foo` and `.bar`
        attributes
    formatter()
        Data-formatter callable mapping CKAs to a plot-keyword--value
        mapping, or to a sequence of such mappings; e.g.:

        >>> from typing import Union
        >>>
        >>> def formatter(
        ...     *,
        ...     foo: Union[float, None] = None,
        ...     bar: Union[int, None] = None,
        ... ) -> typing.Union[typing.Sequence[Keywords], Keywords]:
        ...     return [
        ...         # Plot this first
        ...         dict(
        ...             linestyle='none',
        ...             marker='o',
        ...             markersize=foo,
        ...             color='black',
        ...         ),
        ...         # Then this
        ...         dict(
        ...             linestyle='none',
        ...             marker='+',
        ...             markersize=foo,
        ...             color=(
        ...                 'white'
        ...                 if bar is None else
        ...                 dict(a='r', b='g', c='b')[bar]
        ...             ),
        ...         ),
        ...     ]

        defines `foo` and `bar` to be CKAs and formats the marker of a
        data point according to the values thereof;
        see section "DATA FORMATTERS" in
        `<CL_MODULE>.__doc__`

    Parameters (sub-plotting)
    -------------------------
    split_x, split_y
        Optional category name at which to split the "data rows" up into
        subplots along each direction; e.g.

        >>> split_x, split_y = 'foo', 'bar'

        puts "data rows" categorized to be of each `foo` category on a
        different column of subplots, and those of each `bar` category
        on a different row; and

        >>> split_x, split_y = None, 'foo'

        puts "data rows" of each `foo` category on a different row of
        subplots, but leave those of different `bar` categories in the
        same subplots;
        cannot be used together with `split_xy`
    split_xy
        Optional category name at which to split the "data rows" up into
        subplots on a 2D grid, like when both `split_x` and `split_y`
        are used, but with the splitting done solely based on the value
        of said category;
        can also be a tuple of form

        >>> (category_name, nrows, ncols)  # noqa # doctest: +SKIP

        where

        >>> (  # noqa # doctest: +SKIP
        ...     nrows * ncols >= len(unique_split_xy_cat_values)
        ... );

        cannot be used together with `split_{x|y}` in any case
    plenary_subplot
        Whether or how to include extra plenary subplot(s) including all
        the data points (without the split as specified by
        `split_{x|y|xy}`):
        'before' or True
            Put the plenary subplot(s) before the split subplots
        'after'
            Put the plenary subplot(s) after the split subplots
        'none' or None or False (default)
            Don't make plenary subplot(s)
        alternatively, it can also be a mapping from category names to
        such values, designating whether or how plenary subplots are to
        be made for each of the splits;
        note that this only matters when `split_{x|y|xy}` is used, and
        when more than one categorized values are found in the `rows`
        for the category(-ies) for which split subplots are to be made
    plenary_label
        String label for the plenary subplot(s);
        must not coincide with the value `categorizer()` assigns to any
        of the categories used for splitting, i.e.

        >>> plenary_label not in {  # noqa # doctest: +SKIP
        ...     categorizer(row)[category]
        ...     for category in (split_x, split_y, split_xy)
        ...     if plenary_subplot.get(category)
        ...     for row in rows
        ... }

    tile_from
        Point on the grid from which tiling of the split subplots should
        start;
        examples:
        ('left', 'top') (default)
            Subplots tiled from left-to-right, then top-to-bottom (i.e.
            rows filled first)
        ('top', 'left')
            Subplots tiled from top-to-bottom, then left-to-right (i.e.
            columns filled first)
        'right' -> ('right', 'top')
            Subplots tiled from right-to-left, then top-to-bottom;
        note that the order between the directions only matters when
        `split_xy` is used
    category_value_sorters (alias: label_sorters)
        Optional mapping from category names to sorter callables and/or
        mappings for their values, affecting how subplots (if
        `split_{x|y|xy}` are used) and legend handles (if
        `categorized_legend` is used) are ordered;
        e.g.:

        >>> category_value_sorters = dict(
        ...     foo=lambda x: -x,
        ...     bar=dict(c=0, a=1, b=2),
        ... )

        sorts values of the category `foo` in descending order, and
        those of the category `bar` in the order 'c' -> 'a' -> 'b'
    gridspec_kwargs
        Optional keyword--value mapping (i.e. "keywords") for creating
        the grid spec with `add_gridspec_method()`
    unify_axlims
        Whether to set the axis limits on subplots (if more than one) to
        the same values after plotting all the data:
        True or 'xy'
            Unify limits on both axes
        None or False or 'none'
            No-op
        'x', 'y'
            Only unify x-axis (resp. y-axis) limits
        {'x': share_x, 'y': share_y}
            Unify said axis limits with the appropriate `share_{x|y}`
            settings (see
            `matplotlib.figure.Figure.subplots(share{x|y}=...)`)
    drop_ticklabels
        Whether to drop the axis tick labels sandwiched between subplots
        of unified limits (require `unify_axlims` to be true for said
        axis, at least on a per-column (resp. -row) basis for the x-
        (resp. y-) axis):
        True or 'xy'
            Drop interstitial labels on both axes
        None or False or 'none'
            No-op
        'x', 'y'
            Only drop x-axis (resp. y-axis) tick labels

    Parameters (subplot labelling)
    ------------------------------
    label_subplots
        Optional keywords for labelling subplots (if any) with
        `annotate_method()`;
        if provided, draw on each subplot an appropriate label when
        split;
        said label has the format

        >>> (  # noqa # doctest: +SKIP
        ...     f'{category_name}={category_value}'
        ... ),

        where each are respectively typeset by
        `category_name_typesetter` and `category_value_typesetters`,
        where available;
        if however `show_category_names = False`, the label is
        simplified to just `category_value`;
        can also be `True` or `False`, which are shorthards for resp.
        `{}` (i.e. label with no extra keyword arguments) and `None`
        (i.e. no labelling)
    show_category_names (alias: show_categories_with_labels)
        Whether to show the category names in the subplot labels;
        can also be a mapping from specific category names to booleans
    category_name_typesetter (alias: category_typesetter)
        Optional callable, formating string, or mapping, which converts
        a category name to its typeset form
    category_value_typesetters (alias: label_typesetters)
        Optional mapping of category names to their respective callable,
        formatting string, or mapping, which each converts a category
        value to its typeset form

    Parameters (others)
    -------------------
    xlabel, ylabel
        Optional string axis labels;
        if provided, they are added to the figure
    hide_split_categories
        Whether to hide the categories used for sub-plotting (via
        `split_x` and/or `split_y`) from `formatter()`, not using them
        for formatting the data marker/line, which may help with
        reducing visual clutter;
        default is to do so only if subplots are labeled, i.e. when
        `label_subplots` is provided;
        note that split categories are not hidden in the plenary
        subplot(s)
    categorized_legend
        Optional keywords;
        if provided, they are provided to

        >>> (  # noqa # doctest: +SKIP
        ...     <CL_MODULE>
        ...     .Figure.categorized_legend()
        ... )

        to make a categorized legend based on the values of each
        category encountered;
        can also be `True` or `False`, which are shorthands for resp.
        `{}` (i.e. construct the legend with no extra keyword arguments)
        and `None` (i.e. no legend)
    fig
        Optional figure or axes to plot on;
        matplotlib.figure.Figure
            Plot on the figure
        matplotlib.axes.Axes
            Plot on its `.figure`
        <not provided>
            Plot on a new figure (via `matplotlib.pyplot.figure()`)
    method
        Name of the `matplotlib.axes.Axes` method (or any other method
        registered as a recipe with
        `<CL_MODULE>.register_cl_recipe()`)
        to use to plot the data;
        defaults to 'plot' (i.e. call `Axes.cl_plot()`, which is a
        wrapper around `Axes.plot()`)
    add_gridspec_method()
        Optional unbound-method callable with a signature compatible
        with `Figure.add_gridspec()`, taking the figure, the grid
        dimensions and the `gridspec_kwargs` and returning a
        `matplotlib.gridspec.GridSpec` object;
        defaults to `type(fig).add_gridspec()`
    annotate_method()
        Unbound-method callable with a signature compatible with
        `<TOP_MODULE>.aligned_annotations.annotate_aligned()`
        (also the default), taking the axes object, the annotation text,
        the position specification, and `label_subplots` keyword
        arguments (among others), and returning the appropriate
        `matplotlib.text.Annotation` object
    **kwargs
        Fed to

        >>> (  # noqa # doctest: +SKIP
        ...     <CL_MODULE>
        ...     .Axes.cl_<method>()
        ... )

        a wrapper around `matplotlib.Axes.<method>()` which takes CKAs
        (and other keyword arguments) and generates the suitable keyword
        arguments with `formatter()`;
        the result of `categorizer()` over each "data row" takes
        precedence over these in case of conflicts

    Return
    ------
    If `split_{x|y}` both used in subplot splitting:

        >>> (  # noqa # doctest: +SKIP
        ...     wrapped_figure,
        ...     dict[tuple[cka_y_value, cka_x_value], wrapped_axes],
        ... )

    If any single one of `split_{x|y|xy}` used in subplot splitting:

        >>> (  # noqa # doctest: +SKIP
        ...     wrapped_figure,
        ...     dict[cka_value, wrapped_axes],
        ... )

    If no subplot splitting is done:

        >>> wrapped_figure, wrapped_axes  # noqa # doctest: +SKIP

    where:
    wrapped_figure
        `<CL_MODULE>.Figure`
        on which everything is drawn
    cka_[x_,y_]value
        Value of the categorized keyword argument over which subplots
        are split, or possibly `plenary_label` if a plenary subplot is
        to be made for said category
    wrapped_axes
        `<CL_MODULE>.Axes`
        on which data points are plotted

    Notes
    -----
    - Specifying `split_x, split_y, split_xy = cat, cat, None` is
      equivalent to `split_x, split_y, split_xy = None, None, cat`.

    - If the dimensions of the grid is not given by `split_xy` (via its
      tuple form), they are resolved by

      >>> resolver = (  # noqa # doctest: +SKIP
      ...     <CL_MODULE>
      ...     .utils.get_grid_dimensions
      ... )
      >>> nrows, ncols = resolver(  # noqa # doctest: +SKIP
      ...     size=len(unique_split_xy_cat_values),
      ...     aspect=fig.get_figwidth() / fig.get_figheight(),
      ... ).

    - If not otherwise specified in `categorized_legend`, the
      aforementioned argument--alias pairs (e.g.
      `category_value_sorters` <-> `label_sorters`) are passed to
      `fig.categorized_legend()` via the aliases.

    - If `categorized_legend['loc'] = 'best'` and neither of
      `bbox_to_anchor` nor `bbox_transform` is provided in
      `categorized_legend`, the function tries to optimize the position
      of the drawn legend (see Optimized Legend Placement).

    - The default placement of the subplot labels uses the
      `<TOP_MODULE>.aa.annotate_aligned()`
      arguments `xy='top left', outer='y'`, which are not part of the
      standard call signature of `Axes.annotate()`;
      the argument `xy` is, but it cannot be a string.
      Hence, user-supplied values of `annotate_method()` should at least
      be compatible with these arguments, in addition to those in the
      standard `Axes.annotate()` call signature.

    Optimized Legend Placement
    --------------------------
    (See
    `<CL_MODULE>.plot.place_legend_optimally()`
    for the optimizing function.)

    - A basic legend is drawn for an estimate of the legend dimensions.

    - The bounds of the entirety of the `matplotlib.gridspec.GridSpec`
      on which the subplots are arranged are retrieved.

    - If the `GridSpec` is not fully occupied:
          The optimizing function is used to find the optimal legend
          placement (bounding box, alignment, etc.) within the
          aformentioned bounds, minimizing overlaps with the subplots;
          if an appropriate bounding box is found and it touches said
          bounds, the legend is snapped to the touching edge(s).

    - Else, or if no such bounding box is found within the `GridSpec`
      bounds:
          The optimizing function is used to find the optimal legend
          placement within the entire figure minimizing overlaps with
          the subplots.

    - The legend is then redrawn with such placement.

    See also
    --------
    `<CL_MODULE>.__doc__`
    `<CL_MODULE>.Figure.categorized_legend()`
    `<TOP_MODULE>.aligned_annotations.annotate_aligned()`
    """
    T1, T2 = (typing.TypeVar(t) for t in ('T1', 'T2'))

    # Helper functions
    def getitem(obj, index):
        # Note: `operator.getitem` doesn't work with keyword arguments,
        # so we need to manually define this here
        return obj[index]

    def resolve_with_mapping(
        value: T1, resolver: typing.Mapping[typing.Hashable, T2],
    ) -> typing.Union[T1, T2]:
        return resolver.get(value, value)

    # *********************** Input wrangling ************************ #

    # data_getters
    data_getters = list(data_getters)
    for i, getter in enumerate(data_getters):
        if callable(getter):
            try:
                inspect.signature(getter).bind(None)
            except Exception:
                msg = (
                    f'data_getters[{i}] = {getter!r}: '
                    'cannot verify that it is a single-argument callable; '
                    'to be used in item access on rows, instead of being '
                    'called on them'
                )
                warn(msg)
            else:
                continue
        data_getters[i] = functools.partial(getitem, index=getter)
    data_getters: typing.List[typing.Callable[[Row], PlottableData]]
    # (no need to check formatter() explicitly now, we will work on it
    # later)
    # (split_x, split_y, split_xy) -> (splits, nrows_ncols)
    if (split_x == split_y is not None) and (split_xy is None):
        # This used to be an error, but there isn't a good reason to not
        # allow it;
        # Just take split_x = split_y to be an alias for split_xy = ...
        split_x, split_y, split_xy = None, None, split_x
    nrows_ncols: typing.Union[
        GridPoint, typing.Tuple[None, None],
    ] = [None, None]
    if not _is_none_or_identifier(split_xy):  # category, nrols, ncols
        split_xy, nrows_ncols[0], nrows_ncols[1] = split_xy
    if split_xy and (split_x or split_y):
        raise ValueError(
            f'split_x = {split_x!r}, split_y = {split_y!r}, '
            f'split_xy = {split_xy!r}: '
            '`split_xy` cannot be used with `split_{x|y}`'
        )
    splits: SplitDict = dict(x=split_x, y=split_y, xy=split_xy)
    # plenary_subplot
    resolve_plenary_mode = functools.partial(
        resolve_with_mapping, resolver=PLENARY_MODES,
    )
    is_plenary_value = functools.partial(
        _check_membership_strict_boolean, items=ALLOWED_PLENARY_VALUES,
    )
    if is_plenary_value(plenary_subplot):
        plenary_subplot = dict.fromkeys(
            (category for category in splits.values() if category),
            resolve_plenary_mode(plenary_subplot),
        )
    if (
        isinstance(plenary_subplot, typing.Mapping) and
        all(
            is_plenary_value(plenary_subplot.get(category))
            for category in splits.values() if category
        )
    ):
        plenary_subplot = {
            category: resolve_plenary_mode(plenary_subplot.get(category))
            for category in splits.values() if category
        }
        assert all(
            value in ('before', 'after', None)
            for value in plenary_subplot.values()
        )
        plenary_subplot: typing.Dict[
            CategoryIdentifier, typing.Literal['before', 'after']
        ] = {
            category: mode
            for category, mode in plenary_subplot.items() if mode
        }
    else:
        raise TypeError(
            f'plenary_subplot = {plenary_subplot!r}: '
            f'expected any of {ALLOWED_PLENARY_VALUES!r}, or a mapping from '
            'category names (as provided in `split_{x|y|xy}`) to the '
            'aforementioned'
        )
    # tile_from
    tile_from = ALLOWED_TILINGS[tile_from]
    # category_value_sorters
    if category_value_sorters is None:
        category_value_sorters = {}
    category_value_sorters = dict(category_value_sorters)
    for category, sorter in category_value_sorters.items():
        if isinstance(sorter, typing.Mapping):
            category_value_sorters[category] = sorter.get
    # unify_axlims -> (unify_axlims, unify_axlims_resolved)
    unified: UnifyAxLimDict = dict(x=False, y=False)
    if isinstance(unify_axlims, typing.Mapping):
        default = (
            inspect.signature(plot_categorized)
            .parameters['unify_axlims'].default
        )
        for axis in 'xy':
            value = unify_axlims.get(axis, default)
            unified[axis] = AXIS_SHARING_MODES.get(value, value)
    elif unify_axlims in (None, 'none', False):
        unified['x'] = unified['y'] = False
    elif unify_axlims in ('xy', True):
        unified['x'] = unified['y'] = True
    else:
        assert unify_axlims in ('x', 'y')
        unified[unify_axlims] = True
    # drop_ticklabels -> (drop_ticklabels, drop_ticklabels_resolved)
    drop_labels: DropTickLabelDict = dict(x=False, y=False)
    if drop_ticklabels in (None, 'none', False):
        drop_labels['x'] = drop_labels['y'] = False
    elif drop_ticklabels in ('xy', True):
        drop_labels['x'] = drop_labels['y'] = True
    else:
        assert drop_ticklabels in ('x', 'y')
        drop_labels[drop_ticklabels] = True
    # show_category_names
    if show_category_names is None:  # Resolve to default value
        show_category_names = CATEGORIZED_LEGEND_DEFAULTS['show_category_names']
    # gridspec_kwargs, label_subplots, categorized_legend
    if gridspec_kwargs is None:
        gridspec_kwargs = {}
    if label_subplots in (True, False):
        label_subplots = {} if label_subplots else None
    if categorized_legend in (True, False):
        categorized_legend = {} if categorized_legend else None
    # fig
    if isinstance(fig, Axes):
        fig = fig.figure
    if fig is None:
        # Note: keep track of the figures created from within this
        # function, and `plt.close()` them should things go south
        fig, fig_is_created = plt.figure(), True
    else:
        fig_is_created = False
    if not isinstance(fig, Figure):
        raise TypeError(
            f'fig = {fig!r}: expected a `{Figure.__module__}.Figure`'
        )

    try:
        # add_gridspec_method
        bound_add_gridspec: BoundFigureAddGridSpecLike
        if add_gridspec_method is None:
            bound_add_gridspec = fig.add_gridspec
        else:
            bound_add_gridspec = functools.partial(add_gridspec_method, fig)

        # ******* Actual work (offloaded to another function) ******** #

        return _plot_categorized(
            rows=rows,
            data_getters=data_getters,
            categorizer=categorizer,
            formatter=formatter,
            splits=splits,
            nrows_ncols=nrows_ncols,
            plenary_subplot=plenary_subplot,
            plenary_label=plenary_label,
            tile_from=tile_from,
            category_value_sorters=category_value_sorters,
            gridspec_kwargs=gridspec_kwargs,
            bound_add_gridspec_method=bound_add_gridspec,
            unify_axlims=unify_axlims,
            unify_axlims_resolved=unified,
            drop_ticklabels=drop_ticklabels,
            drop_ticklabels_resolved=drop_labels,
            label_subplots=label_subplots,
            show_category_names=show_category_names,
            category_name_typesetter=category_name_typesetter,
            category_value_typesetters=category_value_typesetters,
            xlabel=xlabel,
            ylabel=ylabel,
            hide_split_categories=hide_split_categories,
            categorized_legend=categorized_legend,
            fig=fig,
            method=method,
            annotate_method=annotate_method,
            kwargs=kwargs,
        )
    except Exception:  # Cleanup
        if fig_is_created:
            plt.close(fig)
        raise


@tjekmate.add_annotations(
    **dict(
        {
            param.name: param.annotation
            for param in inspect.signature(plot_categorized).parameters.values()
            if param.annotation is not param.empty
        },
        show_category_names=(
            CATEGORIZED_LEGEND_TYPE_HINTS['show_category_names']
        ),
    ),
)
def _plot_categorized(
    rows,
    data_getters: typing.Sequence[typing.Callable[[Row], PlottableData]],
    categorizer,
    formatter,
    # These two are from `split_{x|y|xy}`
    splits: SplitDict,
    nrows_ncols: typing.Union[GridPoint, typing.Tuple[None, None]],
    plenary_subplot: typing.Mapping[
        CategoryIdentifier, typing.Literal['before', 'after'],
    ],
    plenary_label,
    tile_from,
    category_value_sorters,
    gridspec_kwargs: Keywords,
    bound_add_gridspec_method: BoundFigureAddGridSpecLike,
    # These two are from `unify_axlims`
    unify_axlims,
    unify_axlims_resolved: UnifyAxLimDict,
    # These two are from `drop_ticklabels`
    drop_ticklabels,
    drop_ticklabels_resolved: DropTickLabelDict,
    label_subplots: OptionalKeywords,
    show_category_names,
    category_name_typesetter, category_value_typesetters,
    xlabel, ylabel,
    hide_split_categories,
    categorized_legend: OptionalKeywords,
    fig: Figure,
    method,
    annotate_method,
    # Note: because of the sheer number of arguments which are to be
    # communicated between the pre-processor and this method, it is
    # unsafe and error-prone to allow this method to take arbitrary
    # arguments;
    # hence we've made `kwargs` a single positional argument
    kwargs: Keywords,
) -> PlotCategorizedReturnValue:
    """
    Method acutally doing the plotting after the pre-processing of the
    data.
    """
    Member = typing.TypeVar('Member', bound=typing.Hashable)
    Cat2IndicesFunc = typing.Callable[
        # Type hint for functions taking a categorization
        # (dict[category_name, category_value]) and returning a pair of
        # grid indices
        [Categorization], GridPoint
    ]

    def get_power_set_iter(
        hashables: typing.Iterable[Member],
    ) -> typing.Iterable[typing.Set[Member]]:
        hashables = set(hashables)
        return (
            subset for n in range(len(hashables) + 1)
            for subset in itertools.combinations(hashables, n)
        )

    def get_index_of_category(
        categorization: Categorization, category_name: CategoryIdentifier,
    ) -> numbers.Integral:
        return category_values[category_name].index(
            categorization.get(category_name, EMPTY),
        )

    def flip_grid_indices(
        func: typing.Optional[Cat2IndicesFunc] = None,
        axes: typing.Union[
            typing.Literal[0, 1], typing.Collection[typing.Literal[0, 1]],
        ] = (0, 1),
    ) -> typing.Union[
        Cat2IndicesFunc, typing.Callable[[Cat2IndicesFunc], Cat2IndicesFunc],
    ]:
        if func is None:
            return functools.partial(flip_grid_indices, axes=axes)
        try:
            iter(axes)
        except TypeError:
            axes = axes,
        # Note: the index pairs are always in the (r, c) [i.e. (y, x)]
        # layout
        invert = [i in axes for i in (1, 0)]

        @functools.wraps(func)
        def func_with_inverted_indices(*args, **kwargs):
            result = func(*args, **kwargs)
            inverted = np.array(nrows_ncols) - 1 - result
            return tuple(np.where(invert, inverted, result))

        return func_with_inverted_indices

    def make_single_split_grid_mapper(
        split_xy: CategoryIdentifier, fill_row_first: bool,
    ) -> Cat2IndicesFunc:
        def map_single_split(categorization: Categorization) -> GridPoint:
            # Note: we can't pre-compute nrows_ncols[nrnc_index] outside
            # because when the function maker is called nrows_ncols are
            # not necessarily defined, so leave it till actual use
            divisor = nrows_ncols[nrnc_index]
            index = get_index_of_category(categorization, split_xy)
            return divmod(index, divisor)[::step]

        if fill_row_first:  # Mod with ncols
            nrnc_index, step = 1, 1
            step = 1
        else:  # Mod with nrows and transpose the grid
            nrnc_index, step = 0, -1

        return map_single_split

    def make_double_split_grid_mapper(
        split_x: typing.Union[CategoryIdentifier, None],
        split_y: typing.Union[CategoryIdentifier, None],
    ) -> Cat2IndicesFunc:
        def map_double_split(categorization: Categorization) -> GridPoint:
            return tuple(
                get_index_of_category(categorization, split) if split else 0
                for split in (split_y, split_x)
            )

        return map_double_split

    def place_label(
        ax: Axes, xy: _AnnotateXY, **kwargs
    ) -> typing.Tuple[_AnnotateXY, Keywords]:
        """
        For now, just echo the position and the passed kwargs
        """
        return xy, kwargs

    def clean_kwargs(
        kwargs: Keywords, drop: typing.Collection[Keyword],
    ) -> Keywords:
        return {k: v for k, v in kwargs.items() if k not in drop}

    # **************** Gather CKAs and plotting data ***************** #

    category_values: typing.Dict[
        CategoryIdentifier, typing.Set[CategoryValue]
    ] = {}
    categorization_results: typing.List[Categorization] = []
    arg_lists: typing.List[
        typing.List[PlottableData]
    ] = [[] for _ in data_getters]
    for row in rows:
        categorization, *_ = cat_args = tuple(
            func(row) for func in (categorizer, *data_getters)
        )
        if not utils.is_keyword_mapping(categorization):
            raise TypeError(
                f'categorizer = {categorizer!r}: '
                'expected a callable returning a mapping from '
                'categorized keyword arguments to hashable values, got '
                f'{categorization!r} on the row \n{row!r}'
            )
        for category_name in splits.values():
            if not category_name:
                continue
            category_values.setdefault(category_name, set()).add(
                categorization.get(category_name, EMPTY)
            )
        for aggregator, value in zip(
            (categorization_results, *arg_lists), cat_args
        ):
            aggregator.append(value)
    if arg_lists and not arg_lists[0]:
        raise RuntimeError(f'rows = {rows!r}: No data')
    # If there is only one value for a category, no need to make another
    # plenary subplot
    for category in list(plenary_subplot):
        nvalues = len(category_values.get(category, ()))
        if nvalues > 1:
            continue
        if not nvalues:
            del plenary_subplot[category]
            continue
        msg = (
            'split_{0} = {1!r}, plenary_subplot[{1!r}] = {2!r}: '
            'not making an additional plenary subplot for the category '
            'since only one value is encountered in the provided data'
        ).format(
            next(xy for xy, cat in splits.items() if cat == category),
            category,
            plenary_subplot[category],
        )
        warn(msg)
    # Check whether plenary_label is in a category which is used for
    # plot-splitting
    category_values_splitted_on: typing.Dict[
        CategoryIdentifier, typing.Set[CategoryValue]
    ] = {
        category: values
        for category, values in category_values.items()
        if category in splits.values()
    }
    if (
        plenary_subplot and
        any(
            plenary_label in values
            for values in category_values_splitted_on.values()
        )
    ):
        offending_category = next(
            category
            for category, values in category_values_splitted_on.items()
            if plenary_label in values
        )
        offending_split = next(
            split for split, category in splits.items()
            if category == offending_category
        )
        msg = (
            'split_{} = {!r}, plenary_subplot = {!r}, plenary_label = {!r}: '
            '`plenary_label` coincides with the categorization of some rows '
            'in a category used for subplot-splitting'
        ).format(
            offending_split,
            offending_category,
            plenary_subplot,
            plenary_label,
        )
        raise RuntimeError(msg)
    # Sort the values in each category, and insert the plenary-plot
    # label in the appropriate position
    for category, values in category_values.items():
        sorter = category_value_sorters.get(category)
        try:
            values = tuple(sorted(values, key=sorter))
        except Exception:
            values = tuple(values)
        plenary_placement = plenary_subplot.get(category)
        if plenary_placement == 'before':
            values = (plenary_label, *values)
        elif plenary_placement == 'after':
            values = (*values, plenary_label)
        category_values[category] = values
    category_values: typing.Dict[
        CategoryIdentifier,
        typing.Tuple[typing.Union[CategoryValue, PlenaryLabel], ...]
    ]

    # * Bookkeeping with formatter(), method, and categorized_legend * #

    # Hide unused CKAs from formatter() where appropriate
    split_categories = {cat for cat in splits.values() if cat}
    non_plenary_split_categories = split_categories - set(plenary_subplot)
    if hide_split_categories is None:
        hide_split_categories = label_subplots is not None
    if hide_split_categories and non_plenary_split_categories:
        def keep_param(param: inspect.Parameter) -> bool:
            if param.kind is param.VAR_KEYWORD:  # Allow var keywords
                return True
            return (  # Allow keywords which aren't on the ban list
                param.name not in non_plenary_split_categories and
                param.kind is param.KEYWORD_ONLY
            )

        sig = inspect.signature(CLF._check_formatter(formatter))
        new_sig = sig.replace(
            parameters=tuple(
                param for param in sig.parameters.values() if keep_param(param)
            ),
        )
        # Twofold protection:
        # - Filter the incoming kwargs so that the hidden CKAs are not
        #   passed (`clean_kwargs()`)
        # - Wrap the signature of `formatter()` so that the CL method
        #   doesn't register the args to be filtered as CKAs to begin
        #   with
        formatter = copy_func(formatter)
        formatter.__signature__ = new_sig
    # Set up CL method in figure
    fig = CLF(fig)
    fig.register_cl_method('cl_' + method, formatter)
    # categorized_legend (finalization)
    if categorized_legend is not None:
        # Low priority: use parameters provided to plot_categorized()
        cl_kwargs = (
            lambda namespace:
            {varname: namespace[varname] for varname in CL_ALIASED_ARGS}
        )(locals())
        cl_kwargs.update(
            # Mid priority: categorized_legend keywords
            core.resolve_cl_aliased_args(categorized_legend),
            # High priority: delimit what goes into the drawn legend
            cl_method=('cl_' + method),
            formatter=formatter,
        )
        categorized_legend = cl_kwargs

    # ***************** Distribute data to subplots ****************** #

    # Create gridspec and mapper from categorization to grid indices
    cat2indices: Cat2IndicesFunc
    if splits['xy']:  # From split_xy
        cat2indices = make_single_split_grid_mapper(
            splits['xy'],
            # Whether mapper is to tile rows or columns first
            fill_row_first=(tile_from[0] in HORIZONTAL_TILINGS),
        )
        nplots = len(category_values[splits['xy']])
        if splits['xy'] not in category_values:
            raise ValueError(
                f'split_xy = {splits["xy"]!r}: '
                'not a category (i.e. key in return value of '
                '`categorizer()`)'
            )
        if nrows_ncols == [None, None]:
            nrows_ncols = utils.get_grid_dimensions(
                size=nplots,
                aspect=(fig.get_figwidth() / fig.get_figheight()),
            )
        elif nrows_ncols[0] * nrows_ncols[1] < nplots:
            raise ValueError(
                f'split_xy = {(splits["xy"],) + tuple(nrows_ncols)!r}: '
                'not enough room for {nplots} subplots'
            )
    else:  # From split_{x|y}
        cat2indices = make_double_split_grid_mapper(splits['x'], splits['y'])
        for i, axis in enumerate('yx'):
            if splits[axis] is None:
                nrows_ncols[i] = 1
                continue
            if splits[axis] in category_values:
                nrows_ncols[i] = len(category_values[splits[axis]])
                continue
            raise ValueError(
                f'split_{axis} = {splits[axis]!r}: '
                'not a category (i.e. key in return value of '
                '`categorizer()`)'
            )
    # Invert mapping tiling directions if needed
    if 'right' in tile_from:
        cat2indices = flip_grid_indices(cat2indices, axes=0)
    if 'bottom' in tile_from:
        cat2indices = flip_grid_indices(cat2indices, axes=1)
    gridspec = bound_add_gridspec_method(*nrows_ncols, **gridspec_kwargs)
    # Spawn subplots in order so as to recover the correct z-order
    # (This HELPS (but not ensures) interstitial axis tick labels to not
    # be covered by surounding subplots)
    subplot_assignments: typing.List[
        typing.Dict[
            typing.FrozenSet[CategoryIdentifier], GridPoint,
        ]
    ] = [
        {
            # Replace one or more of the categorization values with
            # plenary_label in the split categories if plenary subplots
            # are to be made for them;
            # the base (non-plenary) case is covered by the empty subset
            frozenset(plenary_categories): cat2indices(
                {**cat, **dict.fromkeys(plenary_categories, plenary_label)}
            )
            for plenary_categories in get_power_set_iter(plenary_subplot)
        }
        for cat in categorization_results
    ]
    subplots: typing.Dict[GridPoint, CLA] = {}
    row_occupancies: typing.Dict[numbers.Integral, typing.List[CLA]] = {}
    col_occupancies: typing.Dict[numbers.Integral, typing.List[CLA]] = {}
    for i_row, i_col in sorted({
        indices for locations in subplot_assignments
        for indices in locations.values()
    }):
        row_axes = row_occupancies.setdefault(i_row, [])
        col_axes = col_occupancies.setdefault(i_col, [])
        # Figure out axis sharing
        axis_sharing: typing.Dict[Keyword, CLA] = {}
        axes_objs: typing.Collection[CLA]
        for axis, unify_mode in unify_axlims_resolved.items():
            if unify_mode == 'row':
                # Sharing = 'row' -> share with row leader
                axes_objs = row_axes
            elif unify_mode == 'col':
                # Sharing = 'col' -> share with column leader
                axes_objs = col_axes
            elif unify_mode:
                # Sharing = True -> share with first axes object
                axes_objs = subplots.values()
            else:
                continue
            if axes_objs:
                axis_sharing['share' + axis], *_ = axes_objs
        ax = fig.add_subplot(gridspec[i_row, i_col], **axis_sharing)
        subplots[i_row, i_col] = ax
        row_axes.append(ax)
        col_axes.append(ax)
    # Plot each data point to the appropriate subplot(s)
    for categorization, plenary_categories, indices, *pos_args in (
        (categorization, plenary_categories, indices, *pos_args)
        for categorization, locations, *pos_args in zip(
            categorization_results, subplot_assignments, *arg_lists,
        )
        for plenary_categories, indices in locations.items()
    ):
        # Notes:
        # - Non-plenary split categories are dropped from the formatter
        #   so passing them to a CL method will be an error;
        #   so we have to filter them with clean_kwargs()
        # - Split CKAs should be preserved in plenary subplots and
        #   dropped in non-plenary ones
        plot = getattr(subplots[indices], 'cl_' + method)
        if hide_split_categories:
            dropped_categories = (
                non_plenary_split_categories |
                (set(plenary_subplot) - plenary_categories)
            )
        else:
            dropped_categories = {}
        plot_kwargs = (
            dict(kwargs, **clean_kwargs(categorization, dropped_categories))
        )
        plot(*pos_args, **plot_kwargs)

    # *********************** Post-processing ************************ #

    # Plot labels
    if (
        len(subplots) > 1 and
        not all(label is None for label in (xlabel, ylabel))
    ):
        # >1 subplots: make dummy enclosing axis and add axis labels
        # there
        _make_dummy_axes_with_labels(
            fig, subplots.values(), gridspec, xlabel, ylabel,
        )
    elif len(subplots) == 1:  # 1 subplot: just add axis labels here
        ax, = subplots.values()
        for axis, label in dict(x=xlabel, y=ylabel).items():
            if label is None:
                continue
            getattr(ax, f'set_{axis}label')(label)
    # Remove tick labels in the middle if the bounds are unified
    for axis, partial_unify_mode in zip('xy', ('col', 'row')):
        if not drop_ticklabels_resolved[axis]:
            continue
        elif unify_axlims_resolved[axis] in (True, partial_unify_mode):
            # Drop labels
            _drop_interstitial_tick_labels(fig, subplots.values(), axis)
            continue
        msg = (
            f'unify_axlims = {unify_axlims!r} -> {unify_axlims_resolved!r}, '
            f'drop_ticklabels = {drop_ticklabels!r} '
            f'-> {drop_ticklabels_resolved!r}: '
            f'{axis}-axis tick labels not dropped because the subplot '
            'axis limits are not unified'
        )
        warn(msg)

    # ****************** Label subplots and return ******************* #

    axes: typing.Union[
        CLA,
        typing.Mapping[typing.Union[CategoryValue, PlenaryLabel], CLA],
        typing.Mapping[
            typing.Tuple[
                typing.Union[CategoryValue, PlenaryLabel],
                typing.Union[CategoryValue, PlenaryLabel],
            ],
            CLA
        ],
    ] = {}
    subplots_map: typing.Mapping[
        typing.Tuple[
            typing.Union[
                typing.Tuple[
                    CategoryIdentifier, CategoryValue, typing.Literal[True],
                ],
                typing.Tuple[
                    CategoryIdentifier, PlenaryLabel, typing.Literal[False],
                ],
            ],
            ...
        ],
        CLA
    ] = {}
    cat_names: typing.Tuple[CategoryIdentifier, ...]
    seen_categorizations: typing.Set[
        typing.Tuple[typing.Tuple[CategoryIdentifier, CategoryValue], ...]
    ] = set()
    if all(sp is None for sp in splits.values()):
        # No split -> no labels
        axes, = subplots.values()
        if categorized_legend is not None:
            _draw_categorized_legend(fig, (axes,), **categorized_legend)
        return fig, axes
    if sum(1 for sp in splits.values() if sp) == 1:
        # Split by one category
        cat_names = next(sp for sp in splits.values() if sp),
    else:  # Split by two categories
        cat_names = (splits['y'], splits['x'])
    cat_as_tuples: typing.Generator[
        typing.Tuple[typing.Tuple[CategoryIdentifier, CategoryValue], ...],
        None,
        None
    ] = (
        tuple((name, categorization.get(name, EMPTY)) for name in cat_names)
        for categorization in categorization_results
    )
    # We already have a mapping from indices to axes, now construct
    # mappings from categorizations to axes
    for cat_tuples, locations in zip(cat_as_tuples, subplot_assignments):
        if cat_tuples in seen_categorizations:
            continue
        seen_categorizations.add(cat_tuples)
        for plenary_categories, indices in locations.items():
            resolved_cat_tuples = tuple(
                (
                    (cat_name, plenary_label, True)
                    if cat_name in plenary_categories else
                    (cat_name, cat_value, False)
                )
                for cat_name, cat_value in cat_tuples
            )
            axes_key = tuple(
                category_value for _, category_value, _ in resolved_cat_tuples
            )
            if len(axes_key) == 1:
                axes_key, = axes_key
            ax = subplots[indices]
            axes[axes_key] = subplots_map[resolved_cat_tuples] = ax
    # Legend-placing strategy
    if label_subplots is not None:
        label_subplots = dict(label_subplots)
        label_loc = label_subplots.pop('xy', 'top left')
        label_placer = functools.partial(place_label, xy=label_loc, outer='y')
        _label_subplots(
            subplots_map=subplots_map,
            label_placer=label_placer,
            labeller=annotate_method,
            label_subplots=label_subplots,
            show_category_names=show_category_names,
            category_name_typesetter=category_name_typesetter,
            category_value_typesetters=category_value_typesetters,
        )
    # Draw categorized legend (taking into account the subplot labels)
    if categorized_legend is not None:
        _draw_categorized_legend(fig, axes.values(), **categorized_legend)
    return fig, axes


plot_categorized.__doc__ = plot_categorized.__doc__.replace(
    '<CL_MODULE>', core.CategorizedLegendFigure._get_cl_module_name(),
)
plot_categorized.__doc__ = plot_categorized.__doc__.replace(
    '<TOP_MODULE>',
    '.'.join(
        core.CategorizedLegendFigure._get_cl_module_name().split('.')[:-1]
    ),
)
