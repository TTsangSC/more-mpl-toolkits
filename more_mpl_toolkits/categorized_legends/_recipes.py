"""
Recipe class for creating legend handles
"""
import inspect
import types
import typing
import warnings

from matplotlib.axes import Axes

from . import utils, _types

__all__ = ('CategorizedLegendRecipe',)

_LegendHandlePrecursor = typing.TypeVar('_LegendHandlePrecursor')
_LegendHandleRetriever = typing.Callable[
    [_LegendHandlePrecursor], _types.LegendHandle
]
_RecipeAxesMethod = typing.Callable[[Axes], _LegendHandlePrecursor]
_RecipeMethod = typing.Union[_types.Keyword, _RecipeAxesMethod]


class CategorizedLegendRecipe:
    """
    Recipe class for generating the handles to be put on a CATEGORIZED
    LEGEND (CL) from `matplotlib.axes.Axes` methods;
    also doubling as a registry for CL-compatible `matplotlib.axes.Axes`
    methods.

    Attributes
    ----------
    method()
        Callable acting on an `Axes` object and returning a legend
        handle
    *args, **kwargs
        Minimal arguments to be supplied to `.method()` after the first
        argument (the `Axes`) for generating such a handle
    indexing
        Optional indexing with which the handle can be retrieved from
        the return value of `.method()`;
        if `None`, no indexing is done (default);
        if a list, index iteratively through each of its items
    postprocess()
        Optional function further applied on the object retrieved after
        the indexing (if any), whose return value is used as the legend
        handle
    version (readonly)
        Number indicating the version of the recipe
    registered_name (readonly)
        Name of the recipe (if registered)

    Notes
    -----
    Not intended to be instantiated nor manipulated directly by end-
    users; use instead
    `<MODULE>.register_cl_recipe()`
    """
    def __new__(cls, method: _RecipeAxesMethod, *args, **kwargs) -> typing.Self:
        """
        For each method only one instance is created;
        see `.__init__()` for details.

        Notes
        -----
        Calling `cls.__new__(method)` resets any existing associated
        instance;
        to prevent overwriting existing data, use `.get(method)`.
        """
        if utils.is_identifier(method):
            method_name, method_is_func = method, False
        elif callable(method):
            method_name, method_is_func = method.__name__, True
            if not utils.is_identifier(method_name):
                raise TypeError(
                    f'method = {method!r}: '
                    f'.__name__ = {method_name!r} not a valid identifier'
                )
        else:
            raise TypeError(
                f'method = {method!r}: '
                'expected a string identifier or a callable'
            )
        method_is_ax_method = (
            callable(getattr(Axes, method_name, None)) and
            hasattr(getattr(Axes, method_name), '__get__')
        )
        if method_is_func or method_is_ax_method:
            if method_name not in cls._valid_instances:
                cls._valid_instances[method_name] = object.__new__(cls)
            return cls._valid_instances[method_name]
        # If the method is not found (e.g. due to a wrong matplotlib
        # version), just create a dummy object
        return object.__new__(cls)

    def __init_subclass__(cls, *args, **kwargs) -> None:
        """
        Create new registry and instances of recipes for each subclass.

        Parameters
        ----------
        *args, **kwargs
            Ignored

        Notes
        -----
        Asserts that the subclass permits the same instantiation
        signature as the parent.
        """
        cls._valid_instances, inherited_recipes = {}, cls._valid_instances
        for recipe in inherited_recipes.values():
            try:
                a = (recipe.method, *recipe.args)
                k = {**recipe.kwargs}
                for attr in 'indexing', 'postprocess':
                    value = getattr(recipe, attr, None)
                    if value is not None:
                        k[attr] = value
                cls(*a, **k)
            except Exception as e:
                msg = f'{cls!r}: cannot inherit recipe {recipe!r}: {e!r}'
                warnings.warn(msg)

    def __init__(
        self,
        method: _RecipeAxesMethod,
        *args,
        indexing: typing.Optional[typing.Union[list, typing.Any]] = None,
        postprocess: typing.Optional[
            typing.Callable[[_LegendHandlePrecursor], _types.LegendHandle]
        ] = None,
        **kwargs
    ) -> None:
        """
        Initialize a recipe.

        Parameters
        ----------
        method()
            String name of a `matplotlib.axes.Axes` method, or a
            callable taking an `Axes` object as its first argument
        (other arguments)
            See the class documentation
        """
        method_name = ''
        try:
            # Basic initialization
            if utils.is_identifier(method):
                method_name, self.method = method, getattr(Axes, method, None)
            elif callable(method):
                method_name, self.method = method.__name__, method
            else:
                raise TypeError(
                    f'method = {method!r}: '
                    'expected either the name of a `matplotlib.axes.Axes` '
                    'method, or a function taking an `Axes` instance as its '
                    'first argument'
                )
            self.indexing = indexing
            if postprocess is None or callable(postprocess):
                self.postprocess = postprocess
            else:
                raise TypeError(
                    f'postprocess = {postprocess!r}: '
                    'expected `None` or a callable'
                )
            self.args = tuple(args)
            self.kwargs = types.MappingProxyType(kwargs)
            # Check if the method signature is valid
            if self.method is not None:
                sig = inspect.signature(self.method)
                try:
                    sig.bind(None, *self.args, **self.kwargs)
                except TypeError as e:
                    msg = (
                        '.method = {0.method!r}, `.args = {0.args!r}, '
                        '``.kwargs = {0.kwargs!r}: '
                        'failure in argument binding: {1}'
                    ).format(self, e)
                    raise TypeError(msg) from None
        except TypeError:  # Initialization failed, remove the recipe
            type(self)._valid_instances.pop(method_name, None)
            raise
        # Versioning of the recipe
        if hasattr(self, 'version'):
            self.version += 1
        else:
            self.version = 0

    def __repr__(self):
        return f'<{type(self).__name__} {str(self)!r} (v. {self.version})>'

    def __str__(self):
        if self.method is None:
            return '(defunct)'
        signature = '{}({})'.format(
            self.method.__qualname__,
            ', '.join(
                ['ax'] +
                [repr(a) for a in self.args] +
                [f'{k}={v!r}' for k, v in self.kwargs.items()]
            )
        )
        if isinstance(self.indexing, tuple):
            if self.indexing:
                index_repr = ', '.join(repr(i) for i in self.indexing)
            else:
                index_repr = repr(self.indexing)
            signature += f'[{index_repr}]'
        elif isinstance(self.indexing, list) and self.indexing:
            signature += ''.join(f'[{i!r}]' for i in self.indexing)
        elif self.indexing is None:
            pass
        else:
            signature += f'[{self.indexing!r}]'
        if callable(self.postprocess):
            signature = f'{self.postprocess.__qualname__}({signature})'
        return signature

    def get_handle(
        self, ax: Axes, formatter: _types.CategoryFormatter, **kwargs
    ) -> _types.LegendHandle:
        """
        Return a handle to be placed on a CL.

        Parameters
        ----------
        ax
            (Dummy) `matplotlib.axes.Axes` on which `.method()` is to be
            called
        formatter()
            Callable responsible for generating from the `kwargs` either
            a keyword mapping of arguments for `.method()`, or a
            sequence of such mappings
        **kwargs
            Keyword arguments to be supplied to `formatter()`.

        Return
        ------
        Handle object which is to be used in `ax.legend()`

        Notes
        -----
        In case of conflicts between `formatter()`-generated keyword
        arguments and the recipe's `.kwargs`, the former set of keyword
        arguments takes precedence.
        """
        if self.indexing is None:
            def accessor(results):
                return results
        elif isinstance(self.indexing, list):
            def accessor(results):
                for index in self.indexing:
                    results = results[index]
                return results
        else:
            def accessor(results):
                return results[self.indexing]

        keywords = formatter(**kwargs)
        if isinstance(keywords, typing.Mapping):
            kws, unpack = [keywords], True
        else:
            kws, unpack = keywords, False
        handles = [
            accessor(self.method(ax, *self.args, **dict(self.kwargs, **kw)))
            for kw in kws
        ]
        if self.postprocess is not None:
            handles = [self.postprocess(handle) for handle in handles]
        if unpack:  # Return a simple handle from kwargs
            return handles[0]
        else:  # Return a conposite handle from a sequence of kwargs
            return _types.CLCompositeHandle(handles)

    def show_help(self) -> None:
        """
        Display the pydoc help for the associated `matplotlib.axes.Axes`
        method (if any).
        """
        if self.method is None:
            return
        help(self.method)

    @classmethod
    def get_cl_compatible_methods(cls) -> typing.Tuple[_types.Keyword]:
        """
        Return a tuple of method names which are capable of being
        registered as CL methods.
        """
        return tuple(sorted(cls._valid_instances))

    @classmethod
    def get(cls, method: _RecipeMethod) -> typing.Self:
        """
        Return an existing `CategorizedLegendRecipe` object for
        `method`;
        if one does not already exist, raise a `NotImplementedError`.

        Parameters
        ----------
        method()
            (Name of a) Method which acts on an `matplotlib.axes.Axes`
            object

        Return
        ------
        Existing instance of the recipe class

        Notes
        -----
        If `method()` is a callable, its name is used for the lookup;
        and a warning is triggered when the `.method` of the existing
        recipe is not identical with `method()`.
        """
        if utils.is_identifier(method):
            method_name, method_is_callable = method, False
        elif callable(method):
            method_name, method_is_callable = method.__name__, True
            if not utils.is_identifier(method_name):
                raise TypeError(
                    f'method = {method!r}: '
                    f'.__name__ = {method_name!r} not a valid identifier'
                )
        else:
            raise TypeError(
                f'method = {method!r}: '
                'expected a string identifier or a callable'
            )
        if method_name not in cls._valid_instances:
            raise NotImplementedError(
                f'method = {method!r}: '
                'not a defined CL-compatible method; '
                f'available methods are: {cls.get_cl_compatible_methods()!r}'
            )
        recipe = cls._valid_instances[method_name]
        if method_is_callable and method is not recipe.method:
            msg = (
                f'method = {method!r}: '
                'not the same as the `.method` of the retrieved recipe '
                f'{recipe!r}'
            )
            warnings.warn(msg)
        return recipe

    @property
    def registered_name(self) -> typing.Union[_types.Keyword, None]:
        """
        The name of the recipe in the class registry if a valid recipe;
        `None` otherwise.
        """
        if self.method is None:
            return None
        method_name = self.method.__name__
        recipe = type(self)._valid_instances.get(method_name)
        if recipe is self:
            return method_name
        return None

    # Class variables
    _valid_instances = {}
    MethodAnnotation = _RecipeAxesMethod
    MethodOrNameAnnotation = _RecipeMethod

    # Instance variables
    __slots__ = (
        'method', 'args', 'kwargs', 'indexing', 'postprocess', 'version',
    )
