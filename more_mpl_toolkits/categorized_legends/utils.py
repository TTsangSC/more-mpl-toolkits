"""
Miscellaneous utilities.
"""
import inspect
import itertools
import numbers
import typing

import numpy as np
from matplotlib.axes import Axes

import tjekmate

from .._utils.decorators import partial_sole_positional_arg

__all__ = (
    'always',
    'unique',
    'get_attribute_origin',
    'get_subplot_geometry',
    'get_gridspec_occupancies',
    'get_grid_dimensions',
    'is_identifier',
    'is_keyword_mapping',
)

Item = typing.TypeVar('Item')
Identifier = typing.TypeVar('Identifier', bound=str)
ReturnType = typing.TypeVar('ReturnType')

# *********************** Misc util functions ************************ #

is_identifier = tjekmate.is_keyword


@tjekmate.logical
@partial_sole_positional_arg
def is_keyword_mapping(
    obj: typing.Any, *, optional: bool = False, **kwargs
) -> bool:
    """
    Check if `obj` is a keyword mapping of the specified value type;
    if `optional = True`, also allows the special value `None`;
    if `allow_value`, also allows `obj` to pass if itself passes the
    value check.

    Example
    -------
    >>> objs = [None, {}, {1: 2}, {'one': 2}, {'two': 1.}]
    >>> [is_keyword_mapping(obj) for obj in objs]
    [False, True, False, True, True]
    >>> [is_keyword_mapping(obj, optional=True) for obj in objs]
    [True, True, False, True, True]
    >>> [
    ...     is_keyword_mapping(obj, value_checker=lambda x: x > 1)
    ...     for obj in objs
    ... ]
    [False, True, False, True, False]
    >>> [is_keyword_mapping(obj, value_type=float) for obj in objs]
    [False, True, False, False, True]
    >>> is_keyword_mapping(2., value_type=float)
    False
    >>> is_keyword_mapping(2., value_type=float, allow_value=True)
    True
    >>> is_keyword_mapping(value_type=float, allow_value=True)(2.)
    True

    See also
    --------
    `~.tjekmate.is_[keyword_]mapping()`
    """
    if optional and obj is None:
        return True
    if kwargs:
        checker = tjekmate.is_mapping(key_checker=is_identifier, **kwargs)
    else:
        # is_keyword_mapping() is faster due to having fewer checks
        checker = tjekmate.is_keyword_mapping
    return checker(obj)


def always(value: ReturnType = True) -> typing.Callable[..., ReturnType]:
    """
    Return a callable returning `value` no matter what.

    Example
    -------
    >>> always_true = always()
    >>> always_true(), always_true(1), always_true(1, 2, foo=3)
    (True, True, True)
    >>> always_false = always(False)
    >>> always_false(), always_false(1), always_false(1, 2, foo=3)
    (False, False, False)
    """
    return lambda *a, **k: value


@tjekmate.check(
    iterable=tjekmate.is_instance(typing.Iterable),
    identity=tjekmate.takes_positional_args(1),
    on_duplicate=tjekmate.is_in(('ignore', 'error', 'stop')),
)
def unique(
    iterable: typing.Iterable[Item],
    *,
    identity: typing.Callable[[Item], typing.Hashable] = id,
    on_duplicate: typing.Literal['ignore', 'error', 'stop'] = 'ignore',
) -> typing.Generator[Item, None, None]:
    """
    Iterate over the unique elements of the iterable

    Parameters
    ----------
    iterable
        Iterable of items
    identity()
        Callable taking an item and returning a hashable (e.g. `hash()`,
        `id()`, etc.) representing the object identity
    on_duplicate
        What to do when a duplicate value (as deemed by `identity()`) is
        encountered: 'ignore' to pass over it, 'stop' to stop iterating
        over `iterable`, and 'error' to raise a `ValueError`

    Yield
    -----
    Unique items in `iterable`

    Examples
    --------
    >>> class Item:
    ...     def __init__(self, i: int) -> None:
    ...         self.i = i
    ...
    ...     def __hash__(self) -> int:
    ...         return hash((type(self), self.i))
    ...

    Identifying items:

    >>> item1, item2, item3 = (Item(0) for _ in range(3))
    >>> assert [
    ...     id(s) for s in unique([item1, item2, item3, item2, item1])
    ... ] == [id(item1), id(item2), id(item3)]
    >>> assert [
    ...     id(s) for s in unique(
    ...         [item1, item2, item3, item2, item1],
    ...         identity=hash,
    ...     )
    ... ] == [id(item1)]

    Behaviors upon reaching a duplicate:

    >>> list(unique([4, 3, 4, 1, 2, 4, 5]))
    [4, 3, 1, 2, 5]
    >>> list(unique([4, 3, 4, 1, 2, 4, 5], on_duplicate='stop'))
    [4, 3]
    >>> list(unique([4, 3, 4, 1, 2, 4, 5], on_duplicate='error'))
    Traceback (most recent call last):
      ...
    ValueError: iterable = [4, 3, 4, 1, 2, 4, 5]: duplicate `4` detected
    """
    ids = set()
    error = on_duplicate == 'error'
    stop = on_duplicate == 'stop'
    for item in iterable:
        obj_id = identity(item)
        if obj_id not in ids:
            ids.add(obj_id)
            yield item
            continue
        if stop:
            return
        if error:
            raise ValueError(
                f'iterable = {iterable!r}: duplicate `{item!r}` detected'
            )


def get_attribute_origin(
    cls: typing.Any,
    attr: Identifier,
    is_type: typing.Union[bool, None] = None,
) -> typing.Union[type, None]:
    """
    Check where the class got its attribute from.

    Parameters
    ----------
    cls
        Type or instance thereof
    attr
        Class attribute identifier name
    is_type
        If true, `cls.mro()` is used;
        else `type(cls).mro()` is used;
        default is to rely on `isinstance(cls, type)`

    Return
    ------
    None if the origin (or the attribute itself) cannot be found;
    the most immediate class in the dictionary of which the attribute
    is found
    otherwise

    Example
    -------
    >>> class Foo:
    ...     def foo(self):
    ...         ...
    >>> class Bar(Foo):
    ...     def bar(self):
    ...         ...
    >>> class Baz(Bar):
    ...     foo = Foo.foo
    ...
    >>> print(get_attribute_origin(Bar, 'foo').__name__)
    Foo
    >>> print(get_attribute_origin(Baz, 'foo').__name__)
    Baz
    >>> print(  # Also works on instances
    ...     get_attribute_origin(Baz(), 'bar').__name__,
    ... )
    Bar
    >>> print(get_attribute_origin(Baz, 'baz'))
    None
    """
    _is_type = isinstance(cls, type)
    if is_type is None:
        is_type = _is_type
    if is_type and not _is_type:
        raise TypeError(f'is_type = {is_type!r}: not a type')
    if not is_type:
        cls = type(cls)
    try:
        static_attr = inspect.getattr_static(cls, attr)
    except AttributeError:
        return None
    for base_cls in cls.mro():
        try:
            class_dict = vars(base_cls)
            if attr in class_dict and class_dict[attr] == static_attr:
                return base_cls
        except Exception:
            return None
    return None


# ********************* Get gridspec occupation ********************** #


def get_subplot_geometry(
    subplot: Axes,
    check: bool = True,
) -> typing.Tuple[(numbers.Integral,) * 4]:
    r"""
    Get the grid bounds of a subplot.

    Parameters
    ----------
    subplot
        Subplot axes
    check
        Whether to check that `subplot` is a subplot axes

    Return
    ------
    Tuple `(row_min, row_max, col_min, col_max)` (inclusive)

    See also
    --------
    matplotlib.gridspec.SubplotSpec.get_rows_columns()
        Old API (deprecated since 3.3, removed since 3.5)
    matplotlib.gridspec.SubplotSpec.{row|col}span
        New API (available since 3.2)

    Example
    -------
    >>> import contextlib
    >>> import matplotlib.pyplot as plt
    >>>
    >>> with (plt.ioff() or contextlib.nullcontext()):
    ...     fig = plt.figure()
    ...     gridspec = fig.add_gridspec(3, 5)
    ...     ax1 = fig.add_subplot(gridspec[8:15])  # = [1:3, 3:5]
    ...     ax2 = fig.add_subplot(gridspec[-1, 0])
    ...     ax3 = fig.add_subplot(gridspec[:2, :2])
    ...     print(
    ...         *(get_subplot_geometry(ax) for ax in (ax1, ax2, ax3)),
    ...         sep = '\n'
    ...     )
    ...     plt.close(fig)
    (1, 2, 3, 4)
    (2, 2, 0, 0)
    (0, 1, 0, 1)
    """
    if check:
        error = TypeError(f'subplot = {subplot!r}: expected a subplot axes')
        if not isinstance(subplot, Axes):
            raise error
        spec = subplot.get_subplotspec()
        if not spec:
            raise error
    # Note: we stay way from spec.get_rows_columns() (old API),
    # spec.rowspan, spec.colspan (new API) for compatibility
    _, ncols, start, stop = subplot.get_subplotspec().get_geometry()
    i1, j1 = divmod(start, ncols)
    i2, j2 = divmod(stop, ncols)
    if i2 < i1:
        i1, i2 = i2, i1
    if j2 < j1:
        j1, j2 = j2, j1
    return i1, i2, j1, j2


def get_gridspec_occupancies(
    subplots: typing.Iterable[Axes],
    count_invisible: bool = True,
) -> np.ndarray:
    """
    Get the grid occupancies from a collection of subplots.

    Parameters
    ----------
    subplots
        Iterable of subplot axes belonging to the same
        `matplotlib.gridspec.GridSpec` (checked and enforced))
    count_invisible
        Whether to count invisible subplots

    Return
    ------
    2-D array of boolean occupancies (occupied = true)

    Example
    -------
    >>> import contextlib
    >>> import matplotlib.pyplot as plt
    >>>
    >>> with (plt.ioff() or contextlib.nullcontext()):
    ...     fig = plt.figure()
    ...     gridspec = fig.add_gridspec(3, 5)
    ...     ax1 = fig.add_subplot(gridspec[8:15])  # = [1:3, 3:5]
    ...     ax2 = fig.add_subplot(gridspec[-1, 0])
    ...     ax3 = fig.add_subplot(gridspec[:2, :2])
    ...     print(  # doctest: +NORMALIZE_WHITESPACE
    ...         get_gridspec_occupancies([ax1, ax2, ax3])
    ...     )
    ...     plt.close(fig)
    [[ True  True False False False]
     [ True  True False  True  True]
     [ True False False  True  True]]
    """
    # Verification
    count_invisible = bool(count_invisible)
    subplots = list(subplots)
    if not (subplots and all(isinstance(ax, Axes) for ax in subplots)):
        raise TypeError(
            f'subplots = {subplots!r}: expected a collection of subplot axes'
        )
    gridspecs = {ax.get_gridspec() for ax in subplots}
    if not (len(gridspecs) == 1 and all(gridspecs)):
        raise TypeError(
            f'subplots = {subplots!r}: not of the same gridspec'
        )
    # Make a grid, remember where there is a (visible) subplot, return
    occupancies = np.zeros(gridspecs.pop().get_geometry(), dtype=bool)
    for ax in subplots:
        if not (count_invisible or ax.get_visible()):
            continue
        i1, i2, j1, j2 = get_subplot_geometry(ax, check=False)
        occupancies[i1:i2 + 1, j1:j2 + 1] = True
    return occupancies


# ************* Automatically determine grid dimensions ************** #


@tjekmate.check(n=tjekmate.is_positive_integer)
def _factorize_to_primes(
    n: numbers.Integral,
) -> typing.Dict[numbers.Integral, numbers.Integral]:
    """
    Get the prime factorization of an integer.

    Parameters
    ----------
    n
        Positive integer

    Return
    ------
    Dictionary such that

    >>> n == np.prod([  # noqa: F821 # doctest: +SKIP
    ...     prime ** power
    ...     for prime, power in _factorize_to_primes(n).items()
    ... ])

    Example
    -------
    >>> sorted(_factorize_to_primes(24050).items())
    [(2, 1), (5, 2), (13, 1), (37, 1)]
    """
    def yield_prime_divisors(
        n: numbers.Integral
    ) -> typing.Generator[numbers.Integral, None, None]:
        if n <= 1:  # Early exit: unit
            return
        if n in primes:  # Early exit: known prime
            yield n
            return
        root_n = int(n ** .5)
        # Try factors one by one
        for factor in range(2, root_n + 1):
            quotient, remainder = divmod(n, factor)
            if not remainder:
                yield from yield_prime_divisors(factor)
                yield from yield_prime_divisors(quotient)
                return
        # If we're here, it must have been a prime
        primes.add(n)
        yield n

    primes: typing.Set[numbers.Integral] = {2, 3, 5, 7, 11, 13}
    factorization: typing.Dict[numbers.Integral, numbers.Integral] = {}
    for prime in yield_prime_divisors(n):
        try:
            factorization[prime] += 1
        except KeyError:
            factorization[prime] = 1
    return factorization


@tjekmate.check(
    size=tjekmate.is_positive_integer,
    aspect=tjekmate.is_positive_real,
    **dict.fromkeys(
        ('weight_aspect', 'weight_size'),
        (tjekmate.identical_to(None) | tjekmate.is_finite_real),
    ),
    max_frac_empty=tjekmate.is_nonnegative_real,
)
def get_grid_dimensions(
    size: numbers.Integral,
    aspect: numbers.Real = 1,
    *,
    weight_aspect: typing.Optional[numbers.Real] = None,
    weight_size: typing.Optional[numbers.Real] = None,
    max_frac_empty: numbers.Real = .8,
) -> typing.Tuple[numbers.Integral, numbers.Integral]:
    """
    Get the dimensions of the smallest 2D grid of an aspect ratio close
    to the required one capable of holding the required number of items.

    Parameters
    ----------
    size
        Integer number of items (> 0) the grid should be able to hold
    aspect
        Positive finite real aspect ratio (width:height) the grid should
        ideally be of
    weight_aspect, weight_size
        Optional nonnegative finite real weights for the minimization
        objectives:

        >>> def aspect_ratio_frac_dev(
        ...     nrows: int, ncols: int,
        ... ) -> float:  # noqa: F821
        ...     ratio_of_ratios = (ncols / nrows) / aspect
        ...     if ratio_of_ratios > 1:
        ...         ratio_of_ratios = 1 / ratio_of_ratios
        ...     return 1 - ratio_of_ratios
        ...
        >>> def size_frac_dev(
        ...     nrows: int, ncols: int,
        ... ) -> float:  # noqa: F821
        ...     ratio_of_sizes = nrows * ncols / size  # Should be > 1
        ...     return ratio_of_sizes - 1

        if either weight is supplied, it is clipped to the range [0, 1]
        and the other weight is chosen to be complementary to it;
        if both weights are supplied, either (or both) should be
        positive, and they are taken to be relative to one another;
        if neither is supplied (default), the two objectives are
        weighted equally
    max_frac_empty
        Real finite fraction (clipped below to 0) of grid points allowed
        to be empty

    Return
    ------
    Tuple `(nrows, ncols)`

    Caveats
    -------
    Sensitive to the size of `size` and especially `max_frac_empty`.

    Example
    -------
    >>> get_grid_dimensions(8, weight_aspect=1)
    (3, 3)
    >>> get_grid_dimensions(15, aspect=1.3)
    (3, 5)
    >>> get_grid_dimensions(8, aspect=1.1, max_frac_empty=0)
    (2, 4)
    """
    AnsatzGenerator = typing.Generator[
        typing.Tuple[numbers.Integral, numbers.Integral], None, None
    ]

    # Helper functions
    def yield_factor_pairs(n: numbers.Integral) -> AnsatzGenerator:
        all_factors = sorted(
            np.prod(prime_powers)
            for prime_powers in itertools.product(*(
                [prime ** power for power in range(pmax + 1)]
                for prime, pmax in _factorize_to_primes(n).items()
            ))
        )
        i_mid, is_square = divmod(len(all_factors), 2)
        small_factors, big_factors = all_factors[:i_mid], all_factors[i_mid:]
        for fac1, fac2 in zip(small_factors, big_factors[::-1]):
            yield (fac1, fac2)
            yield (fac2, fac1)
        if is_square:
            yield (big_factors[0],) * 2

    def is_real_num(x: typing.Any) -> bool:
        return isinstance(x, numbers.Real) and np.isfinite(x)

    def ceil(x: numbers.Real) -> int:
        return np.ceil(x).astype(int)

    def floor(x: numbers.Real) -> int:
        return np.floor(x).astype(int)

    def get_size_dev(
        rc: typing.Tuple[numbers.Integral, numbers.Integral],
    ) -> numbers.Real:
        """
        Deviation (surplus) of grid size from the requested number of
        cells
        """
        return rc[0] * rc[1] / size - 1

    def get_aspect_dev(
        rc: typing.Tuple[numbers.Integral, numbers.Integral],
    ) -> numbers.Real:
        """
        Fractional deviation of the grid size from the requested aspect
        ratio
        """
        frac = rc[0] * aspect / rc[1]
        return 1 - (frac if frac <= 1 else 1 / frac)

    def get_weighted_objective(
        rc: typing.Tuple[numbers.Integral, numbers.Integral],
    ) -> typing.Tuple[numbers.Real, numbers.Real]:
        """
        Weighted deviation between the grid-dimension and grid-aspect
        deviations, with the former as the tie-breaker
        """
        aspect_dev, size_dev = get_aspect_dev(rc), get_size_dev(rc)
        return (weight_size * size_dev + weight_aspect * aspect_dev), size_dev

    # Post-proc
    if weight_aspect is weight_size is None:
        weight_aspect = weight_size = 1
    elif weight_aspect is None:
        weight_size = np.clip(weight_size, 0, 1)
        weight_aspect = 1 - weight_size
    elif weight_size is None:
        weight_aspect = np.clip(weight_aspect, 0, 1)
        weight_size = 1 - weight_aspect
    weight_sum = weight_aspect + weight_size
    if weight_sum:
        weight_aspect /= weight_sum
        weight_size /= weight_sum
    else:
        raise ValueError(
            f'weight_aspect = {weight_aspect!r}, '
            f'weight_size = {weight_size!r}: '
            'cannot both be zero'
        )
    size_max = floor((1 + np.clip(max_frac_empty, 0, None)) * size)
    # Minimize objective function over the ansaetze
    ansaetze: AnsatzGenerator
    if weight_aspect:
        objective = get_weighted_objective
        ansaetze = (
            (nrows, ncols)
            for nrows in range(1, size_max + 1)
            for ncols in range(ceil(size / nrows), floor(size_max / nrows) + 1)
        )
    else:
        objective = get_aspect_dev
        ansaetze = yield_factor_pairs(size)
    return min(ansaetze, key=objective)
