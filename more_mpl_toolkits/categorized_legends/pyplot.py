"""
Clone and drop-in replacement of the `matplotlib.pyplot` interface.

Notes
-----
Figures and axes created with utilities in this module:

1. Are instances of
   `_MODULE_.{Figure|Axes}`, and
2. Exist outside of the figure-management ecosystem of
   `matplotlib.pyplot`.

As such, they are invisible to `matplotlib.pyplot` utilities like
`.get_fignums()`, `.gcf()`, `.sca()`, `.close()`, etc.;
the corresponding utilities under this namespace should be used instead.
This is intentional, and is achieved by maintaining separate copies of
the `matplotlib.pyplot` module and its associated figure managers.

Original `matplotlib.pyplot` documentation
------------------------------------------
_DOC_
"""
import inspect
import textwrap
import types
import typing

import matplotlib.pyplot
from matplotlib import _pylab_helpers  # <- Figure management

from .._utils.monkeypatching import clone_module as _clone_module
# We import `Axes` here solely for symmetry with Figure
from .axes import CategorizedLegendAxes as Axes  # noqa: F401
from .figure import CategorizedLegendFigure as Figure
from ._types import Keyword

_MODULE_ = (lambda: None).__module__.rpartition('.')[0]

# Also put `Subplot` into the global namespace
if matplotlib.pyplot.Subplot is matplotlib.pyplot.Axes:
    Subplot = Axes
else:  # `Subplot` is distinct from `Axes` in older matplotlib versions
    Subplot = Axes._get_auto_subclass(matplotlib.pyplot.Subplot)


def _resolve_all() -> typing.List[Keyword]:
    """
    Decide on the names to put into `.__all__`.
    """
    others: typing.List[Keyword] = []  # Global constants etc.
    classes: typing.List[Keyword] = []
    callables: typing.List[Keyword] = []
    for name, value in vars(_plt).items():
        if name.startswith('_'):
            continue
        if isinstance(value, types.ModuleType):
            continue
        if not callable(value):
            others.append(name)
            continue
        try:
            module = value.__module__
            if not module.startswith('matplotlib'):
                continue
        except AttributeError:
            continue
        (classes if isinstance(value, type) else callables).append(name)
    return [
        name for ls in (others, classes, callables) for name in sorted(ls)
    ]


def __getattr__(attr: Keyword) -> typing.Any:
    """
    Load names from (the internal copy of) `matplotlib.pyplot`.
    Names loaded are placed in the global namespace.
    """
    globals()[attr] = result = getattr(_plt, attr)
    return result


def __dir__() -> typing.List[Keyword]:
    """
    List names from (the internal copy of) `matplotlib.pyplot`.
    """
    names = dir(_plt)
    for name in '__getattr__', '__dir__', '__all__':
        if name not in names:
            names.append(name)
    return names


_plt = _clone_module(matplotlib.pyplot)
_plt._pylab_helpers = _clone_module(_pylab_helpers)


_figure_func_orig = _plt.figure


def figure(*args, **kwargs) -> Figure:
    """
    Create a `_MODULE_.Figure` object with
    (the internal copy of) `matplotlib.pyplot.figure()`.

    Parameters
    ----------
    *args, **kwargs
        Passed to `matplotlib.pyplot.figure()`

    Return
    ------
    `_MODULE_.Figure` instance (of
    `FigureClass` if available as an argument)

    ------------------- Original documentation below -------------------

    _DOC_
    """
    fig = _figure_func_orig(*args, **kwargs)
    return Figure.wrap(fig)


# Since `.figure()` is not strictly equivalent to `pyplot.figure()`,
# just update the attributes we want instead of using
# `@functools.wraps()`
figure.__doc__ = (
    textwrap.dedent(figure.__doc__)
    .replace('_MODULE_', _MODULE_)
    .replace('_DOC_', textwrap.dedent(_figure_func_orig.__doc__).strip('\n'))
)
figure.__signature__ = (
    inspect.signature(_figure_func_orig).replace(return_annotation=Figure)
)


# Since pyplot invariably creates figures through `.figure()`, it should
# suffice to replace said function
_plt.figure = figure


# Note: `matplotlib.pyplot` doesn't have an `.__all__` (at least between
# v3.1 and v3.8)
__all__ = getattr(_plt, '__all__', None)
if __all__ is None:
    __all__ = _resolve_all()
__all__ = (*(__all__ or ()), '__getattr__', '__dir__')

__doc__ = (
    __doc__
    .replace('_MODULE_', _MODULE_)
    .replace('_DOC_', textwrap.dedent(_plt.__doc__).strip('\n'))
)
