"""
Recipes for getting legend handles from `Axes` functions.

Define recipes which describes how legend-ready handles can be retrieved
from the return values of CL-compatible `matplotlib.axes.Axes` methods,
or other functions similarly acting on `Axes` objects.

Notes
-----
Some recipes may be marked defunct, due to how their respective
`matplotlib.axes.Axes` methods do not exist in the version currently in
use.
"""
import functools
import inspect
import typing
import warnings

from . import _types
from ._recipes import CategorizedLegendRecipe
from ._utils import set_subpackage_name_in_docs

# ************************ Predefined recipes ************************ #

acorr = CategorizedLegendRecipe('acorr', [0., 1.], indexing=2, maxlags=None)
arrow = CategorizedLegendRecipe('arrow', 0., 0., .1, .1)
locals().update({
    method: CategorizedLegendRecipe(method) for method in ('axhline', 'axvline')
})
locals().update({
    method: CategorizedLegendRecipe(method, 0., 0.)
    for method in ('axhspan', 'axvspan', 'bar', 'barh')
})
axline = CategorizedLegendRecipe('axline', (0., 0.), (1., 1.))
if axline.method is None:
    try:
        # Replace Axes.axline() in older versions of matplotlib with
        # rays.axline()
        from ..rays import axline as _axline
    except ImportError:
        pass
    else:
        axline = CategorizedLegendRecipe(_axline, *axline.args, **axline.kwargs)
        del _axline
barbs = CategorizedLegendRecipe(
    'barbs', [0], [0], [1], [1], barbcolor='k', flagcolor='r',
)
broken_barh = CategorizedLegendRecipe('broken_barh', [(0, 1)], (0, 1))
boxplot = CategorizedLegendRecipe('boxplot', [[0., 1.]], indexing=['boxes', 0])
csd = CategorizedLegendRecipe(
    'csd', *(([1, 4, 0, 2, 3],) * 2), indexing=[2, 0], return_line=True,
)
errorbar = CategorizedLegendRecipe('errorbar', [1, 2], [1, 2], .1, .1)
locals().update({
    method: CategorizedLegendRecipe(method, [0, 0], [1, 1])
    for method in ('fill_between', 'fill_betweenx')
})
hist = CategorizedLegendRecipe('hist', [0], indexing=[2, 0])
locals().update({
    method: CategorizedLegendRecipe(method, 0, 0, 1)
    for method in ('hlines', 'vlines')
})
phase_spectrum = CategorizedLegendRecipe('phase_spectrum', [0, 1])
pie = CategorizedLegendRecipe('pie', [1], indexing=[0, 0])
plot = CategorizedLegendRecipe('plot', [0], [0], indexing=0)
plot_date = CategorizedLegendRecipe('plot_date', ['1 Jan'], [0], indexing=0)
psd = CategorizedLegendRecipe(
    'psd', [0, 0, 1], indexing=[2, 0], return_line=True,
)
quiver = CategorizedLegendRecipe('quiver', [0], [0], [1], [1])
scatter = CategorizedLegendRecipe('scatter', [0], [0])
stairs = CategorizedLegendRecipe('stairs', [0], [0, 1])
stem = CategorizedLegendRecipe('stem', [0, 1])
xcorr = CategorizedLegendRecipe(
    'xcorr', [0., 1.], [1., 0.], indexing=2, maxlags=None,
)

# ************************ Utility functions ************************* #


@set_subpackage_name_in_docs
def register_cl_recipe(
    method: typing.Optional[
        CategorizedLegendRecipe.MethodOrNameAnnotation
    ] = None,
    *args,
    verbose: bool = True,
    **kwargs
) -> typing.Union[
    typing.Callable[
        [CategorizedLegendRecipe.MethodOrNameAnnotation],
        CategorizedLegendRecipe.MethodAnnotation
    ],
    CategorizedLegendRecipe.MethodAnnotation,
]:
    """
    Register a recipe for getting a legend handle from the return value
    of a `matplotlib.axes.Axes` method.

    Parameters
    ----------
    method
        Name of the `matplotlib.axes.Axes` method, or alternatively a
        callable taking an `Axes` object as its first argument
    indexing
        Index with which the handle to be put on the legend can be
        retrieved from the return value of the method;
        If `None`, no indexing is done;
        If a list, the indexing is done iteratively thorugh each of its
        items
    postprocess()
        Optional function which is further applied on the handle
        retrieved after the indexing (if any), whose return value is
        used as the handle on a legend
    verbose
        Whether to be verbose
    *args, **kwargs:
        Minimal input required to generate a handle, e.g. the
        coordinates of a single point

    Return
    ------
    `method()` provided
        `.method` of the recipe, which is either `method()` (if a
        callable) or the `Axes` method of that name (if a name)
    else
        A decorator permitting registration of a function by decoration

    Side effects
    ------------
    - Recipe created/altered and registered with
      `{RECIPE_CLASS_NAME}`
    - Recipe added to the global scope in
      `<MODULE>`.recipes
      if the recipe name is deemed "safe"
    - Output emitted (if `verbose`)

    Notes
    -----
    If `method` is already registered, a warning is issued;
    if `method` cannot be registered, an error is raised.

    Examples
    --------
    Registering a recipe for

    >>> Axes.plot(  # noqa # doctest: +SKIP
    ...     Axes, float, float
    ... ) -> list[Line2D]:

    (Note: there's already a recipe for `.plot()`, so this will trigger
    a warning)

    >>> with warnings.catch_warnings():
    ...     warnings.simplefilter('ignore')
    ...     _ = register_cl_recipe(
    ...         'plot', 0., 0.,
    ...         # Extract the first Line2D object from the list of lines
    ...         indexing=0,
    ...     )
    Redefined CL method recipe 'plot': 'Axes.plot(ax, 0.0, 0.0)[0]'

    Registering user-defined functions as recipes:

    (By function call)

    >>> from matplotlib.axes import Axes
    >>> from matplotlib.lines import Line2D
    >>>
    >>> from _MODULE_ import recipes, register_cl_recipe
    >>>
    >>> def my_first_ax_func(
    ...     ax: Axes, foo: str, bar: str, **kwargs
    ... ) -> typing.Dict[str, typing.Sequence[Line2D]]:
    ...     ...
    ...
    >>> registered_func = register_cl_recipe(
    ...     my_first_ax_func,
    ...     # Supply foo = 'foo', bar = 'bar', baz = 'baz' by default
    ...     'foo', 'bar', baz='baz',
    ...     # Returned dictionary
    ...     # -> get the "key" list
    ...     # -> get first element
    ...     indexing=['key', 0],
    ...     verbose=False,
    ... )
    >>> assert registered_func is my_first_ax_func
    >>> recipe = recipes.my_first_ax_func
    >>> assert recipe.method is my_first_ax_func

    (By decoration)

    >>> @register_cl_recipe(
    ...     # Supply spam = 'foo', eggs = 'bar' by default
    ...     spam='foo', eggs='bar',
    ...     # Use a function to extract the Line2D object from the dict
    ...     postprocess=lambda result: result['jam'][len(result)],
    ...     verbose=False,
    ... )
    ... def my_second_ax_func(
    ...     ax: Axes, *args, **kwargs
    ... ) -> typing.Dict[str, typing.Sequence[Line2D]]:
    ...     ...
    """
    if method is None:
        assert not args
        return (
            functools.partial(register_cl_recipe, **kwargs, verbose=verbose)
        )
    # Are we overwriting a previous recipe?
    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        try:
            old_recipe = CategorizedLegendRecipe.get(method)
        except NotImplementedError:
            is_new = True
        else:
            is_new = False
    if not is_new:
        msg = (
            f'method = {method!r}: '
            f'redefining CL method recipe {old_recipe.registered_name!r}: '
            f'{str(old_recipe)!r}'
        )
        warnings.warn(msg)
    # Create recipe and put in the module scope
    method_name = method.__name__ if callable(method) else method
    module_namespace = globals()
    module_name = register_cl_recipe.__module__
    try:
        recipe = CategorizedLegendRecipe(method, *args, **kwargs)
        if recipe.registered_name is None:
            raise RuntimeError(f'method = {method!r}: registration failed')
        if is_new:
            if recipe.registered_name in module_namespace:
                msg = (
                    'CL method recipe {!r} cannot be added to the global '
                    'scope of module `{}` because of name clashes'
                ).format(recipe.registered_name, module_name)
                warnings.warn(msg)
            else:
                module_namespace.update(
                    {recipe.registered_name: recipe},
                    __all__=(*__all__, method),
                )
    except Exception:
        # We botched the recipe, remove it from the module
        if isinstance(
            module_namespace.get(method_name),
            CategorizedLegendRecipe,
        ):
            msg = (
                'Re-definition of CL method recipe {!r} failed; '
                'removing from the global scope of module `{}`'
            ).format(method_name, module_name)
            warnings.warn(msg)
            del module_namespace[method_name]
        raise
    # Emit output and return
    if verbose:
        print(
            '{} CL method recipe {!r}: {!r}'.format(
                ('Registered new' if is_new else 'Redefined'),
                recipe.registered_name,
                str(recipe),
            )
        )
    return recipe.method


def _fix_rcr_signature() -> None:
    """
    Splice positional and keyword arguments from the recipe instantiator
    into the signature of `.register_cl_recipe()`
    """
    sig = inspect.signature(register_cl_recipe)
    sig_base = inspect.signature(acorr.__init__)
    orig_params = list(sig.parameters.values())
    interpolated_params = list(sig_base.parameters.values())
    positionals = [
        p for p in orig_params
        if p.kind in (p.POSITIONAL_ONLY, p.POSITIONAL_OR_KEYWORD)
    ]
    interpolated_positionals = [
        p for p in interpolated_params
        if p.kind not in (p.KEYWORD_ONLY, p.VAR_KEYWORD)
    ]
    keywords = [p for p in orig_params if p.kind is p.KEYWORD_ONLY]
    interpolated_keywords = [
        p for p in interpolated_params
        if p.kind in (p.KEYWORD_ONLY, p.VAR_KEYWORD)
    ]
    seen_param_names: typing.Set[_types.Keyword] = set()
    finalized_params: typing.List[inspect.Parameter] = []
    for param in (
        positionals + interpolated_positionals
        + keywords + interpolated_keywords
    ):
        if param.name in seen_param_names:
            continue
        seen_param_names.add(param.name)
        finalized_params.append(param)
    register_cl_recipe.__signature__ = sig.replace(parameters=finalized_params)


_fix_rcr_signature()
del _fix_rcr_signature
register_cl_recipe.__doc__ = register_cl_recipe.__doc__.format(
    RECIPE_CLASS_NAME=(
        '{.__module__}.{.__name__}'
    ).format(register_cl_recipe, CategorizedLegendRecipe),
)


@set_subpackage_name_in_docs
def get_cl_recipes() -> typing.Dict[_types.Keyword, CategorizedLegendRecipe]:
    """
    Get the currently-defined recipes for handle retrival from the
    results of `matplotlib.axes.Axes` methods.

    Return
    ------
    Dictionary of `matplotlib.axes.Axes` method names (or name of
    methods acting on `Axes` otherwise defined) and their corresponding
    recipes

    Example
    -------
    >>> import contextlib
    >>> import matplotlib.pyplot as plt
    >>> from matplotlib.lines import Line2D
    >>> from numpy import allclose
    >>> from _MODULE_ import recipes, get_cl_recipes
    >>>
    >>> plot_recipe = get_cl_recipes()['plot']
    >>> assert isinstance(plot_recipe, recipes._RECIPE_CLASS_NAME_)
    >>> with (plt.ioff() or contextlib.nullcontext()):
    ...     ax = plt.gca()
    ...     color = [1, .5, .3]
    ...     try:
    ...         handle = plot_recipe.get_handle(
    ...             ax, lambda **kw: dict(color=color),
    ...         )
    ...         assert isinstance(handle, Line2D)
    ...         assert allclose(handle.get_color(), color)
    ...     finally:
    ...         plt.close(ax.figure)
    """
    return {
        name: CategorizedLegendRecipe.get(name)
        for name in CategorizedLegendRecipe.get_cl_compatible_methods()
    }


get_cl_recipes.__doc__ = get_cl_recipes.__doc__.replace(
    '_RECIPE_CLASS_NAME_', CategorizedLegendRecipe.__name__,
)

__all__ = (
    'register_cl_recipe',
    'get_cl_recipes',
    'CategorizedLegendRecipe',
    *CategorizedLegendRecipe.get_cl_compatible_methods(),
)
