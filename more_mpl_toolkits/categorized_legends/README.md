`more_mpl_toolkits.categorized_legends`
=======================================

[Alias(es): `~.cl`, `~.categorized_legend` (for backward compatibility)]

[TOC]

Motivations
-----------

### Categorized legends

Say you have the following data: 
```
  x    y    foo   bar
---  ---  -----  ----
 1.   0.   True  spam
1.1   2.   True   ham
1.5  3.2  False  spam
2.2  1.5   True  eggs
 3.  3.5  False   ham
...
```
and you want to visualize the data points by mapping `foo` to the
filled-ness of the marker and `bar` to the color.
Of course,
instead of showing one legend item for each combination of `foo` and
`bar` value
(or worse, one for each _data point_),
it's preferrable to, say, have the legend arranged in this manner:

| (Handles)      | (Labels)       |
| :-:            | :-:            |
| 🔴             | Foo            |
| ⭕️             | Not foo        |
| \<blank line\> | \<blank line\> |
| 🔵             | Bar: spam      |
| 🟠             | Bar: ham       |
| 🟢             | Bar: eggs      |

[Automated legend creation][tutorial-scatter-legend] is unfortunately
insufficient in this case here because it only works with marker colors
and sizes,
i.e. the `c` and `s` parameters of the [`Axes.scatter()`][axes-scatter]
method.
In general, we would however like to have access to all the
flexibilities afforded by the base plotting (i.e. `Axes`) method,
programmatically generating the plotting arguments based on a
combination of the "presets" generated from the data points,
and optionally user input on top of that.

### Categorized subplots

Alternatively, one may want to partition the data according to certain
categories,
group and visualize each partition in a separate subplot/panel,
and arrange such subplots on a grid: 

| row\\col # | 0                       | 1                        |
| -:         | :-:                     | :-:                      |
| 0          | `foo=True`, (all `bar`) | `foo=False`, (all `bar`) |
| 1          | `foo=True, bar='spam'`  | `foo=False, bar='spam'`  |
| 2          | `foo=True, bar='ham'`   | `foo=False, bar='ham'`   |
| 3          | `foo=True, bar='eggs'`  | `foo=False, bar='eggs'`  |

Particularly, in the data-exploratory phase of a project,
it is useful to be able to combine such data-partitioning capabilities
with the data-formatting aids as discussed above,
to quickly generate visualizations based on different "rotations"/
pivoting of the "data cube",
highlighting various trends and correlations in the data set.

Solutions
---------

Both possibilities are made easy with this subpackage:
```python
from typing import Any, Literal, Sequence, TypeVar

import matplotlib
import matplotlib.pyplot as plt

from more_mpl_toolkits import categorized_legends as cl

Row = TypeVar('Row')
Bar = TypeVar('Bar', bound=str)
Keyword = TypeVar('Keyword', bound=str)


def read_data(...) -> Sequence[Row]: ...  # noqa


def get_x(row: Row) -> float: ...


def get_y(row: Row) -> float: ...


def get_foo(row: Row) -> bool: ...


def get_bar(row: Row) -> Bar: ...


def get_plot_kwargs(
    *, foo: bool | None = None, bar: Bar | None = None,
) -> dict[Keyword, Any]:
    """
    `foo` determines whether the marker is filled or hollow;
    `bar` determines the color of the marker.
    """
    base_kwargs = dict(linestyle='none', marker='o')
    color = dict(spam='blue', ham='orange', eggs='green').get('red')
    if foo in (None, True):
        return dict(base_kwargs, facecolor=color, edgecolor='black')
    return dict(base_kwargs, facecolor='none', edgecolor=color)


def categorize_row(row: Row) -> dict[Keyword, Any]:
    return dict(foo=get_foo(row), bar=get_bar(row))

ROWS = read_data(...)
```

### Wrapper classes

`more_mpl_toolkits.cl` is mainly based on the two **wrapper classes**,
[`~.cl.Axes`][cl-axes] and [`~.cl.Figure`][cl-figure].
They are called "wrapper classes" because they are instantiated by
wrapping instances of their base classes.
```python
fig = cl.Figure(instantiator=plt.figure, dpi=200, figsize=(6.8, 4.6))
```
here we've passed an `instantiator()` function which creates the
`matplotlib.figure.Figure` instance to be wrapped from the other
supplied arguments;
if we hadn't, the default instantiator of the wrapped instance would
have been the class `matplotlib.figure.Figure`.
This is also equivalent to
```python
inner_fig = plt.figure(dpi=200, figsize=(6.8, 4.6))
fig = cl.Figure(inner_fig)

# Equivalently:
# >>> fig = cl.Figure.wrap(inner_fig)
```

The wrapper classes have been such written so that all their created
child `Axes` (subplots, inset axes, etc.) and `[Sub]Figure` objects are
also themselves instances of `~.cl.{Axes|Figure}`.

### Data formatters, CL methods, and base methods

#### CL methods

"Categorized legends" (CLs) can be easily created by the registration of
data formatters to CL methods, which are wrappers around
`matplotlib.axes.Axes` methods (i.e. **base methods**):
```python
ax = fig.gca()
ax.register_cl_method('cl_plot', get_plot_kwargs)

# Equivalently, when get_plot_kwargs was defined:
# >>> @ax.register_cl_method('cl_plot')
# ... def get_plot_kwargs(...):
# ...     ...
```
Here we have registered the data formatter `get_plot_kwargs()` to the
**CL method** `ax.cl_plot()`, which is a wrapper around the base method
`ax.plot()`.
Since `get_plot_kwargs()` takes the keyword arguments `foo` and `bar`,
`.cl_plot()` also takes the same keyword arguments,
which are then passed to `get_plot_kwargs()`,
which generates the keyword arguments
(`linestyle`, `marker`, `facecolor`, `edgecolor`) passed to `ax.plot()`.
Of course, as an `Axes` instance,
all the base methods are still accessible to `ax`.

CL methods:

- Can also be registered without directly specifying the data formatter,
  in which case it will then fall back to the
  *`.default_data_formatter`*:
  ```python
  ax.register_cl_method('cl_axhline')
  ax.default_data_formatter = lambda *, foo=None, bar=None: ...

  # Equivalently:
  # >>> ax.set_default_data_formatter(...)
  # or
  # >>> @ax.set_default_data_formatter
  # ... def ...
  ```

- Can be _registered_ to both `~.cl.Figure` and `~.cl.Axes` instances,
  but can however only be _called_ on `~.cl.Axes`.

- Are (along with their associated data formatters) automatically
  forwarded to child `Axes` and `Figure` objects when registered.

#### Data formatters

A **data formatter** is a function which only takes optional
keyword-only arguments,
called [categorized keyword arguments 
(CKAs)](#categorized-keyword-arguments-and-legends),
and returns either a mapping (e.g. dictionary) of keyword arguments or a
sequence of such mappings for the base method.

- If it returns a single mapping,
  the CL method calls the base method with those arguments and returns
  whatever the base method returns.

- If it returns a sequence of mappings,
  the CL method calls the base method once with each set of arguments
  and returns a list of the return values of the base method.
  This is useful for when one wants more complex data formatting, e.g.:
  ```python
  def get_composite_marker_kwargs(
      *,
      num: int | None = None,
      sides: Literal[3, 4, 5, 6] | None = None,
  ) -> tuple[dict[Keyword, Any], dict[Keyword, Any]]:
      base_kwargs = dict(linestyle='none', color='black')
      # Draw a geometric backdrop
      bd_size = 10
      bd_shape = {3: '^', 4: 's', 5: 'p', 6: 'h'}.get(sides, 'o')
      # Overlay with a number
      num_size = .7 * bd_size
      num_overlay = '${}$'.format('#' if num is None else num)
      return tuple(
          dict(base_kwargs, marker=m, markersize=s)
          for m, s in [(bd_shape, bd_size), (num_overlay, num_size)]
      )
  ```

Optionally, the data formatter can also take a variadic keyword
argument,
which allows it to intercept keyword arguments meant for the base
method,
suitably processing them before passing them on.
For example, this formatter takes a single CKA `foo`,
uses it to choose the (line) color,
and turns all markers (if used at all) into the circle:
```python
def get_massaged_kwargs(
    *, foo: Literal[0, 1, 2] | None = None, **kwargs,
) -> dict[Keyword, Any]:
    marker_style = None if kwargs.get('marker') is None else 'o'
    color = 'k' if foo is None else 'rgb'[foo]
    return dict(color=k, marker=marker_style)
```

If such a variadic keyword argument doesn't exist,
keyword arguments meant for the base method can nonetheless be supplied
and will simply fall through to the base method.
In case of conflicts,
they take priority over the keywords generated by the data formatter.

The data can then be plotted like this:
```python
for row in ROWS:
    ax.cl_plot(get_x(row), get_y(row), **categorize_row(row))

    # Approximately equivalent to
    # >>> ax.plot(
    # ...     get_x(row), get_y(row),
    # ...     **get_plot_kwargs(**categorize_row(row)),
    # ... )
```

>>>
- Data formatters should be _hashable_.

- Data formatters should provide defaults for its CKAs.
>>>

### Categorized keyword arguments and legends

Of course it wouldn't have been worthwhile if all we've done was to
`plot()` each row of the data with a different set of keyword arguments.
What has been done is a lot of book-keeping and convenience methods
based on it:

- Since `get_plot_kwargs()` has the keyword-only arguments `foo` and
  `bar`,
  they are being kept track of as **categorized keyword arguments
  (CKAs)**.

- The shared (i.e. common to `~.cl.Axes` and `~.cl.Figure`) method
  `.get_cl_categories()` gathers the values passed to each CKA on the
  axes/figure object and all its children,
  with optional filtering by the `cl_method` called,
  `formatter` used,
  and number of levels (`max_level`) of decendent figures/axes object to
  traverse, etc.

- The shared method `.get_cl_handles_labels()` returns an `OrderedDict`
  mapping CKA names to lists of tuples `handle, label`;
  the handles and labels can then e.g. be used to draw separate legends
  for each CKA:
  ```python
  >>> legends: dict[Keyword, matplotlib.legend.Legend] = {}
  >>> for ckarg, handle_label_pairs in (
  ...     fig.get_cl_handles_labels().items()
  ... ):
  ...     handles, labels = zip(*handle_label_pairs)
  ...     legends[ckarg] = fig.legend(handles, labels)
  ...     ...
  ```
  - Such handles and labels are differently formatted based on whether
    the CKA looks like it's for discrete numbers,
    a range of numbers,
    a True–False toggle, etc.
    The classifications and the parameters controlling the assignment of
    each CKA thereto are documented by the
    [`~.cl.core.DataCatClass`][cl-dcc] enum and its `.from_collection()`
    method,
    and can also optionally be provided explicitly
    (`category_value_classifications`).
  - The typesetting and sorting of CKA names and CKA values are also
    customizable via the `show_category_names`,
    `category_name_{sort|typeset}er`, and
    `category_value_{sort|typeset}ers` arguments.
  - To reduce the number of legend entries shown
    (e.g. when formatting is shared between CL methods,
    when certain CKAs are to be hidden,
    or when multiple entries are to be compressed into one),
    one can use the `conflate`, `suppress_categories`,
    and `values_per_line` arguments.
  - All parameters for the above method can be passed to this method for
    entry selection.

When all the data has been plotted,
the shared method **`.categorized_legend()`** can be used to draw the
combined [categorized legend (CL)](#categorized-legends)
detailing the effects of each CKA on the data visualization:
```python
legend = ax.categorized_legend()
```

- Legend entries are grouped by CKAs,
  between which gaps are by default inserted;
  this can be toggled with `gap_between_categories`.

- Long (resp. wide) legends with a specified number of columns (resp.
  rows) are created with
  `.categorized_legend(ncols=..., fill_columns_first=True)`
  (resp. `.categorized_legend(nrows=..., fill_rows_first=True)`).

- All the parameters for the above two methods (plus any `Axes.legend()`
  arguments) can be passed to this method for entry selection and output
  formatting.

>>>
- Values passed to categorized keyword parameters should be _hashable_.

- Legend handles are constructed from the gathered CKA values from the
  registered [recipes](#recipes).

- When in doubt, consult the `python` documentation (`pydoc`) of the
  `.categorized_legend()` method,
  and also the source code of
  [`~.cl.Axes.categorized_legend()`][cl-axes].

- As with `matplotlib.figure.Figure.legend()`,
  it is an error to call `~.cl.Figure.categorized_legend(loc='best')`.
  For the suggestion of legend location,
  see the [`~.legend_placement`][lp] subpackage.
>>>

### Recipes

When a CL is drawn, it is populated with entries (legend handles)
reconstructed from the values passed to the CKAs of the CL methods.
Each base method which can be [registered as a CL method](#cl-methods)
has a **recipe** associated therewith,
which uses item access and/or postprocessing hook functions to retrieve
a handle
(e.g. `matplotlib.lines.Line2D`, `matplotlib.collection.PolyCollection`)
recognized by a [legend handler][legend-handler] from the return value
of the base method on a dummy `Axes` object.

`more_mpl_toolkits.cl` already ships with predefined recipes for common
`Axes` methods
(e.g. `.plot()`, `.scatter()`),
which are available via the [`~.cl.recipes`][cl-recipes] namespace and
the `~.cl.get_cl_recipes()` method.
In case if recipes should be (re-)defined,
new/updated recipes can be registered via the
`~.cl.register_cl_recipe()` method:
```python
from typing import TypedDict

import numpy as np


class ViolinPlotResult(TypedDict):
    bodies: matplotlib.collections.PolyCollection
    cmeans: matplotlib.collections.LineCollection
    cmins: matplotlib.collections.LineCollection
    cmaxes: matplotlib.collections.LineCollection
    cmbars: matplotlib.collections.LineCollection
    cmedians: matplotlib.collections.LineCollection
    cquantiles: matplotlib.collections.LineCollection


def violin_postprocess(
    return_dict: ViolinPlotResult,
) -> tuple[
    matplotlib.collections.PolyCollection,
    matplotlib.collections.LineCollection,
]:
    return return_dict['bodies'], return_dict['cbars']


GAUSSIAN_SAMPLE = np.random.normal(size=10)

# Registering a recipe for a matplotlib.axes.Axes method:
# This uses
# >>> violin_postprocess(
# ...     dummy_axes.violinplot(
# ...         GAUSSIAN_SAMPLE,
# ...         **dict(
# ...             data_formatter(**{cka_name: cka_value}), points=10,
# ...         ),
# ...     )
# ... )
# as the legend handle for each `cka_name, cka_value` pair
if hasattr(matplotlib.axes.Axes, 'violinplot'):
    register_cl_recipe(
        'violinplot', GAUSSIAN_SAMPLE,
        postprocess=violin_postprocess, points=10,
    )
else:  # Registering a non-Axes method function which takes an Axes:
    @register_cl_recipe(
        dataset=GAUSSIAN_SAMPLE,
        postprocess=violin_postprocess,
        points=10,
    )
    def violinplot(
        ax: matplotlib.axes.Axes, dataset: np.ndarray, *args, **kwargs
    ) -> ViolinPlotResult:
        """
        Homebrew implementation of `matplotlib.axes.Axes.violinplot()`.
        """
        ...
        return ViolinPlotResult(...)
```
As seen from the above example,
recipes can be based on any method which takes an `Axes` plus additional
arguments and returns some value from which a handle can be retrieved.
This can be useful for e.g. supplying missing `Axes` method or
superseding them
(e.g. `Axes.axline -> `[`~.ra.axline()`][ra]).

>>>
- Depending on your `matplotlib` version, some of the recipes may show
  as being disabled because their base methods aren't available yet.

- Registered recipes are identified by names.

- Redefining existing recipes causes warnings to be emitted.
>>>

### Data partitioning

As briefly touched upon in
[Categorized subplots](#categorized-subplots),
another important aspect of data visualization is to select the proper
dimensions,
visualizing slices of the data set based on the value of the items in
those dimensions/categories.

This is mainly supported by the **[`~.cl.plot_categorized()`][cl-plot]**
function,
combining these facets of the problem:

- Categorization of the data points (`categorizer()`);

- Retrieval of the plotting variables (`data_getters`);

- Formatting of the data ([`formatter()`](#data-formatter)); and

- Partitioning and recombining of data across subplots
  (`split_{x|y|xy}`, `plenary_subplot`).

```python
fig2, subplots = cl.plot_categorized(
    ROWS,
    (get_x, get_y),  # data_getters
    categorizer=categorize_row,
    formatter=get_plot_kwargs,
    split_x='foo',
    split_y='bar',
    plenary_subplot={'bar': 'before'},
)

subplots['spam', True]  # Panel with bar='spam', foo=True
```

In addition, there are other details which can be controlled via the
function:

- Panel captioning (`label_subplots`);

- Axis limits and labelling (`unify_axlims`, `{x|y}label`);

- Reduction of visual clutter
  (`hide_split_categories`, `drop_ticklabels`); and

- Visualization with other base/`Axes` methods (`method`).

>>>
- Many keyword arguments are shared with
  [`~.cl.{Axes|Figure}.categorized_legend()`](
    #categorized-keyword-arguments-and-legends
  ).

- The backward-incompatible change of replacing the previous (pre-1.0.0)
  `x` and `y` arguments with the more general `data_getters` argument
  is to increase compatibility with base methods which doesn't have the
  three-argument signature `method(ax, x, y[, ...])`.

- When in doubt, consult the `python` documentation (`pydoc`) and the
  [source code][cl-plot] of the method.
>>>

`more_mpl_toolkits.categorized_legends.pyplot`
----------------------------------------------

The common use-case of
```python
import matplotlib.pyplot as plt

from more_mpl_toolkits.cl import Figure

fig = Figure(..., instantiator=plt.figure)
...
```
has several minor gotchas;
most noticeably, figure retrival (e.g. with `plt.gcf()` and
`plt.figure(fig_id)`) retrieves the inner `matplotlib.figure.Figure`
instead of the `~.cl.Figure` object.

As a workaround and for convenience, here we provide an alternative:
```python
import more_mpl_toolkits.cl.pyplot as cplt

cfig = cplt.figure(...)
assert isinstance(cfig, Figure)
```
This [`~.cl.pyplot`][cl-pyplot] module is a semi-independent mirror of
its `matplotlib` equivalent, in that they have the same user interface,
use the same underlying `matplotlib` machineries, but maintain separate
registries of open figures:
```python
from more_mpl_toolkits.cl.wrapper_proxy import get_wrapped_instance

assert [1] == plt.get_fignums() == cplt.get_fignums()
fig_inner = get_wrapped_instance(fig)
assert plt.figure(1) is fig_inner is not fig
assert cplt.figure(1) == cfig
assert fig is not cfig is not fig_inner
```
Obviously, beside the separate figure stacks, the main feature of the
module is that its utilities always return `~.cl.{Axes|Figure}`
instances instead of those of vanilla
`matplotlib.{axes.Axes|figure.Figure}`.

Notes
-----

### Why...

- _use wrapper classes and even a metaclass for them,
  while you could've just subclassed `matplotlib.axes.Axes` and
  `matplotlib.figure.Figure` and used `fig.add_subplot(axes_class=...)`
  and `plt.figure(FigureClass=...)`?_
  - These keyword arguments were introduced later than the `matplotlib`
    version I initially wrote this for.
  - The groundwork for this was also largely written as an exercise to
    familiarize myself with metaclasses.

### Why not...

- _... create instead an object which stores the figure and axes,
  does the book-keeping,
  and provides the specialized plotting methods?_
  - Because I was a fool who ain't heard of
    [composition over inheritance][comp-over-inh] when I first wrote
    this.

- _... call the subpackage something like `~.categorical` or just
  `.categorized`?
  The legend is only a part (though a huge one) of its functionalities._
  - It's also the original raîson-d'être for this subpackage –
    scratch that, for the _entire package_ (of which about a half is
    `~.cl` by size and by lines-of-code).
    Yep, all this just because I was tired of making a few plots and
    manually creating a few legend handles.

[axes-scatter]: https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.scatter.html
[comp-over-inh]: https://en.wikipedia.org/wiki/Composition_over_inheritance
[legend-handler]: https://matplotlib.org/stable/api/legend_handler_api.html
[tutorial-scatter-legend]: https://matplotlib.org/stable/gallery/lines_bars_and_markers/scatter_with_legend.html#automated-legend-creation

[cl-axes]: more_mpl_toolkits/categorized_legends/axes.py
[cl-dcc]: more_mpl_toolkits/categorized_legends/data_classification.py
[cl-figure]: more_mpl_toolkits/categorized_legends/figure.py
[cl-plot]: more_mpl_toolkits/categorized_legends/plot.py
[cl-pyplot]: more_mpl_toolkits/categorized_legends/pyplot.py
[cl-recipes]: more_mpl_toolkits/categorized_legends/recipes.py
[lp]: more_mpl_toolkits/legend_placement
[ra]: more_mpl_toolkits/rays
