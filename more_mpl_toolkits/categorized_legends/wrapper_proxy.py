"""
Metaclass for classes instantiating by wrapping base instances.
"""
import functools
import inspect
import textwrap
import types
import typing
import weakref

import tjekmate

from . import utils

__all__ = ('WrapperProxyMeta', 'get_wrapped_instance')

WrappedClass = typing.TypeVar('WrappedClass', bound=type)
WrappingClass = typing.TypeVar(
    'WrappingClass', bound=typing.Type[WrappedClass],
)
WrappedInstance = typing.TypeVar('WrappedInstance', bound=WrappedClass)
Wrapper = typing.TypeVar('Wrapper', bound=WrappingClass)

Keyword = typing.TypeVar('Keyword', bound=str)
Keywords = typing.Mapping[Keyword, typing.Any]

T = typing.TypeVar('T')


def _default_on_failure(
    func: typing.Callable[..., T],
    *,
    exception: typing.Union[
        typing.Type[Exception],
        typing.Tuple[typing.Type[Exception]],
    ] = Exception,
    default: typing.Optional[T] = None,
) -> typing.Callable[..., typing.Union[T, None]]:  # Outdated method
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except exception:
            return default
    return wrapper


def get_wrapped_instance(wrapper: Wrapper) -> WrappedInstance:
    """
    Return the wrapped instance of a wrapper object (an instance of a
    `WrapperProxyMeta` instance).

    Parameters
    ----------
    wrapper
        Wrapper object

    Return
    ------
    Wrapped object
    """
    if not isinstance(type(wrapper), WrapperProxyMeta):
        raise TypeError(
            f'type(wrapper) = {type(wrapper)!r}: '
            f'expected an instance of {WrapperProxyMeta!r}'
        )
    return _get_wrapped_instance(wrapper)


def _get_wrapped_instance(wrapper: Wrapper) -> WrappedInstance:
    return getattr(wrapper, type(type(wrapper)).wrapped_instance_marker)


def _attr_is_inherited_from_base_class(self, attr: str) -> bool:
    """
    Checks if the wrapped object's class attribute is to be taken in
    preference to that of the wrapping object, which may be the case
    when the wrapping class inherited from a subclass of the instance's
    class.

    Parameters
    ----------
    self
        Instance of a
        `<MODULE>.wrapper_proxy.WrapperProxyMeta` instance
    attr
        Attribute name

    Return
    ------
    Whether the class attribute of the wrapped object (if any) comes
    from a subclass of the class where the corresponding class a
    ttribute of the wrapping instance comes from

    Notes
    -----
    Outdated method;
    since we now always create a new subclass for each wrapped class, we
    don't have to worry about clobbering subclass methods when wrapping
    an instance of a strict subclass of the base class.
    """
    try:
        wrapped = _get_wrapped_instance(self)
    except AttributeError:  # No wrapped object (shouldn't happen)
        return False
    self_src, wrapped_src = (
        utils.get_attribute_origin(type(obj), attr) for obj in (self, wrapped)
    )
    if any(source is None for source in (self_src, wrapped_src)):
        return False
    return issubclass(wrapped_src, self_src) and wrapped_src is not self_src


def _build_attribute_accessor(
    obj_accessor: typing.Callable[..., T],
    accessor: typing.Callable[..., T],
    *,
    doc: typing.Optional[str] = None,
) -> typing.Callable[..., T]:
    """
    Build an attribute-accessing method (getter, setter, or deleter) for
    a `WrapperProxyMeta` instance.

    Parameters
    ----------
    obj_accessor()
        object.__{getattribute|setattr|delattr}__()
    accessor()
        builtins.{get|set|del}attr()
    doc
        Optional documentation

    Return
    ------
    Wrapper around `obj_accessor()`
    """
    @functools.wraps(obj_accessor)
    def attr_accessor(self, attr: Keyword, *args, **kwargs):
        cls = type(self)
        defer = type(cls)._attribute_should_be_deferred(self, attr)
        if defer:  # Deferred to wrapped object
            acc = accessor
            try:
                # Note: don't use _get_wrapped_instance() here or we may
                # have a recursion (because it uses getattr())
                inst = object.__getattribute__(
                    self, type(cls).wrapped_instance_marker,
                )
            except AttributeError:  # Nothing to defer to
                acc, inst = obj_accessor, self
        else:  # Direct attribute access
            acc, inst = obj_accessor, self
        # Hack: remap method calls routed to the wrapped object to self
        result = acc(inst, attr, *args, **kwargs)
        if type(result) is types.MethodType:  # noqa: E721
            if result.__self__ is inst is not self:  # Instance method
                return types.MethodType(result.__func__, self)
            elif (
                isinstance(result.__self__, type) and
                result.__self__ != cls and
                isinstance(self, result.__self__)
            ):  # Classmethod
                return types.MethodType(result.__func__, cls)
        return result

    if doc:
        attr_accessor.__doc__ = doc
    return attr_accessor


class _WrapperProxyDict(typing.MutableMapping):
    """
    Wrapper over `.__dict__` so that access on the keys are shunted to
    the appropriate mappings.

    Example
    -------
    >>> class Foo:
    ...     def __init__(self, *_, num: int, **__) -> None:
    ...         self.num = num
    ...
    >>> class FooWrapper(
    ...     Foo,
    ...     metaclass=WrapperProxyMeta,
    ...     allow_def_method_redefine=True,
    ... ):
    ...     def __init__(self, *_, num2: int=1, **__) -> None:
    ...         object.__setattr__(self, 'num2', num2)
    ...
    >>> def get_raw_dict(obj):
    ...     return object.__getattribute__(obj, '__dict__')
    ...
    >>> wrapper = FooWrapper(num=1, num2=2)

    Raw object dictionaries:

    >>> (
    ...     set(get_raw_dict(wrapper)) -
    ...     {type(type(wrapper)).wrapped_instance_marker}
    ... )
    {'num2'}
    >>> get_raw_dict(get_wrapped_instance(wrapper))
    {'num': 1}

    Combined view provided by this class:

    >>> def show_var_without_wrapped_instance(obj):
    ...     return {
    ...         k: v for k, v in vars(obj).items()
    ...         if k != type(type(obj)).wrapped_instance_marker
    ...     }
    ...
    >>> type(vars(wrapper)).__name__
    '_WrapperProxyDict'
    >>> sorted(show_var_without_wrapped_instance(wrapper).items())
    [('num', 1), ('num2', 2)]

    Also updatable both ways:

    >>> vars(wrapper).update(num=3, num2=4)
    >>> (
    ...     get_raw_dict(wrapper).get('num2'),
    ...     get_raw_dict(get_wrapped_instance(wrapper)),
    ... )
    (4, {'num': 3})
    >>> wrapper.num2 = 5
    >>> sorted(show_var_without_wrapped_instance(wrapper).items())
    [('num', 3), ('num2', 5)]
    """
    def __init__(self, wrapper: Wrapper) -> None:
        cls = type(wrapper)
        if not isinstance(cls, WrapperProxyMeta):
            raise TypeError(
                f'type(wrapper) = {cls!r}: '
                f'expected a {WrapperProxyMeta!r} instance'
            )
        self._wrapper = wrapper

    def _get_deferred_dest(
        self, key: Keyword,
    ) -> typing.MutableMapping[Keyword, typing.Any]:
        """
        Return the mutable mapping whither the key access should be
        deferred to.
        """
        wrapper = self._wrapper
        metacls = type(type(wrapper))
        if metacls._attribute_should_be_deferred(wrapper, key):
            return self._inner_dict
        return self._outer_dict

    def __getitem__(self, key: Keyword) -> typing.Any:
        return self._get_deferred_dest(key)[key]

    def __setitem__(self, key: Keyword, value: typing.Any) -> None:
        self._get_deferred_dest(key)[key] = value

    def __delitem__(self, key: Keyword) -> None:
        del self._get_deferred_dest(key)[key]

    def __iter__(self) -> typing.Generator[Keyword, None, None]:
        outer_keys = tuple(self._outer_dict)
        inner_keys = tuple(self._inner_dict)
        inner_keys = tuple(
            sorted(set(inner_keys) - set(outer_keys), key=inner_keys.index)
        )
        yield from (outer_keys + inner_keys)

    def __len__(self) -> int:
        return len(tuple(self))

    def __repr__(self) -> str:
        return repr(dict(self))

    @property
    def _outer_dict(self) -> typing.MutableMapping[Keyword, typing.Any]:
        return object.__getattribute__(self._wrapper, '__dict__')

    @property
    def _inner_dict(self) -> typing.MutableMapping[Keyword, typing.Any]:
        return vars(_get_wrapped_instance(self._wrapper))

    __slots__ = '_wrapper',


class WrapperProxyMeta(type):
    """
    Wrapper metaclass, an instance of which instantiates by wrapping
    around an instance of its base class(es).
    The class is idempotent, meaning that each object is wrapped once.

    An instance (hereafter 'wrapper object') of a `WrapperProxyMeta`
    instance refers the suitable attribute access (mostly instance
    attributes) to the wrapped object (stored at the
    `WrapperProxyMeta.wrapped_instance_marker` attribute of the wrapper
    object);
    meanwhile, the aforementioned reference to the wrapped object, and
    attributes, methods, descriptors, etc. either defined in the class
    `vars()` or `.__slots__`, or those found in the class `dir()`, are
    directly accessed on the wrapper.

    For newly defined class attributes, one can access them via

    >>> getattr(cls, type(cls).class_attrs_marker);  # doctest: +SKIP

    And the wrapped object of an instance can be accessed by

    >>> getattr( # noqa # doctest: +SKIP
    ...     obj, type(type(obj)).wrapped_instance_marker
    ... );
    >>> get_wrapped_instance(  # (Equivalent to the above)
    ...     obj,  # noqa # doctest: +SKIP
    ... )

    Known bugs
    ----------
    - Does not work with classes which don't permit `object.__new__()`
      to be called in place of the class constructor (e.g. `list`,
      `dict`, etc.).

    - If a wrapper subclass is automatically created (via wrapping an
      instance of a strict subclass of the non-wrapper base classes),
      methods defined using the zero-argument form of `super()` may fail
      (since the wrapper is only a 'virtual' instance/subclass of the
      'parent' wrapper class);
      to mitigate, use the full form (`super(type(self), self)` or
      `super(cls, cls)`) of `super()`.
      (Alternatively, see `~.core.CategorizedLegendMixin.super`.)
    """
    def __new__(
        metacls,
        name: str,
        bases: typing.Tuple[WrappedClass, ...],
        attrs: Keywords,
        *,
        allow_def_method_redefine: bool = False,
        filter_kwargs: typing.Optional[
            typing.Callable[[Keywords], Keywords]
        ] = None,
        default_instantiator: typing.Optional[
            typing.Callable[..., WrappedClass]
        ] = None,
        deferred_attrs: typing.Optional[typing.Collection[Keyword]] = None,
        **kwargs
    ) -> WrappingClass:
        """
        Set up a new class so that it instantiates by wrapping around an
        instance of one of its direct base classes.
        Most attribute access on an instance of such a class is referred
        to the wrapped object, except for attributes defined in the
        class' `.__dict__`, `.__slots__`, and `.__dir__()`.

        Parameters
        ----------
        name, bases, attrs
            Usual type instantiation arguments
        allow_def_method_redefine
            Whether to allow the following methods (for which default
            inplementations exist) to be redefined in the class
            dictionary `attrs`:

            `.__setattr__()`, `.__getattribute__()`, `.__delattr__()`,
            `.__dir__()`;

            if false and if any of these name exists in `attrs`, it is
            an error
        filter_kwargs()
            Optional callable for filtering the keyword arguments
            supplied to `.__new__()` into keyword arguments suitable for
            the instantiator (either `default_instantiator()`, a direct
            base class, or the `instantiator` provided as a keyword
            argument);
            default is to not perform any filtering
        default_instantiator()
            Optional callable for instantiating an instance to be
            wrapped;
            default is to use the most-direct base class which isn't
            itself a wrapper class
        deferred_attrs
            Optional collection of instance-variable names access to
            which should always be deferred to the wrapped instance

        Return
        ------
        Wrapper subclass around `bases`

        Notes
        -----
        - It is an error for `attrs` to provide a `.__new__()`.

        - Take caution with `allow_def_method_redefine = True`;
          the default implementations are what makes the wrapping work.
          In any case, they are accessible in the class'
          `.__wrapper_meta_methods__` attributes.

        - `filter_kwargs()` only takes effect within the class'
          `.__new__()`;
          the `.__init__()` uses the original `kwargs` mapping.

        - The default `.__init__()` implementation is a no-op;
          a suitable alternative can be supplied in `attrs`.
          Note that `.__init__()` can be called multiple times and
          user-supplied implementation must take care of that;
          most prominently, when instantiating with

          >>> cls(  # noqa # doctest: +SKIP
          ...     *args, [instantiator=..., ]**kwargs,
          ... )

          instead of `cls(wrapped_obj)`, i.e. the object to be wrapped
          is implicitly newly created, `.__init__()` will be called
          twice:

          >>> wrapper.__init__(  # noqa # doctest: +SKIP
          ...     impl_created_wrapped_obj,
          ... )
          >>> wrapper.__init__(  # noqa # doctest: +SKIP
          ...     *args, [instantiator=..., ]**kwargs,
          ... )

        - The standard implementations of these methods are only
          supplied when they are not already defined in the `attrs`
          mapping or in a parent `WrapperProxyMeta` instance:

          >>> (  # doctest: +SKIP
          ...     '__setattr__', '__getattribute__', '__delattr__',
          ...     '__dir__', '__init__',
          ... );

          this is to avoid inadvertantly clobbering methods already
          defined in parent classes, especially in automatically-
          generated subclasses.

        - The standard implementation of `.__del__()` is defined based
          on top of the user-defined implementation or the parent-class
          finalizer/destructor (if any), and is only supplied when it is
          not already defined in a parent `WrapperProxyMeta` instance.
        """

        # ******************* Docstring components ******************* #

        attr_accessor_doc_template = """
        Attribute access ({}): attributes that are not defined
        in the class `.__dict__` or `.__slots__`, or those of its base
        `WrapperProxyMeta` classes (if any), are referred to the wrapped
        object.
        """
        default_class_doc_template = """
        Wrapper class around these classes:
        {}

        Instances are instantiated by wrapping an instances of said
        classes, and attribute access is referred to the wrapped object,
        save for attribute names defined in the class' `.__dict__`,
        `.__slots__`, and `.__dir__()`.
        """
        constructor_doc = """
        Instantiate a new instance.

        Parameters
        ----------
        cls
            Wrapper class
        instantiator()
            Optional callable returning an instance of the base class to
            be wrapped (see Notes)
        *args, **kwargs
            Wrapped instance, or arguments to `instantiator()`;
            see below

        Return
        ------
        args, kwargs = (instance,), dict()
            Wrapper around `instance`, an instance of any of the most-
            immediate base classes of `cls` which are not
            themselves `~.wrapper_proxy.WrapperProxyMeta` instances;
            if already an instance of `cls`, return `instance` as-is;
            if previously wrapped by `cls`, return the existing wrapper.
        else
            Wrapper around instance instantiated from
            `instantiator(*args, **kwargs)`

        Notes
        -----
        """

        # ********************* Helper functions ********************* #

        def repr_func(func: typing.Callable) -> str:
            if isinstance(func, type):
                return repr(func)
            return f'{func.__qualname__}{inspect.signature(func)}'

        def strip_doc(docstring: str) -> str:
            return textwrap.dedent(docstring).strip('\n')

        def get_default_instantiator_from_mro(
            cls: WrappingClass,
        ) -> typing.Type[WrappedClass]:
            return getattr(cls, type(cls).non_wrapper_meta_mro_marker)[0]

        def use_supplied_default_instantiator(
            _
        ) -> typing.Callable[..., WrappedInstance]:
            return default_instantiator

        def method_should_be_redefined(method: Keyword) -> bool:
            """
            Check whether the `method` should be redefined since it's
            inherited from non-wrapper-proxy-meta-classes.
            """
            for cls in bases:
                origin = utils.get_attribute_origin(cls, method)
                if not origin:
                    continue
                if isinstance(origin, metacls):
                    return False
            return True

        # ******************** Basic type checks ********************* #

        # Check class heritage and class dict
        if not bases:
            raise TypeError(
                f'bases = {bases!r}: {metacls.__name__} '
                'instances must have explicit direct base classes'
            )
        for attr in 'new', 'setattr', 'getattribute', 'delattr', 'dir':
            attr = f'__{attr}__'
            if (
                (attr == '__new__' or not allow_def_method_redefine) and
                attr in attrs
            ):
                raise TypeError(
                    f'{name}.{attr} = {attrs[name]!r}: '
                    f'{metacls.__name__} instances must not '
                    f'explicitly define the attribute {"." + attr}'
                )

        # Check kwarg filtering and default instantiator
        constructor_docstring_notes: typing.List[str] = []
        if filter_kwargs is None:
            pass
        elif callable(filter_kwargs):
            constructor_doc_chunk = f"""
        The function

        >>> {repr_func(filter_kwargs)}  # noqa # doctest: +SKIP

        is used to filter the keyword args before they are provided to
        the instantiator.
            """
            constructor_docstring_notes.append(constructor_doc_chunk)
        else:
            raise TypeError(
                f'filter_kwargs = {filter_kwargs!r}: '
                'expected a callable splitting a keyword mapping into two'
            )
        if default_instantiator is None:
            default_instantiator_getter = get_default_instantiator_from_mro
            constructor_doc_chunk = """
        When not provided, `instantiator()` defaults to the first base
        class which is not itself a
        `<MODULE>.wrapper_proxy.WrapperProxyMeta`
        instance.
            """
            constructor_docstring_notes.append(constructor_doc_chunk)
        elif callable(default_instantiator):
            default_instantiator_getter = use_supplied_default_instantiator
            constructor_doc_chunk = f"""
        When not provided, `instantiator()` defaults to

        >>> {repr_func(default_instantiator)}  # noqa # doctest: +SKIP

            """
            constructor_docstring_notes.append(constructor_doc_chunk)
        else:
            raise TypeError(
                f'default_instantiator = {default_instantiator!r}: '
                f'expected a callable returning an instance of {bases!r}'
            )
        constructor_docstring_notes = [
            strip_doc(note) for note in constructor_docstring_notes
        ]

        # Check `deferred_attrs`
        if deferred_attrs is None:
            pass
        elif tjekmate.is_collection(
            deferred_attrs, item_checker=utils.is_identifier,
        ):
            deferred_attrs = frozenset(deferred_attrs)
        else:
            raise TypeError(
                f'deferred_attrs = {deferred_attrs!r}: '
                'expected either a collection of attribute-name identifiers or '
                '`None`'
            )

        # ******** Supply standard implementation of methods ********* #

        attrs = dict(attrs)
        more_attrs: Keywords = {}

        # Class constructor, initializer, and destructor/finalizer
        def __new__(
            cls: WrappingClass,
            *args,
            instantiator: typing.Optional[
                typing.Callable[..., WrappedInstance]
            ] = None,
            **kwargs
        ) -> Wrapper:
            instances = getattr(cls, type(cls).class_instances_marker)
            bases = getattr(cls, type(cls).non_wrapper_meta_mro_marker)
            assert bases
            if not (instantiator is None or callable(instantiator)):
                raise TypeError(
                    f'instantiator = {instantiator!r}: '
                    'expected either `None`, or a callable returning an '
                    'instance of any of {bases!r}'
                )
            if len(args) == 1 and not kwargs:
                instance, = args
                # Only wrap an object once
                if type(instance) is cls:
                    return instance
                if id(instance) in instances:
                    return instances[id(instance)]
                if isinstance(instance, bases):
                    subcls = cls._get_auto_subclass(type(instance))
                    if subcls is not cls:  # New subclass created
                        assert issubclass(subcls, bases)
                        return subcls(instance)
                    # Init blank object so as not to interfere with
                    # lookup in the instance object
                    obj = object.__new__(cls)
                    setattr(obj, type(cls).wrapped_instance_marker, instance)
                    # Remember that we've wrapped the instance object
                    instances[id(instance)] = obj
                    return obj
            if instantiator is None:
                instantiator = default_instantiator_getter(cls)
            instance = instantiator(
                *args,
                **dict(
                    kwargs if filter_kwargs is None else filter_kwargs(kwargs)
                ),
            )
            if not isinstance(instance, bases):
                raise TypeError(
                    f'{cls.__name__}.__new__('
                    f'instantiator = {instantiator!r}, '
                    f'filter_kwargs = {filter_kwargs!r}): '
                    f'expected either an instance of any of {bases!r} as '
                    'the sole positional arg., or the (keyword) arguments '
                    f'to `instantiator()` from which such an instance can '
                    'be instantiated; '
                    f'got a `{type(instance).__qualname__}` object from '
                    f'{repr_func(instantiator)!r} instead'
                )

            # Note: if we auto-created a subclass and instantiated the
            # wrapper from there, its initializer isn't called with the
            # kwargs and we have to do it here
            wrapper = cls(instance)
            if type(wrapper) is not cls:
                assert issubclass(type(wrapper), cls)
                wrapper.__init__(*args, instantiator=instantiator, **kwargs)
            return wrapper

        if len(constructor_docstring_notes) > 1:
            constructor_docstring_notes = [
                textwrap.fill(
                    note,
                    width=1024,
                    initial_indent='- ',
                    subsequent_indent='  ',
                    tabsize=4,
                ) + '\n'
                for note in constructor_docstring_notes
            ]
        __new__.__doc__ = '\n'.join((
            strip_doc(constructor_doc), *constructor_docstring_notes
        ))

        if '__init__' in attrs:
            if not callable(attrs['__init__']):
                raise TypeError(
                    f'{name}.__init__ = {attrs["__init__"]!r}: not callable'
                )
        else:
            def __init__(self, *args, **kwargs) -> None:
                """
        No-op; no initialization of the wrapper object is needed after
        `.__new__()`.
                """
                pass

            if method_should_be_redefined('__init__'):
                more_attrs['__init__'] = __init__

        if '__del__' in attrs:
            if not callable(attrs['__del__']):
                raise TypeError(
                    f'{name}.__del__ = {attrs["__del__"]!r}: not callable'
                )
            finalizer = attrs['__del__']
        else:
            finalizer = next(
                (cls.__del__ for cls in bases if hasattr(cls, '__del__')),
                utils.always(None),
            )

        def __del__(self) -> None:
            cls = type(self)
            instances = getattr(cls, type(cls).class_instances_marker)
            # Call inherited/user-defined finalizer
            finalizer(self)
            # Forget about the instance
            instances.pop(id(self), None)

        if method_should_be_redefined('__del__'):
            more_attrs['__del__'] = __del__

        # Attribute lookup
        # Note: attributes that shouldn't be offloaded to the wrapped
        # object:
        # - .__class__: class of wrapper
        # - .<wrapped_instance_marker>: reference to wrapped object
        # - Various class attributes in .__dict__, .__slots__, and
        #   .__dir__
        class_attrs = {
            metacls.wrapped_instance_marker,
            '__class__',
            *attrs,
            *attrs.get('__slots__', ())
        }
        for base in bases:
            if isinstance(base, WrapperProxyMeta):
                class_attrs.update(getattr(base, metacls.class_attrs_marker))
        # For now other inherited attributes are also non-deferred, but
        # keep a separate registry for those nonetheless
        # (do it now so we don't have to do dir(cls) (which is
        # recursive) in every single call to
        # _attribute_should_be_deferred(inst))
        inherited_attrs = (
            {attr for base in bases for attr in dir(base)} - class_attrs
        )
        accessor_methods = {
            obj_accessor.__name__: _build_attribute_accessor(
                obj_accessor,
                accessor,
                doc=attr_accessor_doc_template.format(purpose),
            )
            for obj_accessor, accessor, purpose in (
                (object.__getattribute__, getattr, 'getter'),
                (object.__setattr__, setattr, 'setter'),
                (object.__delattr__, delattr, 'deleter'),
            )
        }
        # Modify .__getattribute__() so that we get .__dict__ collation
        base_attr_getter = accessor_methods['__getattribute__']

        @functools.wraps(base_attr_getter)
        def __getattribute__(self, attr: Keyword) -> typing.Any:
            """
        Wrapper over `.__getattribute__()` so that the `.__dict__` can
        be overridden with a mutable mapping shunting key access towards
        either the (real) `.__dict__` of the wrapper or that of the
        wrapped object.
            """
            if attr != '__dict__':
                return base_attr_getter(self, attr)
            dunder_dict_allowed = any(
                (
                    '__dict__' in cls.__dict__ or
                    '__slots__' not in cls.__dict__
                )
                for cls in type(self).mro()
            )
            if dunder_dict_allowed and attr == '__dict__':
                return _WrapperProxyDict(self)
            return base_attr_getter(self, attr)

        accessor_methods['__getattribute__'] = __getattribute__

        def __dir__(self) -> typing.List[str]:
            names = list(object.__dir__(self))
            wrapped = _get_wrapped_instance(self)
            return (
                # Local names
                names +
                # Names available on the wrapped instance
                list(set(dir(wrapped)) - set(names))
            )

        # ******* Update class attributes before instantiation ******* #

        wrapper_meta_methods = dict(
            accessor_methods, __dir__=__dir__, __new__=__new__,
        )
        for dunder, value in wrapper_meta_methods.items():
            if (
                dunder == '__new__' or
                (dunder not in attrs and method_should_be_redefined(dunder))
            ):
                more_attrs[dunder] = value
        more_attrs.update({
            metacls.methods_marker: (
                types.MappingProxyType(wrapper_meta_methods)
            ),
            metacls.class_attrs_marker: frozenset(class_attrs),
            metacls.inherited_attrs_marker: frozenset(inherited_attrs),
            metacls.class_create_args_marker: types.MappingProxyType(
                dict(
                    bases=bases,
                    attrs=types.MappingProxyType({
                        # Note: some KVPs are inserted by the class
                        # machinery, strip those
                        key: value for key, value in attrs.items()
                        if key not in (
                            '__module__', '__qualname__', '__classcell__',
                        )
                    }),
                    allow_def_method_redefine=allow_def_method_redefine,
                    filter_kwargs=filter_kwargs,
                    default_instantiator=default_instantiator,
                    deferred_attrs=deferred_attrs,
                    **kwargs,
                )
            ),
            metacls.class_instances_marker: weakref.WeakValueDictionary(),
        })
        if deferred_attrs is not None:
            more_attrs.update(
                {metacls.instance_deferred_attrs_marker: deferred_attrs},
            )
        # Instantiate a class
        cls = type.__new__(
            metacls, name, bases, {**attrs, **more_attrs}, **kwargs,
        )

        # ******************** Final bookkeeping ********************* #

        # Set its pseudo-MRO
        cls._calculate_immediate_non_wrapper_meta_bases()

        # Fix the docstring and signature before returning
        vanilla_base_classes = (
            getattr(cls, type(cls).non_wrapper_meta_mro_marker)
        )
        if '__doc__' not in attrs:
            cls.__doc__ = (
                textwrap.dedent(default_class_doc_template)
                .format(
                    textwrap.indent(
                        '\n'.join(repr(cls) for cls in vanilla_base_classes),
                        '  ',
                    )
                )
            )
        if len(vanilla_base_classes) == 1:
            wrapped_type, = vanilla_base_classes
        else:
            wrapped_type = typing.Union[vanilla_base_classes]
        sig, params = inspect.signature(__new__), []
        for pname, parameter in sig.parameters.items():
            if pname == 'instantiator':
                params.append(
                    parameter.replace(
                        annotation=typing.Union[
                            typing.Callable[..., wrapped_type],
                            None,
                        ],
                        default=default_instantiator_getter(cls),
                    )
                )
            else:
                params.append(parameter)
        __new__.__signature__ = sig.replace(parameters=params)
        return cls

    def __setattr__(cls, attr: Keyword, value: typing.Any) -> None:
        """
        To make post-processing easier (e.g. by class decorators),
        update the internal indices as if the newly-set attribute has
        been here in the class dictionary since the beginning.

        Example
        -------
        >>> class Foo:
        ...     pass
        ...
        >>> class FooWrapper(Foo, metaclass=WrapperProxyMeta):
        ...     def bar(self):
        ...         ...
        ...
        >>> attr_marker = type(FooWrapper).class_attrs_marker
        >>> args_marker = type(FooWrapper).class_create_args_marker
        >>>
        >>> def get_class_creation_args():
        ...     return getattr(FooWrapper, args_marker)
        ...
        >>> def get_class_dict():
        ...     return getattr(FooWrapper, attr_marker)
        ...
        >>> old_dict = get_class_dict()
        >>> old_args = get_class_creation_args()
        >>> FooWrapper.baz = lambda self, arg: None
        >>> new_dict = get_class_dict()
        >>> new_args = get_class_creation_args()
        >>> set(new_dict) - set(old_dict)
        {'baz'}
        >>> set(new_args['attrs']) - set(old_args['attrs'])
        {'baz'}
        """
        attr_marker = type(cls).class_attrs_marker
        args_marker = type(cls).class_create_args_marker
        class_attrs = frozenset(getattr(cls, attr_marker) | {attr})
        creation_kwargs = dict(getattr(cls, args_marker))
        creation_kwargs['attrs'] = types.MappingProxyType(
            {**creation_kwargs['attrs'], attr: value}
        )
        type.__setattr__(cls, attr_marker, class_attrs)
        type.__setattr__(
            cls, args_marker, types.MappingProxyType(creation_kwargs),
        )
        type.__setattr__(cls, attr, value)

    def _calculate_immediate_non_wrapper_meta_bases(
        cls: WrappingClass,
    ) -> None:
        """
        Calculate an ordered tuple of base classes, gathered from:
        (1) direct parent classes which are not `WrapperProxyMeta`
            instances, and
        (2) such parent classes of `WrapperProxyMeta` instances anywhere
            in the ancestry of the class

        Notes
        -----
        To instantiate a `WrapperProxyMeta` instance class, the object
        to be wrapped must be an instance of any of these classes.

        Side effects
        ------------
        `.__wrapper_proxy_meta_reduced_mro__`
        (= `type(cls).non_wrapper_meta_mro_marker`) assigned to

        Example
        -------
        >>> class Foo:
        ...     pass
        ...
        >>> class FooWrapper(Foo, metaclass=WrapperProxyMeta):
        ...     pass
        ...
        >>> class Bar:
        ...     pass
        ...
        >>> class FoobarWrapper(Bar, FooWrapper):
        ...     pass
        ...
        >>> def get_nwmb_classes(cls):
        ...     # Set by ._calculate_immediate_non_wrapper_meta_bases()
        ...     mro_marker = type(cls).non_wrapper_meta_mro_marker
        ...     return getattr(cls, mro_marker)
        ...
        >>> [cls.__name__ for cls in get_nwmb_classes(FooWrapper)]
        ['Foo']
        >>> mro = [
        ...     cls.__name__ for cls in get_nwmb_classes(FoobarWrapper)
        ... ]
        >>> set(mro) == {'Bar', 'Foo'}
        True

        Subclasses that are automatically-created by wrapping a strict-
        base-class-subclass instance likewise calculate this non-wrapper
        MRO:

        >>> class Baaar(Bar):
        ...     pass
        ...
        >>> wrapped_baaar = FoobarWrapper(Baaar())
        >>> mro2 = [
        ...     cls.__name__
        ...     for cls in get_nwmb_classes(type(wrapped_baaar))
        ... ]
        >>> assert mro2 in (['Baaar'] + mro, ['Baaar'] + mro[::-1])
        """
        attr_marker = type(cls).non_wrapper_meta_mro_marker
        MRO = typing.List[type]

        def get_distance_to_base_object(cls: type) -> int:
            if cls is object:
                return 0
            assert cls.__bases__
            return 1 + min(
                get_distance_to_base_object(base_cls)
                for base_cls in cls.__bases__
            )

        def get_total_offsets_in_mros(
            cls: type, mros: typing.Collection[MRO],
        ) -> int:
            offset = 0
            for mro in mros:
                try:
                    offset += mro.index(cls)
                except ValueError:  # Not found
                    pass
            return offset

        # Get all the immediate non-wrapper-meta base classes
        all_bases: typing.Set[WrappedClass] = set()
        for base_cls in cls.__bases__:
            if not isinstance(base_cls, WrapperProxyMeta):
                all_bases.add(base_cls)
        # Get all the such base classes from the wrapper-meta base
        # classes
        for base_cls in cls.mro()[1:]:
            if isinstance(base_cls, WrapperProxyMeta):
                all_bases.update(
                    getattr(
                        base_cls,
                        type(base_cls).non_wrapper_meta_mro_marker,
                    )
                )
        # Resolve into an order which makes sense, taking into account
        # the inheritance relationships between these base classes
        reduced_mros: typing.Dict[
            WrappedClass, typing.List[WrappedClass]
        ] = {
            cls: [bc for bc in cls.mro() if bc in all_bases]
            for cls in all_bases
        }
        dist_from_base_object: typing.Dict[WrappedClass, int] = (
            {cls: get_distance_to_base_object(cls) for cls in all_bases}
        )
        reversed_mro: typing.List[WrappedClass] = []
        while reduced_mros:
            # Metric 1: position (if present) of the class in the other
            # classes' reduced MROs (the bigger the # the more basic the
            # class)
            # Metric 2: length of minimal inheritance path from from the
            # class to object (the smaller the # the more basic the
            # class)
            most_basic_cls = min(
                reduced_mros,
                key=lambda cls: (
                    -get_total_offsets_in_mros(cls, reduced_mros.values()),
                    dist_from_base_object[cls],
                ),
            )
            del reduced_mros[most_basic_cls]
            for mro in reduced_mros.values():
                try:
                    mro.remove(most_basic_cls)
                except ValueError:  # Not found
                    pass
            reversed_mro.append(most_basic_cls)
        setattr(cls, attr_marker, tuple(reversed_mro)[::-1])

    def _get_auto_subclass(
        cls: WrappingClass, subcls: WrappedClass,
    ) -> WrappingClass:
        """
        Return
        ------
        An appropriate class which is a subclass of both `subcls` and
        the bases of `cls`.

        Notes
        -----
        The instatiation arguments of `cls` are inherited, except for:

        - name
            Suitably modified
        - bases
            Prepended with `subcls`
        - attrs['__init__']
            Set to `cls.__init__`
        - default_instantiator
            Set to `subcls` if the original `default_instantiator` is a
            parent class thereof

        Example
        -------
        >>> from inspect import signature
        >>> def get_default_instantiator(cls):
        ...     return signature(cls).parameters['instantiator'].default
        ...
        >>> class Foo:
        ...     pass
        ...
        >>> class FooWrapper(Foo, metaclass=WrapperProxyMeta):
        ...     pass
        ...
        >>> class Foobar(Foo):
        ...     pass
        ...
        >>> wrapped_foo = FooWrapper()
        >>> wrapped_foobar = FooWrapper(Foobar())  # <- Subclass created
        >>> isinstance(wrapped_foobar, FooWrapper)
        True
        >>> isinstance(wrapped_foobar, Foobar)
        True
        >>> type(wrapped_foobar) is FooWrapper
        False
        >>> type(wrapped_foobar).__bases__ == (Foobar, Foo)
        True
        >>> type(wrapped_foobar).__name__
        'FooWrapperSubclassedFoobar'
        >>> type(get_wrapped_instance(wrapped_foo)) is Foo
        True
        >>> type(get_wrapped_instance(wrapped_foobar)) is Foobar
        True
        >>> get_default_instantiator(FooWrapper) is Foo
        True
        >>> get_default_instantiator(type(wrapped_foobar)) is Foobar
        True
        """
        bases, init_args = (
            getattr(cls, getattr(type(cls), attr + '_marker'))
            for attr in ('non_wrapper_meta_mro', 'class_create_args')
        )
        assert bases
        if subcls in (cls, bases[0]):
            # Class identity match, no need for an extra subclass
            return cls
        subclasses = type(cls).auto_wrapped_classes
        key = (subcls, cls)
        if key not in subclasses:
            kwargs = dict(init_args)
            base_name = subcls.__name__.lstrip('_')
            name_prefix = '_' * (len(subcls.__name__) - len(base_name))
            name = f'{name_prefix}{cls.__name__}Subclassed{base_name}'
            bases = (subcls, *kwargs.pop('bases'))
            attrs = dict(
                kwargs.pop('attrs'),
                # Note: don't let `subcls.__init__()` override
                # `cls.__init__()`
                __init__=cls.__init__,
            )
            if isinstance(kwargs.get('default_instantiator'), type):
                instantiator = kwargs['default_instantiator']
                if issubclass(subcls, instantiator):
                    kwargs['default_instantiator'] = subcls
            new_subcls = type(cls)(name, bases, attrs, **kwargs)
            # Alias the newly-spawn subclass to itself
            subclasses[key] = subclasses[new_subcls, cls] = new_subcls
        return subclasses[key]

    @staticmethod
    def _attribute_should_be_deferred(wrapper: Wrapper, attr: Keyword) -> bool:
        """
        Parameters
        ----------
        wrapper
            Wrapper instance
        attr
            Attribute name

        Return
        ------
        Whether the attribute should be accessed at the wrapper object
        (False) or the wrapped object (True)

        Example
        -------
        >>> class Foo:
        ...     def __init__(self, num: int) -> None:
        ...         self.num = num
        ...
        ...     def add(self, x: int) -> int:
        ...         return self.num + x
        ...
        ...     num_type = int
        ...
        >>> class FooWrapper(Foo, metaclass=WrapperProxyMeta):
        ...     def set_instance_variable(self, num: int) -> None:
        ...         object.__setattr__(self, 'instance_num', num)
        ...
        >>> attrs = [
        ...     # Protected names are not deferred
        ...     '__class__', type(FooWrapper).wrapped_instance_marker,
        ...     # Methods and other class attributes are not deferred
        ...     'add', 'num_type',
        ...     # Instance attributes of the wrapper are not deferred
        ...     'instance_num',
        ...     # Other lookups are deferred
        ...     'num',
        ... ]
        >>> wrapper = FooWrapper(1)
        >>> wrapper.set_instance_variable(2)
        >>> wrapper.num
        1
        >>> wrapper.instance_num
        2
        >>> dict(vars(get_wrapped_instance(wrapper)))
        {'num': 1}
        >>> (
        ...     object.__getattribute__(wrapper, '__dict__')
        ... ).get('instance_num')
        2
        >>> [
        ...     type(wrapper)._attribute_should_be_deferred(wrapper, a)
        ...     for a in attrs
        ... ]
        [False, False, False, False, False, True]
        """
        cls = type(wrapper)
        instance_deferred_attrs = getattr(
            # Note: the attribute may not exist if neither the wrapper
            # class nor its wrapper base classes (if any) made use of
            # the `deferred_attrs` class-instantiation argument
            cls, type(cls).instance_deferred_attrs_marker, (),
        )
        special_class_attrs = getattr(cls, type(cls).class_attrs_marker)
        other_class_attrs = getattr(cls, type(cls).inherited_attrs_marker)
        base_getattr = object.__getattribute__
        checks: typing.Sequence[
            typing.Tuple[typing.Callable[[Wrapper, str], bool], bool]
        ] = [
            # attr on the special list of attrs marked for access on the
            # wrapped instance
            # -> deferred access
            (lambda wrapper, attr: attr in instance_deferred_attrs, True),
            # attr on the banlist (marked for special treatment)
            # -> direct access
            (lambda wrapper, attr: attr in special_class_attrs, False),
            # attr available from the class (e.g. a method)
            # -> direct access
            (lambda wrapper, attr: attr in other_class_attrs, False),
            # attr is an instance variable of the wrapper
            # -> direct access
            (
                (
                    lambda wrapper, attr:
                    attr in base_getattr(wrapper, '__dict__')
                ),
                False,
            ),
        ]
        for checker, defer_if_check_passed in checks:
            try:
                if checker(wrapper, attr):
                    return defer_if_check_passed
            except Exception:
                pass
        return True

    def __subclasscheck__(cls, subcls: type) -> bool:
        """
        Subclass check which succeeds for subclasses spawned when
        wrapping around subclass instances of the base classes.

        Return
        ------
        If the MRO of `subcls` contains `cls`
            True
        If:
        - (1) `subcls` is an instance of `type(cls)`; and
        - (2) All immediate non-proxy base classes (INBCs) of `cls` are
              also INBCs of `subclass`; and
        - (3) The classes share the exact same creation arguments,
              except for 'bases', '__init__', 'default_instantiator',
              and 'set'
            True
        If any of the proxy base classes of `subcls` is considered a
        subclass of `cls`
            True
        Else
            False
        """
        # Helper functions
        def get_non_proxy_bases(cls):
            return getattr(cls, type(cls).non_wrapper_meta_mro_marker)

        def get_proxy_bases(cls):
            return tuple(
                base_cls for base_cls in cls.mro()[1:]
                if isinstance(base_cls, WrapperProxyMeta)
            )

        def get_creation_args(cls):
            return dict(getattr(cls, type(cls).class_create_args_marker))

        # subcls is not a wrapper proxy class -> F
        if not isinstance(subcls, type(cls)):
            return False
        # subcls concretely inherits from cls -> T
        mro = subcls.mro()
        if cls in mro:
            return True
        # subcls not a subclass of all the INBCs of cls -> F
        this_bases, other_bases = (
            get_non_proxy_bases(c) for c in (cls, subcls)
        )
        if not set(other_bases) >= set(this_bases):
            return False
        # subcls shares creation arguments with cls -> T
        this_kwargs, other_kwargs = (
            get_creation_args(c) for c in (cls, subcls)
        )
        for kwargs in this_kwargs, other_kwargs:
            for key in (
                # These are edited by ._get_auto_subclass()
                'bases', 'default_instantiator',
            ):
                kwargs.pop(key, None)
            attrs = kwargs['attrs'] = dict(kwargs.get('attrs', {}))
            for key in (
                # This is set by
                # ._calculate_immediate_non_wrapper_meta_bases()
                type(cls).non_wrapper_meta_mro_marker,
                # This is set by
                # matplotlib.artist.Artist.__init_subclass__()
                'set',
                # This is set by ._get_auto_subclass()
                '__init__',
            ):
                attrs.pop(key, None)
        if this_kwargs == other_kwargs:
            return True
        # subcls has parent classes which are considered subclasses of
        # cls -> T
        return any(
            issubclass(base_cls, cls) for base_cls in get_proxy_bases(subcls)
        )

    def __instancecheck__(cls, inst: typing.Any) -> bool:
        """
        Subclass check which succeeds for instances of subclasses
        spawned when wrapping around subclass instances of the base
        classes.

        See also
        --------
        .__subclasscheck__()
        """
        return cls.__subclasscheck__(type(inst))

    # Calss-level dunder markers
    # Marker for the wrapped instance in the .__dict__
    wrapped_instance_marker = '__wrapper_proxy_meta_instance__'
    # Marker for a dictionary of default implementation of methods (e.g.
    # attribute accessors)
    methods_marker = '__wrapper_proxy_meta_methods__'
    # Marker for a set of class attributes which are to be overriden by
    # instance variables, and thus should be deferred to the wrapped
    # instance
    instance_deferred_attrs_marker = (
        '__wrapper_proxy_meta_instance_deferred_attrs__'
    )
    # Marker for a set of class attributes which are intrinsically tied
    # to wrapper proxy classes
    class_attrs_marker = '__wrapper_proxy_meta_class_attrs__'
    # Marker for a set of class attributes which are inherited from
    # other (non-wrapper-proxy) base classes
    inherited_attrs_marker = '__wrapper_proxy_meta_inherited_attrs__'
    # Marker for the internal registry of instances of the class
    class_instances_marker = '__wrapper_proxy_meta_class_instances__'
    # Marker for the cached instantiating arguments of the class
    # (required to construct virtual subclasses from subclasses of a
    # base non-wrapper class without explicitly inheriting from the
    # wrapper class itself)
    class_create_args_marker = '__wrapper_proxy_meta_creation_args__'
    # Marker for the computed MRO based only on the immediate non-
    # wrapper base classes
    non_wrapper_meta_mro_marker = '__wrapper_proxy_meta_reduced_mro__'

    auto_wrapped_classes: typing.Dict[
        typing.Tuple[WrappedClass, WrappingClass],
        WrappingClass
    ] = {}
