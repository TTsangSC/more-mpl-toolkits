"""
Enumeration class for data-type classification.
"""
import enum
import numbers
import typing

import numpy as np

from tjekmate import (
    gt, is_finite_real, is_instance, is_positive_integer, is_strict_boolean,
    check,
)

__all__ = ('DataCatClass',)


@enum.unique
class DataCatClass(enum.Enum):
    """
    Classification for data categories.
    """
    DISCRETE_INTEGERS = enum.auto()
    DISCRETE_NUMBERS = enum.auto()
    LINEAR_RANGE = enum.auto()
    LOG_RANGE = enum.auto()
    BOOLS = enum.auto()
    STRINGS = enum.auto()
    GENERICS = enum.auto()

    @classmethod
    @check(
        collection_set=is_instance((set, frozenset)),
        order_base=(is_finite_real & gt(1)),
        max_linear_orders=is_positive_integer,
        max_discrete_numbers=is_positive_integer,
    )
    def from_collection(
        cls,
        collection_set: typing.Union[set, frozenset],
        *,
        order_base: numbers.Real = 10,
        max_linear_orders: numbers.Integral = 3,
        max_discrete_numbers: numbers.Integral = 5,
    ) -> typing.Self:
        """
        Classify a collection of hashable values.

        Parameters
        ----------
        collection_set
            Set of values
        order_base
            Real-number (> 1) base in which orders of magnitude are
            measured
        max_linear_orders
            Maximum positive-integer orders of magnitude a number
            collection can span before being classified as a logarithmic
            range instead of a linear one
        max_discrete_numbers
            Maximum positive-integer size a number collection can be
            before being classified as a range instead of a discrete
            collection

        Return
        ------
        `<MODULE>.core.DataCatClass`
        enum corresponding to the classification

        Notes on enum values
        --------------------
        DISCRETE_INTEGERS
            A few integers (at most `max_discrete_numbers` many)

        DISCRETE_NUMBERS
            A few real numbers (at most `max_discrete_numbers` many)

        LINEAR_RANGE
            1) A range of many real numbers crossing/containing zero, or
            2) a range of many real numbers of the same sign and within
               `max_linear_orders` orders of magnitude of one another in
               base-`order_base`
            Examples:

            >>> [1, 2, 5, 10, 20, 40, 50, 100]  # doctest: +SKIP
            >>> [2 * i - 20 for i in range(100)]  # doctest: +SKIP

        LOG_RANGE
            A range of many real numbers of the same sign and spanning
            more than `max_linear_orders` orders of magnitude
            (e.g. `[1e-2, 5e-2, 1e-0, 5e-0, 1e2, 5e2]`)

        BOOLS, STRINGS
            Self-explanatory

        GENERICS
            Anything else, be that unrecognized objects or a mixture of
            the aforementioned

        Example
        -------
        >>> import functools
        >>> import numpy as np
        >>> geomspace = functools.partial(np.geomspace, num=10)
        >>> collections = dict(
        ...     DISCRETE_INTEGERS=[[1, 3, 5, 7, 9]],
        ...     DISCRETE_NUMBERS=[[1, 1.5, 2, 3, 5]],
        ...     LINEAR_RANGE=[range(20), geomspace(1E-3, 1E-1)],
        ...     LOG_RANGE=[geomspace(1E-6, 1E2)],
        ...     BOOLS=[[True, True, False], [True]],
        ...     STRINGS=[['foo', 'bar', 'baz']],
        ...     GENERICS=[[1, 'foo', None]],
        ... )
        >>> for coll_type, colls in collections.items():
        ...     category = DataCatClass[coll_type]
        ...     assert all(
        ...         category == DataCatClass.from_collection(set(coll))
        ...         for coll in colls
        ...     )
        """
        def basic_log(x: numbers.Real, base: numbers.Real = order_base):
            """
            Basic implementation of `log_{base}(x)`;
            where possible, use the more specific predefined functions
            (`numpy.log[2|10]()`)
            """
            return np.log(x) / np.log(base)

        if not collection_set:
            # Nothing in the collection (shouldn't happen, but it
            # doesn't hurt to allow it)
            return cls.GENERICS, collection_set
        for classification, checker in [
            (cls.BOOLS, is_strict_boolean), (cls.STRINGS, is_instance(str)),
        ]:
            if all(checker(i) for i in collection_set):
                return classification
        is_short = len(collection_set) <= max_discrete_numbers
        is_real = all(isinstance(i, numbers.Real) for i in collection_set)
        is_int = all(isinstance(i, numbers.Integral) for i in collection_set)
        if is_short and is_real:
            return cls.DISCRETE_INTEGERS if is_int else cls.DISCRETE_NUMBERS
        if is_real:
            log: typing.Callable[[numbers.Real], numbers.Real] = {
                2: np.log2, np.e: np.log, 10: np.log10,
            }.get(order_base, basic_log)
            values = np.array(sorted(collection_set))
            signs = set(np.sign(values))
            if signs == {-1}:
                values, signs = -values, {1}
            if signs == {1}:
                if abs(log(values[0]) - log(values[-1])) < max_linear_orders:
                    return cls.LINEAR_RANGE
                else:
                    return cls.LOG_RANGE
            return cls.LINEAR_RANGE
        return cls.GENERICS
