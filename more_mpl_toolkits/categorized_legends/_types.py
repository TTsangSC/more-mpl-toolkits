"""
Typing stuff
"""
from importlib import import_module
import typing

from matplotlib.figure import Figure
from matplotlib.legend import Legend
from matplotlib.legend_handler import HandlerTuple
try:
    from matplotlib.figure import _AxesStack as AxesStack
except ImportError:
    from matplotlib.figure import AxesStack
try:
    from matplotlib.figure import FigureBase
except ImportError:
    FigureBase = Figure

__all__ = (
    'Keyword', 'Keywords', 'KeywordDict',
    'FormatString', 'Sortable', 'LegendHandle',
    'CategorizedKeyword', 'Categorization', 'CategoryFormatter',
    'CLMethodName', 'CLCompositeHandle', 'CLHandleCollection',
    # Aliases for matplotlib classes
    'AxesStack', 'Figure', 'FigureBase',
)

for _name in 'Literal', 'NotRequired', 'ParamSpec', 'Self', 'TypedDict':
    if not hasattr(typing, _name):
        setattr(
            typing, _name, getattr(import_module('typing_extensions'), _name),
        )
del _name

Keyword = typing.TypeVar('Keyword', bound=str)
Keywords = typing.Mapping[Keyword, typing.Any]
KeywordDict = typing.Dict[Keyword, typing.Any]

FormatString = typing.TypeVar('FormatString', bound=str)
Sortable = typing.TypeVar('Sortable')
LegendHandle = typing.TypeVar('LegendHandle')

CategorizedKeyword = typing.TypeVar('CategorizedKeyword', bound=Keyword)
Categorization = typing.TypeVar('Categorization', bound=typing.Hashable)

CategoryFormatter = typing.Callable[
    ..., typing.Union[Keywords, typing.Sequence[Keywords]]
]

CLMethodName = typing.TypeVar('CLMethodName', bound=str)


class CLCompositeHandle(tuple):
    """
    Private class for handler mapping, representing a sequence of
    artists used to generate a composite artist (e.g. two overlapping
    markers plotted at the same coordinates as a single marker).
    """
    pass


class CLHandleCollection(tuple):
    """
    Private class for handler mapping, representing a disjoint sequence
    of artists.
    """
    pass


Legend.update_default_handler_map({
    # Spread artists horizontally
    CLHandleCollection: HandlerTuple(None),
    # Overlap them (for composite markers)
    CLCompositeHandle: HandlerTuple(1),
})
