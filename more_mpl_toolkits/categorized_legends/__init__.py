"""
Utilities for the visualization of categorized data.

API
---
Figure, Axes
    Classes wrapping around matplotlib entities, forming the basis for
    the user interface:

    .register_cl_method()
        Convert `matplotlib.axes.Axes` methods to their corresponding
        CATEGORIZED-LEGEND (CL) METHODS;
        when plotting with CL METHODS, DATA FORMATTERS are used to
        format the plotted artists (markers, lines, etc.) according to
        the keyword arguments received, and the values can be later used
        to construct a CL.

    .default_cl_formatter
        Settable data descriptor;
        DEFAULT DATA FORMATTER for CL METHODS registered without a
        formatter.

    .get_cl_handles_labels()
        Create handles and labels for a CL: that is, a
        `matplotlib.legend.Legend` object categorizing the keyword
        arguments encountered in the registered CL METHODS, and
        generating for each value in each category a handle, showing how
        said value is represented in the figure.

    .categorized_legend()
        Create and return a CL, formatting the handles and labels
        returned from `.get_cl_handles_labels()` properly.

    .add_subplot(), .gca(), etc.
        These `~.Figure` methods now return `~.Axes` objects instead of
        `matplotlib.axes.Axes`;
        such `Axes` objects remember the registered methods and
        formatters of the parent figure, but also allow overriding them.

plot_categorized()
    Function for ingesting and categorizing data, and subsequently
    partitioning, formatting, and plotting them based on that;
    includes convenience functions like subplot labelling, unification
    of subplot bounds, and 'smart' tick- and axis-label elision.

get_cl_recipes(), register_cl_recipe()
    Functions for manipulating recipes: instruction sets for retrieving
    legend-compatible handles from `Axes` methods;
    the currently-available recipes are accessible via
    `~.get_cl_recipes()`;
    if more are needed, they are to be supplied via
    `~.register_cl_recipe()`.

Motivations
-----------
I. Categorized Legends

When making plots, one often would like to represent more dimensions in
the data set than is possible via the two data axes;
such is achieved by changing the representation of the plotted data
according to the remaining data dimensions.
For example:

>>> from itertools import product
>>> import numpy as np
>>> try:
...     from pandas import DataFrame
... except ImportError:
...     DataFrame = None
>>> FOO_COLORS = 'red', 'green', 'blue', 'orange'
>>> BAR_MARKERS = dict(spam='^', ham='s', eggs='h')
>>> BAZ_VALUES = True, False
>>>
>>> def make_dataframe(length: int = 10) -> DataFrame:
...     return DataFrame(dict(
...         x = np.random.random(length),
...         y = np.random.random(length),
...         foo = np.random.choice(range(len(FOO_COLORS)), length),
...         bar = np.random.choice(list(BAR_MARKERS), length),
...         baz = np.random.choice(list(BAZ_VALUES), length),
...     ))
...
>>> def make_plot_with_categorized_markers(df: DataFrame) -> None:
...     for (foo, color), (bar, marker) in product(
...         enumerate(FOO_COLORS), BAR_MARKERS.items()
...     ):
...         df.query(f'foo == {foo!r} and bar == {bar!r}').plot(
...             'x', 'y',
...             linestyle='none',
...             marker=marker,
...             color=color,
...             label=f'.foo = {foo!r}, .bar = {bar!r}',
...         )
...
>>> make_plot_with_categorized_markers(  # doctest: +SKIP
...     make_dataframe()
... )

In the above example, each `row` in the dataframe is converted to a
point `(row.x, row.y)`, and the point is colored by its `.foo` and
shaped by its `.bar`.
A legend suitable for representing the data would then look like:

    .foo = 1       <red circular marker>
    .foo = 2       <green circular marker>
    .foo = 3       <blue circular marker>
    .foo = 4       <orange circular marker>

    .bar = 'spam'  <black triangular marker>
    .bar = 'ham'   <black square marker>
    .bar = 'eggs'  <black hexagonal marker>,

which tells the viewer what aspect of the data each degree of freedom in
the data representation corresponds to.
This is what we would call a CATEGORIZED LEGEND (CL) -- data points are
categorized based on their `.foo` and `.bar` values, and are formatted
accordingly.

However, the default method for constructing legends is not aware of
such correspondance, resulting in something like:

    .foo = 0, .bar = 'spam'  <red triangular marker>
    .foo = 1, .bar = 'spam'  <green triangular marker>
    .foo = 2, .bar = 'spam'  <blue triangular marker>
    .foo = 3, .bar = 'spam'  <orange triangular marker>
    [...]
    .foo = 0, .bar = 'eggs'  <red hexagonal marker>
    .foo = 1, .bar = 'eggs'  <green hexagonal marker>
    .foo = 2, .bar = 'eggs'  <blue hexagonal marker>
    .foo = 3, .bar = 'eggs'  <orange hexagonal marker>,

which quickly explodes in length as (1) the number of data dimensions
compressed into the represention and (2) the number of distinct values
in each such dimensions increase.
This thus motivates utilities permitting the facile management of such
compressed data dimensions -- the bookkeeping of their names and values,
and the generation of suitable legend entries for each of them.

II. Categorized Subplots

Another common use-case is for the data to be separated by the
categorizations of individual data points, and for each category to be
plotted in a separate subplot/panel:

>>> def make_categorized_subplots(df: DataFrame) -> None:
...     baz_values = df.baz.unique()
...     gridspec = fig.add_gridspec(1, len(baz_values))
...     subplots = {}
...     for i, baz in enumerate(baz_values):
...         subplots[baz] = fig.add_subplot(gridspec[0, i])
...         subplots[baz].annotate(
...             # Put label in top left corner of subplot
...             f'.baz = {baz!r}', (0, 1),
...             xycoords='axis fraction', va='top',
...         )
...     for (foo, color), (bar, marker), baz in product(
...         enumerate(FOO_COLORS), BAR_MARKERS.items(), baz_values
...     ):
...         query = ' and '.join([
...             f'foo == {foo!r}', f'bar == {bar!r}', 'baz == {baz!r}',
...         ])
...         df.query(query).plot(
...             'x', 'y',
...             linestyle='none',
...             marker=marker,
...             color=color,
...             label=f'.foo = {foo!r}, .bar = {bar!r}',
...             ax=subplots[baz],
...         )
...
>>> make_categorized_subplots(make_dataframe())  # doctest: +SKIP

Here each row is put into a different subplot based on its `.baz`, and
formatted into a marker based on its `.foo` and `.bar`.
Again, this motivates utilities for ingesting, categorizing, and
partitioning the data.

DATA FORMATTERS
---------------
Functions taking CATEGORIZED KEYWORD ARGUMENTS and returning either
mapping of `Axes`-method keyword arguments, or a sequence of such
mappings.

When plotting with a CL METHOD, the keyword arguments are fed to the
DATA FORMATTER of the method (or the DEFAULT DATA FORMATTER).
The DATA FORMATTER then transforms the keyword arguments received into
keyword arguments for the underlying `matplotlib.axes.Axes` method,
which then makes the plot.

A DATA FORMATTER should be a hashable callable, which accepts ONLY:

- CATEGORIZED KEYWORD ARGUMENTS (CKAs), and

- A variable-length `kwargs` optionally for non-CKA keyword arguments,
  which may additionally affect the processed keyword arguments (not
  recommended);
  If not provided, any non-CKA keyword argument is passed directly to
  the `Axes` method underlying the CL METHOD, and will have precedence
  over the keyword arguments generated by the formatter.

When called with multiple CKAs, the DATA FORMATTER should collate the
`Axes` method keyword arguments generated by each argument, and return
them as either a mapping of keyword arguments for its corresponding base
`Axes` method, or a sequence of such mappings.

If a single mapping is returned:
    The `Axes` method is called with those keyword arguments when
    plotting.

If a sequence of mappings are returned:
    The `Axes` method is called once with each mapping of keyword
    arguments to plot overlapping/'composite' artists.

CATEGORIZED KEYWORD ARGUMENTS (CKAs)
------------------------------------
Non-variable-length keyword arguments of a DATA FORMATTER.

Each CKA should:

- Be keyword-only and with hashable values;

- Permit omission by supplying a reasonable default, representing
  neutrality/undefinedness regarding the category; and

- For each possible value, generate a mapping of keyword arguments (or a
  sequence of such mappings) for the `Axes` method, which overlap with
  those generated by other CKAs as little as possible (i.e. individual
  CKAs should control separate parts of the plotting style).

Examples
--------

>>> import matplotlib.pyplot as plt
>>> import _MODULE_ as cl

Defining data formatters:

(No var-kwargs -> CL method accepts var-kwargs, which have precedence
over data-formatter-generated values)
>>> colorizer1 = lambda *, foo=0, bar=0: dict(
...     marker='o',
...     markerfacecolor='rgb'[foo],
...     markeredgecolor='rgb'[bar],
... )

(Accepting var-kwargs -> var-kwargs received by CL method provided and
transformed by the formatter)
>>> colorizer2 = lambda *, foo=0, bar=1, **kwargs: dict(
...     # This does the same thing as colorizer1() but silently discards
...     # any extra kwargs received
...     colorizer1(foo=foo, bar=bar)
... )

(Returning sequence of keyword mappings -> CL method invokes base method
once for each of the mappings)
>>> colorizer3 = lambda *, foo=0, bar=0, baz=0, **kwargs: [
...     # This does the same thing as colorizer1(), except that foo is
...     # scaled by a factor of 2
...     dict(kwargs, **colorizer1(foo=(2 * foo), bar=bar)),
...     # This then plots a '3' on top of the original marker
...     dict(
...         marker='$3$',
...         markerfacecolor='rgbk'[baz],
...         markeredgecolor=([0] * 4),
...     )
... ]

Basic usage:

>>> fig = cl.Figure(
...     # Make a pyplot-managed figure
...     instantiator=plt.figure,
...     # This sets the default formatter for all CL methods
...     default_cl_formatter=colorizer1,
...     # Register Axes.cl_plot() as wrapper over Axes.plot()
...     cl_plot=None,
... )
>>> ax1 = fig.gca()
>>> x1, x2, y1, y2 = [1, 2], [3, 4], [5, 6], [7, 8]
>>> ax1.cl_plot(  # doctest: +ELLIPSIS
...     x1, y1, foo=2, linestyle='-', marker='H',
... )
[<matplotlib.lines.Line2D object ...>]
>>> ax1.cl_plot(x2, y2, foo=0)  # doctest: +ELLIPSIS
[<matplotlib.lines.Line2D object ...>]

(The above is roughly equivalent to:)
>>> ax1.plot(  # doctest: +SKIP
...     x1, y2, **dict(colorizer1(foo=2), marker='H', linestyle='-'),
... )
>>> ax1.plot(x2, y2, **colorizer1(foo=0))  # doctest: +SKIP

Assigning a CL-method-specific data formatter:

>>> fig.register_cl_method(  # doctest: +ELLIPSIS
...     # Only set for .cl_axvline()
...     'cl_axvline', colorizer2,
... )
<function <lambda> at ...>
>>> ax2 = fig.add_subplot(211)
>>> ax2.cl_axvline(x1[0], foo=2, linestyle='--')  # doctest: +ELLIPSIS
<matplotlib.lines.Line2D object ...>

(The above is roughly equivalent to:)
>>> ax2.axvline(  # doctest: +SKIP
...     x1[0], **colorizer2(foo=2, linestyle='--'),
... )

Assigning an axes-specific data formatter:

>>> ax3 = fig.add_subplot(212)
>>> ax3.default_cl_formatter = colorizer3  # Only set for ax3
>>> ax3.cl_plot(  # doctest: +ELLIPSIS, +NORMALIZE_WHITESPACE
...     x1, y1, foo=1, bar=2, baz=3,
... )
[[<matplotlib.lines.Line2D object ...>],
 [<matplotlib.lines.Line2D object ...>]]

(The above is roughly equivalent to:)
>>> [  # doctest: +SKIP
...     ax3.plot(x1, y1, **kwargs)
...     for kwargs in colorizer3(foo=1, bar=2, baz=3)
... ]

Setting default data formatters:

(Definition at object instantiation)
>>> def formatter_1(*, foo=None, bar=None, **kwargs):
...     return []
...
>>> new_fig = cl.Figure(default_cl_formatter=formatter_1)
>>> new_fig.default_cl_formatter is formatter_1
True

(Decoration)
>>> @new_fig.set_default_cl_formatter
... def formatter_2(*, foo=None, bar=None, **kwargs):
...     return {}
...
>>> new_fig.default_cl_formatter is formatter_2
True

(Attribute assignment)
>>> new_fig.default_cl_formatter = formatter_1
>>> new_fig.default_cl_formatter is formatter_1
True

Registration of CL methods and of their respective formatters:

(Method-based)
>>> def formatter_3(*, foo=None, bar=None):
...     return {}
>>> new_fig.register_cl_method(  # doctest: +ELLIPSIS
...     'cl_scatter', formatter_3,
... )
<function formatter_3 ...>
>>> new_fig.cl_method_formatters['cl_scatter'] is formatter_3
True

(Decorator-based)
>>> @new_fig.register_cl_method('cl_scatter')
... def formatter_4(*, foo=None, bar=None):
...     return {}
>>> new_fig.cl_method_formatters['cl_scatter'] is formatter_4
True

Retrieval of CKAs:

>>> def print_categories(*args, **kwargs) -> None:
...     categories = fig.get_cl_categories(*args, **kwargs)
...     for category, values in sorted(categories.items()):
...         print(f'{category}: {sorted(values)}')
>>> print_categories()
bar: [2]
baz: [3]
foo: [0, 1, 2]
>>> print_categories(formatter=colorizer3)  # Select by data formatter
bar: [2]
baz: [3]
foo: [1]
>>> print_categories(ax=ax1)  # Select by axes
foo: [0, 2]
>>> print_categories(cl_method='cl_axvline')  # Select by method
foo: [2]

Making a CL:

(This will look something like
+-----------------------------------------------------------------+
| <handle colorizer1(foo=0)>, <handle colorizer1(foo=2)>    0, 2  |
|               <handle colorizer2(foo=2)>                   2    |
|               <handle colorizer3(foo=1)>                   1    |
|                                                                 |
|               <handle colorizer3(bar=2)>                BAR = 2 |
|                                                                 |
|               <handle colorizer3(baz=2)>                baz = 2 |
+-----------------------------------------------------------------+

>>> fig.categorized_legend(  # doctest: +ELLIPSIS
...     # `bar` and `baz` appear with their category labels
...     show_category_names=dict.fromkeys(('bar', 'baz'), True),
...     # Sort categories the order "foo" -> "bar" -> "baz" instead of
...     # alphabetically
...     category_name_sorter=('foo', 'bar', 'baz').index,
...     # The category "bar" is typeset into "BAR"
...     category_name_typesetter=dict(bar='BAR'),
...     # Collate at most three values together in a single legend entry
...     values_per_line=3,
... )
<matplotlib.legend.Legend object ...>

(Close the spawned figure to be nice to `matplotlib`:)

>>> plt.close(fig)
"""
from .core import (
    CategorizedLegendFigure as Figure, CategorizedLegendAxes as Axes
)
from .plot import plot_categorized
from .recipes import register_cl_recipe, get_cl_recipes

__doc__ = __doc__.replace('_MODULE_', Figure._get_cl_module_name())

__all__ = (
    'Figure', 'Axes',
    'plot_categorized',
    'register_cl_recipe', 'get_cl_recipes',
)
