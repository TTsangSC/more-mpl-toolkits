"""
`matplotlib.axes.Figure` subclass.
"""
import functools
import inspect
import numbers
import typing

from matplotlib.axes import Axes
from matplotlib.legend import Legend

import tjekmate

from .._deprecations import deprecate
from . import axes, recipes, mixin, utils, wrapper_proxy, _types, _utils

__all__ = ('CategorizedLegendFigure',)

resolve_cl_aliased_args = _utils.resolve_cl_aliased_args

CategorizedLegendFigure = typing.TypeVar(
    'CategorizedLegendFigure', bound='CategorizedLegendFigure',
)


def _filter_figure_init_kwargs(kwargs: _types.Keywords) -> _types.Keywords:
    """
    Filter for the proper initialization keyword arguments for the
    wrapped figure class.
    """
    kwargs, _ = CategorizedLegendFigure._init_kwarg_helper(kwargs)
    return kwargs


@mixin.CategorizedLegendMixin.inject
class CategorizedLegendFigure(
    _types.FigureBase,
    metaclass=wrapper_proxy.WrapperProxyMeta,
    default_instantiator=_types.Figure,
    filter_kwargs=_filter_figure_init_kwargs,
    deferred_attrs=('zorder',),
):
    """
    A `matplotlib.figure.Figure[Base]` class enabling the easy creation
    of CATEGORIZED LEGENDS (CLs);
    see the module documentation.
    """
    def __init__(self, *args, **kwargs) -> None:
        """
        Initialize the instance.

        Parameters
        ----------
        *args
            Ignored
        **kwargs
            Suitable arguments extracted and used where appropriate:
            default_cl_formatter()
                Optional data formatter for setting the
                `.default_cl_formatter`
            cl_plot(), cl_axhline(), cl_scatter(), ...
                Optional formatters to register to the eponymous CL
                methods
        """
        # Upgrade the axes stack so that all axes added to/spawned by
        # the figure are CL axes
        wrapped = wrapper_proxy._get_wrapped_instance(self)
        if not isinstance(wrapped._axstack, axes.CategorizedLegendAxesStack):
            axes.CategorizedLegendAxesStack._replace_stack(self)
        # Replace .figure with self if .figure is the wrapped instance
        self._replace_wrapped_instance_generic(
            self, ('_parent', '_figure', 'figure'),
        )
        # Register formatters and CL methods
        _, kwargs = self._init_kwarg_helper(kwargs)
        if 'default_cl_formatter' in kwargs:
            self.default_cl_formatter = kwargs.pop('default_cl_formatter')
        for cl_method, formatter in kwargs.items():
            self.register_cl_method(cl_method, formatter)

    @classmethod
    def _init_kwarg_helper(
        cls, kwargs: _types.Keywords
    ) -> typing.Tuple[_types.Keywords, _types.Keywords]:
        """
        Helper function for `.__new__()` and `.__init__()`.

        Parameters
        ----------
        kwargs
            Keyword mapping

        Return
        ------
        Tuple of two keyword mappings;
        the first goes into the regular `matplotlib.figure.Figure`,
        while the second provides functionality similar to that of
        `.wrap()`
        """
        prefix = cls.cl_method_prefix
        ax_methods = recipes.CategorizedLegendRecipe.get_cl_compatible_methods()
        cl_arguments = (
            {'default_cl_formatter'} |
            {prefix + ax_method for ax_method in ax_methods}
        )
        vanilla_kwargs: _types.Keywords = {}
        wrapper_kwargs: _types.Keywords = {}
        for key, value in kwargs.items():
            (
                wrapper_kwargs if key in cl_arguments else vanilla_kwargs
            )[key] = value
        return vanilla_kwargs, wrapper_kwargs

    @_utils.copy_annotations_from(
        mixin.CategorizedLegendMixin._find_cl_children_at,
    )
    def get_cl_children(
        self, *args, **kwargs
    ):
        """
        List all the child figures and/or axes relevant to making a
        categorized legend.

        Parameters
        ----------
        *args, **kwargs
            Passed to `.get_cl_children()` of child objects
        max_level
            Optional limit for how many levels of children to descend
            into;
            the default `None` indicates that there is no limit

        Return
        ------
        >>> list[  # noqa # doctest: +SKIP
        ...     # Each list represents a level
        ...     list[axes_or_figure]
        ... ]

        Notes
        -----
        At the moment, `*args` and `**kwargs` has no effect;
        this may change in a future version.
        """
        # Note: since .axes of subfigures are registered to the parent's
        # ._axstack, we need to explicitly filter for them here and only
        # include the IMMEDIATE descendents (i.e. `.figure is self`)
        return self._find_cl_children_at(
            *args,
            **dict(
                kwargs,
                # Locate all child axes and subfigures
                _locations=('axes', 'subfigs'),
                # Check parentage of child axes (and subfigures)
                _immediate_child_parentage_loc='figure',
            ),
        )

    # ********** Get categories from child axes/subfigures *********** #

    @typing.overload
    def get_cl_categories(
        self,
        *args,
        concise: typing.Literal[True] = True,
        **kwargs
    ) -> typing.Dict[
        _types.CategorizedKeyword, typing.Set[_types.Categorization]
    ]:
        ...

    @typing.overload
    def get_cl_categories(
        self,
        *args,
        concise: typing.Literal[False] = False,
        **kwargs
    ) -> typing.Dict[
        typing.Tuple[
            _types.CategorizedKeyword,
            _types.CLMethodName,
            _types.CategoryFormatter,
        ],
        typing.Set[_types.Categorization]
    ]:
        ...

    @deprecate.cla_clf_method_pos_args
    @_utils.insert_kwargs_from(axes.CategorizedLegendAxes.get_cl_categories)
    def get_cl_categories(
        self,
        ax: typing.Optional[
            typing.Union[
                axes.CategorizedLegendAxes,
                CategorizedLegendFigure,
                typing.Collection[
                    typing.Union[
                        axes.CategorizedLegendAxes, CategorizedLegendFigure,
                    ]
                ],
            ]
        ] = None,
        cl_method: typing.Optional[
            typing.Union[
                _types.CLMethodName, typing.Collection[_types.CLMethodName],
            ]
        ] = None,
        formatter: typing.Optional[
            typing.Union[
                _types.CategoryFormatter,
                typing.Collection[_types.CategoryFormatter],
            ]
        ] = None,
        *,
        max_level: typing.Union[numbers.Integral, bool, None] = False,
        **kwargs
    ) -> typing.Union[
        typing.Dict[
            _types.CategorizedKeyword, typing.Set[_types.Categorization]
        ],
        typing.Dict[
            typing.Tuple[
                _types.CategorizedKeyword,
                _types.CLMethodName,
                _types.CategoryFormatter,
            ],
            typing.Set[_types.Categorization]
        ],
    ]:
        """
        Search for and get all the values encountered so far for the
        categories.

        Parameters
        ----------
        ax (alias: subfig)
            Optional
            `<MODULE>.Axes`
            and/or
            `<MODULE>.Figure`
            instance(s) that is/are descendent(s) of the figure
            (inclusive);
            if supplied, start the search for values therefrom;
            default is start the search from the figure itself
        max_level
            True or None
                Look for values recursively in `ax`, its/their child
                axes and subfigures (from `.get_cl_children()`), and
                their child axes and subfigures, etc.
            False (default) or 0
                Only look at values encountered in CL methods bound to
                `ax` (if an axes object) or its immediate child-axes
                objects (if a figure)
            Non-boolean integer
                Include values in (up to) this many further levels of
                child axes and subfigures of `ax`
        other arguments
            See the documentation for
            `<MODULE>.Axes.get_cl_categories()`

        Return
        ------
        concise = True

            >>> dict[  # noqa # doctest: +SKIP
            ...     category, set[value_encountered]
            ... ];

        else

            >>> dict[  # noqa # doctest: +SKIP
            ...     tuple[category, cl_method, formatter],
            ...     set[value_encountered]
            ... ].

        Notes
        -----
        - If `ax`/`subfig` is supplied and it is/they are not among the
          descendents (inclusive) of the `Figure` instance, a
          `ValueError` is raised.
        """
        CHILDREN_CLASSES = axes.CategorizedLegendAxes, CategorizedLegendFigure
        Child = typing.Union[CHILDREN_CLASSES]

        def is_descendent(ax_fig: typing.Union[Child, None]) -> bool:
            """
            Check whether `ax_fig` is a descendent of `self` (inclusive)
            """
            if ax_fig is self:
                return True
            if isinstance(ax_fig, axes.CategorizedLegendAxes):
                return is_descendent(getattr(ax_fig, 'figure', None))
            if isinstance(ax_fig, CategorizedLegendFigure):
                return any(
                    is_descendent(getattr(ax_fig, attr, None))
                    for attr in ('_parent', 'figure')
                )
            return False

        # Resolve alias
        ax_param_name: _types.Keyword
        if ax is not None:  # Non-default value supplied for ax
            ax_param_name = 'ax'
        elif 'subfig' in kwargs:  # Alias supplied for ax
            ax_param_name = 'subfig'
        else:
            ax_param_name = 'ax'
        kwargs = resolve_cl_aliased_args(
            dict(
                kwargs,
                ax=ax, cl_method=cl_method, formatter=formatter,
                max_level=max_level,
            ),
            defaults={
                param.name: param.default
                for param in (
                    inspect.signature(self.get_cl_categories)
                    .parameters.values()
                )
                if param.default is not param.empty
            },
            resolve=dict(subfig='ax'),
        )
        ax, cl_method, formatter, max_level = (
            kwargs.pop(name)
            for name in ('ax', 'cl_method', 'formatter', 'max_level')
        )
        # Type checks
        if ax is None:
            pass
        elif not isinstance(ax, typing.Collection):
            ax = ax,
        if ax is not None:
            if not all(isinstance(ax_fig, CHILDREN_CLASSES) for ax_fig in ax):
                raise TypeError(
                    f'{ax_param_name} = {ax!r}: '
                    f'expected `{self._get_cl_module_name()}.Axes` and/or '
                    '`.Figure` instance(s)'
                )
            if not all(is_descendent(ax_fig) for ax_fig in ax):
                raise ValueError(
                    f'{ax_param_name} = {ax!r}: '
                    f'not `{self._get_cl_module_name()}.Axes` and/or `.Figure` '
                    f'instance(s) belonging to the figure {self!r} (inclusive)'
                )
        if tjekmate.is_strict_boolean(max_level):
            max_level = None if max_level else 0
        if not (max_level is None or isinstance(max_level, numbers.Integral)):
            raise TypeError(
                f'max_level = {max_level!r}: expected an integer or boolean'
            )
        # Resolve/Refine ax: where do we start to look for values in?
        lookup_locs: typing.Iterable[Child]
        if ax is None:
            lookup_locs = self,
        else:
            lookup_locs = utils.unique(ax)
        # Actual work: collect child axes, then collect values therefrom
        results = {}
        for key, values in (
            kvp for child_ax in utils.unique(
                ca for ax_fig in lookup_locs
                for ca in self._get_cl_axes_descendents(ax_fig, max_level)
            )
            # Note: descendent `Axes` lookup is done in the above
            # iterator, so we no longer have to descend into child
            # objects
            for kvp in child_ax.get_cl_categories(
                cl_method=cl_method, formatter=formatter, max_level=False,
                **kwargs,
            ).items()
        ):
            results.setdefault(key, set()).update(values)
        return results

    @staticmethod
    def _get_cl_axes_descendents(
        ax_fig: typing.Union[
            axes.CategorizedLegendAxes, CategorizedLegendFigure,
        ],
        max_level: typing.Union[numbers.Integral, None],
    ) -> typing.List[axes.CategorizedLegendAxes]:
        result: typing.List[axes.CategorizedLegendAxes] = []
        is_axes = isinstance(ax_fig, axes.CategorizedLegendAxes)
        if is_axes:
            result.append(ax_fig)
        descendent_levels = ax_fig.get_cl_children(max_level=max_level)
        try:
            *_, last_level = descendent_levels
        except ValueError:  # No descendents
            if is_axes:
                return result
            else:
                last_level = [ax_fig]
        # Include immediate child axes of subfigures in the last level
        extension_level: typing.List[
            typing.Union[axes.CategorizedLegendAxes, CategorizedLegendFigure]
        ] = []
        for fig in (
            f for f in last_level
            if isinstance(f, CategorizedLegendFigure)
        ):
            try:
                children, = fig.get_cl_children(max_level=1)
            except ValueError:  # No immediate children
                continue
            extension_level.extend(children)
        if extension_level:
            descendent_levels.append(extension_level)
        # Gather all `Axes` objects
        result.extend(
            child_ax for level in descendent_levels for child_ax in level
            if isinstance(child_ax, axes.CategorizedLegendAxes)
        )
        return result

    # ******************* Legend handle wrangling ******************** #

    @deprecate.cla_clf_method_pos_args
    @_utils.insert_kwargs_from(
        mixin.CategorizedLegendMixin._get_cl_handles_labels,
        order=mixin.KEYWORD_ARGUMENT_ORDER,
    )
    @_utils.copy_method_signature_from(get_cl_categories, drop='concise')
    def _get_cl_handles_labels(
        self, *args, **kwargs
    ) -> typing.OrderedDict[
        _types.CategorizedKeyword,
        typing.List[typing.Tuple[_types.LegendHandle, str]]
    ]:
        cl_categories = self.get_cl_categories(*args, concise=False, **kwargs)
        return mixin.CategorizedLegendMixin._get_cl_handles_labels(
            self, cl_categories, self, **resolve_cl_aliased_args(kwargs)
        )

    @deprecate.cla_clf_method_pos_args
    @_utils.insert_kwargs_from(
        mixin.CategorizedLegendMixin._categorized_legend,
        order=mixin.KEYWORD_ARGUMENT_ORDER,
    )
    @_utils.copy_method_signature_from(_get_cl_handles_labels)
    def categorized_legend(self, *args, **kwargs) -> Legend:
        return self._categorized_legend(
            *args, **resolve_cl_aliased_args(kwargs)
        )

    categorized_legend.__doc__ = (
        axes._CATEGORIZED_LEGEND_DOC_TEMPLATE
        .replace('<BASIC_PARAMS>', 'ax, cl_method, formatter(), max_level')
        .replace('<OBJ_CLASS>', 'Figure')
        .replace('<OBJ_NAME>', 'fig')
    )

    # ************************ Helper methods ************************ #

    def _wrap_axes(self, ax: Axes) -> axes.CategorizedLegendAxes:
        """
        Wrap an `Axes` object and return it, supplying appropriate
        defaults from the figure's remembered settings.
        """
        def iter_figure_chain(
            fig: _types.Figure = self,
        ) -> typing.Generator[_types.Figure, None, None]:
            while fig is not None:
                yield fig
                fig = getattr(fig, 'figure', None)

        ax = axes.CategorizedLegendAxes.wrap(
            ax,
            default_cl_formatter=self.default_cl_formatter,
            **self.cl_method_formatters
        )
        ax_fig = ax.figure
        wrapped = wrapper_proxy._get_wrapped_instance(self)
        if not any(
            fig is self or fig is wrapped
            for fig in utils.unique(
                iter_figure_chain(ax_fig), on_duplicate='stop',
            )
        ):
            raise AssertionError(
                f'ax.figure = {ax_fig!r}: '
                'neither the object, one of its child figures, or its '
                'wrapped figure'
            )
        if ax_fig is wrapped or ax_fig is self:
            ax.figure = self
        else:
            ax.figure = CategorizedLegendFigure.wrap(ax_fig)
        return ax

    # *********** Wrapped matplotlib.figure.Figure methods *********** #

    if hasattr(_types.Figure, 'add_subfigure'):
        @functools.wraps(_types.Figure.add_subfigure)
        def add_subfigure(self, *args, **kwargs):
            subfig = self.super().add_subfigure(*args, **kwargs)
            self.subfigs.remove(subfig)
            subfig = CategorizedLegendFigure.wrap(
                subfig,
                default_cl_formatter=self.default_cl_formatter,
                **self.cl_method_formatters,
            )
            subfig.figure = self
            self.subfigs.append(subfig)
            return subfig

    @_utils.copy_method_signature_from(_types.Figure._add_axes_internal)
    def _add_axes_internal(self, *args, **kwargs) -> axes.CategorizedLegendAxes:
        """
        Private helper for `.add_axes()` and `.add_subplot()`.
        The base class method does not take the return value from the
        axes stack, so another explicit wrapping is needed to ensure
        that we get a
        `<MODULE>.Axes` object.
        """
        # Note: call signature changed between mpl versions, introspect
        # to get the input axes
        ba = inspect.signature(self._add_axes_internal).bind(*args, **kwargs)
        ax = ba.arguments['ax']
        assert isinstance(ax, Axes)
        ba.arguments['ax'] = self._wrap_axes(ax)
        return self.super()._add_axes_internal(*ba.args, **ba.kwargs)

    __slots__ = mixin.CategorizedLegendMixin.__slots__
