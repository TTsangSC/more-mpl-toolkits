"""
Internal utilities
"""
import functools
import importlib
import inspect
import numbers
import typing

from . import utils, _types
from tjekmate import (
    identical_to, is_collection, is_mapping, is_instance, is_sequence,
)
from .._utils import decorators

__all__ = (
    'CATEGORIZED_LEGEND_KWARGS_ALIASES',
    'CL_MODULE',
    'load_cl_namespace',
    'set_subpackage_name_in_docs',
    'resolve_cl_aliased_args',
    'insert_kwargs_from',
    'copy_method_signature_from',
    'copy_annotations_from',
    'super_recursion_guard',
)

Params = typing.ParamSpec('Params')
Annotation = typing.TypeVar('Annotation')
ReturnType = typing.TypeVar('ReturnType')

Wrappee = typing.TypeVar('Wrappee')
Wrapper = typing.TypeVar('Wrapper', bound=Wrappee)

FunctionLike = typing.TypeVar(
    'FunctionLike', typing.Callable, staticmethod, classmethod,
)
FunctionLikeNonConstrained = typing.Union[
    typing.Callable, staticmethod, classmethod,
]
FunctionLikeOrType = typing.TypeVar('FunctionLikeOrType', FunctionLike, type)

MAX_NUM_RECURSION_LOOPS = 1

CATEGORIZED_LEGEND_KWARGS_ALIASES: typing.Dict[
    _types.Keyword, _types.Keyword
] = dict(
    label_sorters='category_value_sorters',
    label_typesetters='category_value_typesetters',
    label_classifications='category_value_classifications',
    category_sorter='category_name_sorter',
    category_typesetter='category_name_typesetter',
    show_categories_with_labels='show_category_names',
    labels_per_line='values_per_line',
    # Note: in newer matplotlib versions 'ncols' is the preferred one;
    # but since it is downright unusable in older versions, we always
    # unalias to the old spelling 'ncol' for compatibility
    ncols='ncol',
    nrows='ncol',
    nrow='ncol',
)

CL_MODULE, *_ = (lambda: None).__module__.rpartition('.')

insert_kwargs_from = decorators.insert_kwargs_from
copy_method_signature_from = decorators.copy_method_signature_from
copy_annotations_from = decorators.copy_annotations_from


def set_subpackage_name_in_docs(arg: FunctionLikeOrType) -> FunctionLikeOrType:
    """
    Convenience function to replace occurrences of '<MODULE>' or
    '_MODULE_' in docstrings with the name of the subpackage.
    """
    return decorators.replace_in_docs(
        arg, dict.fromkeys(('<MODULE>', '_MODULE_'), CL_MODULE),
    )


@set_subpackage_name_in_docs
def load_cl_namespace(path: str) -> typing.Any:
    """
    Dynamically load other submodules and/or objects therein.

    Example
    -------
    >>> from _MODULE_ import utils as mod1
    >>> from _MODULE_.utils import is_identifier as func1
    >>> mod2 = load_cl_namespace('utils')
    >>> func2 = load_cl_namespace('utils.is_identifier')
    >>> assert mod1 is mod2
    >>> assert func1 is func2
    """
    module_path, _, attr = path.rpartition('.')
    if module_path:
        full_path = f'{CL_MODULE}.{module_path}'
    else:
        full_path = CL_MODULE
    module = importlib.import_module(full_path)
    return getattr(module, attr)


@set_subpackage_name_in_docs
def resolve_cl_aliased_args(
    kwargs: _types.Keywords,
    defaults: typing.Optional[_types.Keywords] = None,
    resolve: typing.Optional[
        typing.Mapping[_types.Keyword, _types.Keyword]
    ] = None,
    update_defaults: bool = True,
    update_resolve: bool = True,
) -> _types.KeywordDict:
    """
    Resolve argument names for compatibility.

    Parameters
    ----------
    kwargs
        Keyword mapping
    defaults
        Optional mapping from keywords to default values;
        default is to use the default values of
        `<MODULE>.Figure.categorized_legend()`
    resolve
        Optional mapping from old keywords/aliases to the resolved
        keywords;
        default is to use
        `<MODULE>.core.CATEGORIZED_LEGEND_KWARGS_ALIASES`
    update_defaults, update_resolve
        Whether the supplied mappings for `defaults` and `resolve` are
        to supplement (if true) or supersede (if false) their respective
        defaults

    Return
    ------
    Keyword mapping, where old names/aliases are replaced by new ones
    (resp. the keys and values of the
    `<MODULE>.core.CATEGORIZED_LEGEND_KWARGS_ALIASES`
    dictionary)

    Notes
    -----
    Rather than being used to supplying the defaults (which a function
    can already do on its own), here they are used for detecting whether
    a non-default value has been passed to the alias of an argument;
    if the canonical argument has its default value but not its alias,
    the value from the alias is allowed to override the default value.
    Otherwise, in case of a collision, the value at the canonical
    argument takes priority.
    """
    if not utils.is_keyword_mapping(kwargs):
        raise TypeError(f'kwargs = {kwargs!r}: not a keyword mapping')
    fig_cls = (
        importlib.import_module(f'{CL_MODULE}.figure').CategorizedLegendFigure
    )
    default_defaults = {
        name: param.default
        for name, param in inspect.signature(
            fig_cls.categorized_legend,
        ).parameters.items()
        if param.default is not param.empty
    }
    default_resolve = CATEGORIZED_LEGEND_KWARGS_ALIASES
    if defaults is None:
        defaults = default_defaults
    elif update_defaults:
        defaults = {**default_defaults, **defaults}
    if resolve is None:
        resolve = default_resolve
    elif update_resolve:
        resolve = {**default_resolve, **resolve}
    if not all(utils.is_identifier(v) for v in resolve.values()):
        raise TypeError(
            f'resolve = {resolve!r}: not a mapping from keywords to keywords'
        )
    processed_kwargs = dict(kwargs)
    for old, new in resolve.items():
        if old not in processed_kwargs:
            continue
        # Remove old value no matter what;
        # If the new value isn't specified (or is same as the default),
        # replace with the old one
        value = processed_kwargs.pop(old)
        if (
            new in defaults and
            processed_kwargs.get(new, defaults[new]) == defaults[new]
        ):
            processed_kwargs[new] = value
        elif new not in defaults and new not in processed_kwargs:
            processed_kwargs[new] = value
    return processed_kwargs


def super_recursion_guard(
    func: typing.Optional[typing.Callable[Params, ReturnType]] = None,
    *,
    max_loops: numbers.Integral = MAX_NUM_RECURSION_LOOPS,
) -> typing.Union[
    typing.Callable[Params, ReturnType],
    typing.Callable[
        [typing.Callable[Params, ReturnType]],
        typing.Callable[Params, ReturnType]
    ],
]:
    """
    Decorator for methods using `super()` so that it knows to break
    loops and look further down the MRO.

    Parameters
    ----------
    func()
        Method (callable) to decorate
    max_loops
        Maximum number of loops permitted

    Return
    ------
    func() provided
        Wrapped function
    else
        Decorator

    Notes
    -----
    The call stack is examined to catch repeated invocations of the
    method, and the innermost call is redirected to one of the
    base-class implementations.
    """
    frame_locals_preserved_names = (
        'nloops', 'excluded_base_classes', 'mro', 'impls_and_origins',
    )
    if func is None:
        return functools.partial(super_recursion_guard, max_loops=max_loops)
    if not (isinstance(max_loops, numbers.Integral) and max_loops > 0):
        raise TypeError(
            f'max_loops = {max_loops!r}: expected a positive integer'
        )

    def is_impl_origin_pair(
        impl_origin: typing.Tuple[typing.Any, typing.Any],
    ) -> bool:
        impl, origin = impl_origin
        return callable(impl) and isinstance(origin, type)

    def super_recursion_guard_wrapper(
        instance_or_class: typing.Union[typing.Type[Wrapper], Wrapper],
        *args,
        **kwargs
    ):
        # Put reference to func() in the local scope
        unbound_method = func
        # Note: we're using the decorator when defining the
        # CategorizedLegendMixin class, so the resolution of references
        # to said class must be deferred to execution time
        mixin_cls = load_cl_namespace('mixin.CategorizedLegendMixin')
        frame_checkers: typing.Dict[
            _types.Keyword,
            typing.Tuple[typing.Callable[[typing.Any], bool], bool]
        ] = dict(
            nloops=(is_instance(int), False),
            unbound_method=(identical_to(unbound_method), False),
            instance_or_class=(identical_to(instance_or_class), False),
            excluded_base_classes=(
                (is_instance(set) & is_collection(item_type=type)), False,
            ),
            mro=(
                is_mapping(
                    key_checker=is_instance(type),
                    value_checker=is_instance(int),
                ),
                True,
            ),
            impls_and_origins=(
                is_sequence(
                    item_checker=(is_sequence(length=2) & is_impl_origin_pair),
                ),
                True,
            ),
        )
        get_base_implementations = functools.partial(
            (
                mixin_cls
                .injectables['_get_base_class_implementations']
                .__func__
            ),
            method=func.__name__,
            bound_desc=True,
        )

        def is_recursed_frame(frame_info: inspect.FrameInfo) -> bool:
            if frame_info.function != wrapper_name:
                return False
            if frame_info.filename != __file__:
                return False
            # Check local names
            f_locals = frame_info.frame.f_locals
            for name, (checker, allow_absence) in frame_checkers.items():
                if not (allow_absence or name in f_locals):
                    return False
                if name not in f_locals:
                    continue
                if not checker(f_locals[name]):
                    return False
            return True

        # Get most-immediate upstreamm frame calling on the same method
        call_stack = inspect.stack()
        nloops = 0
        excluded_base_classes: typing.Set[type] = set()
        upstream_frame_locals = next(
            (
                frame_info.frame.f_locals for frame_info in call_stack[1:]
                if is_recursed_frame(frame_info)
            ),
            {},
        )
        # Avoid keeping frame objects alive beyond usefulness
        del call_stack
        upstream_frame_locals = {
            name: value for name, value in upstream_frame_locals.items()
            if name in frame_locals_preserved_names
        }
        if upstream_frame_locals:
            nloops = upstream_frame_locals['nloops'] + 1
            excluded_base_classes.update(
                upstream_frame_locals['excluded_base_classes']
            )
            too_many_loops = nloops > max_loops
        else:
            too_many_loops = False

        # Perform base-class method lookup if necessary
        if isinstance(instance_or_class, type):  # Class method
            cls, inst = instance_or_class, None
        else:
            cls, inst = type(instance_or_class), instance_or_class
        excluded_base_classes.add(cls)
        if too_many_loops:
            # Loop detected, fall back to base-class implementation
            if 'mro' in upstream_frame_locals:
                mro = upstream_frame_locals['mro']
            else:
                mro = {base_cls: i for i, base_cls in enumerate(cls.mro())}
            if 'impls_and_origins' in upstream_frame_locals:
                impls_and_origins = upstream_frame_locals['impls_and_origins']
            else:
                impls_and_origins = [
                    (method, base_cls)
                    for base_cls, method
                    in get_base_implementations(cls, instance=inst)
                    if callable(method)
                ]
            max_excluded_mro = max(mro[cls] for cls in excluded_base_classes)
            base_method, base_cls = next(
                (
                    (method, base_cls)
                    for method, base_cls in impls_and_origins
                    if mro[base_cls] > max_excluded_mro
                ),
                (None, None),
            )
            if base_method is base_cls is None:
                msg = (
                    '{}(): '
                    'maximum recursion count {!r} for the method exceeded; '
                    'lookup for base-class implementations among these also '
                    'failed to resolve the recursive loop: {!r}'
                ).format(
                    unbound_method.__qualname__,
                    max_loops,
                    {origin: impl for impl, origin in impls_and_origins},
                )
                raise RecursionError(msg)
            excluded_base_classes.add(base_cls)
            return base_method(*args, **kwargs)
        # Use the provided (default) implementation if not in recursion
        return unbound_method(instance_or_class, *args, **kwargs)

    wrapper_name = super_recursion_guard_wrapper.__name__
    return functools.wraps(func)(super_recursion_guard_wrapper)
