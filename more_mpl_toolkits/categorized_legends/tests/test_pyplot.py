"""
Test our replacement of `matplotlib.pyplot`.
"""
import functools
import typing

import matplotlib
import numpy as np
from matplotlib import pyplot as orig_plt

from .. import pyplot as plt, Axes, Figure
from .._types import Keyword, Keywords
from .utils import parametrize_all

MethodResult = typing.TypeVar('MethodResult')
PS = typing.ParamSpec('PS')
T = typing.TypeVar('T')
Callable = typing.Callable[PS, T]

# ************************* Helper functions ************************* #


def close_figs(func: Callable) -> Callable:
    """
    Decorator for closing any newly-created figures.
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        fig_nums_orig, fig_nums_copy = (
            set(module.get_fignums())
            for module in (orig_plt, plt)
        )
        try:
            result = func(*args, **kwargs)
        finally:
            for module, existing_fig_nums in [
                (orig_plt, fig_nums_orig), (plt, fig_nums_copy),
            ]:
                new_figs = set(module.get_fignums()) - existing_fig_nums
                for fig_id in new_figs:
                    try:
                        module.close(fig_id)
                    except Exception:
                        pass
        return result

    return wrapper


def gather_subplots_results(
    results: typing.Tuple[Figure, typing.Union[Axes, np.ndarray]]
) -> typing.Sequence[typing.Union[Figure, Axes]]:
    fig, subplots = results
    if hasattr(subplots, 'flatten'):
        return (fig, *subplots.flatten())
    return fig, subplots


def gather_plot_results(
    results: typing.List[matplotlib.lines.Line2D],
) -> typing.Tuple[Figure, Axes]:
    line, *_ = results
    return line.figure, line.axes


def gather_gca_results(
    results: Axes,
) -> typing.Tuple[Figure, Axes]:
    return results.figure, results


# ****************************** Tests ******************************* #


@parametrize_all(('subplots',), ('gcf',), ('plot',), ('sca',), ('get_fignums',))
def test_attr_retrieval(attr: Keyword) -> None:
    """
    Test that attributes are correctly retrieved from the internal copy
    of `matplotlib.pyplot`.
    """
    value = getattr(plt, attr, None)
    assert callable(value), attr
    assert value is not getattr(orig_plt, attr)


@parametrize_all(('gcf',), ('figure',))
@close_figs
def test_fig_registry_independence(method: Keyword) -> None:
    """
    Test that figures created in the drop-in replacement are stored in
    a separate registry from `matplotlib.pyplot`.
    """
    for module, other in (plt, orig_plt), (orig_plt, plt):
        other_fignums = list(other.get_fignums())
        fig = getattr(module, method)()
        assert fig.number in module.get_fignums()
        assert len(other.get_fignums()) == len(other_fignums)


@parametrize_all(
    ('gcf',),
    ('gca', None, None, gather_gca_results),
    ('figure',),
    ('subplots', (2, 2), None, gather_subplots_results),
    ('plot', ([1, 2], [3, 4]), dict(marker='o'), gather_plot_results),
)
@close_figs
def test_cl_object_creation(
    method: Keyword,
    args: typing.Union[typing.Sequence, None],
    kwargs: typing.Union[Keywords, None],
    get_cl_objects: typing.Union[
        typing.Callable[
            [MethodResult], typing.Collection[typing.Union[Axes, Figure]]
        ],
        None,
    ],
) -> None:
    """
    Test that figures and axes created are `~.{Figure|Axes}` instances.
    If `get_cl_objects()` isn't explicitly provided, assume the return
    value of the `method` to be the sole `~.{Figure|Axes}` instance to
    inspect.
    """
    if args is None:
        args = ()
    if kwargs is None:
        kwargs = {}
    if get_cl_objects is None:
        def get_cl_objects(
            result: MethodResult,
        ) -> typing.Tuple[typing.Union[Axes, Figure]]:
            return result,

    result = getattr(plt, method)(*args, **kwargs)
    for obj in get_cl_objects(result):
        assert (
            isinstance(obj, (Axes, Figure))
        ), 'plt.{}(...) = {!r}'.format(method, result)


@close_figs
def test_figure_identity() -> None:
    """
    Test that `fig is plt.figure(fig.number)`.
    """
    fig = plt.figure()
    assert fig is plt.figure(fig.number)
