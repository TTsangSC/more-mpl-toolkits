"""
Test for deprecations that they are working as expected.
"""
import itertools
import re
import typing
try:
    from typing import Literal
except ImportError:
    from typing_extensions import Literal

from .. import Figure
from .utils import is_deprecated, not_deprecated


def test_cla_clf_method_pos_args(
    make_fig: typing.Callable[..., Figure],
) -> None:
    """
    Test for the `cla_clf_method_pos_args` deprecation: the
    `~.{Axes|Figure}` methods `.get_cl_categories()`,
    `.get_cl_handles_labels()`, and `.categorized_legend()` should
    complain when the `ax|subfig`, `cl_method`, and `formatter`
    arguments are passed positionally.
    """
    arg_names = 'ax', 'cl_method', 'formatter'
    ArgName = Literal[arg_names]

    def iter_args_argnames_kwargs() -> typing.Generator[
        typing.Tuple[
            typing.List[None],
            typing.List[ArgName],
            typing.Dict[ArgName, None],
        ],
        None,
        None
    ]:
        for sep in range(len(arg_names) + 1):
            args, kwargs = arg_names[:sep], arg_names[sep:]
            yield (
                [None] * len(args),
                args,
                dict.fromkeys(kwargs),
            )

    fig = make_fig()

    @fig.register_cl_method('cl_plot')
    def formatter(
        *, foo: typing.Optional[int] = None,
    ) -> typing.Dict[str, typing.Any]:
        return {}

    ax = fig.gca()
    ax.cl_plot([0], [0], foo=1)

    for obj, method, (args, arg_names, kwargs) in itertools.product(
        (fig, ax),
        ('get_cl_categories', 'get_cl_handles_labels', 'categorized_legend'),
        iter_args_argnames_kwargs(),
    ):
        bound_method = getattr(obj, method)
        if arg_names:  # Check that the appropriate warning is issued
            warning_text = r',\s*'.join(
                r'{}\s*=\s*{}'.format(name, re.escape(repr(value)))
                for name, value in zip(arg_names, args)
            )
            ctx = is_deprecated(
                match=r'{}\(.*\): {}: '.format(method, warning_text),
                other='ignore',
            )
        else:
            ctx = not_deprecated()
        with ctx:
            bound_method(*args, **kwargs)
