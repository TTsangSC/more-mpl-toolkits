"""
Tests for `~.cl.plot_categorized()`
"""
import ast
import contextlib
import functools
import inspect
import itertools
import numbers
import operator
import os
import re
import typing

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pytest

from tjekmate import (
    is_instance, is_mapping, is_sequence, is_nonnegative_real,
    items_fulfill, le, ne,
)

from .. import Axes, Figure
from ..plot import plot_categorized as _plot_categorized
from ..wrapper_proxy import get_wrapped_instance
from .utils import parametrize_all, apply_typesetter

RNG = 42
NUM_POINTS = 20
LEN_ROW = 3
BAR_VALUES = 0, 1, 2
ANIMAL_VALUES = 'parrot', 'rabbit', 'hamster', 'eel'
DIRECTIONS = 'left', 'right', 'top', 'bottom'
PLENARY_LABEL = '(all)'
AXES_DESIGNATIONS = 'xy', 'x', 'y', True, False, None, 'none'
PLOT_CATEGORIZED_SIG = inspect.signature(_plot_categorized)
CATEGORY_VALUES = dict(foo=[True, False], bar=BAR_VALUES, animal=ANIMAL_VALUES)
CATEGORY_SIZES = {
    cat_name: len(cat_values)
    for cat_name, cat_values in CATEGORY_VALUES.items()
}

PS = typing.ParamSpec('PS')
T = typing.TypeVar('T')
Foo = bool
Bar = typing.Literal[BAR_VALUES]
Animal = typing.Literal[ANIMAL_VALUES]

Identifier = typing.TypeVar('Identifier', bound=str)
Keywords = typing.Mapping[Identifier, typing.Any]
KeywordDict = typing.Dict[Identifier, typing.Any]
CategoryName = typing.TypeVar('CategoryName', bound=Identifier)
CategoryValue = typing.Union[Foo, Bar, Animal]
Sortable = typing.TypeVar('Sortable')
CKAs = typing.ParamSpec('CKAs')
PlenarySubplot = PLOT_CATEGORIZED_SIG.parameters['plenary_subplot'].annotation
PlenaryLabel = typing.TypeVar('PlenaryLabel', bound=typing.Hashable)
Formatter = typing.Callable[
    CKAs, typing.Union[Keywords, typing.Sequence[Keywords]],
]
Typesetter = typing.Union[
    str,
    typing.Mapping[typing.Hashable, str],
    typing.Callable[[typing.Hashable], str],
    None,
]
Sorter = typing.Union[
    typing.Mapping[CategoryValue, Sortable],
    typing.Callable[[CategoryValue], Sortable],
]
MakeFullFigResult = typing.Tuple[
    Figure,
    typing.Union[
        typing.Dict[typing.Union[CategoryValue, PlenaryLabel], Axes],
        typing.Dict[
            typing.Tuple[
                typing.Union[CategoryValue, PlenaryLabel],
                typing.Union[CategoryValue, PlenaryLabel],
            ],
            Axes
        ],
    ],
]
AxesDesignation = typing.Literal[AXES_DESIGNATIONS]


class Categorization(typing.TypedDict, total=False):
    foo: Foo
    bar: Bar
    animal: Animal


Row = typing.Tuple[Categorization, np.ndarray, np.ndarray]

# ***************************** Fixtures ***************************** #


@pytest.fixture
def rows() -> typing.List[Row]:
    """
    Data rows
    """
    rng = np.random.default_rng(RNG)
    foos = rng.choice((True, False), NUM_POINTS)
    bars = rng.choice(BAR_VALUES, NUM_POINTS)
    animals = rng.choice(ANIMAL_VALUES, NUM_POINTS)
    for cat_name, values in dict(foo=foos, bar=bars, animal=animals).items():
        assert len(set(values)) == CATEGORY_SIZES[cat_name], (cat_name, values)
    data = rng.random((NUM_POINTS, LEN_ROW * 2))
    return [
        (dict(foo=foo, bar=bar, animal=animal), xy[:LEN_ROW], xy[LEN_ROW:])
        for foo, bar, animal, xy in zip(foos, bars, animals, data)
    ]


@pytest.fixture
def plot_categorized(
) -> typing.Callable[..., PLOT_CATEGORIZED_SIG.return_annotation]:
    """
    Wrapper around `~.plot_categorized()` which automatically closes the
    spawned figures once the test is over.
    """
    @functools.wraps(_plot_categorized)
    def wrapper(
        *args,
        fig: typing.Optional[matplotlib.figure.Figure] = None,
        **kwargs
    ):
        _fig, subplots = _plot_categorized(*args, fig=fig, **kwargs)
        if fig is None:  # New figure created
            created_figures.append(_fig)
            _fig.__pytest_created_during__ = (
                os.environ.get('PYTEST_CURRENT_TEST')
            )
            _fig.__pytest_created_within__ = (
                '{0.__module__}.{0.__name__}'.format(plot_categorized)
            )
        return _fig, subplots

    created_figures: typing.List[Figure] = []
    yield wrapper

    for fig in created_figures:
        plt.close(fig)


@pytest.fixture
def make_full_figure(
    rows: typing.List[Row],
    plot_categorized: typing.Callable[
        ..., PLOT_CATEGORIZED_SIG.return_annotation
    ],
) -> typing.Callable[..., MakeFullFigResult]:
    """
    Callable which creates full figures with data.
    """
    def _make_full_figure(**kwargs) -> MakeFullFigResult:
        plenary_label = kwargs.get('plenary_label', PLENARY_LABEL)
        fig, subplots = plot_categorized(
            rows,
            **{
                **dict(
                    data_getters=[get_x, get_y],
                    categorizer=get_categorization,
                    formatter=no_op_formatter,
                    plenary_label=PLENARY_LABEL,
                ),
                **kwargs,
            },
        )
        if not isinstance(subplots, typing.Mapping):
            subplots = {plenary_label: subplots}
        return fig, subplots

    yield _make_full_figure
    # Note: when we get back to here we will run plot_categorized()'s
    # teardown


# ************************* Helper functions ************************* #


def no_op_formatter(
    *,
    foo: typing.Optional[Foo] = None,
    bar: typing.Optional[Bar] = None,
    animal: typing.Optional[Animal] = None,
) -> KeywordDict:
    """
    Formatter function which doesn't do any formatting based on the
    arguments.
    """
    return {}


def simple_formatter(
    *,
    foo: typing.Optional[Foo] = None,
    bar: typing.Optional[Bar] = None,
    animal: typing.Optional[Animal] = None,
) -> KeywordDict:
    """
    Formatter function which draw a simple marker based on the provided
    arguments.
    """
    return dict(
        linestyle='none',
        markeredgecolor='black',
        markerfacecoloralt='white',
        fillstyle=('top' if foo else 'full'),
        color=('black' if bar is None else 'rgb'[bar]),
        marker=('o' if animal is None else '${}$'.format(animal[0].upper())),
    )


def complex_formatter(
    *,
    foo: typing.Optional[Foo] = None,
    bar: typing.Optional[Bar] = None,
    animal: typing.Optional[Animal] = None,
) -> typing.List[KeywordDict]:
    """
    Formatter function which draws two markers on top of one another.
    """
    overlay = dict(
        simple_formatter(foo=foo, bar=bar, animal=animal), markersize=6,
    )
    if animal is None:
        return [overlay]
    bg_color, bg_shape = dict(
        parrot=('cyan', 's'),
        eel=('tab:blue', 'o'),
        ).get(animal, ('tab:green', '^'))
    return [
        dict(
            linestyle='none',
            color=bg_color,
            marker=bg_shape,
            markersize=8,
        ),
        overlay,
    ]


def scatter_formatter(
    *,
    foo: typing.Optional[Foo] = None,
    bar: typing.Optional[Bar] = None,
    animal: typing.Optional[Animal] = None,
) -> KeywordDict:
    """
    Formatter function for use with
    `~.plot_categorized(method='scatter')` or
    `~.Axes.cl_scatter()`.
    """
    return dict(
        marker=('o' if animal is None else '${}$'.format(animal[0].upper())),
        edgecolor='black',
        s=((2 if foo else 1) * (matplotlib.rcParams['lines.markersize'] ** 2)),
        c=('black' if bar is None else 'rgb'[bar]),
    )


def get_categorization(row: Row) -> Categorization:
    return row[0]


def get_x(row: Row) -> np.ndarray:
    return row[1]


def get_y(row: Row) -> np.ndarray:
    return row[2]


def get_categorization_from_subplot_key(
    key: typing.Union[
        CategoryValue, typing.Tuple[CategoryValue, CategoryValue],
    ],
    split_x: typing.Optional[CategoryName] = None,
    split_y: typing.Optional[CategoryName] = None,
    plenary_subplot: PlenarySubplot = False,
    plenary_label: PlenaryLabel = PLENARY_LABEL,
) -> Categorization:
    """
    Take the key of the subplot from the return value of
    `make_full_figure()` and return a categorization dict.
    """
    def inner():
        if split_x and split_y and split_x != split_y:
            return dict(zip((split_y, split_x), key))
        split = split_x if split_x else split_y
        if split:
            return {split: key}
        return {}

    result = inner()
    if plenary_subplot in (None, False, 'none'):
        return result
    return {
        key: value for key, value in result.items() if value != plenary_label
    }


def get_sorting_key(
    sorter: typing.Union[Sorter, None],
) -> typing.Callable[[CategoryValue], Sortable]:
    def no_op(value: CategoryValue) -> CategoryValue:
        return value

    if sorter is None:
        return no_op
    if isinstance(sorter, typing.Mapping):
        return functools.partial(operator.getitem, sorter)
    return sorter


def get_subplot_ij(
    subplot: Axes,
) -> typing.Tuple[numbers.Integral, numbers.Integral]:
    _, ncols, start, stop = subplot.get_subplotspec().get_geometry()
    assert start == stop
    return divmod(start, ncols)


def add_ckas_as_label(formatter: Formatter) -> Formatter:
    """
    Make a formatter that adds passed categorized keyword arguments
    (CKAs) to the `label` of the returned keywords.
    If it returns a single keywords mapping, the label is the `repr()`
    of the CKA dictionary;
    if a sequence of keywords mappings, the label is
    `(index, cka_repr)`.
    """
    @functools.wraps(formatter)
    def wrapper(**kwargs):
        ckas = {
            param.name for param in wrapper.__signature__.parameters.values()
            if param.kind == param.KEYWORD_ONLY
        }
        label_dict = {
            key: value for key, value in kwargs.items() if key in ckas
        }
        assert ast.literal_eval(repr(label_dict)) == label_dict
        results = formatter(**kwargs)
        if is_mapping(results):
            return dict(results, label=repr(label_dict))
        return [
            dict(result, label=repr((i, label_dict)))
            for i, result in enumerate(results)
        ]

    wrapper.__signature__ = inspect.signature(formatter)
    return wrapper


def approx_eq(x: typing.Any, y: typing.Any) -> bool:
    """
    Wrapper around `pytest.approx(x) == y` which permits comparing non-
    numeric scalar values.

    Notes
    -----
    For backward compatibility (`_pytest.approx.ApproxScalar` was
    introduced in `pytest` v6.2.0)
    """
    is_num_or_nums = is_sequence(
        item_checker=is_instance(numbers.Real),
        allow_item=True,
    )
    if all(is_num_or_nums(xy) for xy in (x, y)):
        # Allow approximate matches for numbers
        return pytest.approx(x) == y
    # Fallback to vanilla equality
    return x == y


# ****************************** Tests ******************************* #


@parametrize_all(
    (True, dict(split_x='bar', split_y='foo', xlabel='x', ylabel='y')),
    (True, dict(split_xy='bar', xlabel='x')),
    (False, dict(xlabel='x', ylabel='y')),
    exclude='make_full_figure',
)
def test_dummy_axes(
    make_full_figure: typing.Callable[..., MakeFullFigResult],
    expect_dummy: bool,
    kwargs: Keywords,
) -> None:
    """
    Ensure that the dummy axes housing the axis labels is (1) only
    spawned when needed and (2) beneath the other subplots.
    """
    fig, subplots = make_full_figure(**kwargs)
    subplot_identities: typing.Set[int] = {id(ax) for ax in subplots.values()}
    extra_axes = [ax for ax in fig.axes if id(ax) not in subplot_identities]
    assert len(extra_axes) == (1 if expect_dummy else 0)
    if expect_dummy:
        dummy, = extra_axes
        min_subplots_zorder = min(ax.get_zorder() for ax in subplots.values())
        assert dummy.get_zorder() < min_subplots_zorder
        for label in 'xlabel', 'ylabel':
            expected = kwargs.get(label)
            if expected:
                assert (expected == getattr(dummy, 'get_' + label)()), label


@parametrize_all(
    (no_op_formatter,),
    (no_op_formatter, dict(split_x='foo', hide_split_categories=True),),
    (
        simple_formatter,
        dict(
            split_xy='bar',
            hide_split_categories=True,
            plenary_subplot=True,
        ),
        tuple(simple_formatter()),
    ),
    (complex_formatter, {}, (*simple_formatter(), 'markersize')),
    exclude='make_full_figure',
)
def test_formatter(
    make_full_figure: typing.Callable[..., MakeFullFigResult],
    formatter: Formatter,
    kwargs: typing.Union[Keywords, None],
    attrs_to_check: typing.Union[typing.Collection[Identifier], None]
) -> None:
    """
    Test that the formatter is (1) unperturbed and (2) properly used to
    format the data points.
    """
    def compare(attr: Identifier, x: typing.Any, y: typing.Any) -> None:
        if 'color' in attr and not (is_rgba(x) and is_rgba(y)):
            # Comparison for colors
            return compare(
                attr, *(matplotlib.colors.to_rgba(xy) for xy in (x, y)),
            )
        if all(xy in ('none', 'None', None) for xy in (x, y)):
            # Let None match 'none', etc.
            return
        assert approx_eq(x, y), f'{attr}: {x!r} != {y!r}'

    is_rgba = is_sequence(item_checker=(is_nonnegative_real & le(1)), length=4)

    wrapped_formatter = add_ckas_as_label(formatter)
    old_formatter_dict = dict(vars(wrapped_formatter))
    fig, subplots = make_full_figure(
        formatter=wrapped_formatter, **(kwargs or {}),
    )
    # Check that we haven't tempered with the passed wrapper function
    assert old_formatter_dict == vars(wrapped_formatter)
    # Check that the formatter has correctly formatted the given
    # attributes
    if not attrs_to_check:
        return
    for line in (line for ax in subplots.values() for line in ax.lines):
        label = line.get_label()
        if not label:
            continue
        try:
            ckas = ast.literal_eval(label)
        except (TypeError, ValueError, SyntaxError):
            continue
        if is_mapping(ckas):
            plotting_kwargs = wrapped_formatter(**ckas)
        else:  # Tuple (i, kwargs)
            i, ckas = ckas
            plotting_kwargs = wrapped_formatter(**ckas)[i]
        for attr in (attrs_to_check or ()):
            if attr not in plotting_kwargs:
                continue
            compare(attr, plotting_kwargs[attr], getattr(line, 'get_' + attr)())


def test_split_xy_with_grid_dimensions(
    make_full_figure: typing.Callable[..., MakeFullFigResult],
) -> None:
    """
    Test the behavior when the form `split_xy=(cat_name, nrows, ncols)`
    is used.
    """
    cat_name = 'animal'
    dims_under, dims_over = (1, 2), (3, 2)
    assert (
        dims_under[0] * dims_under[1]
        < len(ANIMAL_VALUES)
        < dims_over[0] * dims_over[1]
    )
    # Oversized grid -> tile the grid
    fig, subplots = make_full_figure(split_xy=(cat_name, *dims_over))
    gridspec = next(iter(subplots.values())).get_subplotspec().get_gridspec()
    assert gridspec.get_geometry() == dims_over
    all_index_boundaries = {
        tuple(ax.get_subplotspec().get_geometry())[-2:]
        for ax in subplots.values()
    }
    assert all_index_boundaries == {(i, i) for i, _ in enumerate(ANIMAL_VALUES)}
    # Undersized grid -> error
    with pytest.raises(ValueError, match='not enough'):
        make_full_figure(split_xy=(cat_name, *dims_under))


@parametrize_all(
    (True, 'animal', None),
    (True, 'foo', 'bar'),
    ('after', 'foo', 'bar'),
    (dict(foo='after'), 'foo', 'bar'),
    (False, 'foo', None),
    ('before', 'foo', 'foo'),
    (dict(foo='before', animal='after'), 'animal', 'foo'),
    exclude='make_full_figure',
)
def test_plenary_plots(
    make_full_figure: typing.Callable[..., MakeFullFigResult],
    plenary_subplot: PlenarySubplot,
    split_x: typing.Union[CategoryName, None],
    split_y: typing.Union[CategoryName, None],
) -> None:
    """
    Test the behavior of `plenary_subplot`.
    """
    def check_plenary_loc_1d(
        subplots: typing.Dict[typing.Union[CategoryValue, PlenaryLabel], Axes],
    ) -> None:
        ax_plenary = subplots[PLENARY_LABEL]
        subplots_by_indices: typing.Dict[numbers.Integral, Axes] = {
            ax.get_subplotspec().get_geometry()[-1]: ax
            for ax in subplots.values()
        }
        plenary_index = dict(
            before=min, after=max,
        )[plenary_specs[split_x or split_y]](subplots_by_indices)
        assert subplots_by_indices[plenary_index] is ax_plenary

    def check_plenary_loc_2d(
        subplots: typing.Dict[
            typing.Tuple[
                typing.Union[CategoryValue, PlenaryLabel],
            ],
            Axes
        ],
    ) -> None:
        subplot_labels_to_indices: typing.Dict[
            typing.Tuple[
                typing.Union[CategoryValue, PlenaryLabel],
                typing.Union[CategoryValue, PlenaryLabel],
            ],
            typing.Tuple[numbers.Integral, numbers.Integral]
        ] = {
            cat_values: get_subplot_ij(ax)
            for cat_values, ax in subplots.items()
        }
        for i, xy in enumerate('yx'):
            try:
                plenary_index_agg = dict(
                    before=min, after=max,
                )[plenary_specs.get(splits[xy])]
            except KeyError:
                continue
            all_indices = {ij[i] for ij in subplot_labels_to_indices.values()}
            all_plenary_indices = {
                ij[i] for cat_values, ij in subplot_labels_to_indices.items()
                if cat_values[i] == PLENARY_LABEL
            }
            assert len(all_plenary_indices) == 1
            assert all_plenary_indices.pop() == plenary_index_agg(all_indices)

    if isinstance(plenary_subplot, typing.Mapping):
        plenary_specs = dict(plenary_subplot)
    else:
        plenary_specs = dict.fromkeys(
            (split_x, split_y),
            (False if plenary_subplot == 'none' else plenary_subplot),
        )
    splits = dict(x=split_x, y=split_y)
    plenary_specs = {
        cat_name: 'before' if loc in (True,) else loc
        for cat_name, loc in plenary_specs.items()
        if cat_name in splits.values()
        if loc
    }
    # Check the:
    # - Dimensions of the grid
    # - Positions of the plenary subplots
    if split_x == split_y is not None:
        def check_num_subplots(
            nrows: numbers.Integral, ncols: numbers.Integral,
        ) -> None:
            assert nrows * ncols >= min_num_subplots

        min_num_subplots = CATEGORY_SIZES[split_x]
        if plenary_specs.get(split_x):
            min_num_subplots += 1
        check_plenary_loc = check_plenary_loc_1d
    else:
        def check_num_subplots(
            nrows: numbers.Integral, ncols: numbers.Integral,
        ) -> None:
            assert nrows == expected_dims['y']
            assert ncols == expected_dims['x']

        expected_dims: typing.Dict[typing.Literal['x', 'y'], int] = {
            xy: (
                CATEGORY_SIZES[splits[xy]] + (
                    1 if plenary_specs.get(splits[xy]) else 0
                )
                if splits[xy] else
                1
            )
            for xy in 'xy'
        }
        check_plenary_loc = (
            check_plenary_loc_2d
            if split_x and split_y else
            check_plenary_loc_1d
        )
    _, subplots = make_full_figure(
        split_x=split_x, split_y=split_y, plenary_subplot=plenary_subplot,
    )
    assert all(  # Sanity check: each subplot span one cell
        len(set(ax.get_subplotspec().get_geometry()[-2:])) == 1
        for ax in subplots.values()
    )
    ax, *_ = subplots.values()
    nrows, ncols, *_ = ax.get_subplotspec().get_geometry()
    check_num_subplots(nrows, ncols)
    if plenary_specs:
        check_plenary_loc(subplots)


@parametrize_all(
    # Horizontal single-category layout (w/ & w/o sorting function)
    *(
        (('top', lr), d, 'foo')
        for lr, d in itertools.product(
            ('left', 'right'),
            ({}, dict(foo=ne(True))),
        )
    ),
    # Vertical single-category layout (w/ & w/o sorting function)
    *(
        ((tb, 'left'), d, None, None, ('foo', 2, 1))
        for tb, d in itertools.product(
            ('top', 'bottom'),
            ({}, dict(foo=ne(True))),
        )
    ),
    # Mixed single-category layout (w/o sorting function)
    *(
        (tf, {}, None, None, ('bar', 2, 2))
        for horizontal in ('left', 'right')
        for vertical in ('top', 'bottom')
        for tf in ((horizontal, vertical), (vertical, horizontal))
    ),
    # Mixed two-category layout (w/sorting mappings)
    *(
        (
            tf,
            dict(foo={True: 0, False: 1}, bar={0: 2, 1: 0, 2: 1}),
            'bar',
            'foo',
        )
        for tf in (
            # Sanity check: order of directions doesn't matter
            ('top', 'left'), ('left', 'top'),
            # Sanity check: picking a different tiling direction
            # still works
            ('top', 'right'), ('right', 'bottom'),
        )
    ),
    exclude=('print_msg', 'make_full_figure'),
)
def test_subplot_layout(
    print_msg: typing.Callable,
    make_full_figure: typing.Callable[..., MakeFullFigResult],
    tile_from: typing.Tuple[
        typing.Literal[DIRECTIONS], typing.Literal[DIRECTIONS],
    ],
    sorters: typing.Mapping[CategoryName, Sorter],
    split_x: typing.Union[CategoryName, None],
    split_y: typing.Union[CategoryName, None],
    split_xy: typing.Union[
        CategoryName,
        typing.Tuple[CategoryName, numbers.Integral, numbers.Integral],
        None,
    ],
) -> None:
    """
    Test the behavior of `tile_from` and `category_value_sorters`.
    """
    horizontal_dirs = 'left', 'right'
    vertical_dirs = 'top', 'bottom'
    if split_x == split_y is not None:
        assert split_xy is None
        splits = dict(split_xy=split_x)
    else:
        splits = {
            split: cat_name
            for split, cat_name in dict(
                split_x=split_x, split_y=split_y, split_xy=split_xy,
            ).items()
            if cat_name
        }
        if items_fulfill(str, numbers.Integral, numbers.Integral)(split_xy):
            splits['split_xy'], *_ = split_xy
    splits: typing.Dict[
        typing.Literal['split_x', 'split_y', 'split_xy'], CategoryName
    ]
    if not splits:  # No splits -> nothing to test
        pytest.skip('No splits -> no layout')
    print_msg(
        'Splits:', splits, 'Tiling:', tile_from, 'Sorting:', sorters, sep='\n',
    )
    # Make the plot first, because we kinda need that to figure out the
    # grid size
    _, subplots = make_full_figure(
        tile_from=tile_from,
        category_value_sorters=sorters,
        split_x=split_x,
        split_y=split_y,
        split_xy=split_xy,
    )
    assert all(  # Sanity check: each subplot spans one cell
        len(set(ax.get_subplotspec().get_geometry()[-2:])) == 1
        for ax in subplots.values()
    )
    # Get the ordering of the grid cells
    ax, *_ = subplots.values()
    nrows, ncols, *_ = ax.get_subplotspec().get_geometry()
    indices = np.arange(nrows * ncols)
    tile_dir_1, tile_dir_2 = tile_from
    if tile_dir_1 in horizontal_dirs:
        fill_horizontal_first = True
        assert tile_dir_2 in vertical_dirs
        index_grid = indices.reshape((nrows, ncols))
    elif tile_dir_1 in vertical_dirs:
        fill_horizontal_first = False
        assert tile_dir_2 in horizontal_dirs
        index_grid = indices.reshape((ncols, nrows)).T
    else:
        assert False, tile_from
    if 'right' in tile_from:
        index_grid = np.flip(index_grid, axis=-1)
    if 'bottom' in tile_from:
        index_grid = np.flip(index_grid, axis=0)
    print_msg('Grid layout:', index_grid, sep='\n')
    # Construct the sorting-key function
    if len(splits) == 2:  # 2d grid
        def sorting_key(
            key: typing.Tuple[CategoryValue, CategoryValue],
        ) -> Sortable:
            return tuple(
                sorter(subkey) for sorter, subkey in zip(sorting_funcs, key)
            )[::(1 if fill_horizontal_first else -1)]

        sorting_funcs = [
            get_sorting_key(sorters.get(split)) for split in (split_y, split_x)
        ]
        dtype = np.dtype([(split, object) for split in (split_y, split_x)])
        cat_name = split_y, split_x
        fill_value = 'n/a', 'n/a'
    else:  # 1d grid
        cat_name, = sorting_cat_name, = splits.values()
        sorting_key = get_sorting_key(sorters.get(sorting_cat_name))
        dtype = np.dtype(object)
        fill_value = 'n/a'
    # Sort the subplots and see if they are in order
    subplot_categorizations_sorted = sorted(subplots, key=sorting_key)
    subplot_locs_sorted: typing.List[
        typing.Tuple[numbers.Integral, numbers.Integral]
    ] = [
        get_subplot_ij(subplots[key]) for key in subplot_categorizations_sorted
    ]
    # Note: apparently np.full() tries to unpack the fill value, so
    # promote it to a rank-0 array
    grid_visualization = np.full(
        shape=index_grid.shape,
        fill_value=np.array(fill_value, dtype=dtype),
        dtype=dtype,
    )
    for categorization, ij in zip(
        subplot_categorizations_sorted, subplot_locs_sorted,
    ):
        grid_visualization[ij] = categorization
    print_msg(
        f'Subplot assignments ({cat_name!r}):', grid_visualization, sep='\n',
    )
    occupied_indices = index_grid[tuple(zip(*subplot_locs_sorted))]
    assert (  # Check if the subplot locations are correctly sorted
        occupied_indices[1:] > occupied_indices[:-1]
    ).all(), occupied_indices


@parametrize_all(
    *(  # Check that label-dropping is direction-sensitive
        (
            True, True, bt, lr,
            [
                [bt == 'top', False, True],
                [False, bt == 'top', False],
                [bt == 'bottom', bt == 'bottom', True],
            ],
            [
                [True, False, True],
                [lr == 'left', lr == 'right', False],
                [lr == 'left', False, lr == 'right'],
            ],
        )
        for bt in ('bottom', 'top') for lr in ('left', 'right')
    ),
    # Check that we are handling directional label-dropping correctly,
    # and only when the limits are unified
    *(
        (
            *(True, axes_designation)[::step], 'bottom', 'left',
            [
                [(not drop_x), False, True],
                [(not drop_x), (not drop_x), False],
                [True, True, True],
            ],
            [
                [True, False, True],
                [True, (not drop_y), False],
                [True, (not drop_y), (not drop_y)],
            ],
        )
        for axes_designation, drop_x, drop_y in [
            ('x', True, False),
            ('y', False, True),
            ('xy', True, True),
            ('none', False, False),
            (None, False, False),
            # (True, True, True),  # Already covered above
            (False, False, False),
        ]
        for step in (1, -1)
    ),
    # Check that for per-row/-column limit unification the
    # label-dropping is handled correctly
    *(
        (
            dict(x=unify_x, y=unify_y), True, 'bottom', 'left',
            [
                [(unify_x in (False, 'row')), False, True],
                [
                    (unify_x in (False, 'row')),
                    (unify_x in (False, 'row')),
                    False,
                ],
                [True, True, True],
            ],
            [
                [True, False, True],
                [True, (unify_y in (False, 'col')), False],
                [
                    True,
                    (unify_y in (False, 'col')),
                    (unify_y in (False, 'col')),
                ],
            ],
        )
        for unify_x, unify_y in itertools.product(*(
            ((True, False, 'row', 'col'),) * 2
        ))
        if not ({unify_x, unify_y} <= {True, False})
    ),
    exclude=['print_msg', 'plot_categorized'],
)
def test_axis_limits(
    print_msg: typing.Callable,
    plot_categorized: typing.Callable[
        ..., PLOT_CATEGORIZED_SIG.return_annotation
    ],
    unify_axlims: typing.Union[
        AxesDesignation,
        typing.Mapping[
            typing.Literal['x', 'y'],
            typing.Literal['row', 'col', True, False, 'all', 'none']
        ]
    ],
    drop_ticklabels: AxesDesignation,
    xlabel_pos: typing.Literal['bottom', 'top'],
    ylabel_pos: typing.Literal['left', 'right'],
    expected_x_labels: typing.Tuple[
        typing.Tuple[bool, bool, bool],
        typing.Tuple[bool, bool, bool],
        typing.Tuple[bool, bool, bool],
    ],
    expected_y_labels: typing.Tuple[
        typing.Tuple[bool, bool, bool],
        typing.Tuple[bool, bool, bool],
        typing.Tuple[bool, bool, bool],
    ],
) -> None:
    r"""
    Test the behavior of `unify_axlims` and `drop_ticklabels`.

    Parameters
    ----------
    unify_axlims, drop_ticklabels
        See `~.plot_categorized()`
    xlabel_pos, ylabel_pos
        The side of the plot area the tick labels are to be found
    expected_x_labels, expected_y_labels
        Whether the respective axis labels are supposed to be present
        for each of the subplots

    Notes
    -----
    The following grid of 7 subplots is created:

    food \ foobar   foo   bar   baz
                  +-----+     +-----+
             spam |     |     |     |
                  +-----+-----+-----+
              ham |     |     |
                  +-----+-----+-----+
             eggs |     |     |     |
                  +-----+-----+-----+

    The presence of labels is ignored for the absent subplots
    (`('spam', 'bar')` and `('ham', 'baz')`).
    """
    # Hand-craft a data set which:
    # - Fits in a 3x3 grid
    # - Spans a different range in each subplot
    # - Drops the subplots at (0, 1) and (1, 2)
    foobar_values = 'foo', 'bar', 'baz'
    food_values = 'spam', 'ham', 'eggs'
    split_x, split_y = 'foobar', 'food'
    excluded_subplots = ('spam', 'bar'), ('ham', 'baz')
    Foobar = typing.Literal[foobar_values]
    Food = typing.Literal[food_values]
    Row = typing.Tuple[Foobar, Food, typing.List[float], typing.List[float]]
    Categorization = typing.TypedDict(
        'Categorization', dict(foobar=Foobar, food=Food),
    )

    data: typing.List[Row] = [
        (foobar, food, *(([i, i + 1],) * 2))
        for i, (food, foobar) in enumerate(
            itertools.product(food_values, foobar_values)
        )
        if (food, foobar) not in excluded_subplots
    ]

    def get_cat(row: Row) -> Categorization:
        return dict(zip(('foobar', 'food'), row))

    def get_x(row: Row) -> typing.List[float]:
        return row[2]

    def get_y(row: Row) -> typing.List[float]:
        return row[3]

    def no_op_formatter(
        *,
        foobar: typing.Optional[Foobar] = None,
        food: typing.Optional[Food] = None,
    ) -> dict:
        return {}

    def translate_axes_designations(
        des: AxesDesignation,
    ) -> typing.Dict[typing.Literal['x', 'y'], bool]:
        if des in (None, 'none'):
            des = False
        if isinstance(des, str):
            return {xy: xy in des for xy in 'xy'}
        return dict.fromkeys('xy', des)

    def translate_unify_axlims(
        unify: typing.Union[
            AxesDesignation,
            typing.Mapping[
                typing.Literal['x', 'y'],
                typing.Literal['row', 'col', True, False, 'all', 'none']
            ]
        ],
    ) -> typing.Mapping[
        typing.Literal['x', 'y'], typing.Set[typing.Literal['row', 'col']]
    ]:
        if unify in AXES_DESIGNATIONS:
            result = translate_axes_designations(unify)
            return {
                xy: {'row', 'col'} if unified else {}
                for xy, unified in result.items()
            }
        result = {}
        for xy in 'xy':
            unified = unify.get(xy, False)
            if unified in (False, 'none'):
                result[xy] = {}
            elif unified in (True, 'all'):
                result[xy] = {'row', 'col'}
            else:
                result[xy] = {unified}
        return result

    def get_axis_data_lim(
        ax: Axes, axis: typing.Literal[0, 1],
    ) -> typing.Tuple[numbers.Real, numbers.Real]:
        data = [xy for line in ax.lines for xy in line.get_xydata()[:, axis]]
        return min(data), max(data)

    def get_subarray(
        array_2d: np.ndarray, rc: typing.Literal['row', 'col'], i: int,
    ) -> np.ndarray:
        if rc == 'row':
            return array_2d[i]
        return array_2d[:, i]

    def compare_with_dropped_na(
        comparator: typing.Callable[[np.ndarray, np.ndarray], np.ndarray],
        a1: np.ndarray,
        a2: np.ndarray,
    ) -> bool:
        if a1.shape != a2.shape:
            return False
        a1 == a1.flatten()
        a2 == a2.flatten()
        not_na_mask = ~(np.isnan(a1) | np.isnan(a2))
        return comparator(pytest.approx(a1[not_na_mask]), a2[not_na_mask])

    # Are we actually dropping the labels or is that ignored with a
    # warning?
    axis_labels_are_dropped = translate_axes_designations(drop_ticklabels)
    axis_limits_are_unified = translate_unify_axlims(unify_axlims)
    expect_dropping_warning = any(
        axis_labels_are_dropped[xy] and
        requirement not in axis_limits_are_unified[xy]
        for xy, requirement in (('x', 'col'), ('y', 'row'))
    )
    print_msg(
        f'unify_axlims = {unify_axlims!r} -> {axis_limits_are_unified!r},',
        f'drop_ticklabels = {drop_ticklabels!r}',
        f'-> {axis_labels_are_dropped!r}:',
        'warning expected',
    )

    # Actual plotting
    rc_params = dict(
        (f'{tick}.{option_prefix}{direction}', direction == label_pos)
        for tick, directions, label_pos in [
            ('xtick', ('bottom', 'top'), xlabel_pos),
            ('ytick', ('left', 'right'), ylabel_pos),
        ]
        for option_prefix in ('', 'label')
        for direction in directions
    )
    print_msg('Resolved rcParams:', rc_params, sep='\n')
    with contextlib.ExitStack() as stack:
        if expect_dropping_warning:
            stack.enter_context(pytest.warns(UserWarning, match='not unified'))
        stack.enter_context(matplotlib.rc_context(rc_params))
        _, subplots = plot_categorized(
            data,
            (get_x, get_y),
            get_cat,
            no_op_formatter,
            category_value_sorters=dict(
                foobar=foobar_values.index,
                food=food_values.index,
            ),
            unify_axlims=unify_axlims,
            drop_ticklabels=drop_ticklabels,
            split_x=split_x,
            split_y=split_y,
        )
    assert all(
        (food_foobar in subplots) ^ (food_foobar in excluded_subplots)
        for food_foobar in itertools.product(food_values, foobar_values)
    )

    # Check if the axis limits behave as expected: axis limits are the
    # same iff the limits are unified
    for xy, unify_rc in axis_limits_are_unified.items():
        lower_lims = np.full((len(food_values), len(foobar_values)), np.nan)
        upper_lims = lower_lims.copy()
        lower_data_lims = lower_lims.copy()
        upper_data_lims = lower_lims.copy()
        get_lim = getattr(Axes, f'get_{xy}lim')
        get_data_lim = functools.partial(
            get_axis_data_lim, axis=(0 if xy == 'x' else 1),
        )
        for (food, foobar), ax in subplots.items():
            ij = (food_values.index(food), foobar_values.index(foobar))
            lower_lims[ij], upper_lims[ij] = get_lim(ax)
            lower_data_lims[ij], upper_data_lims[ij] = get_data_lim(ax)
        print_msg(
            f'{xy}-axis limits:',
            'Lower:', lower_lims,
            'Upper:', upper_lims,
            'Lower (data):', lower_data_lims,
            'Upper (data):', upper_data_lims,
            sep='\n',
        )
        for (
            (lim_name, lim_array), (rc_sampled, rc_other, ref_index, n),
        ) in itertools.product(
            [('lower', lower_lims), ('upper', upper_lims)],
            [
                ('row', 'col', -1, len(food_values)),
                ('col', 'row', 0, len(foobar_values)),
            ],
        ):
            # Note: if we unified limits for each COLUMN, then each ROW
            # would look the same
            get_row_or_col = functools.partial(
                get_subarray, lim_array, rc_sampled
            )
            ref_row_or_col = get_row_or_col(ref_index)
            comparator = operator.eq if rc_other in unify_rc else operator.ne
            error_msg = (
                'unify_axlims = {unify_axlims!r}: '
                '{xy}-axis {lim} limits: expected {rc}s to have {comp} limits '
                '(reference {rc}: {ref}), '
                'got\n{full_array}'
            ).format(
                unify_axlims=unify_axlims,
                xy=xy,
                lim=lim_name,
                rc=rc_sampled,
                comp=('unified' if rc_other in unify_rc else 'different'),
                ref=ref_row_or_col,
                full_array=lim_array,
            )
            for i in range(n):
                if i == (n if ref_index < 0 else 0) + ref_index:
                    continue
                assert compare_with_dropped_na(
                    comparator, ref_row_or_col, get_row_or_col(i),
                ), error_msg

    # Check if the presence of tick labels are as expected
    for expected_labels, axis, tick_param_names in [
        (expected_x_labels, 'xaxis', ('labelbottom', 'labeltop')),
        (expected_y_labels, 'yaxis', ('labelleft', 'labelright')),
    ]:
        ax_have_labels = np.zeros(
            (len(food_values), len(foobar_values)), dtype=bool,
        )
        mask = ax_have_labels.copy()
        for (i, food), (j, foobar) in itertools.product(
            enumerate(food_values), enumerate(foobar_values),
        ):
            try:
                ax = subplots[food, foobar]
            except KeyError:
                mask[i, j] = True
                continue
            axis_obj = getattr(ax, axis)
            visible_labels = [
                label for label in axis_obj.get_ticklabels()
                if label.get_visible()
            ]
            print_msg(
                f'subplots[{food!r}, {foobar!r}].{axis} = {axis_obj!r};',
                f'Visible labels: {visible_labels!r}',
                sep='\n',
            )
            ax_have_labels[i, j] = bool(visible_labels)
        ax_have_labels = np.ma.array(ax_have_labels, mask=mask)
        error_msg = (
            'presence of {} labels:\nexpected:\n{}\nactual:\n{}'
        ).format(axis, expected_labels, ax_have_labels)
        assert (ax_have_labels == expected_labels).all(), error_msg


@parametrize_all(
    ('_{}_', dict(foo='_foo_', bar='_bar_'), {}, 'foo', 'bar'),
    (dict(foo='FOO'), dict(foo='FOO'), {}, 'foo', 'foo'),
    (
        functools.partial(operator.mul, 2),
        dict(animal='animalanimal'),
        {},
        None,
        'animal',
    ),
    (None, dict(foo='foo', bar='bar'), {}, 'foo', 'bar'),
    exclude='make_full_figure',
)
def test_category_name_typesetting(
    make_full_figure: typing.Callable[..., MakeFullFigResult],
    typesetter: Typesetter,
    expected_typeset_names: typing.Mapping[CategoryName, str],
    kwargs: Keywords,
    split_x: typing.Union[CategoryName, None],
    split_y: typing.Union[CategoryName, None],
    *,
    plenary_subplot: PlenarySubplot = True,
    plenary_label: PlenaryLabel = PLENARY_LABEL,
) -> None:
    """
    Test that the category names are suitably formatted.
    """
    fig, subplots = make_full_figure(
        category_name_typesetter=typesetter,
        show_category_names=True,
        label_subplots=True,
        split_x=split_x,
        split_y=split_y,
        plenary_subplot=plenary_subplot,
        plenary_label=plenary_label,
        **kwargs,
    )
    typesetter = functools.partial(apply_typesetter, typesetter)
    for key, ax in subplots.items():
        categorization = get_categorization_from_subplot_key(
            key, split_x, split_y, plenary_subplot, plenary_label,
        )
        typeset_cat_names = [
            typesetter(cat_name) for cat_name in categorization
        ]
        # Internal consistency check
        expected = [
            expected_typeset_names[cat_name] for cat_name in categorization
        ]
        assert typeset_cat_names == expected
        # Check the actual label
        subplot_label, = ax.texts
        label_text = subplot_label.get_text()
        if not categorization and (split_x or split_y):  # Plenary plot
            assert label_text == plenary_label
        else:
            assert all(
                typeset_cat_name in label_text
                for typeset_cat_name in typeset_cat_names
            ), (label_text, categorization, typeset_cat_names)


def test_category_value_typesetting(
    make_full_figure: typing.Callable[..., MakeFullFigResult],
) -> None:
    """
    Test that the category values are suitably formatted.
    """
    def sarcasm_case(animal: Animal) -> str:
        return ''.join(
            (char.upper if i % 2 else char.lower)()
            for i, char in enumerate(animal)
        )

    split_x_1, split_y_1 = 'foo', 'bar'
    split_x_2, split_y_2 = 'animal', 'foo'
    typesetters_1 = dict(
        foo={True: 'Yes', False: 'No'},
        bar='{:.1f}',
    )
    typesetters_2 = dict(
        animals=sarcasm_case,
    )
    label_subplots_1 = dict(
        size=12,
        color='blue',
    )
    # Test of string- and mapping-based formatters
    fig_1, subplots_1 = make_full_figure(
        category_value_typesetters=typesetters_1,
        split_x=split_x_1,
        split_y=split_y_1,
        label_subplots=label_subplots_1,
    )
    for key, ax in subplots_1.items():
        categorization = get_categorization_from_subplot_key(
            key, split_x_1, split_y_1,
        )
        subplot_label, = ax.texts
        # Check the text
        label_text = subplot_label.get_text()
        assert label_text == '{}, {}'.format(*(
            apply_typesetter(typesetters_1.get(split), categorization[split])
            for split in (split_y_1, split_x_1)
        ))
        # Check the properties
        assert approx_eq(subplot_label.get_fontsize(), label_subplots_1['size'])
        assert approx_eq(subplot_label.get_color(), label_subplots_1['color'])
    # Test of callable-based formatters and the null formatter
    fig_2, subplots_2 = make_full_figure(
        category_value_typesetters=typesetters_2,
        split_x=split_x_2,
        split_y=split_y_2,
        label_subplots=True,
    )
    assert fig_1 is not fig_2
    assert get_wrapped_instance(fig_1) is not get_wrapped_instance(fig_2)
    for key, ax in subplots_2.items():
        categorization = get_categorization_from_subplot_key(
            key, split_x_2, split_y_2,
        )
        subplot_label, = ax.texts
        label_text = subplot_label.get_text()
        assert label_text == '{}, {}'.format(*(
            apply_typesetter(typesetters_2.get(split), categorization[split])
            for split in (split_y_2, split_x_2)
        ))


def test_plotting_method(
    make_full_figure: typing.Callable[..., MakeFullFigResult],
) -> None:
    """
    Test the behavior of `method`.
    """
    formatter = add_ckas_as_label(scatter_formatter)
    _, subplots = make_full_figure(formatter=formatter, method='scatter')
    for path_coll in (
        coll for ax in subplots.values() for coll in ax.collections
        if isinstance(coll, matplotlib.collections.PathCollection)
    ):
        try:
            ckas = ast.literal_eval(path_coll.get_label())
        except SyntaxError:
            continue
        if is_mapping(ckas):
            scatter_kwargs = formatter(**ckas)
        else:  # Tuple (i, kwargs)
            i, ckas = ckas
            scatter_kwargs = formatter(**ckas)[i]
        for key, quantity in dict(
            edgecolor='edgecolors',
            s='sizes',
            c='facecolors',
        ).items():
            value = scatter_kwargs[key]
            if 'color' in quantity:
                value = matplotlib.colors.to_rgba(value)
            values = getattr(path_coll, 'get_' + quantity)()
            assert (value == values).all(), (quantity, value, values)


@pytest.mark.parametrize('obj_type', ['ax', 'fig'])
def test_passing_fig_explicitly(
    make_fig: typing.Callable[..., Figure],
    make_ax: typing.Callable[..., Axes],
    make_full_figure: typing.Callable[..., MakeFullFigResult],
    obj_type: typing.Literal[Axes, Figure, 'ax', 'fig'],
) -> None:
    """
    Test the behavior of `fig`.
    """
    if obj_type in (Axes, 'ax'):
        obj_type = 'ax'
        obj = make_ax()
        fig = obj.figure
    elif obj_type in (Figure, 'fig'):
        obj_type = 'fig'
        obj = fig = make_fig()
    else:
        assert False, f'obj_type = {obj_type!r}'
    _fig, _ = make_full_figure(fig=obj)
    assert fig is _fig


@parametrize_all(
    # Sanity check: no legend
    (None, False),
    (None, None),
    # Test auto legend placement
    (
        # When there's empty space left by the gridspec, the legend
        # should be placed within said space
        dict(gridspec_kwargs=dict(left=.5, right=.95)),
        dict(loc='best'),
        None,
        (.25, .5),
    ),
    (
        # When there's empty space left in the grid, the legend
        # should be placed in the empty cell(s)
        dict(
            # Bottom-left should be empty
            split_xy=('bar', 2, 2),
            tile_from=('top', 'right'),
            hide_split_categories=True,
            gridspec_kwargs=dict(
                # Make our own live easier figuring out where the
                # empty cell is by having a gridspec which covers
                # the entire figure
                left=0, right=1, bottom=0, top=1, wspace=0, hspace=0,
            ),
        ),
        dict(loc='best'),
        None,
        (.25, .25),
    ),
    # Test label ordering/formatting
    (
        dict(  # Passed to .categorized_legend()
            show_category_names=True,
        ),
        True,
        # Category names and values should be sorted alphabetically
        # by default
        [
            '{}.+=.+{}'.format(cat_name, str(cat_value))
            for cat_name, cat_values in sorted(CATEGORY_VALUES.items())
            for cat_value in sorted(cat_values)
        ],
        None,
        True,  # Use regex match
    ),
    (  # Category-value only
        None,
        True,
        [
            str(cat_value)
            for cat_name, cat_values in sorted(CATEGORY_VALUES.items())
            for cat_value in sorted(cat_values)
        ],
    ),
    (  # Both the category names and values formatted
        dict(
            show_category_names=True,
            category_name_typesetter=str.upper,
            category_value_typesetters=dict(
                foo={True: 'T', False: 'F'},
                animal=str.title,
            ),
            category_value_sorters=dict(foo=ne(True)),
        ),
        # Special sorting for category names
        dict(category_name_sorter=dict(foo=0, bar=1, animal=2)),
        [
            '{}.+=.+{}'.format(
                cat_name.upper(), apply_typesetter(typesetter, cat_value),
            )
            for cat_name, typesetter in [
                ('foo', {True: 'T', False: 'F'}),
                ('bar', None),
                ('animal', str.title),
            ]
            for cat_value in sorted(
                CATEGORY_VALUES[cat_name],
                key=(ne(True) if cat_name == 'foo' else None),
            )
        ],
        None,
        True,
    ),
    (  # Hide some categories (or their names)
        dict(
            show_category_names=dict(foo=True, animal=False),
            hide_split_categories=True,
            split_x='bar',
        ),
        True,
        [  # Note: 'animal' < 'foo'
            ('{}.+=.+{}' if show else '{1}').format(cat_name, cat_value)
            for cat_name, show in [('animal', False), ('foo', True)]
            for cat_value in sorted(CATEGORY_VALUES[cat_name])
        ],
        None,
        True,
    ),
    exclude='make_full_figure',
)
def test_categorized_legend(
    make_full_figure: typing.Callable[..., MakeFullFigResult],
    kwargs: typing.Union[Keywords, None],
    cl_kwargs: typing.Union[bool, Keywords, None],
    expected_labels: typing.Union[typing.Sequence[str], None],
    expected_pos: typing.Union[
        typing.Tuple[numbers.Real, numbers.Real], None,
    ],
    match_labels_with_regex: typing.Union[bool, None],
) -> None:
    """
    Test the behavior of `categorized_legend`.
    """
    draw_cl = isinstance(cl_kwargs, typing.Mapping) or cl_kwargs
    fig, _ = make_full_figure(
        formatter=simple_formatter,
        categorized_legend=cl_kwargs,
        **(kwargs or {}),
    )
    if draw_cl:
        legend = fig.legends[-1]
    else:
        assert not fig.legends
        return
    # Check position
    if expected_pos is not None:
        fig.draw(fig.canvas.get_renderer())
        legend_bbox = legend.get_window_extent()
        expected_pos_px = (  # Figure fractions to pixels
            fig.transFigure.transform(expected_pos)
        )
        assert legend_bbox.contains(*expected_pos_px), (
            'Bbox: {}; expected position: {} -> {}'.format(
                legend_bbox.extents, expected_pos, expected_pos_px,
            )
        )
    # Check legend labels
    if expected_labels is not None:
        matcher = re.fullmatch if match_labels_with_regex else operator.eq
        labels = [
            label
            for label in (label.get_text() for label in legend.get_texts())
            if label
        ]
        error_msg = 'Labels: expected {}, got {}'.format(
            expected_labels, labels,
        )
        assert len(expected_labels) == len(labels), error_msg
        assert all(
            matcher(expected, actual)
            for expected, actual in zip(expected_labels, labels)
        ), error_msg
