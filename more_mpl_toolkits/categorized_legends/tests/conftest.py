"""
Shared fixtures.
"""
import functools
import numbers
import os
import typing

import matplotlib.pyplot as plt
import pytest
try:  # 7.0.0+
    from pytest import Config
except ImportError:
    from _pytest.config import Config

from .. import Axes, Figure

__all__ = (
    'make_fig', 'make_ax', 'print_msg',
)


@pytest.fixture
def make_fig() -> typing.Callable[..., Figure]:
    """
    Make a managed figure

    Call signature
    --------------
    (Same as `matplotlib.pyplot.figure()`)

    Return
    ------
    `~.Figure`
    """
    @functools.wraps(plt.figure)
    def _make_fig(*args, **kwargs) -> Figure:
        fig = Figure.wrap(plt.figure(*args, **kwargs))
        instances.append(fig)
        fig.__pytest_created_during__ = os.environ.get('PYTEST_CURRENT_TEST')
        fig.__pytest_created_within__ = (
            '{0.__module__}.{0.__name__}'.format(make_fig)
        )
        return fig

    instances: typing.List[Figure] = []

    yield _make_fig

    for fig in instances:
        plt.close(fig)


@pytest.fixture
def make_ax(
    make_fig: typing.Callable[..., Figure],
) -> typing.Callable[..., Axes]:
    """
    Make a managed axes object

    Call signature
    --------------
    *args, **kwargs
        Passed to `figure()`
    subplots
        Optional tuple `(nrows, ncols, index[, kwargs])` specifying that
        a grid of subplots of size `ncols`-by-`nrows` should be created,
        and the one at

        >>> fig.subplots(  # noqa # doctest: +SKIP
        ...     nrows, ncols[, **kwargs]
        ... ).flatten()[index]

        should be returned;
        default is to create exactly one subplot

    Return
    ------
    `~.Figure`
    """
    def _make_ax(
        *args,
        subplots: typing.Optional[
            typing.Tuple[numbers.Integral, numbers.Integral, numbers.Integral]
        ] = None,
        **kwargs
    ) -> Axes:
        fig = make_fig(*args, **kwargs)
        if subplots is None:
            return fig.gca()
        if len(subplots) == 3:
            nrows, ncols, index = subplots
            subplots_kwargs = {}
        elif len(subplots) == 4:
            nrows, ncols, index, subplots_kwargs = subplots
        else:
            assert False, f'subplots = {subplots!r}'
        return fig.subplots(
            nrows, ncols, squeeze=False, **subplots_kwargs,
        ).flatten()[index]

    yield _make_ax
    # Note: when we get back to here we will run make_fig()'s teardown


@pytest.fixture
def print_msg(pytestconfig: Config) -> typing.Callable:
    """
    Function having the same signature as `print()` but is suppressed
    unless `verbose = True` for the test session.
    """
    def _print(*args, **kwargs) -> None:
        pass

    if pytestconfig.getoption('verbose', default=False):
        return print
    return _print
