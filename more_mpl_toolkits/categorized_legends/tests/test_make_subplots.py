"""
Simple integration test for making a CL plot with panels.
"""
import contextlib
import numbers
import pathlib
import typing

import matplotlib.patheffects
import matplotlib.pyplot as plt
import numpy as np
import pytest

from ... import categorized_legends as cl
from .utils import parametrize_all

Point = typing.TypeVar('Point', bound=typing.Tuple[numbers.Real, numbers.Real])

RNG = np.random.default_rng(42)
NUM_POINTS = 200
POINTS: typing.Iterable[Point] = RNG.random((NUM_POINTS, 2))

FIGURE_KWARGS = dict(dpi=200)

PLOTTING_KWARGS = dict(
    label_subplots=True,
    show_category_names=True,
    split_xy='foo',
    plenary_subplot='after',
)


@pytest.fixture
def fig() -> matplotlib.figure.Figure:
    """
    Fresh instance of figure, which is closed by
    `matplotlib.pyplot.close()` after use.
    """
    fig = plt.figure(**FIGURE_KWARGS)
    yield fig
    plt.close(fig)


def categorizer(
    xy: Point,
) -> typing.Dict[typing.Literal['foo', 'bar'], numbers.Integral]:
    # Note: in older python versions `round(np.float_(...))` is a float,
    # so we need an explicit conversion to integers
    return dict(
        foo=int(round(xy.sum() * 1.5 - .5)),  # -> (0, 1, 2)
        bar=int(round((xy[0] - xy[1]) + .5)),  # -> (0, 1)
    )


def formatter(
    *,
    foo: typing.Optional[typing.Literal[0, 1, 2]] = None,
    bar: typing.Optional[typing.Literal[0, 1]] = None,
) -> typing.Dict[str, typing.Any]:
    return dict(
        marker='o',
        color='k' if foo is None else 'rgb'[foo],
        markerfacecolor=(None if bar or bar is None else 'w'),
    )


def make_multipanel_plot(
    fig: matplotlib.figure.Figure,
    tmp_path: pathlib.Path,
    write: typing.Union[bool, str] = True,
    kwargs: typing.Optional[typing.Mapping] = None,
):
    """
    Make a multi-panel plot.
    """
    splitting_args = {'split_x', 'split_y', 'split_xy'}
    kwargs = kwargs or {}
    if set(kwargs) & splitting_args:
        default_kwargs = {
            key: value for key, value in PLOTTING_KWARGS.items()
            if key not in splitting_args
        }
    else:
        default_kwargs = PLOTTING_KWARGS
    _, subplots = cl.plot_categorized(
        POINTS,
        data_getters=[0, 1],  # (point[0], point[1]) -> (x, y)
        categorizer=categorizer,
        formatter=formatter,
        fig=fig,
        **{**default_kwargs, **kwargs},
    )
    if write:
        if write in (True,):  # Default filename
            write = test_make_multipanel_plot.__name__ + '.png'
        fig.savefig(tmp_path / write)
    return fig, subplots


@pytest.mark.big
@parametrize_all(
    (True, None),  # Default behavior
    (  # Use smaller markers
        'small_markers.pdf',
        dict(markersize=(plt.rcParams['lines.markersize'] * .5))
    ),
    (
        'with_cl.png',
        dict(
            # Force categorized legend to be drawn
            categorized_legend=dict(loc='best'),
            # Use diff. grid-filling direction (top-down, left-right)
            tile_from=('top', 'left'),
            # Draw plenary panel before split panels
            plenary_subplot='before',
            # Suppress subplot labels
            label_subplots=None,
            # Force usage of an oversized grid
            split_xy=('foo', 2, 3),
        ),
    ),
    (
        # Drop the plenary panel
        # Draw a two-column legend (supposed to be in the lower right
        # corner)
        # Place the panel label in the center with white outlines
        'no_plenary.png',
        dict(
            categorized_legend=dict(loc='best', ncols=2),
            hide_split_categories=False,
            split_xy=('foo', 2, 2),
            plenary_subplot=False,
            label_subplots=dict(
                xy='center',
                size=(plt.rcParams['font.size'] * 2),
                path_effects=[
                    matplotlib.patheffects.PathPatchEffect(
                        edgecolor='white',
                        linewidth=(plt.rcParams['patch.linewidth'] * 2),
                    ),
                    matplotlib.patheffects.Normal(),
                ]
            ),
        ),
    ),
    (
        # Drop the plenaty panels
        # Split with both x and y
        # Hide split categories -> legend becomes empty
        'empty_legend.png',
        dict(
            categorized_legend=True,
            plenary_subplot=False,
            hide_split_categories=True,
            split_x='foo',
            split_y='bar',
            label_subplots=True,
            show_category_names=True,
        ),
        # v1.6.0 made it so that the legend is hidden if empty with a
        # warning, catch that
        pytest.warns(UserWarning, match='invisible .* empty')
    ),
    exclude=('fig', 'tmp_path'),
)
def test_make_multipanel_plot(
    fig: matplotlib.figure.Figure,
    tmp_path: pathlib.Path,
    write: typing.Union[bool, str],
    kwargs: typing.Union[typing.Mapping, None],
    ctx: typing.Union[typing.ContextManager, None],
):
    with (contextlib.nullcontext() if ctx is None else ctx):
        make_multipanel_plot(fig, tmp_path, write, kwargs)
