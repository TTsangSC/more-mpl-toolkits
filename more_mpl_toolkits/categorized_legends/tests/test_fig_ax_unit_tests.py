import ast
import contextlib
import functools
import inspect
import itertools
import math
import numbers
import pprint
import re
import sys
import textwrap
import types
import typing

import matplotlib
import numpy as np
import pytest
from mpl_toolkits.axes_grid1.parasite_axes import host_subplot

from tjekmate import is_keyword_mapping

from .. import Axes, Figure
from ..utils import get_grid_dimensions
from ..wrapper_proxy import get_wrapped_instance
from .utils import wip_test, parametrize_all, apply_typesetter, catch_warnings

FOO_VALUES = True, False
BAR_VALUES = 'spam', 'ham', 'eggs'
FOOBAR_VALUES = 1, 2, 3, 4, 5
BAZ_LIMITS = .1, 100.

NUM_VALUES = dict(
    foo=len(FOO_VALUES),
    bar=len(BAR_VALUES),
    foobar=len(FOOBAR_VALUES),
)
CATEGORIZED_LEGEND_DEFAULTS = {
    param.name: param.default
    for param in inspect.signature(Axes.categorized_legend).parameters.values()
    if param.default is not param.empty
}
# Compatibility: `matplotlib.colormaps` introduced in v3.5;
# `matplotlib.cm.get_cmap()` slated for removal in v3.9
DEFAULT_COLOR_MAP = getattr(matplotlib, 'colormaps', matplotlib.cm).get_cmap(
    'viridis',
)

T = typing.TypeVar('T')
Identifier = typing.TypeVar('Identifier', bound=str)
CLMethodName = typing.TypeVar('CLMethodName', bound=Identifier)
Keywords = typing.Mapping[Identifier, typing.Any]
Foo = typing.TypeVar('Foo', bound=bool)
Bar = typing.Literal[BAR_VALUES]
Foobar = typing.Literal[FOOBAR_VALUES]
Baz = typing.TypeVar('Baz', bound=numbers.Real)
Formatter = typing.Callable[
    ..., typing.Union[Keywords, typing.Sequence[Keywords]]
]
LegendChecker = typing.Callable[
    [matplotlib.legend.Legend, typing.Union[typing.Any, None]], None
]
HierarchicalFigureKey = typing.Literal[
    'fig', 'ax1', 'ax2', 'ax_inset', 'subfig', 'subax1', 'subax2',
]


class HierarchicalFigure(typing.TypedDict):
    fig: Figure
    ax1: Axes
    ax2: Axes
    ax_inset: Axes
    subfig: typing.NotRequired[Figure]
    subax1: typing.NotRequired[Figure]
    subax2: typing.NotRequired[Figure]
    ancestry: typing.Dict[  # parent_name -> {direct_child_name}
        HierarchicalFigureKey, typing.Set[HierarchicalFigureKey]
    ]
    baz_values: typing.Optional[
        typing.Dict[HierarchicalFigureKey, typing.Set[Baz]]
    ] = None

    @classmethod
    def new(cls: typing.Self, *args, **kwargs) -> typing.Self:
        instance = cls(*args, **kwargs)
        for key in cls.__annotations__:
            try:
                default = getattr(cls, key)
            except AttributeError:
                continue
            instance.setdefault(key, default)
        return instance


# ***************************** Fixtures ***************************** #


@pytest.fixture
def hierarchical_fig(
    make_fig: typing.Callable[..., Figure],
) -> HierarchicalFigure:
    """
    Build a figure with this hierachy:

    fig
     +- ax1
         +- ax_inset
             +- ax_inset_2
     +- ax2
     +- [subfig]
         +- [subax1]
         +- [subax2]
    """
    AxesOrFigure = typing.TypeVar('AxesOrFigure', Axes, Figure)

    def bookkeep_ancestry(
        parent: typing.Union[Axes, Figure], child: AxesOrFigure,
    ) -> AxesOrFigure:
        ancestry.setdefault(parent.get_label(), set()).add(child.get_label())
        return child

    ancestry: typing.Dict[
        HierarchicalFigureKey, typing.Set[HierarchicalFigureKey]
    ] = {}
    hierachy = HierarchicalFigure.new(ancestry=ancestry)
    fig = make_fig()
    fig.set_label('fig')
    gs = fig.add_gridspec(2, 2)
    ax1, ax2 = (
        bookkeep_ancestry(fig, fig.add_subplot(gs[i], label=name))
        for i, name in ((0, 'ax1'), (2, 'ax2'))
    )
    ax_inset = bookkeep_ancestry(
        ax1, ax1.inset_axes((.1, .1, .3, .3), label='ax_inset'),
    )
    ax_inset_2 = bookkeep_ancestry(
        ax_inset, ax_inset.inset_axes((.5,) * 4, label='ax_inset_2'),
    )
    hierachy.update(
        fig=fig, ax1=ax1, ax2=ax2, ax_inset=ax_inset, ax_inset_2=ax_inset_2,
    )
    if hasattr(matplotlib.figure.Figure, 'add_subfigure'):
        subfig = bookkeep_ancestry(
            fig, fig.add_subfigure(gs[:, 1], label='subfig'),
        )
        subax1, subax2 = (
            bookkeep_ancestry(subfig, subax)
            for subax in subfig.subplots(2, 1, squeeze=True)
        )
        subax1.set_label('subax1')
        subax2.set_label('subax2')
        hierachy.update(subfig=subfig, subax1=subax1, subax2=subax2)
    yield hierachy
    # Note: when we get back to here we will run make_fig()'s teardown


@pytest.fixture
def populated_hierarchical_fig(
    hierarchical_fig: HierarchicalFigure,
) -> HierarchicalFigure:
    """
    A populated figure with this hierachy:

    fig
     +- ax1
         +- ax_inset
             +- ax_inset_2
     +- ax2
     +- [subfig]
         +- [subax1]
         +- [subax2]

    CL methods have been called on these axes with specific formatters
    and `baz` values:

            ax      CL method            formatter  baz
           ax1     .cl_plot()          formatter()    1
           ax1     .cl_plot()  another_formatter()    3
           ax2     .cl_plot()          formatter()    2
           ax2  .cl_scatter()  scatter_formatter()    4
      ax_inset     .cl_plot()          formatter()    5
    ax_inset_2     .cl_plot()          formatter()    6
        subax1     .cl_plot()          formatter()    7
        subax2     .cl_plot()          formatter()    8
    """
    def plot_foo(
        ax_fig: HierarchicalFigureKey,
        method: CLMethodName,
        baz: Baz,
        *args, **kwargs,
    ) -> None:
        method: types.MethodType = getattr(hierarchical_fig[ax_fig], method)
        method(*args, baz=baz, **kwargs)
        values = hierarchical_fig.get('baz_values')
        if values is None:
            hierarchical_fig['baz_values'] = values = {}
        values.setdefault(ax_fig, set()).add(baz)

    fig, ax1, ax2 = (hierarchical_fig[key] for key in ('fig', 'ax1', 'ax2'))
    fig.register_cl_method('cl_plot', formatter)
    fig.register_cl_method('cl_scatter', scatter_formatter)
    plot_foo('ax1', 'cl_plot', 1, 0, 0)  # baz = 1
    plot_foo('ax2', 'cl_plot', 2, 0, 0)  # baz = 2
    ax1.register_cl_method('cl_plot', another_formatter)
    hierarchical_fig['ax_inset'].register_cl_method('cl_plot', formatter)
    plot_foo('ax1', 'cl_plot', 3, 0, 0)  # baz = 3
    plot_foo('ax2', 'cl_scatter', 4, [0, 1], [0, 1])  # baz = 4
    for key, baz in dict(
        ax_inset=5, ax_inset_2=6, subax1=7, subax2=8,
    ).items():
        if key not in hierarchical_fig:
            # No subfigures in old matplotlib versions
            continue
        plot_foo(key, 'cl_plot', baz, 0, 0)
    yield hierarchical_fig
    # Note: when we get back to here we will run make_fig()'s teardown


# ******************** Helper functions (general) ******************** #


def get_subplots_from_subplot_dict(
    subplots: typing.Mapping[typing.Hashable, matplotlib.axes.Axes],
) -> typing.List[matplotlib.axes.Axes]:
    """
    Retrieve the subplots from the return value of
    `Axes.subplot_mosaic()`.
    """
    return list(subplots.values())


def formatter(
    *,
    foo: typing.Optional[Foo] = None,
    bar: typing.Optional[Bar] = None,
    foobar: typing.Optional[Foobar] = None,
    baz: typing.Optional[Baz] = None,
) -> Keywords:
    """
    Data formatter function for the `Axes.cl_plot()` method.
    """
    markers = dict(zip(BAR_VALUES, 'sPD^v'))
    norm_radius = (np.array(FOOBAR_VALUES, dtype=float) ** .5).mean()
    color_norm = matplotlib.colors.LogNorm(*BAZ_LIMITS)
    creation_kwargs = {
        key: value
        for key, value in dict(
            foo=foo, bar=bar, foobar=foobar, baz=baz,
        ).items()
        if value is not None
    }
    assert ast.literal_eval(repr(creation_kwargs)) == creation_kwargs
    return dict(
        linestyle='none',
        fillstyle=('full' if foo is None else 'top' if foo else 'bottom'),
        marker=markers.get(bar, 'o'),
        markersize=(
            1 if foobar is None else (foobar ** .5 / norm_radius)
        ) * matplotlib.rcParams['lines.markersize'],
        color=(
            'tab:red' if baz is None else DEFAULT_COLOR_MAP(color_norm(baz))
        ),
        # Store info about the passed args at the label
        label=repr(creation_kwargs),
    )


def another_formatter(**kwargs):
    """
    A distinct formatter function from `formatter()`.
    """
    return dict(formatter(**kwargs), markeredgecolor='black')


def scatter_formatter(**kwargs):
    """
    A formatter function for the `Axes.cl_scatter()` method.
    """
    arg_name_mapping = dict(markersize='s')
    excluded_args = {'linestyle', 'fillstyle'}
    return {
        arg_name_mapping.get(name, name): value
        for name, value in formatter(**kwargs).items()
        if name not in excluded_args
    }


for func in another_formatter, scatter_formatter:
    func.__signature__ = inspect.signature(formatter)


def get_kwargs_from_handle(
    handle: typing.Union[
        matplotlib.artist.Artist, typing.Tuple[matplotlib.artist.Artist, ...],
    ],
) -> typing.List[typing.Union[Keywords, None]]:
    """
    Parse the handle-creation keyword arguments in `handle.get_label()`
    (added by `formatter()`) into list of dictionaries;
    if that can't be done, return `None`
    """
    result: typing.List[typing.Union[Keywords, None]] = []
    if not isinstance(handle, tuple):
        handle = handle,
    for hndl in handle:
        try:
            label = hndl.get_label()
            creation_kwargs = ast.literal_eval(label)
            assert is_keyword_mapping(creation_kwargs)
        except Exception:
            result.append(None)
        else:
            result.append(creation_kwargs)
    return result


def setup_plot(ax: Axes, **kwargs) -> None:
    """
    Use `formatter()` and `.cl_plot()` to plot data onto `ax` with the
    CKAs `foo` (`True` or `False`), `bar` (`'spam'`, `'ham'`, or
    `'eggs'`), `foobar` (integers from 1 to 5), and `baz` (real numbers
    between 0.1 and 100.0).
    """
    rng = np.random.default_rng(42)
    n = 50
    if ax.cl_method_formatters.get('cl_plot') is not formatter:
        ax.register_cl_method('cl_plot', formatter)
    for (x, y), foo, bar, foobar, baz in zip(
        rng.random((n, 2)),
        *(
            rng.choice(values, n)
            for values in (FOO_VALUES, BAR_VALUES, FOOBAR_VALUES)
        ),
        # Make baz log-uniformly distributed
        BAZ_LIMITS[0] * ((BAZ_LIMITS[1] / BAZ_LIMITS[0]) ** rng.random(n)),
    ):
        ax.cl_plot(x, y, foo=foo, bar=bar, foobar=foobar, baz=baz, **kwargs)


def repr_levels(
    levels: typing.List[typing.List[T]],
    key: typing.Callable[[T], typing.Any] = id,
    pretty: bool = True,
    indent: typing.Union[str, int, None] = 2,
) -> str:
    """
    Return a representation of a list of levels (e.g. that is returned
    by `{Axes|Figure}.get_cl_children()`.
    """
    levels_sorted = [
        sorted([item for item in level if item is not None], key=key)
        for level in levels
    ]
    result = (pprint.pformat if pretty else repr)(levels_sorted)
    if indent:
        if not isinstance(indent, str):
            indent = ' ' * indent
        return textwrap.indent(result, indent)


def get_legend_landles(
    legend: matplotlib.legend.Legend,
) -> typing.Sequence[matplotlib.artist.Artist]:
    """
    Compatibility layer over `Legend.legend_handles` (used to be
    `Legend.legendHandles` prior to `matplotlib` version 3.7)
    """
    try:
        return legend.legend_handles
    except AttributeError:
        pass
    return legend.legendHandles


# **************** Helper functions (alias-checking) ***************** #


def get_kwarg_from_handle(
    handle: matplotlib.artist.Artist, kwarg: Identifier,
) -> typing.Union[typing.Any, None]:
    """
    Get the value of a CKA from a simple handle formatted with
    `formatter()`.
    """
    try:
        kwargs, = get_kwargs_from_handle(handle)
        return kwargs[kwarg]
    except Exception:
        return None


def check_default_cat_value_classifications(
    legend: matplotlib.legend.Legend,
) -> None:
    """
    Check that `foobar` values are formatted as legend entries
    representing a discrete-number range.
    """
    foobar_values = [
        get_kwarg_from_handle(handle, 'foobar')
        for handle in get_legend_landles(legend)
    ]
    foobar_values = [foobar for foobar in foobar_values if foobar is not None]
    assert foobar_values == list(range(1, 6))


def check_altered_cat_value_classifications(
    legend: matplotlib.legend.Legend,
) -> None:
    """
    Check that `foobar` values are formatted as a single legend entry
    reperesenting a linear-scaled number range.
    """
    nsamples = CATEGORIZED_LEGEND_DEFAULTS['linear_range_samples']
    if nsamples is None:
        nsamples = CATEGORIZED_LEGEND_DEFAULTS['range_samples']
    assert isinstance(nsamples, numbers.Integral)
    # Note: we can't only look at the `.legend_handles` because
    # `matplotlib.legend_handler.HandlerBase.legend_artist()` abridges
    # the tuple of input artists to the first one
    entries: typing.List[int] = []
    for text in legend.texts:
        string = text.get_text()
        matches = re.findall(r'foobar\$=([0-9]+)\$', string)
        if not matches:
            continue
        assert not entries
        entries.extend(int(foobar) for foobar in matches)
    assert len(entries) == nsamples
    assert pytest.approx(entries) == np.linspace(
        FOOBAR_VALUES[0], FOOBAR_VALUES[-1], nsamples,
    )


def check_cat_name_order(
    legend: matplotlib.legend.Legend,
    sorted: typing.Sequence[Identifier],
) -> None:
    """
    Check whether the category names in the legend are ordered as
    specified.
    """
    cat_names: typing.List[Identifier] = []
    for handle in get_legend_landles(legend):
        try:
            (cat_name,), = get_kwargs_from_handle(handle)
        except Exception:
            continue
        if not (cat_names and cat_names[-1] == cat_name):
            cat_names.append(cat_name)
    assert cat_names == list(sorted)


def check_cat_values_order(
    legend: matplotlib.legend.Legend,
    cat_name: Identifier,
    sorted: typing.Sequence[typing.Any],
) -> None:
    """
    Check whether the category names in the legend are ordered as
    specified.
    """
    cat_values: typing.List[typing.Any] = []
    for handle in get_legend_landles(legend):
        try:
            value = get_kwarg_from_handle(handle, cat_name)
            if value is not None:
                cat_values.append(value)
        except Exception:
            continue
    cat_values_copy = list(cat_values)
    cat_values_copy.sort(key=sorted.index)
    assert cat_values == cat_values_copy


def check_cat_names_typesetting(
    legend: matplotlib.legend.Legend,
    typesetter: typing.Callable[[Identifier], str] = str,
    expect_name: bool = True,
) -> None:
    """
    Check whether the category names in the legend are typeset as
    specified.
    """
    for text, handle in zip(legend.texts, get_legend_landles(legend)):
        string = text.get_text()
        if not string:
            continue
        (cat_name,), = get_kwargs_from_handle(handle)
        assert (typesetter(cat_name) in string) ^ (not expect_name)


def check_cat_values_typesetting(
    legend: matplotlib.legend.Legend,
    typesetters: typing.Optional[
        typing.Mapping[
            Identifier,
            typing.Union[
                typing.Callable[[typing.Hashable], str],
                typing.Mapping[typing.Hashable, str],
                str,
            ]
        ]
    ] = None,
) -> None:
    """
    Check whether the category values in the legend are typeset as
    specified.
    """
    if typesetters is None:
        typesetters = {}
    for text, handle in zip(legend.texts, get_legend_landles(legend)):
        string = text.get_text()
        if not string:
            continue
        ((cat_name, cat_value),), = (
            kwargs.items() for kwargs in get_kwargs_from_handle(handle)
        )
        assert apply_typesetter(typesetters.get(cat_name), cat_value) in string


def check_number_of_entries(
    legend: matplotlib.legend.Legend,
    nvalues: typing.Union[
        numbers.Integral, typing.Mapping[Identifier, numbers.Integral],
    ],
) -> None:
    """
    Check whether the number of legend entries for each category is as
    expected.
    """
    default_nvalues = CATEGORIZED_LEGEND_DEFAULTS.get('values_per_line', 1)
    num_legend_entries: typing.Dict[Identifier, int] = {}
    for handle in get_legend_landles(legend):
        try:
            (cat_name,), = get_kwargs_from_handle(handle)
        except Exception:
            continue
        num_legend_entries[cat_name] = num_legend_entries.get(cat_name, 0) + 1
    if not isinstance(nvalues, typing.Mapping):
        nvalues = dict.fromkeys(num_legend_entries, nvalues)
    for cat_name, num_entries in num_legend_entries.items():
        if cat_name not in NUM_VALUES:
            continue
        expected_num_entries = math.ceil(
            NUM_VALUES[cat_name] / nvalues.get(cat_name, default_nvalues)
        )
        assert num_entries == expected_num_entries


def check_ncols(legend: matplotlib.legend.Legend, n: numbers.Integral) -> None:
    for attr in '_ncols', '_ncol':
        if not hasattr(legend, attr):
            continue
        assert getattr(legend, attr) == n
        return
    assert False, 'cannot get number of columns'


# ****************************** Tests ******************************* #


@pytest.mark.parametrize(
    'prop,value', [('zorder', 1), ('visible', False), ('alpha', .5)],
)
@pytest.mark.parametrize('set_at_instantiation', [True, False])
def test_ax_properties(
    make_ax: typing.Callable[..., Axes],
    prop: Identifier,
    value: typing.Any,
    set_at_instantiation: bool,
) -> None:
    """
    Test that the attributes of an `~.Axes` object can be set correctly
    both at instantiation and afterwards.
    """
    kwargs = dict(subplot_kw={prop: value})
    subplots_args: list = [1, 1, 0]
    if set_at_instantiation:
        subplots_args.append(kwargs)
    ax = make_ax(subplots=subplots_args)
    if not set_at_instantiation:
        ax.set(**kwargs['subplot_kw'])
    assert isinstance(ax, Axes)
    ax_inner = get_wrapped_instance(ax)
    assert isinstance(ax_inner, matplotlib.axes.Axes)
    assert not isinstance(ax_inner, Axes)
    ax_value, ax_inner_value = (
        getattr(ax_obj, 'get_' + prop)() for ax_obj in (ax, ax_inner)
    )
    assert value == pytest.approx(ax_value) == ax_inner_value


@parametrize_all(
    ('fig', 'add_axes', ((.1, .1, .8, .8),)),
    ('fig', 'add_subfigure', (matplotlib.gridspec.GridSpec(2, 2)[3],)),
    ('fig', 'add_subplot', (1, 1, 1)),
    ('fig', 'gca'),
    ('fig', 'subfigures', (1, 2), dict(squeeze=False), np.ndarray.flatten),
    (
        'fig', 'subplot_mosaic',
        ([[1, 1, 2], [1, 1, 3], [4, '.', 3]],), None,
        get_subplots_from_subplot_dict,
    ),
    ('fig', 'subplots', (2, 2), None, np.ndarray.flatten),
    # Note: add_child_axes() is indirectly tested via inset_axes()
    ('ax', 'inset_axes', ((.5, .5, .4, .4),)),
    exclude=('make_ax', 'make_fig'),
)
def test_child_spawning_methods(
    make_ax: typing.Callable[..., Axes],
    make_fig: typing.Callable[..., Figure],
    obj_type: typing.Literal[Axes, Figure, 'ax', 'fig'],
    method: Identifier,
    args: typing.Union[typing.Sequence, None],
    kwargs: typing.Union[Keywords, None],
    get_children: typing.Union[
        typing.Callable[
            [typing.Any], typing.Collection[typing.Union[Axes, Figure]]
        ],
        None,
    ]
) -> None:
    """
    Test that the axes-/(sub-)figure-spawning methods of
    `~.{Axes|Figure}` create instances of `~.{Axes|Figure}` which are
    immediate children of the parent.

    Parameters
    ----------
    make_ax(), make_fig()
        Functions used to spawn a parent `~.Axes`/`~.Figure` object
    obj_type
        Type of the parent object
    method
        Name of the method on the parent object to call
    args, kwargs
        Arguments to pass to the method
    get_children()
        Optional callable converting the return value of the method into
        a collection of the created children axes/figure objects;
        default is to assume a single child is created and returned by
        the method
    """
    if obj_type in (Axes, 'ax'):
        obj_type = 'ax'
        parent = make_ax()
    elif obj_type in (Figure, 'fig'):
        obj_type = 'fig'
        parent = make_fig()
    else:
        assert False, f'obj_type = {obj_type!r}'
    if get_children is None:
        AxesOrFigure = typing.TypeVar('AxesOrFigure', Axes, Figure)

        def get_children(
            result: AxesOrFigure,
        ) -> typing.Tuple[AxesOrFigure]:
            return result,

    bound_method = getattr(parent, method, None)
    if bound_method is None:
        pytest.skip(f'Cannot find {obj_type}.{method}()')
    result = bound_method(*(args or []), **(kwargs or {}))
    children_spawned = get_children(result)
    immediate_children, = parent.get_cl_children(max_level=1)
    for i, child in enumerate(children_spawned):
        assert isinstance(child, (Axes, Figure))
        assert child in immediate_children


def test_formatter_propagation_to_child(
    print_msg: typing.Callable,
    hierarchical_fig: HierarchicalFigure,
) -> None:
    """
    Test whether formatter changes (`[de]register_cl_method()`,
    `set_default_cl_formatter()`) are properly propagated to the
    descendents.
    """
    PS = typing.ParamSpec('PS')
    Formatter = typing.Union[
        typing.Callable[PS, Keywords],
        typing.Callable[PS, typing.Sequence[Keywords]],
    ]

    class MyError(RuntimeError):
        pass

    def bad_formatter(*, foo=None, bar=None) -> typing.NoReturn:
        raise MyError

    def formatter_1(*, foo=None, bar=None) -> Keywords:
        return {}

    def formatter_2(*, foo=None, bar=None) -> Keywords:
        return {}

    def get_default(
        obj: typing.Union[Axes, Figure],
    ) -> typing.Union[Formatter, None]:
        return obj.default_cl_formatter

    def get_method(
        obj: typing.Union[Axes, Figure],
    ) -> typing.Union[Formatter, None]:
        return obj.cl_method_formatters.get(method)

    def set_default(
        obj: typing.Union[Axes, Figure], formatter: Formatter,
    ) -> Formatter:
        return obj.set_default_cl_formatter(formatter)

    def set_method(
        obj: typing.Union[Axes, Figure], formatter: Formatter,
    ) -> Formatter:
        return obj.register_cl_method(method, formatter)

    def del_default(obj: typing.Union[Axes, Figure]) -> None:
        obj.default_cl_formatter = None

    def del_method(obj: typing.Union[Axes, Figure]) -> None:
        obj.deregister_cl_method(method)

    def iter_family(
        obj: typing.Union[Axes, Figure]
    ) -> typing.Generator[typing.Union[Axes, Figure], None, None]:
        yield obj
        for level in obj.get_cl_children():
            yield from level

    fig = hierarchical_fig['fig']
    method = 'cl_plot'

    # Test a bad formatter
    for method_name, setter in [
        ('set_default_cl_formatter', set_default),
        ('register_cl_method', set_method),
    ]:
        try:
            setter(fig, bad_formatter)
        except MyError:
            pass
        else:
            assert False, (
                f'`{method_name}()` did not intercept the bad formatter'
            )

    # Test formatter registration
    for obj, formatter, default_formatter in [
        (fig, formatter_1, formatter_2),
        (hierarchical_fig['ax1'], formatter_2, formatter_1),
    ]:
        set_default(obj, default_formatter)
        set_method(obj, formatter)
        family = list(iter_family(obj))
        for member in family:
            assert (
                get_default(member) is default_formatter
            ), (obj, member)
            assert (
                get_method(member) is formatter
            ), (obj, member)
        print_msg(
            'Formatter registration:',
            '(Default) -> {!r}, {} -> {!r}'.format(
                default_formatter, method, formatter,
            ),
            'ok for {!r} @ {:#x} and its {} descendent(s)'.format(
                obj, id(obj), len(family) - 1,
            ),
            sep='\n',
        )
    # Sanity check: setting ax1 doesn't affect fig
    assert fig.cl_method_formatters[method] is formatter_1
    assert fig.default_cl_formatter is formatter_2

    # Test formatter deregistration
    for obj_name in 'ax1', 'subfig', 'fig':
        obj = hierarchical_fig.get(obj_name)
        if obj is None:
            print_msg(f'`{obj_name}` doesn\'t exist', file=sys.stderr)
            continue
        for deleter, getter in [
            (del_default, get_default), (del_method, get_method),
        ]:
            assert getter(obj) is not None, obj
            deleter(obj)
            family = list(iter_family(obj))
            for member in family:
                assert (getter(member) is None), (obj, member)
            print_msg(
                'Formatter deregistration:',
                'ok for {!r} @ {:#x} and its {} descendent(s)'.format(
                    obj, id(obj), len(family) - 1,
                ),
                sep='\n',
            )


def test_ax_descendents(
    print_msg: typing.Callable,
    make_fig: typing.Callable[..., Figure],
) -> None:
    """
    Test that the levels of descendents of an `Axes` object is resolved
    correctly in `.get_cl_children()`.
    """
    fig = make_fig()
    axes_objs: typing.List[typing.List[Axes]] = []
    # Make nested inset axes
    for i in range(4):
        if axes_objs:
            ax = axes_objs[-1][-1].inset_axes((.5,) * 4)
        else:
            # Nowadays (since v3.7) there isn't any difference between
            # an `Axes` and a `SubplotBase` object, but create a subplot
            # explicitly for backwards compatibility
            ax_host = ax = Axes.wrap(host_subplot(111, figure=fig))
        ax.set_label(f'level {i}')
        axes_objs.append([ax])
    # Make a parasite and create children there
    ax_parasite = Axes.wrap(ax_host.twinx())
    assert ax_host.parasites[0] is get_wrapped_instance(ax_parasite)
    ax_host.parasites[0] = ax_parasite
    ax_parasite.set_label('level 1 (parasite)')
    axes_objs[1].append(ax_parasite)
    ax_child_parasite = ax_parasite.inset_axes((.5,) * 4)
    ax_child_parasite.set_label('level 2 (child of parasite)')
    axes_objs[2].append(ax_child_parasite)
    # Check the resolution of descendents
    for max_level, slice_cap in [
        (True, None), (False, 0), (None, None), *((i, i) for i in range(4)),
    ]:
        family_tree = ax_host.get_cl_children(max_level=max_level)
        expected_family_tree = axes_objs[1:][:slice_cap]
        level_ids, expected_level_ids = (
            [{id(descendent) for descendent in level} for level in tree]
            for tree in (family_tree, expected_family_tree)
        )
        msg = (
            'Descendents (max_level=({!r} -> {!r})):\nExpected\n{},\nGot\n{}\n'
        ).format(
            max_level,
            slice_cap,
            repr_levels(expected_family_tree),
            repr_levels(family_tree),
        )
        assert level_ids == expected_level_ids, msg
        print_msg(msg)


def test_fig_descendents(
    print_msg: typing.Callable,
    make_fig: typing.Callable[..., Figure],
) -> None:
    """
    Test that the levels of descendents of a `Figure` object is resolved
    correctly in `.get_cl_children()`.
    """
    nlevels = 5
    fig = make_fig()
    fig.set_label('level 0')
    subfig_available = hasattr(Figure, 'add_subfigure')
    if subfig_available:
        family_tree: typing.List[typing.List[typing.Union[Axes, Figure]]]
    else:
        family_tree: typing.List[typing.List[typing.Union[Axes, Figure, None]]]
    family_tree = [[fig]]
    # Make nested subfigures and add axes thereto
    for i in range(1, nlevels if subfig_available else 1):
        last_fig = family_tree[-1][0]
        gridspec = last_fig.add_gridspec(2, 2)
        if subfig_available:
            subfig = last_fig.add_subfigure(gridspec[0])
            subfig.set_label(f'level {i}')
        else:
            subfig = None
        subplots: typing.List[Axes] = [
            last_fig.add_subplot(
                gridspec[j],
                label='(level {})[{}, {}]'.format(i, *divmod(j, 2)),
            )
            for j in range(1, 4)
        ]
        family_tree.append([subfig, *subplots])
    # Add inset axes to subplots
    for i, level in enumerate(list(family_tree)):
        try:
            _, ax, *_ = level
        except ValueError:  # Level don't have enough entries (no axes)
            continue
        try:
            next_level = family_tree[i + 1]
        except IndexError:  # Last level of subfigs -> add another level
            next_level = [None]
            family_tree.append(next_level)
        ax_inset = ax.inset_axes((.5,) * 4)
        ax_inset.set_label(f'level {i + 1} (inset at {ax.get_label()})')
        next_level.append(ax_inset)
    # Check the resolution of descendents of the main figure
    for max_level, slice_cap in [
        (True, None), (False, 0), (None, None),
        *((i, i) for i in range(nlevels)),
    ]:
        trunc_tree = fig.get_cl_children(max_level=max_level)
        expected_trunc_tree = family_tree[1:][:slice_cap]
        level_ids, expected_level_ids = (
            [
                {
                    id(descendent)
                    for descendent in level if descendent is not None
                }
                for level in tree
            ]
            for tree in (trunc_tree, expected_trunc_tree)
        )
        msg = (
            'Top-level descendents (max_level=({!r} -> {!r})):'
            '\nExpected\n{},\nGot\n{}\n'
        ).format(
            max_level,
            slice_cap,
            repr_levels(expected_trunc_tree),
            repr_levels(trunc_tree),
        )
        assert level_ids == expected_level_ids, msg
        print_msg(msg)
    # Check the resolution of descendents of subfigures
    try:
        _, (subfig, *_), *_ = family_tree
        if subfig is None:
            return
    except ValueError:
        return
    trunc_tree = subfig.get_cl_children(max_level=None)
    expected_trunc_tree = [list(level) for level in family_tree[2:]]
    expected_trunc_tree[0] = [
        # Note: drop the inset axes from the sibling axes of `subfig`'s
        child for child in expected_trunc_tree[0] if child.figure is subfig
    ]
    level_ids, expected_level_ids = (
        [
            {id(descendent) for descendent in level if descendent is not None}
            for level in tree
        ]
        for tree in (trunc_tree, expected_trunc_tree)
    )
    msg = 'Subfigure descendents:\nExpected\n{},\nGot\n{}\n'.format(
        repr_levels(expected_trunc_tree), repr_levels(trunc_tree),
    )
    assert level_ids == expected_level_ids, msg
    print_msg(msg)


# For the next test:
TestCollectedBazValues = typing.Union[
    typing.Collection[Baz],  # Check if these baz values are collected
    typing.Literal['error'],  # Check if a ValueError was raised
    None,  # Skip this section of the test
]


@parametrize_all(
    # All descendents (recursively) + self
    (False, {1, 2, 3, 4, 5, 6, 7, 8}, {1, 3, 5, 6}, {2, 4}, None, None),
    # Self (+ immediate child ax for figures), and one level deeper
    (False, {1, 2, 3, 4, 7, 8}, {1, 3, 5}, {2, 4}, None, None, 1),
    # Axes-/Figure-based selection
    (
        False,
        {1, 3, 5, 6},
        {1, 3, 5, 6},
        'error',  # Since ax1 is not a descendent of ax2
        'ax1',  # Only select values from ax1 and its descendents
    ),
    (
        False,
        {5, 7, 8},
        None, None,  # Skip ax1 and ax2
        ['ax_inset', 'subfig'],  # Only select values from these objects
        None,
        0,  # max_level: only select values immediate to these objects
    ),
    (
        True,  # Test the `subfig` alias
        {1, 3, 7, 8},
        None, None,
        ['ax1', 'subfig'],
        None,
        0,  # max_level
    ),
    # CL-method-based selection
    (
        False,
        {4}, {}, {4},
        None,
        'cl_scatter',  # cl_method
    ),
    (
        False,
        {1, 2, 3, 5, 6, 7, 8}, {1, 3, 5, 6}, {2},
        None,
        ['cl_plot'],
    ),
    # Formatter-based selection
    (
        False,
        {1, 2, 5, 6, 7, 8}, {1, 5, 6}, {2},
        None, None, None,
        formatter,
    ),
    (
        False,
        {3, 4}, {3}, {4},
        None, None, None,
        [another_formatter, scatter_formatter],
    ),
    exclude='populated_hierarchical_fig',
)
def test_category_value_collection_scope(
    populated_hierarchical_fig: HierarchicalFigure,
    use_ax_alias: bool,
    expected_fig_values: TestCollectedBazValues,
    expected_ax1_values: TestCollectedBazValues,
    expected_ax2_values: TestCollectedBazValues,
    ax: typing.Union[
        HierarchicalFigureKey, typing.Collection[HierarchicalFigureKey], None,
    ],
    cl_method: typing.Union[
        CLMethodName, typing.Collection[CLMethodName], None,
    ],
    max_level: typing.Union[numbers.Integral, bool, None],
    formatter: typing.Union[Formatter, typing.Collection[Formatter], None],
) -> None:
    """
    Test the behavior of

    >>> {Axes|Figure}.get_cl_categories(  # noqa # doctest: +SKIP
    ...     {ax|subfig}=..., cl_method=..., formatter=...,
    ... )
    """
    all_present_baz_values: typing.Set[Baz] = {
        # Note: some baz values (and figure components) may be absent in
        # older matplotlib versions
        baz for values in populated_hierarchical_fig['baz_values'].values()
        for baz in values
    }

    if ax is None:
        ax_value = None
    elif isinstance(ax, str):
        ax_value = populated_hierarchical_fig.get(ax)
    else:
        ax_value = [
            populated_hierarchical_fig[key] for key in set(ax)
            if key in populated_hierarchical_fig
        ]
    # Test value collection from axes and figures
    for key, expected_values in dict(
        fig=expected_fig_values,
        ax1=expected_ax1_values,
        ax2=expected_ax2_values,
    ).items():
        if expected_values is None:  # Skip testing this object
            continue
        obj = populated_hierarchical_fig[key]
        kwargs = dict(
            cl_method=cl_method,
            formatter=formatter,
            ax=ax_value,
            max_level=max_level,
        )
        if use_ax_alias:
            kwargs['subfig'] = kwargs.pop('ax')
        if expected_values == 'error':
            # Expect an error during value collection (e.g. when `ax` is
            # not a descendent of the object)
            test_context = pytest.raises(
                ValueError,
                match=r'not .* instance\(s\) belonging to the (figure|axes)',
            )
        else:
            expected_values = set(expected_values) & all_present_baz_values
            test_context = contextlib.nullcontext()
        with test_context:
            assert (
                obj.get_cl_categories(**kwargs, concise=True).get('baz', set())
                == expected_values
            ), (key, expected_values)


@parametrize_all(
    (
        'ax2',
        {
            ('baz', 'cl_plot', formatter): {2},
            ('baz', 'cl_scatter', scatter_formatter): {4},
        },
    ),
    (
        'fig',
        {
            ('baz', 'cl_plot', formatter): {1, 2},
            ('baz', 'cl_plot', another_formatter): {3},
            ('baz', 'cl_scatter', scatter_formatter): {4},
        },
    ),
    exclude='populated_hierarchical_fig',
)
def test_category_value_collection_conciseness(
    populated_hierarchical_fig: HierarchicalFigure,
    key: HierarchicalFigureKey,
    full_categories: typing.Dict[
        typing.Tuple[typing.Literal['baz'], CLMethodName, Formatter],
        typing.Set[Baz]
    ]
) -> None:
    """
    Test the behavior of `{Axes|Figure}.get_cl_categories(concise=...)`.
    """
    obj = populated_hierarchical_fig[key]
    abridged_dict = {}
    for (cka, *_), values in full_categories.items():
        abridged_dict.setdefault(cka, set()).update(values)
    assert obj.get_cl_categories(concise=False) == full_categories
    assert obj.get_cl_categories(concise=True) == abridged_dict


@parametrize_all(
    # Recall: the rules are to (1) only collect descendent axes up to
    # the designated level, and (2) include the immediate child axes of
    # subfigures in the most-distant subfigures
    *((value, ['ax1', 'ax2'], ['ax1']) for value in (0, False)),
    (1, ['ax1', 'ax2', 'subax1', 'subax2'], ['ax1', 'ax_inset']),
    (
        2,
        ['ax1', 'ax2', 'ax_inset', 'subax1', 'subax2'],
        ['ax1', 'ax_inset', 'ax_inset_2'],
    ),
    *(
        (
            value,
            ['ax1', 'ax2', 'ax_inset', 'ax_inset_2', 'subax1', 'subax2'],
            ['ax1', 'ax_inset', 'ax_inset_2'],
        )
        for value in (True, 3, None)
    ),
    exclude='hierarchical_fig',
)
def test_category_value_collection_max_level(
    hierarchical_fig: HierarchicalFigure,
    max_level: typing.Union[int, bool, None],
    incl_fig: typing.Collection[HierarchicalFigureKey],
    incl_ax1: typing.Collection[HierarchicalFigureKey],
) -> None:
    """
    Test the behavior of
    `{Axes|Figure}.get_cl_categories(max_level=...)`.

    Parameters
    ----------
    max_level
        Value of `max_level`
    incl_fig, incl_ax1
        Keys in `hierarchical_fig()` indicating whence the values
        collected at `hierarchical_fig()['fig']` (resp. `...['ax1']`)
        should come from
    """
    def get_cl_cats(
        key: HierarchicalFigureKey,
    ) -> typing.Dict[typing.Literal['baz'], typing.Set[Baz]]:
        return (
            hierarchical_fig[key]
            .get_cl_categories(concise=True, max_level=max_level)
        )

    values: typing.Dict[HierarchicalFigureKey, Baz] = {
        ax: i + 1
        for i, ax in enumerate(
            ('ax1', 'ax2', 'ax_inset', 'ax_inset_2', 'subax1', 'subax2')
        )
        # Note: older matplotlib versions don't have subfig
        if ax in hierarchical_fig
    }
    hierarchical_fig['fig'].register_cl_method('cl_plot', formatter)
    for ax, baz in values.items():
        hierarchical_fig[ax].cl_plot(0, 0, baz=baz)
    fig_cat_values, ax1_cat_values = (
        dict(baz={i for ax, i in values.items() if ax in incl})
        for incl in (incl_fig, incl_ax1)
    )
    assert get_cl_cats('fig') == fig_cat_values
    assert get_cl_cats('ax1') == ax1_cat_values


@wip_test
def test_label_collection(
    make_fig: typing.Callable[..., Figure],
) -> None:
    """
    TODO: test to check the collection of legend entries via the
    `get_cl_handles_labels()` method
    """
    # Behaviors to test:
    # Selection:
    # - suppressed_categories
    # - conflate
    # Typesetting:
    # - category_{name_{sorter|typesetter},value_{sorters|typesetters}}
    #   (Already covered by tests in `test_plot_unit_tests.py`?)
    # - category_value_classifications
    # - order_base
    # - max_{linear|discrete}_orders
    # - [log_|linear_]range_samples
    # - show_category_names
    # - values_per_line
    # Layout:
    # - ncol[s]|nrow[s]
    # - fill_{columns|rows}_first
    # - gap_between_categories


@parametrize_all(
    (
        'category_value_classifications',
        ('label_classifications',),
        dict(foobar='LINEAR_RANGE'),
        check_default_cat_value_classifications,
        check_altered_cat_value_classifications,
        dict(
            category_value_typesetters=dict(foobar='foobar$={}$', baz='{:.1E}'),
        ),
    ),
    (
        'category_name_sorter',
        ('category_sorter',),
        ('foo', 'bar', 'foobar', 'baz').index,
        functools.partial(
            check_cat_name_order,
            sorted=sorted(['foo', 'bar', 'foobar', 'baz']),
        ),
        functools.partial(
            check_cat_name_order, sorted=['foo', 'bar', 'foobar', 'baz'],
        ),
    ),
    (
        'category_value_sorters',
        ('label_sorters',),
        dict(bar=('spam', 'ham', 'eggs').index),
        functools.partial(
            check_cat_values_order,
            cat_name='bar', sorted=sorted(['spam', 'ham', 'eggs']),
        ),
        functools.partial(
            check_cat_values_order,
            cat_name='bar', sorted=['spam', 'ham', 'eggs'],
        ),
    ),
    (
        'category_name_typesetter',
        ('category_typesetter',),
        str.upper,
        check_cat_names_typesetting,
        functools.partial(check_cat_names_typesetting, typesetter=str.upper),
        dict(show_category_names=True),
    ),
    (
        'category_value_typesetters',
        ('label_typesetters',),
        dict(foo={True: 'T', False: 'F'}, baz='{:.1E}'),
        check_cat_values_typesetting,
        functools.partial(
            check_cat_values_typesetting,
            typesetters=dict(foo={True: 'T', False: 'F'}, baz='{:.1E}'),
        ),
    ),
    (
        'show_category_names',
        ('show_categories_with_labels',),
        True,
        functools.partial(check_cat_names_typesetting, expect_name=False),
        check_cat_names_typesetting,
    ),
    (
        'values_per_line',
        ('labels_per_line',),
        5,
        functools.partial(check_number_of_entries, nvalues=1),
        functools.partial(check_number_of_entries, nvalues=5),
    ),
    (
        'ncol',
        ('ncols', 'nrow', 'nrows'),
        2,
        functools.partial(check_ncols, n=1),
        functools.partial(check_ncols, n=2),
    ),
    exclude='make_fig',
)
def test_pass_args_by_aliases(
    make_fig: typing.Callable[..., Figure],
    param_name: Identifier,
    aliases: typing.Collection[Identifier],
    passed_value: typing.Any,
    check_default: LegendChecker,
    check_passed: LegendChecker,
    other_kwargs: typing.Union[Keywords, None]
) -> None:
    """
    Test to check that the behavior of `categorized_legend()` is
    identical no matter whether the argument or its alias is passed

    Parameters
    ----------
    make_fig()
        Callable spawning a figure
    param_name
        Name of the aliased parameter
    aliases
        Collection of aliases to `param_name`
    passed_value
        Value to pass to `param_name`
    check_default()
        Checker callable which takes a legend object (from the axes
        object to which the parameter is not passed when calling
        `.categorized_legend()`), and raises an exception if it doesn't
        behave as expected
    check_passed()
        Checker callable which takes a legend object (from the axes
        objects to which the parameter (or any of its aliases) is passed
        when calling `.categorized_legend()`), and raises an exception
        if it doesn't behave as expected
    other_kwargs
        Optional keyword arguments to pass to `.categorized_legend()`

    Notes
    -----

    Tested aliases
    --------------
    label_classifications
    -> category_value_classifications
    category_sorter
    -> category_name_sorter
    label_sorters
    -> category_value_sorters
    category_typesetter
    -> category_name_typesetter
    label_typesetters
    -> category_value_typesetters
    show_categories_with_labels
    -> show_category_names
    labels_per_line
    -> values_per_line
    ncols, nrows, nrow
    -> ncol
    """
    # Set up the subplots
    aliases = list(set(aliases))
    if other_kwargs is None:
        other_kwargs = {}
    fig = make_fig()
    gridspec = fig.add_gridspec(*get_grid_dimensions(2 + len(aliases)))
    ax_default, ax_canonical, *ax_objs_aliases = ax_objs = [
        fig.add_subplot(gridspec[i]) for i in range(2 + len(aliases))
    ]
    ax_default.set_label('default')
    ax_canonical.set_label('canonical: `{}`'.format(param_name))
    for ax, alias in zip(ax_objs_aliases, aliases):
        ax.set_label('aliased: `{}` -> `{}`'.format(alias, param_name))
    for ax in ax_objs:
        setup_plot(ax)
    # Set up the cateorized legends and check them
    for ax, checker, kwargs in zip(
        ax_objs,
        itertools.chain((check_default,), itertools.cycle((check_passed,))),
        itertools.chain(
            (other_kwargs, {**other_kwargs, param_name: passed_value}),
            ({**other_kwargs, alias: passed_value} for alias in aliases),
        ),
    ):
        legend = ax.categorized_legend(**kwargs)
        try:
            checker(legend)
        except Exception as e:
            raise (
                type(e)('{!r} ({!r}): {}'.format(ax, ax.get_label(), e.args[0]))
                .with_traceback(e.__traceback__)
            ) from None


@parametrize_all(
    *((h, t) for h in (True, False) for t in (True, False)),
    exclude='populated_hierarchical_fig',
)
def test_hide_empty_legend_with_warning(
    populated_hierarchical_fig: HierarchicalFigure,
    handles: bool,
    title: bool,
) -> None:
    """
    Test for warnings against empty categorized legends:

    - If a legend has neither handles nor a title, it is set to be
      invisible with an appropriate warning.

    - If it has a title but not handles, a warning is issued.
    """
    for fig_component in 'fig', 'ax1', 'ax_inset_2', 'subfig', 'subax1':
        if fig_component not in populated_hierarchical_fig:
            # Old `matplotlib` versions
            assert fig_component.startswith('sub')
            continue
        ax_fig = populated_hierarchical_fig[fig_component]
        legend_visible = handles or title
        kwargs: Keywords = {}
        if not handles:
            kwargs['suppressed_categories'] = 'baz'
        if title:
            kwargs['title'] = 'legend'
        with (
            pytest.warns(UserWarning, match='invisible .* empty')
            if not legend_visible else
            pytest.warns(UserWarning, match='empty except .* title')
            if not handles else
            catch_warnings(action='error', category=UserWarning)
        ):
            legend = ax_fig.categorized_legend(**kwargs)
        assert legend.get_visible() == legend_visible
