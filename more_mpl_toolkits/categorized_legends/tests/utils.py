import contextlib
import functools
import inspect
import re
import types
import typing
import warnings

import pytest

from tjekmate import check, identical_to, is_collection, is_keyword, is_sequence

from ..._utils.monkeypatching import clone_module
# Note: we need this to backport names from `typing_extensions` to
# `typing`
from .. import _types  # noqa: F401

__all__ = (
    'wip_test', 'parametrize_all', 'apply_typesetter',
    'catch_warnings', 'is_deprecated', 'not_deprecated',
)

PS = typing.ParamSpec('PS')
Identifier = typing.TypeVar('Identifier', bound=str)
TestCallable = typing.Callable[PS, typing.TypeVar('T')]
Typesetter = typing.Union[
    str,
    typing.Mapping[typing.Hashable, str],
    typing.Callable[[typing.Hashable], str],
    None,
]
XC = typing.TypeVar('XC', bound=Exception)
_WarningsChecker = type(pytest.warns(Warning))

if callable(getattr(_WarningsChecker, 'matches', None)):  # pytest v8.0+
    pytest_warns = pytest.warns
else:
    import importlib

    _wc_module = clone_module(
        importlib.import_module(_WarningsChecker.__module__),
    )
    assert _wc_module.WarningsChecker is not _WarningsChecker

    _wc_exit = _wc_module.WarningsChecker.__exit__

    @functools.wraps(_wc_exit)
    def exit_and_reissue_unmatched(
        self,
        cls: typing.Optional[typing.Type[XC]] = None,
        xc: typing.Optional[XC] = None,
        tb: typing.Optional[types.TracebackType] = None,
    ) -> None:
        """
        Partial backport of PR #10937 (`pytest` v8.0.0), re-emitting all
        non-matching warnings.
        """
        def is_match(wm: warnings.WarningMessage) -> bool:
            if not issubclass(wm.category, self.expected_warning):
                return False
            if self.match_expr is None:
                return True
            return bool(re.search(self.match_expr, str(wm.message)))

        try:
            _wc_exit(self, cls, xc, tb)
        finally:
            for wm in self:
                if is_match(wm):
                    # The original .__exit__() already handled the
                    # matching warning
                    continue
                kwargs = dict(
                    filename=wm.filename,
                    lineno=wm.lineno,
                    module=wm.__module__,
                    source=wm.source,
                )
                if isinstance(wm.message, Warning):
                    kwargs.update(
                        message=str(wm.message),
                        category=type(wm.message),
                    )
                else:
                    kwargs.update(
                        message=str(wm.message),
                        category=wm.category,
                    )
                warnings.warn_explicit(**kwargs)

    _wc_module.WarningsChecker.__exit__ = exit_and_reissue_unmatched
    pytest_warns = _wc_module.warns


@typing.overload
def wip_test(test_func: bool) -> typing.Callable[[TestCallable], TestCallable]:
    ...


@typing.overload
def wip_test(test_func: TestCallable) -> TestCallable:
    ...


def wip_test(
    test_func: typing.Union[TestCallable, bool]
) -> typing.Union[TestCallable, typing.Callable[[TestCallable], TestCallable]]:
    """
    Xfail tests that are planned but not written.
    Passing `True` instead results in the same decorator, and passing
    `False` indicates a no-op.
    """
    def no_op(test_func: TestCallable) -> TestCallable:
        return test_func

    if not callable(test_func):
        return wip_test if test_func else no_op

    @pytest.mark.xfail(reason='WIP', strict=True, raises=NotImplementedError)
    @functools.wraps(test_func)
    def wrapper(*args, **kwargs):
        raise NotImplementedError('Test under construction')

    return wrapper


@check(
    param_values=is_sequence(item_checker=is_sequence),
    exclude=(
        identical_to(None)
        | is_collection(item_checker=is_keyword, allow_item=True)
    )
)
def parametrize_all(
    *param_values: typing.Sequence,
    fill_value: typing.Any = None,
    exclude: typing.Optional[
        typing.Union[typing.Collection[Identifier], Identifier]
    ] = None,
) -> typing.Callable[[TestCallable], TestCallable]:
    """
    Parametrize on all the positional arguments except the `exclude`-d
    ones;
    if a sequence in `param_values` is shorted than the number of
    arguments, `fill_value` is used to pad it.

    Notes
    -----
    `param_values` are expected to be length-one sequences containing
    the values of the sole argument even when only one parameter is to
    be parametrized;
    this behavior is different from that of `@pytest.mark.parametrize`.
    """
    def wrap_test(test_func: TestCallable) -> TestCallable:
        param_names = [
            param.name
            for param in inspect.signature(test_func).parameters.values()
            if param.default is param.empty
            if param.kind in (
                param.POSITIONAL_ONLY, param.POSITIONAL_OR_KEYWORD,
            )
            if param.name not in exclude
        ]
        return pytest.mark.parametrize(
            ','.join(param_names),
            [
                (
                    tuple(params) +
                    (fill_value,) * (len(param_names) - len(params))
                )[slice(None) if len(param_names) > 1 else 0]
                for params in param_values
            ],
        )(test_func)

    if exclude is None:
        exclude = ()
    elif isinstance(exclude, str):
        exclude = exclude,
    return wrap_test


def apply_typesetter(typesetter: Typesetter, value: typing.Hashable) -> str:
    def get_from_defaults(
        mapping: typing.Mapping[typing.Hashable, str], value: typing.Hashable,
    ) -> str:
        return mapping.get(value, value)

    if typesetter is None:
        typesetter = str
    elif isinstance(typesetter, str):
        typesetter = typesetter.format
    elif callable(typesetter):
        pass
    else:
        typesetter = functools.partial(get_from_defaults, typesetter)
    return str(typesetter(value))


@contextlib.contextmanager
def catch_warnings(
    *args,
    action: typing.Optional[str] = None,
    category: typing.Type[Warning] = Warning,
    lineno: int = 0,
    append: bool = False,
    **kwargs
):
    """
    Backporting of the newer (Python 3.11+) keyword arguments to
    `warnings.catch_warnings()`.

    Example
    -------
    >>> import warnings
    >>>
    >>> with catch_warnings(action='error', category=FutureWarning):
    ...     warnings.warn('foo', FutureWarning)
    ...
    Traceback (most recent call last):
      ...
    FutureWarning: foo
    """
    try:
        ctx = warnings.catch_warnings(
            *args,
            action=action, category=category, lineno=lineno, append=append,
            **kwargs,
        )
    except TypeError:  # Pre 3.11 -> bad arguments
        with warnings.catch_warnings(*args, **kwargs) as ctx:
            if action is not None:
                warnings.simplefilter(
                    action=action,
                    category=category,
                    lineno=lineno,
                    append=append,
                )
            yield ctx
    else:  # Post 3.11
        with ctx:
            yield ctx


not_deprecated = functools.partial(
    catch_warnings, action='error', category=FutureWarning,
)


@contextlib.contextmanager
def is_deprecated(
    *args,
    other: typing.Literal[
        'default', 'error', 'ignore', 'always', 'module', 'once',
    ] = 'error',
    **kwargs,
):
    """
    Check that the appropriate `FutureWarning` is issued.

    Parameters
    ----------
    *args, **kwargs
        Passed to `pytest.warns()`
    other
        What to do with by-catches, that is, non-matching
        `FutureWarning`s

    Example
    -------
    >>> import warnings
    >>>
    >>> with is_deprecated():  # Not OK
    ...     pass
    Traceback (most recent call last):
      ...
    Failed: DID NOT WARN. No warnings of type ...FutureWarning...
    >>> with is_deprecated():  # OK
    ...     warnings.warn('foo', FutureWarning)
    >>> with is_deprecated(match='foo'):  # OK
    ...     warnings.warn('foo', FutureWarning)
    >>> with is_deprecated(match='foo'):
    ...     warnings.warn('foo', FutureWarning)  # OK
    ...     warnings.warn('bar', FutureWarning)  # Not OK
    Traceback (most recent call last):
      ...
    FutureWarning: bar
    >>> with is_deprecated(match='foo', other='ignore'):
    ...     warnings.warn('foo', FutureWarning)  # OK
    ...     warnings.warn('bar', FutureWarning)  # Ignored
    """
    cw_kwargs = dict(
        # Let more specific filters take precedence
        append=True,
        # Catch other `FutureWarning`s
        action=other,
    )
    with contextlib.ExitStack() as stack:
        stack.enter_context(not_deprecated(**cw_kwargs))
        yield stack.enter_context(pytest_warns(FutureWarning, *args, **kwargs))
