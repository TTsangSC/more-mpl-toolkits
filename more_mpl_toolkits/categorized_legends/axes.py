"""
`matplotlib.axes.Axes` subclass.
"""
import functools
import inspect
import itertools
import numbers
import textwrap
import types
import typing
from collections import OrderedDict

from matplotlib.cbook import Grouper
from matplotlib.axis import Axis
from matplotlib.axes import Axes
from matplotlib.legend import Legend

import tjekmate

from .._deprecations import deprecate
from . import utils, mixin, wrapper_proxy, _types, _utils
from .recipes import CategorizedLegendRecipe

__all__ = 'CategorizedLegendAxes', 'CategorizedLegendAxesStack'

_CATEGORIZED_LEGEND_DOC_TEMPLATE = (
        # Two levels of indent to be consistent with other methods
        r"""
        Create and return a CL based on the CKAs encountered.

        Parameters (basic)
        ------------------
        <BASIC_PARAMS>
            Parameters for `.get_cl_categories()` used to retrieve the
            categories and their encountered values;
            see the documentation thereof

        Parameters (CKA classification)
        -------------------------------
        category_value_classifications (alias: label_classifications)
            Optional mapping of the form

            >>> Mapping[  # noqa # doctest: +SKIP
            ...     category_name,
            ...     Union[data_classification_enum, data_cat_class_name]
            ... ]

            specifying how a category is classified, which in turn
            affects how it is formatted in the legend;
            if not given here, a category is classified by

            >>> (  # noqa # doctest: +SKIP
            ...     _MODULE_
            ...     .data_classification.DataCatClass
            ... ).from_collection()

            based on its values
        order_base, max_linear_orders, max_discrete_numbers
            Parameters controlling how

            >>> (  # noqa # doctest: +SKIP
            ...     _MODULE_
            ...     .data_classification.DataCatClass
            ... ).from_collection()

            classifies the values in a category not given in
            `category_value_classifications`;
            see the documentation thereof

        Parameters (CKA sorting)
        ------------------------
        category_name_sorter (alias: category_sorter)
            Optional mapping or callable of the forms

            >>> Callable[  # noqa # doctest: +SKIP
            ...     [category_name], sorting_key,
            ... ]

            or

            >>> Mapping[  # noqa # doctest: +SKIP
            ...     category_name, sorting_key
            ... ]

            specifying how the categories are to be sorted
        category_value_sorters (alias: label_sorters)
            Optional mapping of the form

            >>> Mapping[  # noqa # doctest: +SKIP
            ...     category_name, Callable[[argument], sorting_key]
            ... ]

            or

            >>> Mapping[  # noqa # doctest: +SKIP
            ...     category_name, Mapping[argument, sorting_key]
            ... ]

            specifying how the encountered arguments for each of the
            categories are to be ordered in a customized way; e.g.

            >>> atomic_numbers = {
            ...     elem: n
            ...     for n, elem in enumerate('X H He Li Be ...'.split())
            ... }
            >>> atomic_masses = [1, 1.0080, 4.0026, 6.94, ...]
            >>>
            >>> def parse_formula(formula: str) -> typing.List[int]:
            ...     ...
            ...
            >>> def formula_to_mass(formula: str) -> float:
            ...     return sum(
            ...         atomic_masses[atomic_numbers[symbol]]
            ...         for symbol in parse_formula(formula)
            ...     )
            ...
            >>> category_value_sorters = dict(
            ...     element=atomic_numbers,
            ...     formula=formula_to_mass,
            ... )

        Parameters (CKA formatting)
        ---------------------------
        category_name_typesetter (alias: category_typesetter)
            Optional mapping or callable of the forms

            >>> Callable[  # noqa # doctest: +SKIP
            ...     [category_name], typeset_str
            ... ]

            or

            >>> Mapping[  # noqa # doctest: +SKIP
            ...     category_name, typeset_str
            ... ],

            or a format string, specifying how the categories are to be
            typeset (if `show_categories_with_labels` is true)
        category_value_typesetters (alias: label_typesetters)
            Optional mapping of the form

            >>> Mapping[  # noqa # doctest: +SKIP
            ...     category_name, Callable[[argument], typeset_str]
            ... ]

            or

            >>> Mapping[  # noqa # doctest: +SKIP
            ...     category_name, Mapping[argument, typeset_str]
            ... ],

            or a format string, specifying how the encountered arguments
            for each of the categories are to be typeset in a customize
            way; e.g.

            >>> def format_miller_indices(hkl):
            ...     hkl = ''.join(
            ...         f'\\overline{{{i}}}'
            ...         if i < 0 else str(i) for i in hkl
            ...     )
            ...     return f'$({hkl})$'
            ...
            >>> gas_formulae = dict(
            ...     CO='CO', CO2='CO$_2$', C2H6='H$_3$CCH$_3$',
            ... )
            >>> category_value_typesetters = dict(
            ...     surface=format_miller_indices,
            ...     gas=gas_formulae,
            ...     adsorption_height='{:.2f}',
            ... )

        show_category_names (alias: show_categories_with_labels)
            Whether the labels in the legend are prefixed with
            "<typeset_category> = ";
            can also be a mapping of the form

            >>> Mapping[  # noqa # doctest: +SKIP
            ...     category_name, is_to_show_with_labels
            ... ]

        suppressed_categories
            Optional category name(s) not to be put into the CL;
            can also be a mapping of the form

            >>> Mapping[  # noqa # doctest: +SKIP
            ...     category_name,
            ...     (
            ...         cl_method_name |
            ...         formatter_func |
            ...         Collection[cl_method_name | formatter_func]
            ...     )
            ... ],

            each value of which then specifies for the category the
            entries to exclude;
            where then an entry is excluded if the CL method name and/or
            the data formatter matches the value or any of the items in
            the value
        conflate
            Optional specifications for what values to conflate,
            avoiding creating duplicate entries;
            when given, should be a mapping with any of the following
            keys:
               'categories', 'methods', 'formatters',
            each corresponding to a sequence of values thereof, or a
            collection of such sequences (see Notes);
            values found in one such sequence are conflated to the
            leading entry in the sequence
        range_samples, log_range_samples, linear_range_samples
            Number of samples (> 1) to take from each category
            classified as a range and to put in a legend entry;
            can also be mapping of the form

            >>> Mapping[  # noqa # doctest: +SKIP
            ...     category_name, num_samples
            ... ];

            if a category is classified as a log/linear range but the
            corresponding `<type>_range_samples` is not set,
            `range_samples` provides the fall-back;
            else, the former takes precedence
        values_per_line (alias: labels_per_line)
            Maximum number of labels of the same category to put on a
            single line of the legend for non-range (i.e. discrete-
            valued) categories;
            can also be a dictionary of the form

            >>> Mapping[  # noqa # doctest: +SKIP
            ...     category_name, num_labels
            ... ]

        Parameters (layout and misc)
        ----------------------------
        ncol (aliases: ncols, nrow, nrows)
            Number of columns (if `fill_columns_first` is true) or row
            (if `fill_columns_first` is false) the legend will be split
            into;
            categories are distributed into the columns so that the
            column/row lengths are uniform as possible
        fill_columns_first, fill_rows_first
            Optional switches for whether to populate the legend grid
            columns-first or rows-first;
            when neither is supplied, use the matplotlib default
            (columns-first);
            when both are supplied and are contradictory, an error is
            raised
        gap_between_categories
            Whether to insert gaps between groups of legend items
        legend_method()
            If provided, should be an unbound method with the same
            signature as

            >>> <OBJ_CLASS>.legend(  # noqa # doctest: +SKIP
            ...     <OBJ_NAME>, handles, labels[, ...]
            ... ),

            which constructs a `matplotlib.legend.Legend` object with
            the handles and labels, adds it to `<OBJ_NAME>`, and returns
            the legend object;
            else, `<OBJ_CLASS>.legend()` is used
        **kwargs
            Passed to `legend_method()` as-is, save for the aliased
            arguments;
            those are removed from `kwargs` if found, and take the
            places of their respective unaliased arguments if those are
            not supplied

        Return
        ------
        (`matplotlib.legend.Legend`) Object returned by
        `legend_method()`

        Notes (parameters)
        ------------------
        category_value_typesetters
            Typeset arguments should be strings that are amenable to
            enumeration as a comma-separated list (so e.g. no newlines),
            or representation as part or a numerical range, etc.
        category_name_sorter
            If `ncol > 1`, column length uniformity takes precedence,
            and the sorting of categories only applies within each
            column
        conflate['categories']
            Should be string names of CATEGORIZED KEYWORD ARGUMENTS
        conflate['methods']
            Should be names of CL METHODS for 'methods'
        conflate['formatters']
            Should be DATA FORMATTERS
        suppressed_categories, conflate
            These two occur separately and only as-is on the raw values;
            conflations are not chained, nor is suppression evaluated on
            the changed values
        range_samples, log_range_samples, linear_range_samples
            Ranges are always visualized on the same line as several
            adjacent handles, regardless of the `values_per_line`
            setting

        Notes (misc.)
        -------------
        If the created legend has no entries:
        - If it has a `title` (`categorized_legend=dict(title=...)`),
          a warning is issued.
        - Else, it is made visible, and a warning is issued.
        """
)

CategorizedLegendAxes = typing.TypeVar('CategorizedLegendAxes', bound=Axes)


class CategorizedLegendAxesStack(_types.AxesStack):
    """
    A `matplotlib.figure.AxesStack` class which automatically promotes
    incoming `matplotlib.axes.Axes` to
    `<MODULE>.Axes`.
    """
    def __init__(
        self, figure: typing.Optional[_types.FigureBase] = None,
    ) -> None:
        super().__init__()
        self.figure = figure

    @functools.wraps(_types.AxesStack.add)
    def add(self, *args, **kwargs):
        # Note: call signature of .add() changed between versions, use
        # introspection to handle
        base_add = super().add
        ba = inspect.signature(base_add).bind(*args, **kwargs)
        assert not ba.kwargs
        *args, a = ba.args
        return base_add(*args, self._wrap_axes(a))

    # Compatibility switches: some methods are changed/removed in later
    # matplotlib versions
    if hasattr(_types.AxesStack, 'get'):
        @functools.wraps(_types.AxesStack.get)
        def get(
            self, key: typing.Any
        ) -> typing.Union[CategorizedLegendAxes, typing.Any]:
            result = super().get(key)
            return (
                self._wrap_axes(result) if self.figure and result else result
            )

    if hasattr(_types.AxesStack, 'push'):
        @functools.wraps(_types.AxesStack.push)
        def push(
            self,
            o: typing.Union[
                # Pre-3.4.0
                typing.Tuple[
                    typing.Hashable, typing.Tuple[numbers.Integral, Axes],
                ],
                # 3.4.0+
                typing.Tuple[numbers.Integral, Axes],
            ],
        ) -> typing.Any:
            # Note: the stack-element type was changed around 3.4.0,
            # from (key: Hashable, (i: int, a: Axes)) to
            # (i: int, a: Axes)
            try:
                key, (index, ax) = o
                use_key = True
            except (ValueError, TypeError):  # Wrong unpacking (3.4.0+)
                index, ax = o
                key, use_key = None, False
            assert isinstance(ax, Axes)
            if not isinstance(ax, CategorizedLegendAxes):
                ax = self._wrap_axes(ax)
            if use_key:
                return super().push((key, (index, ax)))
            return super().push((index, ax))

    if hasattr(_types.AxesStack, '_entry_from_axes'):  # Pre-3.6.0
        @functools.wraps(_types.AxesStack._entry_from_axes)
        def _entry_from_axes(self, e: Axes):
            # Note: the stack-element type was changed around 3.4.0,
            # from (key: Hashable, (i: int, a: Axes)) to
            # (i: int, a: Axes)
            is_len_two_seq = tjekmate.is_sequence(length=2)
            assert all(is_len_two_seq(elem) for elem in self._elements)
            if all(isinstance(ax, Axes) for _, ax in self._elements):
                # 3.4.0+
                axes = {a: (ind, a) for ind, a in self._elements}
            elif all(
                is_len_two_seq(value) and isinstance(value[1], Axes)
                for _, value in self._elements
            ):  # Pre-3.4.0
                axes = {a: (k, (ind, a)) for k, (ind, a) in self._elements}
            for ax in tuple(axes):
                wrapped_ax = self._get_wrapped_claxes(ax)
                if wrapped_ax:
                    axes[wrapped_ax] = axes[ax]
            return axes[e]

    def _wrap_axes(self, axes: Axes) -> Axes:
        """
        Wrap the axes object with `.figure` to promote it into a
        `<MODULE>.Axes`.
        """
        if (
            isinstance(type(self.figure), wrapper_proxy.WrapperProxyMeta) and
            callable(getattr(self.figure, '_wrap_axes', None))
        ):
            axes = self.figure._wrap_axes(axes)
            assert isinstance(axes, Axes)
        return axes

    @classmethod
    def _replace_stack(cls, fig: _types.FigureBase) -> typing.Self:
        """
        Replace the axes stack of the figure by an instance, promoting
        the axes objects to `<MODULE>.Axes`.
        """
        def transfer_stack_pre_3_6_0(old: _types.AxesStack, new: cls) -> None:
            """
            Legacy implementation from when
            `matplotlib.figure.AxesStack` was still a subclass of
            `matplotlib.cbook.Stack`.
            """
            while not old.empty():
                ax = old.back()
                assert isinstance(ax, Axes)
                key, entry = old._entry_from_axes(ax)
                try:
                    ind, _ = entry
                    args = key, ax
                except (ValueError, TypeError):
                    # Wrong unpacking (3.4.0+)
                    args = ax,
                new.add(*args)
                old.remove(ax)

        def transfer_stack_post_3_6_0(old: _types.AxesStack, new: cls) -> None:
            """
            After 3.6.0, `matplotlib.figure._AxesStack` uses the
            `._axes: dict[Axes, int]` dictionary to hold the Axes.
            """
            def increment_new_counter() -> int:
                return next(new._counter)

            for ax in old.as_list():
                new.add(ax)
            # Now that the axes are all converted into CL axes, manage
            # the ordering of them
            for claxes in new._axes:
                ax = new._get_wrapped_claxes(claxes)
                new._axes[claxes] = old._axes[ax]
            if new._axes:  # Note: preseve axes index
                target = next(old._counter) - 1
                i = increment_new_counter()
                assert i <= target
                while i < target:
                    i = increment_new_counter()
            old._axes.clear()

        def get_missing_requirements(
            requirements: typing.Dict[
                str, typing.Union[type, typing.Tuple[type]]
            ],
            stack: _types.AxesStack,
        ) -> typing.Dict[str, typing.Any]:
            return {
                attr: getattr(stack, attr, absent)
                for attr, type_or_types in requirements.items()
                if not (
                    hasattr(stack, attr) and
                    isinstance(getattr(stack, attr), type_or_types)
                )
            }

        def get_repr_for_requirements(
            requirements: typing.Dict[
                str, typing.Union[type, typing.Tuple[type]]
            ],
        ) -> str:
            items: typing.List[typing.Tuple[str, str]] = []
            for attr, type_or_types in requirements.items():
                if isinstance(type_or_types, tuple):
                    annotation = str(typing.Union[type_or_types])
                elif type_or_types.__module__ == 'builtins':
                    annotation = type_or_types.__qualname__
                else:
                    annotation = repr(type_or_types)
                items.append((attr, annotation))
            return '{{{}}}'.format(
                ', '.join(
                    f'.{attr}: {annotation}' for attr, annotation in items
                )
            )

        def get_repr_for_missing(missing: typing.Dict[str, typing.Any]) -> str:
            return '{{{}}}'.format(
                ', '.join(
                    f'.{attr}: {wrong_value!r}'
                    for attr, wrong_value in missing.items()
                )
            )

        absent = type('Absent', (), dict(__repr__=lambda self: '<absent>'))()

        transfer_stack_requirements: typing.Dict[
            typing.Callable[[cls], None],
            typing.Dict[str, typing.Union[type, typing.Tuple[type]]]
        ] = {
            transfer_stack_pre_3_6_0: dict(
                empty=typing.Callable,
                back=typing.Callable,
            ),
            transfer_stack_post_3_6_0: dict(
                _axes=dict,
                _counter=itertools.count,
                as_list=typing.Callable,
            ),
        }
        req_failures: typing.List[typing.Tuple[str, str]] = []
        if not hasattr(fig, '_axstack'):  # Create one if not found
            instance = fig._axstack = cls(figure=fig)
            return instance
        for transfer_stack, requirements in transfer_stack_requirements.items():
            missing = get_missing_requirements(requirements, fig._axstack)
            if not missing:  # All requirements fulfilled
                break
            req_failures.append((requirements, missing))
        else:
            repr_failures = ', '.join(
                '{} -> {}'.format(
                    get_repr_for_requirements(requirements),
                    get_repr_for_missing(missing),
                )
                for requirements, missing in req_failures
            )
            raise TypeError(
                f'fig._axstack = {fig._axstack!r}: '
                'expected figure to have any of the following sets of '
                'attributes and their respective type requirements '
                f'(requirements -> failures): {repr_failures}'
            )
        instance = cls(figure=fig)
        transfer_stack(fig._axstack, instance)
        fig._axstack = instance
        return instance

    @typing.overload
    @staticmethod
    def _get_wrapped_claxes(claxes: CategorizedLegendAxes) -> Axes:
        ...

    @typing.overload
    @staticmethod
    def _get_wrapped_claxes(claxes: typing.Any) -> None:
        ...

    @staticmethod
    def _get_wrapped_claxes(
        claxes: typing.Union[typing.Any, CategorizedLegendAxes],
    ) -> typing.Union[Axes, None]:
        """
        Convenience method to get the wrapped `Axes` object from a
        `<MODULE>.Axes` object;
        if it's not a
        `<MODULE>.Axes`
        object, just return `None`.
        """
        if isinstance(claxes, CategorizedLegendAxes):
            return wrapper_proxy._get_wrapped_instance(claxes)
        return None


@mixin.CategorizedLegendMixin.inject
class CategorizedLegendAxes(
    Axes,
    metaclass=wrapper_proxy.WrapperProxyMeta,
    allow_def_method_redefine=True,
    deferred_attrs=('zorder',),
):
    """
    A `matplotlib.axes.Axes` class instantiated by a
    `<MODULE>.Figure`
    object, enabling the easy creation of CATEGORIZED LEGENDS (CLs);
    see the module documentation.
    """
    def __init__(self, *args, **kwargs) -> None:
        """
        Initialize the instance.

        Parameters
        ----------
        *args, **kwargs
            Ignored
        """
        dict_slots = (
            '_categorized_kwargs',
            '_cl_bound_methods',
        )
        grouper_attr_locs = (
            '_twinned_axes',
            # Pre-3.5
            '_shared_x_axes',
            '_shared_y_axes',
        )
        grouper_map_locs = (
            # 3.5+
            '_shared_axes',
        )

        # Wrapper-instance variables
        for attr in dict_slots:
            if not hasattr(self, attr):
                setattr(self, attr, {})

        # Replace references to the wrapped Axes in child objects with
        # self: Groupers
        iter_attr_groupers = (
            getattr(self, attr, None) for attr in grouper_attr_locs
        )
        iter_map_groupers = (
            grouper
            for mapping in (
                getattr(self, attr, {}) for attr in grouper_map_locs
            )
            if isinstance(mapping, typing.Mapping)
            for grouper in mapping.values()
        )
        for grouper in itertools.chain(iter_attr_groupers, iter_map_groupers):
            if isinstance(grouper, Grouper):
                self._replace_wrapped_instance_in_grouper(grouper)
        # .{x|y}axis
        for axis_obj in self.xaxis, self.yaxis:
            self._replace_wrapped_instance_in_axis(axis_obj)
        # .axes
        self._replace_wrapped_instance_generic(self, ('axes', '_axes'))

    def __getattribute__(self, name: str) -> typing.Any:
        """
        If `name` is the name of a registered CL METHOD for which a DATA
        FORMATTER is available (either the default or a specific one):
            Return a method object wrapper over the corresponding normal
            method which takes the appropriate CATEGORIZED KEYWORD
            ARGUMENTS (CKAs), calls the normal method with altered
            (sequences of) keywords, remembers the CKAs passed, and
            returns (the sequences of) the
            normal-method return value(s).
        If `name` is the name of a `matplotlib.axes.Axes` method:
            Return a normal method object.
        If `name` is an attribute specific to
        `<MODULE>.Axes`:
            Return a reference to the local attribute.
        Else:
            Return a reference to the corresponding attribute of the
            wrapped `matplotlib.axes.Axes` object.
        """
        # .cl_method_formatters and .default_cl_formatter call
        # hasattr(), which then calls .__getattribute__(), hence a
        # workaround is needed
        cls = type(self)
        default_methods = getattr(cls, type(cls).methods_marker)
        default_getter = default_methods['__getattribute__']
        # Names for which customized lookup is precluded
        if name in getattr(type(self), type(type(self)).class_attrs_marker):
            return object.__getattribute__(self, name)
        # Names which corresponds to CL methods
        cl_bound_methods = default_getter(self, '_cl_bound_methods')
        prefix = type(self).cl_method_prefix
        if name.startswith(prefix) and name in cl_bound_methods:
            _, recipe_version = cl_bound_methods[name]
            base_name = name[len(prefix):]
            # Check if the recipe has been updated;
            # if so, refresh or delete the cached bound-method instance
            try:
                recipe = CategorizedLegendRecipe.get(base_name)
                bound_method_up_to_date = bool(recipe_version == recipe.version)
            except Exception:
                bound_method_up_to_date = False
            if not bound_method_up_to_date:
                default_getter(self, '_refresh_bound_cl_method')(name)
            bound_method, _ = cl_bound_methods[name]
            return bound_method
        # Default wrapper-proxy-class behavior
        return default_getter(self, name)

    def __dir__(self) -> typing.List[str]:
        cls = type(self)
        default_methods = getattr(cls, type(cls).methods_marker)
        default_dir = default_methods['__dir__']
        # Prepend the bound CL METHODS to the namespace
        return [name for name in self._cl_bound_methods] + default_dir(self)

    def _refresh_bound_cl_method(self, cl_method: _types.CLMethodName) -> None:
        """
        Make, update, or delete the entry for `cl_method` in the private
        registry of bound CL METHODS.
        """
        prefix = type(self).cl_method_prefix
        assert isinstance(cl_method, str) and cl_method.startswith(prefix)
        base_name = cl_method[len(prefix):]
        try:
            recipe = CategorizedLegendRecipe.get(base_name)
            base_method, recipe_version = recipe.method, recipe.version
            if base_method is None:
                raise NotImplementedError
        except NotImplementedError:
            # Recipe is no longer available -> remove from registry
            self._cl_bound_methods.pop(cl_method, None)
            return
        if cl_method not in self.cl_method_formatters:
            # Not/No longer a CL method -> remove from registry
            self._cl_bound_methods.pop(cl_method, None)
            return
        try:
            data_formatter = next(
                f for f in (
                    self.cl_method_formatters[cl_method],
                    self.default_cl_formatter
                )
                if f is not None
            )
        except StopIteration:
            # No data formatter available -> remove from registry
            self._cl_bound_methods.pop(cl_method, None)
            return
        # Identify categorized kwargs and presence of arbitrary kwargs
        sig = inspect.signature(data_formatter)
        ckwargs: typing.OrderedDict[
            _types.CategorizedKeyword, inspect.Parameter
        ] = OrderedDict()
        arb_kwargs: typing.Union[inspect.Parameter, None] = None
        for arg_name, arg in sig.parameters.items():
            if arg.kind == arg.KEYWORD_ONLY:
                ckwargs[arg_name] = arg
            elif arg.kind == arg.VAR_KEYWORD:
                arb_kwargs = arg
            else:
                raise AssertionError

        # Implementation of wrapped method
        def process_kwargs(**kwargs) -> typing.Tuple[
            typing.Union[typing.List[_types.KeywordDict], _types.KeywordDict],
            _types.KeywordDict,
        ]:
            """
            Turn `kwargs` into either a dict or a sequence of dicts of
            keyword arguments for the plotting function.
            If `data_formatter()` accepts non-categorized kwargs, they
            are passed into it;
            else, they are used to update the generated dict(s).

            Return
            ------
            >>> (  # noqa # doctest: +SKIP
            ...     (
            ...         dict[
            ...             keyword_for_base_method,
            ...             value_generated_by_data_formatter
            ...         ] |
            ...         list[
            ...             dict[
            ...                 keyword_for_base_method,
            ...                 value_generated_by_data_formatter
            ...             ]
            ...         ]
            ...     ),
            ...     dict[categorized_keyword_argument, value_supplied],
            ... )
            """
            kwargs_cat, kwargs_others = {}, {}
            for k, v in kwargs.items():
                (kwargs_cat if k in ckwargs else kwargs_others)[k] = v
            if arb_kwargs is None:  # Formatter only accepts CKAs
                plot_kwargs = data_formatter(**kwargs_cat)
            else:  # Formatter accepts both CKAs and non-CKA kwargs
                plot_kwargs, kwargs_others = data_formatter(**kwargs), {}
            if (
                isinstance(plot_kwargs, typing.Sequence) and
                all(utils.is_keyword_mapping(kw) for kw in plot_kwargs)
            ):
                return (
                    [dict(kw, **kwargs_others) for kw in plot_kwargs],
                    kwargs_cat
                )
            elif utils.is_keyword_mapping(plot_kwargs):
                return (dict(plot_kwargs, **kwargs_others), kwargs_cat)
            else:
                raise TypeError(
                    f'.{cl_method}(): '
                    'data formatter should return either a keyword mapping '
                    f'or a sequence thereof; got {plot_kwargs!r}'
                )

        def wrapped_method(self, *args, **kwargs):
            plot_kwargs, ckwargs = process_kwargs(**kwargs)
            if isinstance(plot_kwargs, typing.Sequence):
                results = [base_method(self, *args, **kw) for kw in plot_kwargs]
            elif isinstance(plot_kwargs, dict):
                results = base_method(self, *args, **plot_kwargs)
            else:
                raise AssertionError
            # Remember the categorized kwargs
            for category, value in ckwargs.items():
                self._categorized_kwargs.setdefault(
                    (category, cl_method, data_formatter), set()
                ).add(value)
            return results

        wrapped_method.__name__ = cl_method
        wrapped_method.__qualname__ = cl_method

        # Add documentation
        def strip_doc(docstring: str) -> str:
            return textwrap.dedent(docstring).strip('\n')

        if arb_kwargs is None:
            vakwargs = inspect.Parameter(
                'kwargs', inspect.Parameter.VAR_KEYWORD
            )
            kwargs_clause = (
                f"""
                Passed directly to `Axes.{base_name}()`;
                in case of conflicts, supersedes keyword arguments generated by
                the DATA FORMATTER
                """
            )
        else:
            vakwargs = arb_kwargs
            kwargs_clause = (
                """
                Passed to the DATA FORMATTER along with CKAs;
                their effects on the creation of artists will not be documented
                on the CL
                """
            )
        wrapped_doc = [
            """
            CL METHOD wrapper around
            `matplotlib.axes.Axes.{base_name}()`.

            Parameters
            ----------
            {ckas}
                CATEGORIZED KEYWORD ARGUMENTS (CKAs)
            *args
                Passed to `Axes.{base_name}()`
            **{kwargs_name}
            {kwargs_clause}

            DATA FORMATTER
            --------------
            {data_formatter}: {sig}

            Return
            ------
            - If the DATA FORMATTER returns a sequence

              >>> kws = Sequence[  # noqa # doctest: +SKIP
              ...     Mapping[{base_name}_keyword, ...]
              ... ]

              of keyword mappings,

              >>> [  # noqa # doctest: +SKIP
              ...     self.{base_name}(*args, **kw) for kw in kws
              ... ]

              is returned.

            - Else, it is assumed that its returned value is

              >>> kw = Mapping[  # noqa # doctest: +SKIP
              ...   {base_name}_keyword, ...,
              ... ],

              and

              >>> self.{base_name}(*args, **kw)  # noqa # doctest: +SKIP

              is returned.

            Notes
            -----
            The CKAs passed to the method are remembered by the owner
            `{module_name}.Axes`
            object, and can later be used to construct a CATEGORIZED
            LEGEND (CL) for either the owner or its owning
            `{module_name}.Figure`.
            """,
        ]
        wrapped_doc[0] = strip_doc(wrapped_doc[0]).format(
            base_name=base_name,
            ckas=', '.join(ckwargs) if ckwargs else '<nil>',
            kwargs_name=vakwargs.name,
            kwargs_clause=textwrap.indent(
                strip_doc(kwargs_clause),
                prefix='    ',
                predicate=utils.always(),
            ),
            data_formatter=repr(data_formatter),
            sig=str(sig),
            module_name=self._get_cl_module_name()
        )
        wrapped_doc.append('\n'.join([
            '',
            'Base method documentation',
            '-------------------------',
            f'{base_name}{inspect.signature(getattr(Axes, base_name))!s}',
            textwrap.indent(
                strip_doc(getattr(Axes, base_name).__doc__),
                prefix='    ',
                predicate=utils.always(),
            )
        ]))
        wrapped_method.__doc__ = '\n\n'.join(
            strip_doc(chunk) for chunk in wrapped_doc
        )

        # Fix signature of wrapped_method
        BaseMethodReturnType = inspect.signature(base_method).return_annotation
        if BaseMethodReturnType is sig.empty:
            BaseMethodReturnType = typing.Any
        try:
            WrappedMethodReturnType = typing.Union[
                BaseMethodReturnType, typing.List[BaseMethodReturnType]
            ]
        except TypeError:  # Bad annotation
            WrappedMethodReturnType = typing.Any
        wrapped_method.__signature__ = inspect.Signature(
            parameters=[
                inspect.Parameter(
                    'self', inspect.Parameter.POSITIONAL_OR_KEYWORD,
                ),
                inspect.Parameter(
                    'args', inspect.Parameter.VAR_POSITIONAL,
                ),
                *ckwargs.values(),
                vakwargs,
            ],
            return_annotation=WrappedMethodReturnType,
        )

        # Remember both the bound-method object and the version of the
        # recipe used to create it
        self._cl_bound_methods[cl_method] = (
            (types.MethodType(wrapped_method, self), recipe_version)
        )

    # ***************** Management of child objects ****************** #

    def _replace_wrapped_instance_in_grouper(self, grouper: Grouper) -> None:
        """
        If a grouper contains a reference to the `Axes` instance wrapped
        by the wrapper object, replace it with the wrapper.

        Parameters
        ----------
        grouper
            `matplotlib.cbook.Grouper`

        Side effects
        ------------
        `grouper` updated
        """
        ax_wrapped = wrapper_proxy._get_wrapped_instance(self)
        if ax_wrapped not in grouper:
            return
        ax_siblings = [
            ax for ax in grouper.get_siblings(ax_wrapped)
            if ax is not ax_wrapped
        ]
        grouper.remove(ax_wrapped)
        grouper.join(self, *ax_siblings)

    def _replace_wrapped_instance_in_axis(self, axis: Axis) -> None:
        """
        If an `Axis` contains a reference to the `Axes` instance wrapped
        by the wrapper object, replace it with the wrapper.

        Parameters
        ----------
        axis
            `matplotlib.axis.Axis`

        Side effects
        ------------
        `axis` and any of its child `Tick` instances updated
        """
        replace = functools.partial(
            self._replace_wrapped_instance_generic, attr=('axes', '_axes'),
        )
        replace(axis)
        for tick_list in axis.majorTicks, axis.minorTicks:
            for tick in tick_list:
                replace(tick)

    @_utils.copy_annotations_from(
        mixin.CategorizedLegendMixin._find_cl_children_at,
    )
    def get_cl_children(
        self, *args, **kwargs
    ):
        """
        List all the child figures and/or axes relevant to making a
        categorized legend.

        Parameters
        ----------
        *args, **kwargs
            Passed to `.get_cl_children()` of child objects
        max_level
            Optional limit for how many levels of children to descend
            into;
            the default `None` (equivalent to `True`) indicates that
            there is no limit

        Result
        ------
        >>> list[  # noqa # doctest: +SKIP
        ...     # Each list represents a level
        ...     list[axes_or_figure]
        ... ]

        Notes
        -----
        At the moment, `*args` and `**kwargs` has no effect;
        this may change in a future version.
        """
        return self._find_cl_children_at(  # Locate all child axes
            *args,
            **dict(
                kwargs,
                _locations=('child_axes', 'parasites'),
                _immediate_child_parentage_loc=None,
            ),
        )

    # ************** Propagate changes to the children *************** #

    @functools.wraps(mixin.CategorizedLegendMixin.register_cl_method)
    def register_cl_method(self, cl_method, formatter=None):
        if isinstance(cl_method, str):
            cl_method = cl_method,
        result = mixin.CategorizedLegendMixin.register_cl_method(
            self, cl_method, formatter,
        )
        for name in cl_method:
            self._refresh_bound_cl_method(name)
        return result

    @functools.wraps(mixin.CategorizedLegendMixin.deregister_cl_method)
    def deregister_cl_method(self, cl_method):
        if isinstance(cl_method, str):
            cl_method = cl_method,
        mixin.CategorizedLegendMixin.deregister_cl_method(self, cl_method)
        for name in cl_method:
            self._refresh_bound_cl_method(name)
            for cat, mthd, fmtr in self._categorized_kwargs:
                if mthd != name:
                    continue
                del self._categorized_kwargs[cat, mthd, fmtr]

    # ************** Gather categories from child axes *************** #

    @typing.overload
    def get_cl_categories(
        self,
        *args,
        concise: typing.Literal[True] = True,
        **kwargs
    ) -> typing.Dict[
        _types.CategorizedKeyword, typing.Set[_types.Categorization]
    ]:
        ...

    @typing.overload
    def get_cl_categories(
        self,
        *args,
        concise: typing.Literal[False] = False,
        **kwargs
    ) -> typing.Dict[
        typing.Tuple[
            _types.CategorizedKeyword,
            _types.CLMethodName,
            _types.CategoryFormatter,
        ],
        typing.Set[_types.Categorization]
    ]:
        ...

    @deprecate.cla_clf_method_pos_args
    def get_cl_categories(
        self,
        ax: typing.Optional[
            typing.Union[
                CategorizedLegendAxes, typing.Collection[CategorizedLegendAxes],
            ]
        ] = None,
        cl_method: typing.Optional[
            typing.Union[
                _types.CLMethodName, typing.Collection[_types.CLMethodName],
            ]
        ] = None,
        formatter: typing.Optional[
            typing.Union[
                _types.CategoryFormatter,
                typing.Collection[_types.CategoryFormatter],
            ]
        ] = None,
        *,
        concise: bool = True,
        max_level: typing.Union[numbers.Integral, bool, None] = False,
        **kwargs
    ) -> typing.Union[
        typing.Dict[
            _types.CategorizedKeyword, typing.Set[_types.Categorization]
        ],
        typing.Dict[
            typing.Tuple[
                _types.CategorizedKeyword,
                _types.CLMethodName,
                _types.CategoryFormatter,
            ],
            typing.Set[_types.Categorization]
        ],
    ]:
        """
        Search for and get all the values encountered so far for the
        categories.

        Parameters
        ----------
        ax
            Optional
            `<MODULE>.Axes`
            instance(s) that is/are descendent(s) of the axes
            (inclusive);
            if supplied, start the search for values therefrom;
            default is to start the search from the axes object itself
        cl_method
            Optional name(s) of CL METHOD(s);
            if supplied, only include values encountered when calling
            `getattr(..., cl_method)(...)`
        formatter()
            Optional DATA FORMATTER(s);
            if supplied, only include values encountered when a CL
            METHOD used `formatter` as the formatter
        concise
            Level of detail to include in the returned dictionary
        max_level
            True or None
                Look for values recursively in `ax`, its/their child
                axes (from `.get_cl_children()`), and their child axes,
                etc.
            False (default)
                Only look at values encountered in CL methods bound to
                `ax`
            Non-boolean integer
                Include values in (up to) this many further levels of
                child axes of `ax`
        **kwargs
            Passed to `.get_cl_children()` of the child axes

        Return
        ------
        concise = True

            >>> dict[  # noqa # doctest: +SKIP
            ...     category, set[value_encountered]
            ... ];

        else

            >>> dict[  # noqa # doctest: +SKIP
            ...     tuple[category, cl_method, formatter],
            ...     set[value_encountered]
            ... ]

        Notes
        -----
        - If `ax` is supplied and it is/they are not among the
          descendents (inclusive) of the `Axes` instance, a `ValueError`
          is raised.

        - At the moment `**kwargs` has no effect (see
          `.get_cl_children()`).
        """
        def get_full_key(
            category: _types.CategorizedKeyword,
            method_name: _types.CLMethodName,
            formatter: _types.CategoryFormatter,
        ) -> typing.Tuple[
            _types.CategorizedKeyword,
            _types.CLMethodName,
            _types.CategoryFormatter,
        ]:
            return category, method_name, formatter

        def get_simple_key(
            category: _types.CategorizedKeyword, *_,
        ) -> _types.CategorizedKeyword:
            return category

        # Type checks
        prefix = type(self).cl_method_prefix
        method_checker: typing.Callable[[_types.CLMethodName], bool]
        formatter_checker: typing.Callable[[_types.CategoryFormatter], bool]
        if ax is None:
            pass
        elif not isinstance(ax, typing.Collection):
            ax = ax,
        if ax is not None:
            all_descendents: typing.Set[int] = {
                id(child_ax) for level in self.get_cl_children(max_level=None)
                for child_ax in level
            }
            all_descendents.add(id(self))
            if not all(
                isinstance(child_ax, CategorizedLegendAxes) for child_ax in ax
            ):
                raise TypeError(
                    f'ax = {ax!r}: '
                    f'expected `{self._get_cl_module_name()}.Axes` instance(s)'
                )
            if not {id(child_ax) for child_ax in ax} <= all_descendents:
                raise ValueError(
                    f'ax = {ax!r}: '
                    f'not `{self._get_cl_module_name()}.Axes` instance(s) '
                    f'belonging to the axes {self!r} (inclusive)'
                )
        if isinstance(cl_method, str):
            cl_method = cl_method,
        if cl_method is None:
            method_checker = utils.always()
        elif (
            isinstance(cl_method, typing.Collection) and
            all(
                utils.is_identifier(name) and name.startswith(prefix)
                for name in cl_method
            )
        ):
            method_checker = tjekmate.is_in(cl_method)
        else:
            raise TypeError(
                f'cl_method = {cl_method!r}: '
                f'expected identifier(s) starting with {prefix!r}'
            )
        if formatter is None:
            formatter_checker = utils.always()
        elif isinstance(formatter, typing.Collection):
            for fmtr in formatter:
                self._check_formatter(fmtr)
            formatter_checker = tjekmate.is_in(formatter)
        else:
            self._check_formatter(formatter)
            formatter_checker = tjekmate.eq(formatter)
        if tjekmate.is_strict_boolean(max_level):
            max_level = None if max_level else 0
        if not (max_level is None or isinstance(max_level, numbers.Integral)):
            raise TypeError(
                f'max_level = {max_level!r}: expected an integer or boolean'
            )
        # Actual work
        results = {}
        # Base case: only collect local values
        if max_level in (0, False):
            get_key = get_simple_key if concise else get_full_key
            for (cat, mthd, fmtr), values in self._categorized_kwargs.items():
                if not (method_checker(mthd) and formatter_checker(fmtr)):
                    continue
                key = get_key(cat, mthd, fmtr)
                results.setdefault(key, set()).update(values)
            return results
        # max_level: also find child axes and collect their local values
        kwargs.update(cl_method=cl_method, formatter=formatter, concise=concise)
        lookup_locs: typing.Collection[CategorizedLegendAxes]
        if ax is None:
            lookup_locs = self,
        else:
            lookup_locs = tuple(utils.unique(ax))
        iter_strict_descendents = (
            ca for lookup_loc in lookup_locs
            for level in lookup_loc.get_cl_children(max_level=max_level)
            for ca in level
        )
        for key, values in (
            kvp for child_ax in utils.unique(
                itertools.chain(lookup_locs, iter_strict_descendents),
            )
            # Note: descendent `Axes` lookup is done in the above
            # iterator, so we no longer have to descend into child
            # objects
            for kvp in child_ax.get_cl_categories(
                max_level=False, **kwargs,
            ).items()
        ):
            results.setdefault(key, set()).update(values)
        return results

    @deprecate.cla_clf_method_pos_args
    @_utils.copy_method_signature_from(get_cl_categories, drop='concise')
    def _get_cl_handles_labels(
        self, *args, **kwargs
    ) -> typing.OrderedDict[
        _types.CategorizedKeyword,
        typing.List[typing.Tuple[_types.LegendHandle, str]]
    ]:
        cl_categories = self.get_cl_categories(*args, concise=False, **kwargs)
        return mixin.CategorizedLegendMixin._get_cl_handles_labels(
            self, cl_categories, self.figure,
            **_utils.resolve_cl_aliased_args(kwargs),
        )

    @deprecate.cla_clf_method_pos_args
    @_utils.insert_kwargs_from(
        mixin.CategorizedLegendMixin._categorized_legend,
        order=mixin.KEYWORD_ARGUMENT_ORDER,
    )
    @_utils.copy_method_signature_from(_get_cl_handles_labels)
    def categorized_legend(self, *args, **kwargs) -> Legend:
        return self._categorized_legend(
            *args, **_utils.resolve_cl_aliased_args(kwargs)
        )

    categorized_legend.__doc__ = (
        _CATEGORIZED_LEGEND_DOC_TEMPLATE
        .replace('<BASIC_PARAMS>', 'ax, cl_method, formatter(), max_level')
        .replace('<OBJ_CLASS>', 'Axes')
        .replace('<OBJ_NAME>', 'ax')
    )

    # Other overridden methods
    def clear(self) -> None:
        """
        Clear the axes and the internal cache of encountered CKAs.
        """
        mixin.CategorizedLegendMixin.clear(self)
        self._categorized_kwargs.clear()

    def add_child_axes(self, ax: Axes) -> CategorizedLegendAxes:
        """
        Promote the added child `Axes` object to an instance of
        `<MODULE>.Axes`.

        Notes
        -----
        - This method is separately defined because
          `matplotlib.axes.Axes.add_child_axes()` does not add the
          `Axes`
          object to its figure's internal stack, which already manages
          the promotion.

        - Since subclasses of the classes of the promoted (i.e. wrapped)
          objects are created on-the-fly by
          `<MODULE>.Axes.wrap()`
          (see `<MODULE>.wrapper_proxy`),
          the added child axes object is only guaranteed to be an
          instance of `CategorizedLegendAxes` and of `type(ax)`, but not
          necessarily of `type(self)`.
        """
        ax = CategorizedLegendAxes.wrap(
            ax,
            default_cl_formatter=self.default_cl_formatter,
            **self.cl_method_formatters
        )
        return self.super().add_child_axes(ax)

    @functools.wraps(Axes.inset_axes)
    def inset_axes(self, *args, **kwargs):
        # Note: since the method don't directly return the return value
        # of self.add_child_axes() but the Axes object internally
        # created and passed thereto, we have to retrieve the wrapper
        # object manually
        ax_inset = self.super().inset_axes(*args, **kwargs)
        if isinstance(ax_inset, CategorizedLegendAxes):
            return ax_inset
        return next(
            ax for ax in self.child_axes
            if wrapper_proxy.get_wrapped_instance(ax) is ax_inset
        )

    inset_axes.__signature__ = (
        inspect.signature(inset_axes)
        .replace(return_annotation=CategorizedLegendAxes)
    )

    # *************** Descriptors and other attributes *************** #

    @mixin.CategorizedLegendMixin.default_cl_formatter.setter
    @_utils.copy_annotations_from(
        mixin.CategorizedLegendMixin.default_cl_formatter.fset
    )
    def default_cl_formatter(self, formatter):
        """
        Refresh all the cached bound-method instances and let the object
        again figure out what DATA FORMATTER to use with which CL
        METHOD.
        """
        mixin.CategorizedLegendMixin.default_cl_formatter.fset(self, formatter)
        for cl_method in self.cl_method_formatters:
            self._refresh_bound_cl_method(cl_method)

    __slots__ = (
        *mixin.CategorizedLegendMixin.__slots__,
        '_categorized_kwargs', '_cl_bound_methods'
    )
