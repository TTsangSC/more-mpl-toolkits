"""
(Internal) Documentation and implementations for deprecations.

v1.6.1
    `cla_clf_method_pos_args`:
    - Use of positional arguments in calling the
      `~.categorized_legends.{Axes|Figure}` methods
      `.get_cl_categories()`, `.get_cl_handles_labels()`, and
      `.categorized_legend()` deprecated
    - Method signatures to be changed so that all parameters become
      keyword-only by v1.10 or v2.0

v1.6.0
    <NO ID>:
    - Use of the `~._type_checking` subpackage deprecated (since the
      module was spun off into its own repo);
    - Subpackage to be removed by v1.10 or v2.0
"""
from types import SimpleNamespace

from ._utils.decorators import deprecate_positionals

__all__ = (
    'deprecate',
)

deprecate = SimpleNamespace(
    cla_clf_method_pos_args=deprecate_positionals(
        message=(
            'supplying these arguments/this argument as positional(s) is '
            'deprecated and will become invalid by version v1.10 or v2.0, '
            'whichever is sooner; '
            'use the keyword-argument form instead'
        ),
    ),
)
