"""
Core functionalities.
"""
import functools
import importlib.abc
import importlib.machinery
import importlib.util
import numbers
import os
import sys
import types
import typing

from tjekmate import (
    eq, identical_to,
    is_collection, is_instance, is_mapping, is_strict_boolean,
    check,
)
from . import utils
from ._types import (
    Literal, Self, ParamSpec,
    Identifier, IdentifierChain, FilePath, Aliases, AliasesDict,
)

__all__ = (
    'MODULE_ALIASES',
    'AliasedFinder',
    'AliasedSourceFileLoader',
)

MODULE_ALIASES = dict(  # Alias -> real module (or another alias)
    aa='aligned_annotations',
    cl='categorized_legends',
    categorized_legend='categorized_legends',
    lp='legend_placement',
    ra='rays',
    sg='static_grids',
    ex='examples',
)

PS = ParamSpec('PS')
T = typing.TypeVar('T')

_is_none = identical_to(None)
_is_bool_or_none = _is_none | is_strict_boolean


class AliasedFinder(importlib.abc.MetaPathFinder):
    """
    Finder which resolves aliases directly under its scope.
    Also supports being used as a context manager.

    Attributes
    ----------
    .scope (settable)
        Package/Module name directly under which the alias should be
        applied;
        default is `.default_scope`
    .aliases (settable)
        Optional mapping of aliases to other aliases or canonical
        subpackage/-module names;
        default is `.default_aliases`
    .resolved_aliases
        Mapping of aliases to the canonical subpackage/-module names
    .found_specs
        Cache of fully-qualified import paths containing aliases to the
        module specs
    .default_scope, .default_aliases (class attributes)
        Defaults for `.scope` and `.aliases`
    .enabled
        Whether the finder is active
    """
    scope: IdentifierChain
    aliases: Aliases
    resolved_aliases: Aliases  # Note: ordered in descending key lengths
    found_specs: typing.Mapping[IdentifierChain, importlib.machinery.ModuleSpec]
    enabled: bool
    _finder_caches: typing.Tuple[
        # Scope which the importer handles
        IdentifierChain,
        typing.Tuple[
            # Original aliases (alias -> real_name | alias)
            Aliases,
            # Resolved aliases (alias -> real_name)
            Aliases,
        ],
        # Name -> spec
        typing.Dict[IdentifierChain, importlib.machinery.ModuleSpec],
    ]
    _enabled: bool

    def __init__(
        self,
        *args,
        scope: typing.Optional[IdentifierChain] = None,
        aliases: typing.Optional[typing.Mapping[Identifier, Identifier]] = None,
        **kwargs,
    ) -> None:
        super().__init__(*args, **kwargs)
        self._finder_caches = ('', ''), ({}, {}), {}
        if scope is None:
            scope = type(self).default_scope
        if aliases is None:
            aliases = type(self).default_aliases
        self.scope = scope
        self.aliases = aliases
        self.enabled = True

    def __repr__(self) -> str:
        alias_targets: typing.Dict[Identifier, typing.List[Identifier]] = {}
        for alias, unaliased in self.resolved_aliases.items():
            alias_targets.setdefault(unaliased, []).append(alias)
        if self.scope:
            scope, prefix = self.scope, '.'
        else:
            scope, prefix = '<global>', ''
        return '<{} {} at {:#0x}: {}>'.format(
            type(self).__name__,
            ('({})' if self.enabled else '[{}; (disabled)]').format(scope),
            id(self),
            ', '.join(
                '='.join(
                    prefix + attr for attr in (*sorted(aliases), unaliased)
                )
                for unaliased, aliases in alias_targets.items()
            ),
        )

    def __enter__(self) -> Self:
        self.install(enable=True)
        return self

    def __exit__(self, *args, **kwargs) -> None:
        self.uninstall(hard=True)

    # Finder methods
    # (See https://docs.python.org/3/reference/import.html)
    def find_spec(
        self,
        full_name: IdentifierChain,
        path: typing.Union[typing.Sequence[FilePath], None],
        target: typing.Optional[types.ModuleType] = None,
    ) -> typing.Union[importlib.machinery.ModuleSpec, None]:
        """
        Resolve aliases of the form
        `f'[{self.scope}.]{alias}[.{submodules}[...]]'`.

        Parameters
        ----------
        full_name
            Fully-qualified name for the module of the form

            >>> '.'.join([  # noqa: F821 # doctest: +SKIP
            ...     self.scope, recognized_alias, *subpackage_levels,
            ... ])  # If `self.scope`, or
            >>> '.'.join([  # noqa: F821 # doctest: +SKIP
            ...     recognized_alias, *subpackage_levels,
            ... ])  # If `self.scope == ''`
            >>> assert (  # noqa: F821 # doctest: +SKIP
            ...     recognized_alias in self.aliases
            ... )

        path
            `.__path__` attribute of the existing module under which
            the submodule pointed at by `full_name` should be found;
            alternatively, `None` if it's a top-level import
        target
            (Ignored)

        Return
        ------
        `full_name` is a valid alias to a subpackage/-module and the
        finder is `.enabled` (see also Notes)
            Module spec for said subpackage/-module with an appropriate
            `.loader`
        Else
            None

        Side effects
        ------------
        `.found_specs` remembers `ModuleSpec` objects pertaining to
        `full_name` and all the parent modules;
        e.g. for a file structure

        <some directory in sys.path>
        +-- foo/
            +-- __init__.py
            +-- bar.py
            +-- baz/
                +-- __init__.py
                +-- spam.py

        doing `.find_spec('foo.baz.spam')` will cause the finder to
        generate and remember specs for the modules/packages
        `foo.baz.spam`, `foo.baz`, and `foo`.

        Notes
        -----
        - `path` should always be available if we're working with scoped
          aliases, hence `None` is always returned when
          `path is None and self.scope`.
        - The `.loader` of the returned module spec loads the located
          subpackage/-module under both the canonical and aliased names
          in `sys.modules`.

        Examples
        --------
        >>> import importlib
        >>> top_module, top_alias = 'numpy', 'np'
        >>> submodule, subalias = 'linalg', 'la'

        Top-level aliases:

        >>> top_finder = AliasedFinder(
        ...     scope='', aliases={top_alias: top_module},
        ... )
        >>> sub_canon_name = f'{top_module}.{submodule}'
        >>> sub_aliased = f'{top_alias}.{submodule}'
        >>> print(  # If the finder doesn't know, return None
        ...     top_finder.find_spec('foobar', None)
        ... )
        None
        >>> spec = top_finder.find_spec(top_alias, None)
        >>> assert spec.name == top_module
        >>> module_path = spec.submodule_search_locations
        >>> sub_spec = top_finder.find_spec(sub_aliased, module_path)
        >>> assert sub_spec.name == sub_canon_name
        >>> submodule_obj = importlib.import_module(sub_canon_name)
        >>> assert sub_spec.origin == submodule_obj.__file__

        Scoped aliases:

        >>> scoped_finder = AliasedFinder(
        ...     scope=top_module, aliases={subalias: submodule},
        ... )
        >>> sub_aliased = f'{top_module}.{subalias}'
        >>> sub_spec = scoped_finder.find_spec(sub_aliased, module_path)
        >>> assert sub_spec.name == sub_canon_name
        >>> assert sub_spec.origin == submodule_obj.__file__
        """
        def full_name_to_filename(
            resolved_name: str,
            path: typing.Union[typing.Sequence[FilePath], None],
        ) -> typing.Union[
            typing.Tuple[str, FilePath, bool], typing.Tuple[str, None, None],
        ]:
            """
            Get the path to the file defining the module, if any.

            Return
            ------
            File found

                >>> (  # noqa # doctest: +SKIP
                ...     further_resolved_qualified_name,
                ...     path_to_file,
                ...     module_looks_like_a_package
                ... )

            Else
                Tuple `None, None`
            """
            for finder in sys.meta_path:
                if finder is self:
                    continue
                try:
                    spec = finder.find_spec(resolved_name, path)
                except Exception:
                    continue
                if not spec:
                    continue
                origin = spec.origin
                is_package = spec.submodule_search_locations is not None
                if os.path.isfile(origin):
                    return spec.name, origin, is_package
            return resolved_name, None, None

        remainder: IdentifierChain
        if not utils.is_identifier_chain(full_name):
            # Note: raising an error on malformed (i.e. note PEP8
            # compliant) module names inside find_spec() may be
            # problematic;
            # So just say IDK about this module and move on
            return None
            # raise TypeError(
            #     f'full_name = {full_name!r}: '
            #     'expected a chain of string identifiers '
            #     'joined by periods (\'.\')'
            # )
        if not self.enabled:  # Disabled finder, no-op
            return None
        if full_name in self.found_specs:  # Check cache
            return self.found_specs[full_name]
        if self.scope:
            prefix = self.scope + '.'
        else:
            prefix = ''
        matched_prefix, remainder = utils.strip_prefix(full_name, prefix)
        if prefix and not matched_prefix:
            return None  # None of this loader's business
        if path is None and self.scope:
            # Non-top-level aliases are scoped, hence they should be
            # called with path
            return None
        for alias, unaliased in self.resolved_aliases.items():
            if remainder == alias:
                resolved_name = prefix + unaliased
                backpropagate_nlevels = 0
            else:
                more_prefix, more_remainder = utils.strip_prefix(
                    remainder, alias + '.',
                )
                if not more_prefix:
                    continue
                resolved_name = '.'.join((prefix + unaliased, more_remainder))
                backpropagate_nlevels = more_remainder.count('.')
            # Construct module spec pointing to python file holding the
            # (sub-)module/-package, and do the same for its parents
            # which are below .scope (where applicable)
            resolved_name, origin, is_package = (
                full_name_to_filename(resolved_name, path)
            )
            if origin is None:
                continue
            nnop: typing.List[
                typing.Tuple[
                    IdentifierChain,  # Aliased name
                    IdentifierChain,  # Unaliased name
                    FilePath,  # Path to defining file
                    bool,  # is_package
                ]
            ] = [(full_name, resolved_name, origin, is_package)]
            if is_package:
                origin_path = os.path.dirname(origin)
            else:
                origin_path = origin
            for _ in range(backpropagate_nlevels):
                # foo.(b := bar).baz.spam -> foo.b.baz -> foo.b
                full_name, *_ = full_name.rpartition('.')
                resolved_name, *_ = resolved_name.rpartition('.')
                origin_path = os.path.dirname(origin_path)
                origin = os.path.join(origin_path, '__init__.py')
                assert utils.is_identifier_chain(full_name)
                assert utils.is_identifier_chain(resolved_name)
                assert os.path.isfile(origin)
                nnop.append((full_name, resolved_name, origin, True))
            for full_name, resolved_name, origin, is_package in nnop[::-1]:
                # Add module specs for parent modules first
                loader = AliasedSourceFileLoader(
                    resolved_name, origin, aliases=[full_name],
                )
                spec = self._found_specs[full_name] = (
                    # Note: .found_specs is protected,
                    # need to use ._found_specs for setting items
                    importlib.machinery.ModuleSpec(
                        resolved_name, loader,
                        origin=origin, is_package=is_package,
                    )
                )
                spec.submodule_search_locations = (
                    [os.path.dirname(origin)] if is_package else None
                )
                spec.has_location = True
            return spec  # Last spec = requested spec
        return None

    def invalidate_caches(self) -> None:
        self.scope = self.scope
        self.aliases = self.aliases

    @staticmethod
    def _resolve_aliases(**aliases: Identifier) -> AliasesDict:
        bad_aliases = {
            alias: unaliased for alias, unaliased in aliases.items()
            if not utils.is_identifier(unaliased)
        }
        if bad_aliases:
            bad_aliases_formatted = ', '.join(
                '{} = {!r}'.format(*kvp) for kvp in bad_aliases.items()
            )
            raise TypeError(
                f'{bad_aliases_formatted}: expected string identifiers'
            )
        resolved_aliases: AliasesDict = {}
        for alias, unaliased in aliases.items():
            seen_aliases: typing.Set[Identifier] = set()
            while True:
                if unaliased in seen_aliases:
                    bad_aliases_formatted = ', '.join(
                        '{} = {!r}'.format(key, aliases[key])
                        for key in seen_aliases
                    )
                    raise RuntimeError(
                        f'{bad_aliases_formatted}: '
                        'loop detected in resolving aliases'
                    )
                if unaliased in resolved_aliases:
                    # Resolved to a fully-resolved alias -> stop
                    unaliased = resolved_aliases[unaliased]
                    break
                elif unaliased in aliases:
                    # Resolved to another alias
                    # -> continue resolving
                    seen_aliases.add(unaliased)
                    unaliased = aliases[unaliased]
                    continue
                else:  # Can't be further unaliased -> stop
                    break
            resolved_aliases[alias] = unaliased
        return resolved_aliases

    @check(
        more_aliases=is_mapping(value_checker=(_is_none | utils.is_identifier)),
    )
    def set_aliases(
        self, **more_aliases: typing.Union[Identifier, None],
    ) -> None:
        """
        Change the registered aliases;
        existing aliases can be disabled by setting the value to `None`.

        Notes
        -----
        As opposed to `self.aliases = ...`, this does additional work to
        make sure that the changes are reflected in the import system by
        actively pruning stale aliases from parent module objects and
        from `sys.modules`.
        """
        aliases = {
            alias: unaliased
            for alias, unaliased in dict(self.aliases, **more_aliases).items()
            if unaliased
        }
        self._set_aliases(
            aliases,
            prune_parents=True, prune_sys_modules=True, force_parent=True,
        )

    def _set_aliases(
        self,
        aliases: Aliases,
        *,
        prune_parents: bool = False,
        prune_sys_modules: bool = False,
        force_parent: bool = False,
    ) -> None:
        """
        Parameters
        ----------
        aliases
            New table of aliases
        prune_parents
            Whether to prune stale aliases from the parent
        prune_sys_modules
            Whether to prune stale aliases from `sys.modules`
        force_parent
            If true and `prune_parents` and if the supposed parent is
            not in `sys.modules`, use `importlib.import_module()` to
            force it into existence
        """
        old_aliases = self.resolved_aliases
        resolved_aliases = self._resolve_aliases(**aliases)
        stale_aliases = {
            alias: unaliased
            for alias, unaliased in old_aliases.items()
            if unaliased != resolved_aliases.get(alias)
        }
        # Remove stale aliases
        if prune_parents:
            parent_obj = sys.modules.get(self.scope)
            if parent_obj is None and force_parent:
                parent_obj = importlib.import_module(self.scope)
            for alias in stale_aliases:
                self._remove_alias_from_parent(
                    alias, parent=parent_obj, check_identity=False,
                )
        if prune_sys_modules:
            for alias in stale_aliases:
                self._remove_alias_from_sys_modules(alias, check_spec=False)
        # Update cache
        self._finder_caches = (
            self.scope,
            (  # Recalculate the resolved aliases
                types.MappingProxyType(dict(aliases)),
                types.MappingProxyType(resolved_aliases),
            ),
            {},  # Invalidate .found_specs
        )

    @check(index=is_instance(numbers.Integral), enable=_is_bool_or_none)
    def install(
        self,
        index: numbers.Integral = 0,
        enable: typing.Optional[bool] = None,
    ) -> None:
        """
        Install the finder at `sys.meta_path` at the provided index.

        Parameters
        ----------
        index
            Integer index to call `sys.meta_path.insert()` with;
            the only indices which makes sense are often 0 and -1 though
        enable
            Whether to enable or disable the finder on installation;
            default is to not change the `.enabled` attribute

        Notes
        -----
        We first check `sys.meta_path` for `self`, removing it if
        present and reinserting it at the new index.
        As such, any `index not in (0, -1)` may result in insertion at
        an unexpected position.
        """
        self._remove_self_from_finders()
        sys.meta_path.insert(index, self)
        if enable is not None:
            self.enabled = enable

    @check(
        prune_meta_path=_is_bool_or_none,
        prune_sys_modules=_is_bool_or_none,
        hard=is_strict_boolean,
    )
    def uninstall(
        self,
        *,
        prune_meta_path: typing.Optional[bool] = None,
        prune_sys_modules: typing.Optional[bool] = None,
        force_parent: typing.Optional[bool] = None,
        hard: bool = False,
    ) -> None:
        """
        Disable the finder.

        Parameters
        ----------
        prune_meta_path
            If true, also remove this finder from `sys.meta_path` if
            present;
            default is `hard`
        prune_sys_modules
            If true, also remove all the aliases in `sys.modules`
            supplied by the finder, and remove the aliased module
            objects from their parents;
            default is `hard`
        force_parent
            If true and `prune_sys_modules` and if the supposed parent
            is not in `sys.modules`, use `importlib.import_module()` to
            force it into existence;
            default is `hard`
        hard
            Shorthand for setting both `prune_meta_path` and
            `prune_sys_modules`;
            if the two are explicitly passed, those values takes
            priority
        """
        if prune_meta_path is None:
            prune_meta_path = hard
        if prune_sys_modules is None:
            prune_sys_modules = hard
        if force_parent is None:
            force_parent = hard
        if prune_meta_path:
            self._remove_self_from_finders()
        if prune_sys_modules:
            # Remove from parent packages and from sys.modules
            if self.scope:
                parent_obj = sys.modules.get(self.scope)
                if parent_obj is None and force_parent:
                    parent_obj = importlib.import_module(self.scope)
            else:
                parent_obj = None
            for alias in self.resolved_aliases:
                self._remove_alias_from_parent(
                    alias, parent=parent_obj, check_identity=False,
                )
                self._remove_alias_from_sys_modules(alias, check_spec=False)
        self.invalidate_caches()
        self.enabled = False

    def reset(self, hard: bool = False) -> None:
        """
        Reset the scope and aliases to the defaults.
        If `hard`, remove all the stale aliases in `sys.modules`
        supplied by the finder, and remove the aliased module objects
        from their parents.
        """
        self.scope = self.default_scope
        self._set_aliases(
            self.default_aliases,
            prune_parents=hard,
            prune_sys_modules=hard,
        )

    def _remove_self_from_finders(
        self,
        finders: typing.Optional[
            typing.List[importlib.abc.MetaPathFinder]
        ] = None,
        check_identity: bool = True,
    ) -> None:
        """
        Remove occurrences of the finder from a list of finders.

        Parameters
        ----------
        finders
            Optional list;
            default is `sys.meta_path`
        check_identity
            Whether to check for object identity (true) or equality
            (false) when pruning `finders`
        """
        if finders is None:
            finders = sys.meta_path
        checker: typing.Callable[[typing.Any], bool] = {
            True: identical_to(self),
            False: eq(self),
        }[check_identity]
        self_occurrences = [i for i, obj in enumerate(finders) if checker(obj)]
        for i in self_occurrences[::-1]:  # Start trimming from the end
            removed = sys.meta_path.pop(i)
            assert checker(removed)

    def _remove_alias_from_parent(
        self,
        alias: Identifier,
        parent: typing.Optional[types.ModuleType] = None,
        check_identity: bool = True,
    ) -> None:
        r"""
        Remove reference to the alias in the parent module.

        Parameters
        ----------
        alias
            Identifier alias
        parent
            Optional parent object;
            if `self.scope != ''` and if not provided, defaults to
            either `sys.modules[self.scope]`;
            if that is not found, it is a no-op
        check_identity
            True
                Only remove aliases if their unaliased targets are
                IDENTICAL with the aliased objects
            False
                Only remove aliases if their unaliased targets have
                EQUAL specs with the aliased objects

        Notes
        -----
        This is needed for the aliases to be forgotten for the import-
        from statements after having been created, because such
        statements look at the .__dict__ of the parent module object.
        (See https://docs.python.org/3/reference/simple_stmts.html\
        #import)
        """
        def modules_identical(
            mod1: types.ModuleType, mod2: types.ModuleType,
        ) -> bool:
            return mod1 is mod2

        def modules_have_same_specs(
            mod1: types.ModuleType, mod2: types.ModuleType,
        ) -> bool:
            if not (hasattr(mod1, '__spec__') and hasattr(mod2, '__spec__')):
                return False
            return mod1.__spec__ == mod2.__spec__

        if not self.scope:  # No parent
            return
        if alias not in self.resolved_aliases:
            return
        if parent is None:
            parent = sys.modules.get(self.scope)
        if parent is None:
            return
        unaliased = self.resolved_aliases[alias]
        should_delete_alias = (
            modules_identical if check_identity else modules_have_same_specs
        )
        if (
            hasattr(parent, alias) and
            hasattr(parent, unaliased) and
            should_delete_alias(
                getattr(parent, alias), getattr(parent, unaliased),
            )
        ):
            delattr(parent, alias)

    def _remove_alias_from_sys_modules(
        self,
        alias: Identifier,
        check_spec: bool = True,
    ) -> None:
        """
        Remove reference to the alias in `sys.modules`.

        Parameters
        ----------
        alias
            Identifier alias
        check_spec
            Strategy for determining what items to remove:
            True
                Check if `sys.modules[aliased_name].__spec__` matches
                the entry at `.found_specs[aliased_name]`
            False
                Check if `sys.modules[aliased_name].__spec__.name`
                matches the resolved name
        """
        def check_names(
            sys_mod_name: IdentifierChain, module: types.ModuleType,
        ) -> bool:
            name_matchers: typing.List[
                typing.Callable[[IdentifierChain, IdentifierChain], bool]
            ] = [
                eq,
                lambda module_name, namespace: (
                    module_name.startswith(namespace + '.')
                ),
            ]
            return any(
                (
                    match_name(sys_mod_name, aliased_name) and
                    match_name(module.__name__, resolved_name)
                )
                for match_name in name_matchers
            )

        def check_specs(
            sys_mod_name: IdentifierChain, module: types.ModuleType,
        ) -> bool:
            spec = getattr(module, '__spec__', None)
            if not spec:
                return False
            return self.found_specs.get(sys_mod_name) == spec

        if alias not in self.resolved_aliases:
            return
        unaliased = self.resolved_aliases[alias]
        prefix = (self.scope + '.') if self.scope else ''
        aliased_name, resolved_name = (
            prefix + name for name in (alias, unaliased)
        )
        should_delete_alias = check_specs if check_spec else check_names
        modules_to_delete: typing.List[IdentifierChain] = [
            module_name for module_name, module in sys.modules.items()
            if should_delete_alias(module_name, module)
        ]
        for module_name in modules_to_delete:
            del sys.modules[module_name]

    # Descriptors
    @property
    def scope(self) -> typing.Union[IdentifierChain, Literal['']]:
        return self._finder_caches[0]

    @scope.setter
    def scope(self, scope: typing.Union[IdentifierChain, Literal['']]) -> None:
        if not (
            isinstance(scope, str) and
            (utils.is_identifier_chain(scope) or not scope)
        ):
            raise TypeError(
                (
                    '.scope = {!r}: '
                    'expected a chain of string identifiers joined by periods '
                    '({!r}), or the empty string {!r}'
                ).format(scope, '.', '')
            )
        _, aliases_maps, _ = self._finder_caches
        self._finder_caches = (  # Invalidate .found_specs
            scope, aliases_maps, {},
        )

    @property
    def aliases(self) -> Aliases:
        return self._finder_caches[1][0]

    @aliases.setter
    def aliases(self, aliases: Aliases) -> None:
        self._set_aliases(aliases, prune_parents=False, prune_sys_modules=False)

    @property
    def resolved_aliases(self) -> Aliases:
        return self._finder_caches[1][1]

    @property
    def _found_specs(
        self,
    ) -> typing.Dict[IdentifierChain, importlib.machinery.ModuleSpec]:
        return self._finder_caches[2]

    @property
    def found_specs(
        self,
    ) -> typing.Mapping[IdentifierChain, importlib.machinery.ModuleSpec]:
        return types.MappingProxyType(self._found_specs)

    @property
    def enabled(self) -> bool:
        return self._enabled

    @enabled.setter
    def enabled(self, enabled: bool) -> None:
        if not is_strict_boolean(enabled):
            raise TypeError('.enabled = {enabled!r}: expected a boolean')
        self._enabled = bool(enabled)

    __slots__ = '_finder_caches', '_enabled'
    default_scope = utils.get_package_name()
    default_aliases = MODULE_ALIASES


class AliasedSourceFileLoader(importlib.machinery.SourceFileLoader):
    """
    Loader which ensures that a resolved alias is visible to the other
    parts of the importing machineries.

    Motivation
    ----------
    Consider the setup

    <some directory in sys.path>
    +-- foo/
        +-- __init__.py
        +-- bar.py
        +-- baz/
            +-- __init__.py
            +-- spam.py

    Where `foo.b` is aliased to `foo.baz` via the use of
    `AliasedFinder`.

    If one were to do either

    >>> from foo import b  # noqa: F401 # doctest: +SKIP

    or

    >>> from foo.b import spam  # noqa: F401 # doctest: +SKIP

    the code still runs fine, but if one does

    >>> import foo.b.spam  # noqa: F401 # doctest: +SKIP

    then the import fails due to how the inner workings of `importlib`
    presupposes the parent module (i.e. `foo.b` -> `foo.bar`) to exist
    in `sys.modules` under the exact name.

    Thus, it is necessary for the loader to register the initialized
    module under both the canonical name and the aliases.

    Example
    -------
    >>> import importlib.util
    >>> import sys
    >>> module, submodule = 'numpy', 'random'
    >>> aliases = 'rng1', 'rng2'
    >>> for alias in aliases:
    ...     try:
    ...         _ = importlib.import_module(f'{module}.{alias}')
    ...     except ImportError:
    ...         pass
    ...     else:
    ...         assert False, f'`{module}.{alias!r}` already exists'
    >>> modules = [importlib.import_module(f'{module}.{submodule}')]

    Loaders are created in `AliasedFinder.find_spec()`:

    >>> with AliasedFinder(
    ...     aliases=dict.fromkeys(aliases, submodule), scope=module,
    ... ):  # Temporarily register the aliases
    ...     for alias in aliases:
    ...         module_name = f'{module}.{alias}'
    ...         # Imported module object
    ...         modules.append(importlib.import_module(module_name))
    ...         # Module object registered at sys.modules
    ...         modules.append(sys.modules[module_name])
    ...         # Module object created as an attribute of the parent
    ...         modules.append(getattr(sys.modules[module], alias))
    ...     # All the modules should refer to the same object
    ...     assert len(
    ...         set(id(module_obj) for module_obj in modules)
    ...     ) == 1
    ...     module_obj, *_ = modules
    ...     assert isinstance(
    ...         module_obj.__spec__.loader, AliasedSourceFileLoader,
    ...     )
    """
    _alias: typing.Union[IdentifierChain, None]

    @check(
        aliases=(
            _is_none | is_collection(item_checker=utils.is_identifier_chain)
        ),
    )
    def __init__(
        self,
        *args,
        aliases: typing.Optional[typing.Collection[IdentifierChain]] = None,
        **kwargs
    ) -> None:
        super().__init__(*args, **kwargs)
        self._aliases = set(aliases)

    def __eq__(
        self, other: typing.Any,
    ) -> typing.Union[bool, Literal[NotImplemented]]:
        if isinstance(other, importlib.machinery.SourceFileLoader):
            self_attrs = dict(vars(self))
            other_attrs = dict(vars(other))
            # FIXME: we can't yet compare the slots now, until we can
            # figure out a way to robustly manage all the ModuleSpec
            # and Loader instances we spawn
            # Otherwise it interferes with cleanup
            if False:
                for obj, attrs in (self, self_attrs), (other, other_attrs):
                    for attr_name in getattr(type(obj), '__slots__', ()):
                        try:
                            attr_value = getattr(obj, attr_name)
                        except AttributeError:
                            continue
                        attrs.setdefault(attr_name, attr_value)
            return self_attrs == other_attrs
        return NotImplemented

    def create_module(
        self, spec: importlib.machinery.ModuleSpec,
    ) -> typing.Union[types.ModuleType, None]:
        """
        Check `sys.modules` to prevent double inialization.
        """
        module = sys.modules.get(spec.name)
        if not module:
            return None
        # FIXME: we need some way to signal to exec_module() not to
        # re-exec an existing module and just go straight to the
        # bookkeeping
        # This is kinda ugly though...
        module.__spec__ = spec
        setattr(module.__spec__, self._exec_module_flag, False)
        return module

    def exec_module(self, module: types.ModuleType) -> None:
        """
        Initialize the module and make it globally-available via the
        aliases (if any)
        """
        if getattr(module.__spec__, self._exec_module_flag, True):
            # FIXME: do we have a better way of detecting fresh modules?
            super().exec_module(module)
        assert sys.modules[self.name] is module
        if self.aliases:
            for alias in self.aliases:
                sys.modules[alias] = module
        for name in (self.name, *self.aliases):
            # Make sure that the loaded module is available at the
            # parent
            parent, sep, attr = name.rpartition('.')
            if not sep:
                continue
            setattr(sys.modules[parent], attr, module)

    def _call_with_resolved_name(
        self, method: Identifier, full_name: IdentifierChain, *args, **kwargs,
    ) -> typing.Any:
        """
        Call the base-class implementation of the `method` but with the
        `full_name` replaced with `self.name` if it's in `self.aliases`,
        so that we don't trip the
        `@importlib._bootstrap_external._check_name` check.
        """
        if full_name in self.aliases:
            full_name = self.name
        super_method = getattr(super(), method)
        return super_method(full_name, *args, **kwargs)

    @staticmethod
    def _make_name_resolved_method(
        method: typing.Callable[PS, T],
        name: typing.Optional[Identifier] = None,
    ) -> typing.Callable[PS, T]:
        @functools.wraps(method)
        def method_hook(self, *args, **kwargs):
            return self._call_with_resolved_name(name, *args, **kwargs)

        if not name:
            name = method.__name__
        return method_hook

    # Note: these methods uses the
    # `@importlib._bootstrap_external._check_name` decorator, which
    # means that the argument they're passed (the aliases in cases
    # relevant to us) are checked against `self.name` (the unaliased,
    # 'real' module path).
    for _name in 'load_module', 'get_filename', 'get_resource_reader':
        locals()[_name] = _make_name_resolved_method.__func__(
            getattr(importlib.machinery.SourceFileLoader, _name), name=_name,
        )
        del _name

    @property
    def aliases(self) -> typing.Union[IdentifierChain, None]:
        return self._aliases

    __slots__ = '_aliases',
    _exec_module_flag = '_asfl_should_exec_module'
