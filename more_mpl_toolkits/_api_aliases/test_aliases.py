"""
Test for aliased imports.

Requirements
------------
pytest
    Should be installed if installed with the <MODULE>[dev] dependencies
"""
import contextlib
import functools
import importlib.abc
import logging
import operator
import os
import pprint
import subprocess
import sys
import types
import typing

import pytest
try:
    from pytest import MonkeyPatch  # Need pytest v6.2+
except ImportError:
    from _pytest.monkeypatch import MonkeyPatch

from .core import AliasedFinder
from . import ENV_SWITCHES
from ..categorized_legends import __all__ as CL_NAMES
from ..categorized_legends.core import __all__ as CL_CORE_NAMES

LOGGER = logging.getLogger((lambda: None).__module__)
TOP_LEVEL = AliasedFinder.default_scope
THIS_SUBPACKAGE = '.'.join(AliasedFinder.__module__.split('.')[:-1])
IMPORT_TARGET_ALIASED_NAME = f'{TOP_LEVEL}.cl'
IMPORT_TARGET_CANONICAL_NAME = f'{TOP_LEVEL}.categorized_legends'
IMPORT_SUBTARGET_CANONICAL_NAME = f'{TOP_LEVEL}.categorized_legends.core'

DUMMY_EXECUTION_SCRIPT = os.path.join(
    os.path.split(
        importlib.import_module(IMPORT_TARGET_CANONICAL_NAME).__file__
    )[0],
    '__main__.py',
)
CL_HAS_MAIN = os.path.isfile(DUMMY_EXECUTION_SCRIPT)

# ********************* Fixtures and utiilities ********************** #

Fixture = typing.TypeVar('Fixture')


def import_fixture(
    name: str,
    hook: typing.Optional[typing.Callable[[typing.Any], Fixture]] = None,
    fixturize: bool = True,
) -> typing.Union[Fixture, typing.Callable[[], Fixture]]:
    """
    Create a fixture importing from the top-level package.

    Parameters
    ----------
    name
        path to import below the top level
    hook()
        Optional hook to call on the imported object, whose return value
        is returned
    fixturize
        Whether to return a fixture (if true) or the result of calling
        such a fixture (if false)
    """
    def fixture_func() -> Fixture:
        leading, _, last = name.rpartition('.')
        module_to_import = TOP_LEVEL
        if leading:
            module_to_import += '.' + leading
        module_obj = importlib.import_module(module_to_import)
        import_target = getattr(module_obj, last)
        if hook:
            import_target = hook(import_target)
        return import_target

    if fixturize:
        return pytest.fixture(fixture_func)
    return fixture_func()


def get_finder_from_aliases_method(method: typing.Callable) -> AliasedFinder:
    return getattr(method, 'func', method).__self__


FINDER_OBJECT = import_fixture(
    'set_aliases', hook=get_finder_from_aliases_method,
)
FINDER_CLASS = import_fixture('_api_aliases.core.AliasedFinder')
set_aliases, reset_aliases, enable_aliases, disable_aliases = (
    import_fixture(name) for name in [
        'set_aliases', 'reset_aliases', 'enable_aliases', 'disable_aliases',
    ]
)


def clean_modules(
    reset_aliases: typing.Callable,
    destructive: bool = False,
    delete: bool = True,
) -> typing.List[str]:
    """
    Remove `more_mpl_toolkits` submodules in `sys.modules` and clear the
    the import caches.

    Parameters
    ----------
    destructive
        Whether to remove the top-level package and this subpackage
    delete
        Whether to actually do the deletion from `sys.modules`

    Return
    ------
    List of names deleted (if `delete = True`) or to delete (otherwise)
    from `sys.modules`
    """
    reset_aliases()
    submodules = [
        module_name for module_name in sys.modules
        if (
            module_name.startswith(TOP_LEVEL + '.') or
            (destructive and module_name == TOP_LEVEL)
        )
        if destructive or not (
            module_name == THIS_SUBPACKAGE or
            module_name.startswith(THIS_SUBPACKAGE + '.')
        )
    ]
    finder = get_finder_from_aliases_method(
        sys.modules[TOP_LEVEL].set_aliases,
    )
    if delete:
        for module_name in submodules:
            del sys.modules[module_name]
        finder._remove_self_from_finders()
    importlib.invalidate_caches()
    return submodules


@pytest.fixture(scope='module', autouse=True)
def sys_setup_and_teardown() -> None:
    """
    Restore the state of `sys.modules` and `sys.meta_path` after running
    the tests.
    """
    def setattrs(obj: typing.Any, **state) -> None:
        for attr_name, attr_value in state.items():
            setattr(obj, attr_name, attr_value)

    def no_op(*args, **kwargs): return None

    def is_module_to_be_restored(module: str) -> bool:
        return module == TOP_LEVEL or module.startswith(TOP_LEVEL + '.')

    def restore_loaded_class(cls: type) -> type:
        return getattr(sys.modules[cls.__module__], cls.__name__)

    def restore_imported_module(other: types.ModuleType) -> types.ModuleType:
        return sys.modules[other.__spec__.name]

    def safe_repr_with_id(obj: typing.Any) -> str:
        try:
            rep = repr(obj)
        except Exception:
            root_cls = type if isinstance(obj, type) else object
            rep = root_cls.__repr__(obj)
        return '{} (@ {})'.format(rep, id(obj))

    # **************************** Setup ***************************** #

    # Cache sys.meta_path
    finders_and_hooks: typing.List[
        typing.Tuple[
            importlib.abc.MetaPathFinder,
            typing.Callable[[importlib.abc.MetaPathFinder], None]
        ]
    ] = []
    for finder in sys.meta_path:
        if isinstance(finder, AliasedFinder):
            try:
                hook = functools.partial(
                    setattrs,
                    scope=finder.scope,
                    aliases=types.MappingProxyType(finder.aliases),
                )
            except Exception:
                hook = no_op
        else:
            hook = no_op
        finders_and_hooks.append((finder, hook))
    # Cache sys.modules
    orig_modules: typing.Dict[str, types.ModuleType] = {
        name: module for name, module in sys.modules.items()
        if is_module_to_be_restored(name)
    }

    yield

    # *************************** Teardown *************************** #

    # Restore sys.meta_path
    for finder, hook in finders_and_hooks:
        hook(finder)
    sys.meta_path.clear()
    sys.meta_path.extend(finder for finder, _ in finders_and_hooks)
    # Restore sys.modules
    dropped_names = [
        name for name in sys.modules if is_module_to_be_restored(name)
    ]
    for name in dropped_names:
        del sys.modules[name]
    sys.modules.update(orig_modules)
    # Repair internal references to other submodules, and to classes
    # defined therein
    for module_name, module_obj, attr_name, attr_value in (
        (mname, mod, vname, value)
        for mname, mod in orig_modules.items()
        for vname, value in vars(mod).items()
        if isinstance(value, (types.ModuleType, type))
    ):
        if isinstance(attr_value, types.ModuleType):
            restore = restore_imported_module
            submod_name = attr_value.__spec__.name
        else:
            restore = restore_loaded_class
            submod_name = attr_value.__module__
        if not is_module_to_be_restored(submod_name):
            continue
        try:
            restored_value = restore(attr_value)
        except Exception as e:
            msg = '{}.{} = {}: failed to get up-to-date value'.format(
                module_name, attr_name, safe_repr_with_id(attr_value),
            )
            LOGGER.error(msg, exc_info=e)
            continue
        if attr_value is restored_value:
            continue
        try:
            setattr(module_obj, attr_name, restored_value)
        except Exception as e:
            msg = '{}.{} = {} -> {}: failed to update value'.format(
                module_name,
                attr_name,
                safe_repr_with_id(attr_value),
                safe_repr_with_id(restored_value),
            )
            LOGGER.error(msg, exc_info=e)
        else:
            msg = '{}.{} = {} -> {}'.format(
                module_name,
                attr_name,
                safe_repr_with_id(attr_value),
                safe_repr_with_id(restored_value),
            )
            LOGGER.info(msg)
    importlib.invalidate_caches()


# ****************************** Tests ******************************* #


def test_abs_import_alias(
    enable_aliases: typing.Callable,
    reset_aliases: typing.Callable,
):
    """
    Absolute import of an alias
    """
    clean_modules(reset_aliases)
    enable_aliases()
    import more_mpl_toolkits.cl
    assert more_mpl_toolkits.cl.__all__ == CL_NAMES
    assert more_mpl_toolkits.cl.__name__ == IMPORT_TARGET_CANONICAL_NAME


def test_rel_import_alias(
    enable_aliases: typing.Callable,
    reset_aliases: typing.Callable,
):
    """
    Relative import of an alias
    """
    clean_modules(reset_aliases)
    enable_aliases()
    from .. import cl
    assert cl.__all__ == CL_NAMES
    assert cl.__name__ == IMPORT_TARGET_CANONICAL_NAME


def test_from_import_alias(
    enable_aliases: typing.Callable,
    reset_aliases: typing.Callable,
):
    """
    Import-from with an alias
    """
    clean_modules(reset_aliases)
    enable_aliases()
    from more_mpl_toolkits import cl
    assert cl.__all__ == CL_NAMES
    assert cl.__name__ == IMPORT_TARGET_CANONICAL_NAME


def test_abs_import_alias_sub(
    enable_aliases: typing.Callable,
    reset_aliases: typing.Callable,
):
    """
    Absolute import of a submodule of an alias
    """
    clean_modules(reset_aliases)
    enable_aliases()
    import more_mpl_toolkits.cl.core
    assert more_mpl_toolkits.cl.core.__all__ == CL_CORE_NAMES
    assert more_mpl_toolkits.cl.__name__ == IMPORT_TARGET_CANONICAL_NAME
    assert more_mpl_toolkits.cl.core.__name__ == IMPORT_SUBTARGET_CANONICAL_NAME


def test_rel_import_alias_sub(
    enable_aliases: typing.Callable,
    reset_aliases: typing.Callable,
):
    """
    Relative import of a submodule of an alias
    """
    clean_modules(reset_aliases)
    enable_aliases()
    from ..cl import core
    assert core.__all__ == CL_CORE_NAMES
    assert core.__name__ == IMPORT_SUBTARGET_CANONICAL_NAME


def test_from_import_alias_sub(
    enable_aliases: typing.Callable,
    reset_aliases: typing.Callable,
):
    """
    Import-from of a submodule of an alias
    """
    clean_modules(reset_aliases)
    enable_aliases()
    from more_mpl_toolkits.cl import core
    assert core.__all__ == CL_CORE_NAMES
    assert core.__name__ == IMPORT_SUBTARGET_CANONICAL_NAME


def test_canonical_name_alias_identity(
    enable_aliases: typing.Callable,
    reset_aliases: typing.Callable,
):
    """
    Test if importing via the canonical name and the alias of the
    module results in the same object.
    """
    clean_modules(reset_aliases)
    enable_aliases()
    import more_mpl_toolkits.categorized_legends as cl
    import more_mpl_toolkits.categorized_legends
    import more_mpl_toolkits.cl
    assert cl is more_mpl_toolkits.categorized_legends is more_mpl_toolkits.cl


def test_finder_resolves_indirect_alias(
    enable_aliases: typing.Callable,
    set_aliases: typing.Callable,
    reset_aliases: typing.Callable,
):
    """
    Test if importing works for an alias which maps to another alias.
    Also tests if resetting aliases (or doing this implitcitly with
    context managers) works as expected.
    """
    def pre() -> None:
        module = importlib.import_module(f'{TOP_LEVEL}.{new_alias}')
        assert module.__all__ == CL_NAMES

    def post() -> None:
        assert f'{TOP_LEVEL}.{new_alias}' not in sys.modules
        assert not hasattr(sys.modules[TOP_LEVEL], new_alias)

    clean_modules(reset_aliases)
    enable_aliases()
    new_alias, target = 'cl1', 'cl'
    # Explicit reset
    try:
        set_aliases(**{new_alias: target})
        pre()
    finally:
        reset_aliases()
        post()
    # Implicit (context-managed) reset
    try:
        new_finder = AliasedFinder()
        new_finder.set_aliases(**{new_alias: target})
        with new_finder:
            pre()
        assert new_finder not in sys.meta_path
        assert not new_finder.enabled
    finally:
        post()


def test_toogling_alias(
    enable_aliases: typing.Callable,
    disable_aliases: typing.Callable,
    set_aliases: typing.Callable,
    reset_aliases: typing.Callable,
):
    """
    Test if the toggling of aliasing works correctly.
    """
    try:
        clean_modules(reset_aliases)
        enable_aliases()
        from more_mpl_toolkits import cl
        assert cl.__all__ == CL_NAMES
        del cl
        disable_aliases()
        with pytest.raises(ImportError):
            from more_mpl_toolkits import cl
            del cl
        enable_aliases()
        from more_mpl_toolkits import cl
        assert cl.__all__ == CL_NAMES
        del cl
    finally:
        enable_aliases()


def test_soft_disabling_alias(
    enable_aliases: typing.Callable,
    disable_aliases: typing.Callable,
):
    """
    Test for soft disabling of aliases.
    """
    # Hard-disable and enable to forget about any stored references
    disable_aliases(hard=True)
    enable_aliases()
    # Sanity check: does soft disabling work?
    disable_aliases(hard=False)
    with pytest.raises(ImportError):
        from more_mpl_toolkits import cl
        del cl
    # Check that soft disabling doesn't temper with sys.modules
    for hard in False, True:
        enable_aliases()
        import more_mpl_toolkits.cl
        disable_aliases(hard=hard)
        assert hasattr(more_mpl_toolkits, 'cl') == (not hard)


def test_cache_invalidation(
    enable_aliases: typing.Callable,
    disable_aliases: typing.Callable,
    reset_aliases: typing.Callable,
    FINDER_OBJECT: AliasedFinder,
):
    """
    Test for cache invalidation.
    """
    disable_aliases()
    assert FINDER_OBJECT not in sys.meta_path
    clean_modules(reset_aliases)
    enable_aliases()
    assert FINDER_OBJECT in sys.meta_path
    from more_mpl_toolkits import cl
    assert cl.__all__ == CL_NAMES
    assert IMPORT_TARGET_ALIASED_NAME in sys.modules
    cache = set(FINDER_OBJECT.found_specs)
    assert IMPORT_TARGET_ALIASED_NAME in cache
    clean_modules(reset_aliases)
    new_cache = set(FINDER_OBJECT.found_specs)
    assert new_cache < cache


def test_finder_complains_about_alias_loop(
    reset_aliases: typing.Callable,
    FINDER_CLASS: typing.Type[AliasedFinder],
):
    """
    Test if the finder complains about alias-resolution loops.
    """
    clean_modules(reset_aliases)
    buggy_aliases = dict(A='B', B='C', C='D', D='B')
    new_alias, *_ = buggy_aliases
    with pytest.raises(RuntimeError, match='loop detected'):
        with FINDER_CLASS(aliases=buggy_aliases):
            importlib.import_module(f'{TOP_LEVEL}.{new_alias}')


def test_top_level_aliases(
    FINDER_CLASS: typing.Type[AliasedFinder],
):
    """
    Test if the aliasing works with top-level modules.
    """
    alias, unaliased = 'np', 'numpy'
    submodule = 'random'
    finder = FINDER_CLASS(aliases={alias: unaliased}, scope='')
    submod_alias, submod_unaliased = (
        f'{name}.{submodule}' for name in (alias, unaliased)
    )
    assert finder.find_spec(alias, None)
    # Set up
    with finder:
        import np.random as npr1
        from np import random as npr2
        import numpy.random as npr3
        npr4 = importlib.import_module(submod_alias)
        npr5 = sys.modules[submod_unaliased]
        assert npr1.__name__ == submod_unaliased
        assert npr1 is npr2 is npr3 is npr4 is npr5
    # Tear down
    assert alias not in sys.modules
    assert submod_alias not in sys.modules
    assert finder not in sys.meta_path
    with pytest.raises(ImportError):
        importlib.import_module(alias)
    with pytest.raises(ImportError):
        importlib.import_module(submod_alias)


def test_alias_stacking(
    FINDER_CLASS: typing.Type[AliasedFinder],
):
    """
    Test that different finders interacts correctly.
    """
    top_alias, top_unaliased = 'np', 'numpy'
    scoped_aliases = dict(rng='random', la='linalg')
    top_finder = FINDER_CLASS(aliases={top_alias: top_unaliased}, scope='')
    sub_finder = FINDER_CLASS(aliases=scoped_aliases, scope=top_alias)
    # Set up
    with contextlib.ExitStack() as stack:
        stack.enter_context(top_finder)
        stack.enter_context(sub_finder)
        top_module = importlib.import_module(top_alias)
        assert top_module is sys.modules[top_unaliased]
        for alias, unaliased in scoped_aliases.items():
            mod_alias = f'{top_alias}.{alias}'
            mod_unaliased = f'{top_unaliased}.{unaliased}'
            assert sub_finder.find_spec(mod_alias, top_module.__path__)
            module = importlib.import_module(mod_alias)
            assert module is sys.modules[mod_unaliased]
    # Tear down
    assert not set(sys.modules) & {
        top_alias, *(f'{top_alias}.{alias}' for alias in scoped_aliases),
    }
    assert top_finder not in sys.meta_path
    assert sub_finder not in sys.meta_path
    for alias, unaliased in scoped_aliases.items():
        with pytest.raises(ImportError):
            importlib.import_module(f'{top_alias}.{alias}')


@pytest.mark.parametrize(
    'enable,disable,expected_active_aliases',
    [
        ('True', None, True),
        ('False', None, False),
        (None, 'True', False),
        (None, 'False', True),
    ],
)
def test_env_switching(
    enable: typing.Union[str, None],
    disable: typing.Union[str, None],
    expected_active_aliases: bool,
    # Fixtures
    monkeypatch: MonkeyPatch,
    enable_aliases: typing.Callable,
    disable_aliases: typing.Callable,
    reset_aliases: typing.Callable,
    FINDER_OBJECT: AliasedFinder,
):
    """
    Test that the environment variables are correctly read.
    """
    def is_enabled(finder: AliasedFinder) -> bool:
        return finder.enabled and finder in sys.meta_path

    MONKEY_PATCH = True
    aliases_originally_active = is_enabled(FINDER_OBJECT)
    _disable_aliases = disable_aliases
    _disable_aliases()
    assert not is_enabled(FINDER_OBJECT)
    try:
        with monkeypatch.context() as mpatch:
            # Start on a clean slate
            modules_to_remove = clean_modules(
                reset_aliases, destructive=True, delete=False,
            )
            for module in modules_to_remove:
                if MONKEY_PATCH:
                    mpatch.delitem(sys.modules, module)
                else:
                    del sys.modules[module]
            for env_switch in ENV_SWITCHES.values():
                if MONKEY_PATCH:
                    mpatch.delenv(env_switch, raising=False)
                else:
                    os.environ.pop(env_switch, None)
            for enable_if_true, value in (True, enable), (False, disable):
                if value is None:
                    continue
                if MONKEY_PATCH:
                    mpatch.setenv(ENV_SWITCHES[enable_if_true], value)
                else:
                    os.environ[ENV_SWITCHES[enable_if_true]] = value
            assert any(
                os.environ.get(ENV_SWITCHES[boolean]) is not None
                for boolean in (True, False)
            )
            _disable_aliases()
            importlib.invalidate_caches()
            # Actual testing
            assert TOP_LEVEL not in sys.modules
            from more_mpl_toolkits import disable_aliases as _disable_aliases
            assert TOP_LEVEL in sys.modules
            finder = get_finder_from_aliases_method(_disable_aliases)
            assert finder is not FINDER_OBJECT
            from more_mpl_toolkits._api_aliases import determine_use_of_aliases
            try:
                # Make sure that the finder is pruned from sys.meta_path
                # after running this function
                # Note: we can't use `with finder: ...` here because
                # doing that automatically `install()`s the finder
                aliases_should_be_used = determine_use_of_aliases(*(
                    os.environ.get(ENV_SWITCHES[enable_if_true])
                    for enable_if_true in (True, False)
                ))
                assert expected_active_aliases == aliases_should_be_used
                assert (
                    expected_active_aliases == is_enabled(finder)
                ), pprint.pformat(dict(finder=finder, meta_path=sys.meta_path))
                try:
                    import more_mpl_toolkits.cl  # noqa: F401
                except ImportError:
                    imported = False
                else:
                    imported = True
                assert (
                    expected_active_aliases == imported
                ), pprint.pformat(dict(finder=finder, meta_path=sys.meta_path))
            except ():  # Debug
                pytest.set_trace()
            finally:
                _disable_aliases()
    finally:
        importlib.invalidate_caches()
        if aliases_originally_active:
            enable_aliases()
        importlib.invalidate_caches()


def test_check_consistency():
    """
    Sanity check for the consistency of modules at the end.
    If a module loads another module under a different identity from the
    same-named module in `sys.modules`, an error is raised;
    if the module doesn't show in `sys.modules`, a warning is issued.
    """
    def is_submodule(module: str) -> bool:
        return module == TOP_LEVEL or module.startswith(TOP_LEVEL + '.')

    def get_submodules() -> typing.List[str]:
        return sorted(
            (module for module in sys.modules if is_submodule(module)),
            key=operator.methodcaller('count', '.'),
            reverse=True,
        )

    # Hard reset: clean modules, clean meta_path, clean caches
    for module in get_submodules():
        del sys.modules[module]
    for i, finder in list(enumerate(sys.meta_path))[::-1]:
        if type(finder).__name__ == 'AliasedFinder':
            sys.meta_path.pop(i)
    importlib.invalidate_caches()

    # Start over
    try:
        # Note: this can fail if aliasing is disabled by the environment
        import more_mpl_toolkits.cl  # noqa: F401
    except ModuleNotFoundError:
        import more_mpl_toolkits.categorized_legends  # noqa: F401
    modules = get_submodules()

    sys_module_identities = {
        module: id(sys.modules[module]) for module in modules
    }
    missing_in_modules: typing.Dict[str, typing.Dict[str, int]] = {}
    conflicting_with_modules: typing.Dict[str, typing.Dict[str, int]] = {}
    for module in modules:
        module_obj = sys.modules[module]
        for name, other_module_obj in vars(module_obj).items():
            if not isinstance(other_module_obj, types.ModuleType):
                continue
            other_module_qualname = other_module_obj.__spec__.name
            other_module_relname = f'{module}.{name}'
            if not is_submodule(other_module_qualname):
                continue
            if other_module_qualname in sys_module_identities:
                if (
                    sys_module_identities[other_module_qualname]
                    != id(other_module_obj)
                ):
                    conflicting_with_modules.setdefault(
                        other_module_qualname, {},
                    )[other_module_relname] = id(other_module_obj)
            else:
                missing_in_modules.setdefault(
                    other_module_qualname, {},
                )[other_module_relname] = id(other_module_obj)
    if missing_in_modules or conflicting_with_modules:
        all_errors = {
            name: (f'{sys_module_identities[name]} (CONFLICT)', conflicts)
            for name, conflicts in conflicting_with_modules.items()
        }
        all_errors.update(
            (name, ('(MISSING)', references))
            for name, references in missing_in_modules.items()
        )
        all_errors_formatted = pprint.pformat(all_errors)
        if conflicting_with_modules:
            assert False, all_errors_formatted
        else:
            LOGGER.warning(all_errors_formatted)


@pytest.mark.xfail(
    CL_HAS_MAIN,
    reason=(
        f'{DUMMY_EXECUTION_SCRIPT!r} actually exists, '
        'executing it may have side effects; '
        'rewrite test ASAP to use another subpackage without a __main__, '
        'with one that is safe to call'
    ),
    raises=FileExistsError,
    strict=True,
)
def test_execute_alias_as_script():
    """
    Test if Issue #1 has been addressed and if aliased paths can be used
    in `python -m`.
    """
    if CL_HAS_MAIN:
        raise FileExistsError(
            f'{DUMMY_EXECUTION_SCRIPT!r} exists, '
            'refrain from actually executing the module'
        )
    msg_end = '{!r} is a package and cannot be directly executed'.format(
        IMPORT_TARGET_ALIASED_NAME,
    )
    proc = subprocess.run(
        (sys.executable, '-m', IMPORT_TARGET_ALIASED_NAME),
        capture_output=True,
        text=True,
        env={**os.environ, ENV_SWITCHES[True]: 'True'},
    )
    assert (proc.returncode != 0), proc.returncode
    assert (proc.stderr.strip().endswith(msg_end)), proc.stderr.strip()
