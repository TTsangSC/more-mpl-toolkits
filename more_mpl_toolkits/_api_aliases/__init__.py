"""
Implement un-aliasing for API names.

Example
-------
>>> import pytest
>>> from more_mpl_toolkits import categorized_legends
>>> if {_ENV_SHOULD_RUN_DOCTEST_}:  # Aliases {_ENABLED_} via env. vars.
...     from more_mpl_toolkits import cl
...     assert cl is categorized_legends
... else:
...     pytest.skip('Aliases disabled via environment')

Notes
-----
No, we can't use a package-level `__getattr__()` for this;
Attribute lookup only works when accessing the subpackages from the
imported top-level package.

Side effects
------------
Custom importer for this package added
"""
import ast
import functools
import os
import types
import typing
import warnings

from .core import MODULE_ALIASES as DEFAULT_PACKAGE_ALIASES, AliasedFinder

__all__ = (
    'DEFAULT_PACKAGE_ALIASES',
    'enable_aliases', 'disable_aliases', 'set_aliases', 'reset_aliases',
)

# Handle aliases


def determine_use_of_aliases(
    enable_aliases: typing.Union[str, None],
    disable_aliases: typing.Union[str, None],
) -> bool:
    r"""
    Compute whether aliases are to be enabled at launch.

    Examples
    --------
    >>> determine_use_of_aliases('1', None)
    True
    >>> determine_use_of_aliases('0', None)
    False
    >>> determine_use_of_aliases(None, '1')
    False
    >>> determine_use_of_aliases(None, '0')
    True
    >>> determine_use_of_aliases('False', 'True')
    False
    >>> determine_use_of_aliases(None, None)  # Fallback to default
    True

    Handling bad values:

    >>> from warnings import catch_warnings
    >>> with catch_warnings(record=True) as caught_warnings:
    ...     value = determine_use_of_aliases(
    ...         '[',  # Malformed positive value -> ignored with warning
    ...         'False',  # Negative value is falsy -> enable
    ...     )
    ...     print(value, caught_warnings[-1].message, sep='\n')
    True
    ${MPL_ENABLE_ALIASES} = '[': failed ..., ignoring

    Handling conflicts:

    >>> with catch_warnings(record=True) as caught_warnings:
    ...     value = determine_use_of_aliases(
    ...         # Both falsy -> defer to the positive value
    ...         'False', 'None',
    ...     )
    ...     print(  # doctest: +NORMALIZE_WHITESPACE
    ...         value, caught_warnings[-1].message, sep='\n'
    ...     )
    False
    ${MPL_ENABLE_ALIASES} = 'False' -> DISABLE,
      ${MPL_DISABLE_ALIASES} = 'None' -> ENABLE:
      conflicting ..., deferring to MPL_ENABLE_ALIASES
    """
    supplied_values = {True: enable_aliases, False: disable_aliases}
    resolved_values: typing.Dict[bool, bool] = dict()
    enable_by_default = True
    choice_upon_conflict = True
    for enable_if_true, env_value in supplied_values.items():
        if not env_value:
            continue
        try:
            value = bool(ast.literal_eval(env_value))
        except Exception as e:
            msg = (
                f'${{{ENV_SWITCHES[enable_if_true]}}} = {env_value!r}: '
                f'failed to resolve truth value thereof ({e!r}), ignoring'
            )
            warnings.warn(msg)
            continue
        resolved_values[enable_if_true] = value ^ (not enable_if_true)
    if not resolved_values:
        return enable_by_default
    try:
        enable, = set(resolved_values.values())
        return enable
    except ValueError:  # Conflicting values
        msg = '{}: conflicting instructions, deferring to {}'.format(
            ', '.join(
                '${{{}}} = {!r} -> {}'.format(
                    ENV_SWITCHES[enable_if_true],
                    supplied_values[enable_if_true],
                    'ENABLE' if resolved_values[enable_if_true] else 'DISABLE',
                )
                for enable_if_true, resolved in resolved_values.items()
            ),
            ENV_SWITCHES[choice_upon_conflict],
        )
        warnings.warn(msg)
        return resolved_values[choice_upon_conflict]


FINDER = AliasedFinder()
ENV_SWITCHES = types.MappingProxyType({
    True: 'MPL_ENABLE_ALIASES',
    False: 'MPL_DISABLE_ALIASES',
})

enable_aliases = functools.partial(FINDER.install, enable=True)
disable_aliases = functools.partial(
    FINDER.uninstall, hard=True, force_parent=False,
)
set_aliases = FINDER.set_aliases
reset_aliases = functools.partial(FINDER.reset, hard=True)

aliases_enabled_via_env = bool(determine_use_of_aliases(*(
    os.environ.get(ENV_SWITCHES[enable_if_true])
    for enable_if_true in (True, False)
)))
if aliases_enabled_via_env:
    enable_aliases()
__doc__ = __doc__.format(
    _ENV_SHOULD_RUN_DOCTEST_=aliases_enabled_via_env,
    _ENABLED_=('en' if aliases_enabled_via_env else 'dis') + 'abled',
)
