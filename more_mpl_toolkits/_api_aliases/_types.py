"""
Typing stuff.
"""
import importlib
import typing
import types

__all__ = (
    'Literal', 'Self', 'ParamSpec',
    'Identifier', 'IdentifierChain', 'FilePath', 'Aliases', 'AliasesDict',
)


def _import(
    attr: str,
    source: str = 'typing',
    secondary_source: str = 'typing_extensions',
) -> typing.Any:
    """
    Example
    -------
    >>> import typing
    >>> Literal = _import('Literal')
    >>> assert typing.Literal is Literal
    >>> if not hasattr(typing, 'Self'):  # Pre-3.11
    ...     import typing_extensions
    ...     Self = _import('Self')
    ...     assert typing.Self is typing_extensions.Self is Self
    >>> try:
    ...     _import('Litteral')
    ... except ImportError:
    ...     pass
    ... else:
    ...     assert False
    """
    def get_import_error(module: types.ModuleType, attr: str) -> ImportError:
        path = module.__file__
        name = module.__name__
        msg = f'cannot import name {attr!r} from {name!r} ({path})'
        return ImportError(msg, name=name, path=path)

    primary_module = importlib.import_module(source)
    if not hasattr(primary_module, attr):
        secondary_module = importlib.import_module(secondary_source)
        try:
            value = getattr(secondary_module, attr)
        except AttributeError:
            raise get_import_error(secondary_module, attr) from None
        setattr(primary_module, attr, value)
    return getattr(primary_module, attr)


Literal, Self, ParamSpec = (
    _import(name) for name in ['Literal', 'Self', 'ParamSpec']
)
Identifier = typing.TypeVar('Identifier', bound=str)
IdentifierChain = typing.TypeVar('IdentifierChain', bound=str)
FilePath = typing.TypeVar('FilePath', bound=str)
Aliases = typing.Mapping[Identifier, Identifier]
AliasesDict = typing.Dict[Identifier, Identifier]
