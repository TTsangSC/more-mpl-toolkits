"""
Utilities.
"""
import typing

import tjekmate

from .._utils import string_operations
from ._types import Identifier

__all__ = (
    'get_package_name', 'is_identifier', 'is_identifier_chain',
    'strip_prefix', 'strip_suffix',
)

is_identifier = tjekmate.is_keyword
strip_prefix = string_operations.strip_prefix
strip_suffix = string_operations.strip_suffix


def get_package_name() -> Identifier:
    """
    Get the name of the package.
    """
    package_name = '.'.join(get_package_name.__module__.split('.')[:-2])
    assert is_identifier_chain(package_name)
    return package_name


def is_identifier_chain(obj: typing.Any) -> bool:
    """
    Example
    -------
    >>> [
    ...     is_identifier_chain(string)
    ...     for string in [
    ...         '', '1', '_', 'foo', 'foo.bar',
    ...         'foo.bar._.baz', '(foo)', 'lorem ipsum',
    ...     ]
    ... ]
    [False, False, True, True, True, True, False, False]
    """
    if not isinstance(obj, str):
        return False
    if not obj:
        return False
    return tjekmate.is_sequence(
        obj.split('.'), item_checker=is_identifier,
    )
