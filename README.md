more-mpl-toolkits
=================

A collection of tools I've used and developed in recent years for
working with [`matplotlib`][mpl].

<!---
Note: column width modifiers doesn't seem to do anything in GLFM
--->

<table>
 <tr>
  <td>

   [[_TOC_]]

  </td>
  <td>

   ![Repo logo reading "More" "mpl" "toolkits"](icon.png "Repo logo")

  </td>
 </tr>
</table>

Installation
------------

```console
$ {   # Optional but recommended
>     python3 -m venv "new_virtual_environment" &&
>     source new_virtual_environment/bin/activate &&
>     # This may help with the UNKNOWN bug (see Troubleshooting)
>     pip3 install --upgrade pip
> }
$ pip3 install git+https://gitlab.com/TTsangSC/more-mpl-toolkits.git
```

Currently available [installation options][python-package-extras]:

- `dev`: tests, linting, etc.

See also the official documentations on [installing `python` packages
from a `git` repo][python-package-git].

Compatibility
-------------

_Should_ work with anything between: 

- `matplotlib` v3.1 to v3.8
- `python` v3.7 to v3.12

No guarantees though, and not an exhaustive list either.

The toolbox
-----------

### [`.aligned_annotations` (alias(es): `.aa`)][aa]

Alignment of annotations to other plot elements

### [`.categorized_legends` (alias(es): `.cl`, `.categorized_legend`)][cl]

Visualization of categorized data

### [`.legend_placement` (alias(es): `.lp`)][lp]

Optimal placement of figure legends

### [`.rays` (alias(es): `.ra`)][ra]

Drawing of rays (lines extending indefinitely),
as a replacement/backport/augmentation of `Axes.axline()`

### [`.static_grids` (alias(es): `.sg`)][sg]

Grid layouts with static margins

[Code examples][ex]
-------------------

(Alias(es): `.ex`)

[Subpackage aliasing][top-init]
-------------------------------

As hinted at above, via the use of a
[special import hook][meta-path-finder],
the top-level modules can be referred to by their aliases:

```python
from more_mpl_toolkits import aa
# = from ... import aligned_annotations as aa
```

The behavior of alias resolution are controlled and documented by the
following:

### `.DEFAULT_PACKAGE_ALIASES`

The default table of aliases and the targets they resolve to;
the targets can themselves be aliases as long as a reference loop is not
created.

### `.enable_aliases()`, `.disable_aliases()`

En-/Disable the use of aliases

### `.set_aliases()`

Set and/or unset individual aliases

### `.reset_aliases()`

Restore the default table of aliases in the hook

Environment
-----------

The following environment variables are used by `more-mpl-toolkits`:

### [`MPL_ENABLE_ALIASES` (resp. `MPL_DISABLE_ALIASES`)][top-init]

Parsed into a literal object, the truth value of which is used to
determine whether `.enable_aliases()` is called as the package is loaded

Troubleshooting
---------------

### The package is installed as `UNKNOWN` and doesn't work.

Try upgrading your version of `pip` (see [Installation](#installation)).
As far as I can tell version 20 or below struggles with reading the
[`pyproject.toml`][pyproject],
while version 23 or above seems to have no issues.

Disclaimers
-----------

See the [licence](LICENCE.txt). 
Neither this package nor its author is affiliated with or endorsed by 
`matplotlib`, [`numpy`][np], [`pytest`][pytest],
or any other such direct or indirect dependencies of the package.

[python-package-extras]: https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/#install-extras
[python-package-git]: https://pip.pypa.io/en/latest/topics/vcs-support/#git
[meta-path-finder]: https://docs.python.org/3/glossary.html#term-meta-path-finder
[mpl]: https://matplotlib.org/stable/
[np]: https://numpy.org
[pytest]: https://pytest.org

[pyproject]: more_mpl_toolkits/pyproject.toml
[top-init]: more_mpl_toolkits/__init__.py
[aa]: more_mpl_toolkits/aligned_annotations
[cl]: more_mpl_toolkits/categorized_legends
[lp]: more_mpl_toolkits/legend_placement
[ra]: more_mpl_toolkits/rays
[sg]: more_mpl_toolkits/static_grids
[ex]: more_mpl_toolkits/examples
